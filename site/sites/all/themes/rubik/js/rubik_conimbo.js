
/**
 * Submit form on content management.
 */
Drupal.behaviors.rubik_conimbo = function(context) {
  $('.view-id-conimbo_content .views-exposed-widgets select.form-select').change(function() {
    $('#views-exposed-form-conimbo-content-page-1').submit();
  });
}