<?php
/**
 * @file
 * Theme overrides.
 */

/**
 * Remove system-menus.css and adds body classes.
 */
function conimbo_base_preprocess_page($vars) {

  // remove tabs, we use on_actions instead
  $vars['tabs'] = '';

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)

  $body_classes = drupal_map_assoc(explode(" ", $vars['body_classes']));

  if ($vars['language']->language){
    $body_classes[] = 'language-'.$vars['language']->language;
  }

  if (!$vars['is_front']) {
    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $body_classes[] = conimbo_base_id_safe('page-'. $path);
    $body_classes[] = conimbo_base_id_safe('section-'. $section);
  }

  $vars['body_classes'] = implode(' ', $body_classes); // Concatenate with spaces

  // remove system-menus.css
  $css = $vars['css'];
  unset($css['all']['module']['modules/system/system-menus.css']);
  $vars['css'] = $css;
  $vars['styles'] = drupal_get_css($css);

  return $vars;
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * - Preceeds initial numeric with 'n' character.
 * - Replaces space and underscore with dash.
 * - Converts entire string to lowercase.
 * - Works for classes too!
 *
 * @param string $string
 *   The string
 * @return
 *   The converted string
 */
function conimbo_base_id_safe($string) {
  if (is_numeric($string{0})) {
    // If the first character is numeric, add 'n' in front
    $string = 'n'. $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

/**
 * Implementation of hook_form_alter().
 * Adds the 'forgot password' link to the login form
 */
function conimbo_base_form_alter(&$form, $form_state, $form_id) {
  // Node form.
  if ($form_id == 'user_login') {
    $form['pass']['#description'] .= '<br />'. l(t('Forgotten password?'), '/user/password');
  }
  if ($form_id == 'user_pass') {
    $form['name']['#description'] .= t('Enter your username or e-mailaddress and we will send you a new password.');
  }
}

/**
 * Implementation of hook_theme().
 */
function conimbo_base_theme() {
  $items = array();

  $items['pager_list'] = array(
    'arguments' => array('tags' => array(), 'limit' => 10, 'element' => 0, 'parameters' => array(), 'quantity' => 9)
  );

  return $items;
}

/**
 * Override of theme_pager().
 * ensure the markup will not conflict with major other styles
 * (theme_item_list() in particular).
 */
function conimbo_base_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 6) {
  $pager_list = theme('pager_list', $tags, $limit, $element, $parameters, $quantity);

  if ($pager_list) {
    return '<div class="pager clear-block">' . $pager_list . '</div>';
  }
}

/**
 * Force the mini-pager to look like the full pager to avoid theming issues
 */
function conimbo_base_views_mini_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
  $pager_list = theme('pager_list', $tags, $limit, $element, $parameters, $quantity);

  if ($pager_list) {
    return '<div class="pager clear-block">' . $pager_list . '</div>';
  }
}

/**
 * Split out page list generation into its own function.
 */
function conimbo_base_pager_list($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 6) {
  global $pager_page_array, $pager_total, $theme_key;

  if ($pager_total[$element] > 1) {
    // Calculate various markers within this pager piece:
    // Middle is used to "center" pages around the current page.
    $pager_middle = ceil($quantity / 2);
    // current is the page we are currently paged to
    $pager_current = $pager_page_array[$element] + 1;
    // first is the first page listed by this pager piece (re quantity)
    $pager_first = $pager_current - $pager_middle + 1;
    // last is the last page listed by this pager piece (re quantity)
    $pager_last = $pager_current + $quantity - $pager_middle;
    // max is the maximum page number
    $pager_max = $pager_total[$element];
    // End of marker calculations.

    // Prepare for generation loop.
    $i = $pager_first;
    if ($pager_last > $pager_max) {
      // Adjust "center" if at end of query.
      $i = $i + ($pager_max - $pager_last);
      $pager_last = $pager_max;
    }
    if ($i <= 0) {
      // Adjust "center" if at start of query.
      $pager_last = $pager_last + (1 - $i);
      $i = 1;
    }
    // End of generation loop preparation.

    $links = array();

    // $links['pager-first'] = theme('pager_first', ($tags[0] ? $tags[0] : t('First')), $limit, $element, $parameters);
    if ($pager_current != 1) {
      $links['pager-previous'] = theme('pager_previous', ($tags[1] ? $tags[1] : t('Previous')), $limit, $element, 1, $parameters);
    }

    // add first two items if necessary
    if ($i > 1) {
      $links["1 pager-item"] = theme('pager_previous', 1, $limit, $element, ($pager_current - 1), $parameters);
      if ($i > 2) {
        $links["2 pager-item"] = theme('pager_previous', 2, $limit, $element, ($pager_current - 2), $parameters);
        if ($i > 3) {
         $links["pager-ellipsis"] = array('title' => '...');
        }
      }
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      // Now generate the actual pager piece.
      for ($i; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $links["$i pager-item"] = theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters);
        }
        if ($i == $pager_current) {
          $links["$i pager-current"] = array('title' => $i);
        }
        if ($i > $pager_current) {
          $links["$i pager-item"] = theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters);
        }
      }

      // add last two items if necessary
      if ($pager_last <= ($pager_max - 3)) {
        $links["pager-ellipsis"] = array('title' => '...');
      }
      if ($pager_last <= ($pager_max - 2)) {
        $links[$pager_max - 1 . " pager-item"] = theme('pager_next', ($pager_max - 1), $limit, $element, (($pager_max - 1) - $pager_current), $parameters);
      }
      if ($pager_last <= ($pager_max - 1)) {
        $links[$pager_max  . " pager-item"] = theme('pager_next', ($pager_max), $limit, $element, (($pager_max) - $pager_current), $parameters);
      }

      if ($pager_current != $pager_max) {
        $links['pager-next'] = theme('pager_next', ($tags[3] ? $tags[3] : t('Next')), $limit, $element, 1, $parameters);
      }
      //$links['pager-last'] = theme('pager_last', ($tags[4] ? $tags[4] : t('Last')), $limit, $element, $parameters);

      return theme('links', $links, array('class' => 'links pager pager-list'));
    }
  }
  return '';
}

/**
 * Return an array suitable for theme_links() rather than marked up HTML link.
 */
function conimbo_base_pager_link($text, $page_new, $element, $parameters = array(), $attributes = array()) {
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query[] = drupal_query_string_encode($parameters, array());
  }
  $querystring = pager_get_querystring();
  if ($querystring != '') {
    $query[] = $querystring;
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    else if (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

  return array(
    'title' => $text,
    'href' => $_GET['q'],
    'attributes' => $attributes,
    'query' => count($query) ? implode('&', $query) : NULL,
  );
}

/**
 * Add a class 'button' to every button. This is linked to the button style in DS.
 */
function conimbo_base_button($element) {
  // Make sure not to overwrite classes.
  if (isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] = 'form-'. $element['#button_type'] .' '. $element['#attributes']['class'];
  }
  else {
    $element['#attributes']['class'] = 'form-'. $element['#button_type'];
  }
  $element['#attributes']['class'] .= ' button';

  return '<input type="submit" '. (empty($element['#name']) ? '' : 'name="'. $element['#name'] .'" ') .'id="'. $element['#id'] .'" value="'. check_plain($element['#value']) .'" '. drupal_attributes($element['#attributes']) ." />\n";
}

/**
 * add extra parameters
 */
function conimbo_base_item_list($items = array(), $title = NULL, $type = 'ul', $attributes = NULL) {
  $output = '<div class="item-list clear-block">';
  if (isset($title)) {
    $output .= '<h3>'. $title .'</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type". drupal_attributes($attributes) .'>';
    $num_items = count($items);
    foreach ($items as $i => $item) {
      $attributes = array();
      $children = array();
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        $data .= theme_item_list($children, NULL, $type, $attributes); // Render nested list
      }
      $list_id = $i + 1;
      $attributes['class'] = empty($attributes['class']) ? 'item-'. $list_id : ($attributes['class'] .' item-'. $list_id);

      if ($i == 0) {
        $attributes['class'] = empty($attributes['class']) ? 'first' : ($attributes['class'] .' first');
      }
      if ($i == $num_items - 1) {
        $attributes['class'] = empty($attributes['class']) ? 'last' : ($attributes['class'] .' last');
      }
      if ($i%2) {
        $attributes['class'] = empty($attributes['class']) ? 'even' : ($attributes['class'] .' even');
      } else {
        $attributes['class'] = empty($attributes['class']) ? 'odd' : ($attributes['class'] .' odd');
      }
      $output .= '<li'. drupal_attributes($attributes) .'>'. $data ."</li>\n";
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}

/**
 * Form overwrite to add clear-block to the div surrounding the form
 */
function conimbo_base_form($element) {
  $action = $element['#action'] ? 'action="'. check_url($element['#action']) .'" ' : '';
  if (isset($element['#attributes']['class'])){
    $element['#attributes']['class'] .= ' clear-block';
  }
  else {
    $element['#attributes']['class'] = 'clear-block';
  }
  return '<form '. $action .' accept-charset="UTF-8" method="'. $element['#method'] .'" id="'. $element['#id'] .'"'. drupal_attributes($element['#attributes']) .">\n<div class=\"form-inner\">". $element['#children'] ."</div>\n</form>\n";
}

/**
 * Form overwrite to add clear-block to the element
 */
function conimbo_base_form_element($element, $value) {
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  $output = '<div class="form-item clear-block"';
  if (!empty($element['#id'])) {
    $output .= ' id="'. $element['#id'] .'-wrapper"';
  }
  $output .= ">\n";
  $required = !empty($element['#required']) ? '<span class="form-required" title="'. $t('This field is required.') .'">*</span>' : '';

  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="'. $element['#id'] .'">'. $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
    else {
      $output .= ' <label>'. $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
  }

  $output .= " $value\n";

  if (!empty($element['#description'])) {
    $output .= ' <div class="description">'. $element['#description'] ."</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}

/**
 * Return a themed sitemap box.
 *
 * @param $title
 *   The subject of the box.
 * @param $content
 *   The content of the box.
 * @param $class
 *   Optional extra class for the box.
 * @return
 *   A string containing the box output.
 */
function conimbo_base_site_map_box($title, $content, $class = '') {
  return  '<div class="sitemap-box '. $class .'"><h2 class="category-title">'. $title .'</h2><div class="content">'. $content .'</div></div>';
}

/**
 * Process variables for user-picture.tpl.php.
 */
function conimbo_base_preprocess_user_picture(&$variables) {
  $variables['picture'] = '';

  // use the teaser preset
  $imagecache = 'teaser';

  if (variable_get('user_pictures', 0)) {
    $account = $variables['account'];
    if (!empty($account->picture) && file_exists($account->picture)) {
      $picture = $account->picture;
    }
    elseif (variable_get('user_picture_default', '')) {
      $picture = variable_get('user_picture_default', '');
    }

    if (isset($picture)) {
      $alt = t("@user's picture", array('@user' => $account->name ? $account->name : variable_get('anonymous', t('Anonymous'))));
      $variables['picture'] = theme('imagecache', $imagecache, $picture, $alt, $alt, '', FALSE);
    }
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function conimbo_base_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {

    if (function_exists('hansel_breadcrumb_was_restored') && arg(0) != 'admin') {
      $restore = hansel_breadcrumb_was_restored();
      // Add current page title to breadcrumb.
      if ($restore) {
        $breadcrumb[] = drupal_get_title();
      }
    }

    $separator = theme_get_setting('breadcrumb_separator');
    $separator = !empty($separator) ? $separator : '&gt;';
    // Note, due to some strange preprocess issue, the Home link on the homepage will never get into this.
    // That link is rendered in conimbo_preprocess_page().

    return '<span class="breadcrumb_label">'. t('You are here') .':</span> '. implode(' ' . $separator . ' ', $breadcrumb);
  }
}

/**
 * Display of the feed icon.
 */
function conimbo_base_feed_icon($url, $title) {

  $output = array();

  $rss_feed_setting = theme_get_setting('rss_feed_type');
  if (!$rss_feed_setting) $rss_feed_setting = 3;

  // RSS icon.
  if ($rss_feed_setting == 1 || $rss_feed_setting == 3) {
    $image_setting = theme_get_setting('rss_icon_default');
    if ($image_setting || is_null($image_setting)) {
      $image_path = 'misc/feed.png';
    }
    else {
      $image_path = theme_get_setting('rss_icon_path');
    }
    if ($image = theme('image', $image_path, t('Syndicate content'), $title)) {
      $output[] = '<a href="'. check_url($url) .'" class="feed-icon">'. $image .'</a>';
    }
  }

  // RSS link.
  if ($rss_feed_setting == 2 || $rss_feed_setting == 3) {
    $output []= l(t('Subscribe to @title', array('@title' => strtolower($title))), $url, array('class' => 'feed-icon'));
  }

  return implode(' ', $output);
}

/**
 * Override username - to get rid of 'not verified'.
 */
function conimbo_base_username($object) {

  if ($object->uid && $object->name) {
    // Shorten the name when it is too long or it will break many tables.
    if (drupal_strlen($object->name) > 20) {
      $name = drupal_substr($object->name, 0, 15) .'...';
    }
    else {
      $name = $object->name;
    }

    if (user_access('access user profiles')) {
      $output = l($name, 'user/'. $object->uid, array('attributes' => array('title' => t('View user profile.'))));
    }
    else {
      $output = check_plain($name);
    }
  }
  else if ($object->name) {
    // Sometimes modules display content composed by people who are
    // not registered members of the site (e.g. mailing list or news
    // aggregator modules). This clause enables modules to display
    // the true author of the content.
    if (!empty($object->homepage)) {
      $output = l($object->name, $object->homepage, array('attributes' => array('rel' => 'nofollow')));
    }
    else {
      $output = check_plain($object->name);
    }

    //$output .= ' ('. t('not verified') .')';
  }
  else {
    $output = check_plain(variable_get('anonymous', t('Anonymous')));
  }

  return $output;
}
