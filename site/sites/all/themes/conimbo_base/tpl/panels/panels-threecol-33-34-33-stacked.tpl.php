<?php
// $Id: panels-threecol-33-34-33-stacked.tpl.php,v 1.1.2.1 2008/12/16 21:27:58 merlinofchaos Exp $
/**
 * @file
 * Template for a 3 column panel layout.
 *
 * This template provides a three column 25%-50%-25% panel display layout, with
 * additional areas for the top and the bottom.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['top']: Content in the top row.
 *   - $content['left']: Content in the left column.
 *   - $content['middle']: Content in the middle column.
 *   - $content['right']: Content in the right column.
 *   - $content['bottom']: Content in the bottom row.
 */
?>
<div class="panel panel-3col-33-stacked  clear-block " <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if($content['top']) {?>
  <div class="panel-row panel-row-first clear-block">
    <div class="panel-panel"><?php print $content['top']; ?></div>
  </div>
  <?php } ?>

  <?php if($content['left'] || $content['middle'] || $content['right']) {?>
  <div class="panel-row clear-block">
    <div class="panel-col panel-col-first">
      <div class="panel-panel"><?php print $content['left']; ?></div>
    </div>

    <div class="panel-col">
      <div class="panel-panel"><?php print $content['middle']; ?></div>
    </div>

    <div class="panel-col panel-col-last">
      <div class="panel-panel"><?php print $content['right']; ?></div>
    </div>
  </div>
  <?php } ?>

  <?php if($content['bottom']) {?>
  <div class="panel-row panel-row-last clear-block">
    <div class="panel-panel"><?php print $content['bottom']; ?></div>
  </div>
  <?php } ?>
</div>
