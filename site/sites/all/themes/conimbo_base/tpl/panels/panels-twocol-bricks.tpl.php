<?php
// $Id: panels-twocol-bricks.tpl.php,v 1.1.2.1 2008/12/16 21:27:59 merlinofchaos Exp $
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width. It is 5 rows high; the top
 * middle and bottom rows contain 1 column, while the second
 * and fourth rows contain 2 columns.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['top']: Content in the top row.
 *   - $content['left_above']: Content in the left column in row 2.
 *   - $content['right_above']: Content in the right column in row 2.
 *   - $content['middle']: Content in the middle row.
 *   - $content['left_below']: Content in the left column in row 4.
 *   - $content['right_below']: Content in the right column in row 4.
 *   - $content['right']: Content in the right column.
 *   - $content['bottom']: Content in the bottom row.
 */
?>
<div class="panel panel-2col-bricks clear-block " <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if($content['top']) {?>
  <div class="panel-row panel-row-first clear-block">
    <div class="panel-panel"><?php print $content['top']; ?></div>
  </div>
  <?php } ?>

  <?php if($content['left_above'] || $content['right_above']) {?>
  <div class="panel-row clear-block">
    <div class="panel-col panel-col-first">
      <div class="panel-panel"><?php print $content['left_above']; ?></div>
    </div>

    <div class="panel-col panel-col-last">
      <div class="panel-panel"><?php print $content['right_above']; ?></div>
    </div>
  </div>
  <?php } ?>

  <?php if($content['middle']) {?>
  <div class="panel-row clear-block">
    <div class="panel-panel"><?php print $content['middle']; ?></div>
  </div>
  <?php } ?>

  <?php if($content['left_above'] || $content['right_below']) {?>
  <div class="panel-row clear-block">
    <div class="panel-col panel-col-first">
      <div class="panel-panel"><?php print $content['left_below']; ?></div>
    </div>

    <div class="panel-col panel-col-last">
      <div class="panel-panel"><?php print $content['right_below']; ?></div>
    </div>
  </div>
  <?php } ?>

  <?php if($content['bottom']) {?>
  <div class="panel-row panel-row-last clear-block">
    <div class="panel-panel"><?php print $content['bottom']; ?></div>
  </div>
  <?php } ?>

</div>