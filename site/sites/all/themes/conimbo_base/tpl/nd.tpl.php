<?php
// $Id$
/**
 * @file
 *  node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */
?>
<?php if ($node->type != 'panel') {?>
<div class="buildmode-<?php print $node->build_mode; ?>">
<?php } ?>
  <div class="node node-type-<?php print $node->type; ?> <?php if (isset($node_classes)) print $node_classes; ?><?php if ($sticky && $node->build_mode == 'sticky') { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>"><div class="node-inner clear-block">
      <?php print $content; ?>
    </div></div> <!-- /node -->
<?php if ($node->type != 'panel') {?>
</div> <!-- /build-mode -->
<?php } ?>