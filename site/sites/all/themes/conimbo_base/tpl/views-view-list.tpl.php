<?php
// $Id$
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="item-list">
  <?php if (!empty($title)) : ?>
    <h2><?php print $title; ?></h2>
  <?php endif; ?>
  <<?php print $options['type']; ?> class="clearfix">
    <?php foreach ($rows as $id => $row): ?>
     <?php
      $list_id = $id+1;
      $list_classes = 'item-'.$list_id;
      if ($list_id == 1){
          $list_classes .= ' first';
      }
      if ($list_id == count($classes)){
          $list_classes .= ' last';
      }
      if ($list_id % 2) {
        $list_classes .= ' odd';
      } else {
        $list_classes .= ' even';
      }
     ?>
      <li class="<?php print $list_classes; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  </<?php print $options['type']; ?>>
</div>