// Equal height function
function equalHeight(group) {
  var tallest = 0;
  if (group.length > 1) {
	  group.each(function() {
	    var thisHeight = $(this).height();
	    if (thisHeight > tallest) {
	      tallest = thisHeight;
	    }
	  });
	  group.height(tallest);
	}
}

// Apply equal height to panel rows
$(document).ready(function() {
  if ($('#content-middle .panel').length) {
	  $('#content-middle .panel-row').each(function() {
	    equalHeight($(".panel-panel", this));
	  });
	}
  if($('#content-middle .panels-flexible-row').length) {
    $('#content-middle .panels-flexible-row-inside').each(function() {
      equalHeight($(".panels-flexible-region", this));
    });
  }
});
