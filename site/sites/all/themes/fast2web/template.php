<?php

function fast2web_preprocess_page(&$vars) {
  global $theme_key, $theme_info, $language, $base_path;
  $theme_settings = variable_get(str_replace('/', '_', 'theme_'. $theme_key .'_settings'), array());
  $layout = $theme_settings['layout'];
  
  $vars['layout'] = 'layout-' . str_replace("_","-",$layout);

  // Check if there is a custom CSS uploaded with the theme
  if(isset($theme_settings['custom_css_path'])) {
    if(!empty($theme_settings['custom_css_path']) && file_exists($theme_settings['custom_css_path'])) {
      drupal_add_css($theme_settings['custom_css_path'], 'theme');
    }
  }

  if(!empty($vars['left']) && $theme_settings['menus'] == 'horizontal-vertical') {
    $body_classes = explode(' ', $vars['body_classes']);
    foreach($body_classes as $key => $class) {
      if($class == 'no-sidebars') {
        $body_classes[$key] = 'one-sidebar';
        $body_classes[] = 'sidebar-left';
      }
    }
    $vars['body_classes'] = implode(' ', $body_classes);
  }
  
  // Adding a class on the <body> based on layout
  $vars['body_classes'] .= ' layout-' . str_replace("_","-",$layout);

  // Adding a new template suggestion based on layout
  $suggestion = '';
  /*
  switch($layout) {
    case 'sidebars_left':
    case 'sidebars_right':
      $vars['template_files'][] = 'page-sidebars-one-side';
      break;
  }
   */
  // add admin generate style
  $file = '/fast2web/custom.css';
  $file_exists = file_check_location( file_directory_path() . $file , file_directory_path() .'/fast2web/');
  if( $file_exists ) {
    drupal_add_css( variable_get('file_public_path', conf_path() . '/files' . $file ) , 'theme');
    $vars['styles'] = drupal_get_css();
  }

}

/**
 * Implements hook_breadcrumb().
 */
function fast2web_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    $currPage = drupal_get_title();
    if($currPage != end($breadcrumb)) { // if curr page is not last in list, add it
      $breadcrumb[] = $currPage;
    }
    $breadcrumb = array_map('_fast2web_breadwrap',array_filter($breadcrumb));
    $separator = '<span> » </span>';
    return '<div class="breadcrumb">' . implode($separator, $breadcrumb) . '</div>';
  }
}

/*
 * Redecorate non link breadcrumb items for css
 */
function _fast2web_breadwrap($crumb) {
  if(!stristr($crumb,'<a')) {
      return '<em>'.$crumb.'</em>';
  } else {
      return $crumb;
  }
}

/**
 * Implements hook_menu().
 */
function fast2web_menu() {
  $items = array();

  $items[''] = array(
    'title' => '',
    'description' => '',
    'page callback' => '',
    'page arguments' => array()
  );

  return $items;
}

/**
 * Theme function for the 'generic' single file formatter.
 */
function fast2web_filefield_file($file) {
  // Views may call this function with a NULL value, return an empty string.
  if (empty($file['fid'])) {
    return '';
  }

  $path = $file['filepath'];
  $url = file_create_url($path);
  $icon = theme('filefield_icon', $file);

   // MMW - Format file size

  $s = array('bytes', 'KB', 'MB', 'GB', 'TB', 'PB');
  $e = floor(log($file['filesize'])/log(1024));

  //Filesize output
  $filesize = sprintf('%.2f '.$s[$e], ($file['filesize']/pow(1024, floor($e))));

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  // TODO: Possibly move to until I move to the more complex format described
  // at http://darrelopry.com/story/microformats-and-media-rfc-if-you-js-or-css
  $options = array(
    'attributes' => array(
      'type' => $file['filemime'] . '; length=' . $file['filesize'],
  ),
  );

  // Use the description as the link text if available.
  if (empty($file['data']['description'])) {
    $link_text = $file['filename'];
  }
  else {
    $link_text = $file['data']['description'];
    $options['attributes']['title'] = $file['filename'];
  }

  return '<div class="filefield-file">'. $icon . l($link_text, $url, $options) . ' [' . $filesize . ']</div>';
}

/**
 * Theme override for fedict search block
 *
 */
function fast2web_fedict_search_block_form($form) {

  $output = '';
  //   print_r($form);
  unset($form['fedict_search_block_form']['#title']);
  $output .= drupal_render($form);

  return $output;
}

function fast2web_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
  global $pager_page_array, $pager_total, $pager_total_items;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', (isset($tags[0]) ? $tags[0] : t('« first')), $limit, $element, $parameters);
  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('‹ previous')), $limit, $element, 1, $parameters);
  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : t('next ›')), $limit, $element, 1, $parameters);
  $li_last = theme('pager_last', (isset($tags[4]) ? $tags[4] : t('last »')), $limit, $element, $parameters);

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => 'pager-first', 
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => 'pager-previous', 
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => 'pager-ellipsis', 
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => 'pager-item', 
            'data' => theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters),
          );
        }
        if ($i == $pager_current) {
          
          $start = ($i-1) * $limit + 1;
          $end = (($i-1) * $limit) + $limit;
          if($end > $pager_total_items[0]) {
            $end = $pager_total_items[0];
          }          
          
          $items[] = array(
            'class' => 'pager-current', 
            'data' => $start . ' - ' . $end,
//             'data' => theme('pager_link', $i, $limit, $element, $i, $parameters),
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => 'pager-item', 
            'data' => theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => 'pager-ellipsis', 
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => 'pager-next', 
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => 'pager-last', 
        'data' => $li_last,
      );
    }
    return theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));
  }
}


function fast2web_pager_link($text, $page_new, $element, $parameters = array(), $attributes = array(), $limit = 10) {
    global $pager_total_items;
  
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query[] = drupal_query_string_encode($parameters, array());
  }
  $querystring = pager_get_querystring();
  if ($querystring != '') {
    $query[] = $querystring;
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
      t('« first') => t('Go to first page'),
      t('‹ previous') => t('Go to previous page'),
      t('next ›') => t('Go to next page'),
      t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    else if (is_numeric($text)) {
        $start = ($text-1) * $limit + 1;
            $end = (($text-1) * $limit) + $limit;
          
            if($end > $pager_total_items[0]) {
              $end = $pager_total_items[0];
            }
            $text = $start . ' - ' . $end;
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

//   return array(
//       'title' => $text,
//       'href' => $_GET['q'],
//       'attributes' => $attributes,
//       'query' => count($query) ? implode('&', $query) : NULL,
//   );
  
   return l($text, $_GET['q'], array('attributes' => $attributes, 'query' => count($query) ? implode('&', $query) : NULL));
}

/*
 * Implement theme_pager_next
 */
function fast2web_pager_next($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', $text, $limit, $element, $parameters);
    }
    // The next page is not the last page.
    else {
      $output = theme('pager_link', $text, $page_new, $element, $parameters, $limit );
    }
  }

  return $output;
}


/*
* Implement theme_pager_previous
*/
function fast2web_pager_previous($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);

    // If the previous page is the first page, mark the link as such.
    if ($page_new[$element] == 0) {
      $output = theme('pager_first', $text, $limit, $element, $parameters);
    }
    // The previous page is not the first page.
    else {
      $output = theme('pager_link', $text, $page_new, $element, $parameters, $limit);
    }
  }

  return $output;
}

