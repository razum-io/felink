<?php


/**
 * Return default config
 * @return array
 */
function fast2web_load_config() {
  return array(
    'fast2web_colors' => array(
      'menu_hover_color' => '#55a7fc',
      'strong_color' => '#294c95',
      'light_color' => '#b0c5dc',
      'link_hover_color' => '#ec7d2c',
      'link_color' => '#26478c',
      'link_visited_color' => '#759fb4',
    ),
    'fast2web_files' => array(
      'logo' => '',
      'banner' => '',
    ),
  );
}

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function fast2web_settings($saved_settings) {

  $form['#attributes'] = array('enctype' => 'multipart/form-data');

  $directory_path = file_directory_path();
  file_check_directory($directory_path, FILE_CREATE_DIRECTORY, 'file_directory_path');

  // get config
  $fast2web_values = fast2web_load_config();
  $defaults = $fast2web_values['fast2web_colors'];

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Upload the custom css file if any
  $limits = array (
    'extensions' => array('css'),
  );

  // Check for a new uploaded custom css, and use that instead.
  if ($file = file_save_upload('custom_css_file', $limits)) {
    $parts = pathinfo($file->filename);
    $filename = ($key) ? str_replace('/', '_', $key) .'custom_uploaded_css.'. $parts['extension'] : 'custom_uploaded_css.'. $parts['extension'];

    // The image was saved using file_save_upload() and was added to the
    // files table as a temporary file. We'll make a copy and let the garbage
    // collector delete the original upload.
    if (file_copy($file, $filename, FILE_EXISTS_REPLACE)) {
      //$_POST['default_logo'] = 0;
      $_POST['custom_css_path'] = $file->filepath;
      $_POST['toggle_custom_css'] = 1;
    }
  }

  $form['custom_css'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom CSS settings'),
    '#description' => t("You can add a custom css to this theme... without modifying the base theme.")
  );

  $form['custom_css']['custom_css_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to custom icon'),
    '#default_value' => $settings['custom_css_path'],
    '#description' => t('The path to the image file you would like to use as your custom css.')
  );

  $form['custom_css']['custom_css_file'] = array(
    '#type' => 'file',
    '#title' => t('Upload your custom css'),
    '#description' => t("If you don't have direct file access to the server, use this field to upload your custom css.")
  );

  $form['fast2web'] = array(
    '#type' => 'fieldset',
    '#title' => t('Colors'),
    '#description' => t("Customise the look of the theme.")
  );
  // define color fields
  foreach($fast2web_values['fast2web_colors'] as $key => $value) {
    $form['fast2web'][$key] = array(
        '#type' => 'textfield',
        '#title' => t(str_replace('_',' ',$key)),
        '#default_value' => $settings[$key],
        '#description' => t('Default value: @color', array('@color' => $defaults[$key])),
    );
  }
  // get config key
  reset($fast2web_values);
  $first_key = key($fast2web_values['fast2web_colors']);
  // force call to submit hook
  $form['fast2web'][$first_key]['#element_validate'][] = 'fast2web_settings_submit';
//   $form['save']['#executes_submit_callback']=TRUE;
//   $form['save']['#submit']= array('fast2web_settings_submit');
  // Return the additional form widgets
  return $form;
}



/**
* Capture theme settings submissions and generate css file
*/
function fast2web_settings_submit($form, &$form_state) {

  // get defaults & config
  $fast2web_values = fast2web_load_config();

  // create array of valid values
  $replace = array();
  foreach($fast2web_values['fast2web_colors'] as $key => $default_value) {
    $val = $default_value; // just in case value is invalid
    if(isset($form_state['values'][$key]) && preg_match('/^#[a-f0-9]{6}$/i', $form_state['values'][$key])) {
      $val = $form_state['values'][$key];
    }
    $replace['$'.$key] = $val;
  }

  // get css template file & replace markers by actual values
  $csstpl = file_get_contents(dirname(__FILE__).'/sass/partials/_colors.scss');
  $css = str_replace( array_keys($replace), array_values($replace), $csstpl );

  // create the css file
  $path = file_create_path('fast2web');
  file_check_directory($path, FILE_CREATE_DIRECTORY);
  file_save_data($css, $path.'/custom.css', FILE_EXISTS_REPLACE);

  //define your limits for the submission here
  /*
  $limits = array (
    'extensions' => array('css'),
  );

  $validators = array(
    'file_validate_extensions' => array($limits['extensions']),
  );

  // Save new file uploads.
  if ($file = file_save_upload('file_upload', $validators, file_directory_path())) {
    // Do something with $file here.
  }
  */

}


?>
