<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title ?></title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <?php print $head ?>
  <?php print $styles ?>
  <!--[if lt IE 9]>
      <link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/conimbo_base/css/ie.css" >
  <![endif]-->
  <?php print $scripts ?>
</head>
<body class="<?php print $body_classes?>">
<?php if (isset($one_admin)) print $one_admin; ?>
  <div id="body-inner">

    <?php if ($federalheader): ?>
      <div id="federalheader-outer" class="clear-block">
        <div id="federalheader" class="container-<?php print $container_classes?>">
          <div id="blgm_belgiumHeader">
            <div class="blgm_wrapper">
              <?php print $federalheader; ?>
              <div id="blgm_beLink"><?php print t('federal_header_text') ?></div>
              <div id="blgm_beLogo"><img src="/<?php print drupal_get_path('theme', 'conimbo_base'); ?>/images/blgm_beLogo.gif" alt="Logo van de Belgische federale overheid" /></div>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <div id="toolbar-outer" class="clear-block">
      <div id="toolbar-container" class="container-<?php print $container_classes ?>">
        <div id="toolbar-grid" class="grid-<?php print $container_classes ?>">
          
          <div id="toolbar" class="clear-block">
            <?php if (isset($search_box) && !module_exists('fedict_search')) : ?>
              <?php print $search_box; ?>
            <?php endif; ?>
            
            <?php if (isset($toolbar)) : ?>
              <?php print $toolbar; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div><!-- /#toolbar -->

    <div id="header-outer" class="clear-block">
      <div id="header-container" class="container-<?php print $container_classes ?>">
        <div id="header-grid" class="grid-<?php print $container_classes ?>">
          <div id="header" class="clear-block">
            <?php if (isset($placeholder_logo)) : ?>
              <div id="logo">
              <?php if ($is_front) : ?><h1><?php endif; ?>
              <?php if (!$is_front) : ?><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php endif; ?>
              <?php print($placeholder_logo) ?>
              <?php if (!$is_front) : ?></a><?php endif; ?>
              <?php if ($is_front) : ?>
                <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
                </h1>
              <?php endif; ?>
              </div>
            <?php endif; ?>

            <?php if (isset($header)) : ?>
              <?php print $header; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div><!-- /#header -->

    <div id="navigation-outer" class="clear-block">
      <div id="navigation-container" class="container-<?php print $container_classes ?>">
        <div id="navigation-grid" class="grid-<?php print $container_classes ?>">
          <div id="navigation" class="clear-block">
            <?php if (isset($primary_links)) : ?>
              <?php print $primary_links; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div><!-- /#navigation -->

    <?php if (isset($subheader)) : ?>
      <div id="subheader-outer" class="clear-block">
        <div id="subheader-container" class="container-<?php print $container_classes ?>">
          <div id="subheader-grid" class="grid-<?php print $container_classes ?>">
            <div id="subheader" class="clear-block">
              <?php
                if ($is_front) {
                  if (isset($placeholder_homepage)) {
                    print $placeholder_homepage;
                  }
                } else {
                  if (isset($placeholder_allpages)) {
                    print $placeholder_allpages;
                  }
                }
              ?>
              <?php print $subheader; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?><!-- /#subheader -->

    <?php if (isset($breadcrumb)) : ?>
      <div id="breadcrumb-outer" class="clear-block">
        <div id="breadcrumb-container" class="container-<?php print $container_classes ?>">
          <div id="breadcrumb-grid" class="grid-<?php print $container_classes ?>">
            <div id="breadcrumb" class="clear-block">
              <?php print $breadcrumb; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?><!-- /#breadcrumb -->

    <div id="content-outer" class="clear-block">
      <div id="content-container" class="container-<?php print $container_classes ?>">
        <div id="content" class="clear-block">
          
          <?php if ($left && $layout != 'layout-sidebars-right') : ?>
            <div id="sidebar-a-grid" class="<?php print $sidebar_left_classes?>">
              <div id="sidebar-a">
                <?php print $left; ?>
              </div>
            </div>
          <?php endif; ?><!-- /#sidebar-a -->

          <div id="content-middle-grid" class="<?php print $content_middle_classes?>">
            <span class="top"></span>
            <div id="content-middle" class="inner <?php if (isset($page_classes)) print $page_classes; ?>">
              <?php if (isset($title) && !empty($title)) : ?>
                <h1 class="title">
                  <?php print $title; ?>
                </h1>
              <?php endif; ?>

              <?php if ($show_messages && $messages != "") : ?>
                <div id="messages">
                  <?php print $messages; ?>
                </div>
              <?php endif; ?>

              <?php if ($help != "") : ?>
                <div id="help"><?php print $help ?></div>
              <?php endif; ?>

              <?php print $content; ?>
            </div>
            <span class="bottom"></span>
          </div><!--/#content-middle -->

          <?php if ($left && $layout == 'layout-sidebars-right') : ?>
            <div id="sidebar-a-grid" class="<?php print $sidebar_left_classes?>">
              <div id="sidebar-a">
                <?php print $left; ?>
              </div>
            </div>
          <?php endif; ?><!-- /#sidebar-a -->

          <?php if ($right) : ?>
            <div id="sidebar-b-grid" class="<?php print $sidebar_right_classes?>">
              <div id="sidebar-b">
                <?php print $right; ?>
              </div>
            </div>
          <?php endif; ?><!-- /#sidebar-b -->

        </div>
      </div>
    </div><!--/#content -->

    <div id="doormat_footer">

    <?php if (isset($doormat)) : ?>
      <div id="doormat-outer" class="clear-block">
        <div id="doormat-container" class="container-<?php print $container_classes ?>">
          <div id="doormat-grid" class="grid-<?php print $container_classes ?>">
            <div id="doormat" class="clear-block">
                <?php print $doormat; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?><!-- /#doormat -->

    <?php if (isset($footer) || isset($footer_message)) : ?>
      <div id="footer-outer" class="clear-block">
        <div id="footer-container" class="container-<?php print $container_classes ?>">
          <div id="footer-grid" class="grid-<?php print $container_classes ?>">
            <div id="footer" class="clear-block">
              <?php
                if (isset($footer)) {
                  print $footer;
                }

                if (isset($footer_message)) {
                  print '<span id="footer-message">'. $footer_message;
                    if (isset($footer)) {
                      print '&nbsp;-&nbsp;';
                    }
                  print '</span>';
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?><!-- /#footer -->

    </div>

  </div><!--/#body-inner -->
<?php print $closure;?>
</body>
</html>
