<?php
/*
  selector { property: <?php print $placeholders['button']; ?>; }
*/

?>

.button {
<?php if ($placeholders['button']) {?>
  background-image: url(<?php print $placeholders['button']?>);
<?php } ?>
<?php if ($placeholders['button-repeat']) {?>
  background-repeat: <?php print $placeholders['button-repeat']?>;
<?php } ?>
}

#doormat {
<?php if ($placeholders['doormat']) {?>
  background-image: url(<?php print $placeholders['doormat']?>);
<?php } ?>
<?php if ($placeholders['doormat-repeat']) {?>
  background-repeat: <?php print $placeholders['doormat-repeat']?>;
<?php } ?>
}