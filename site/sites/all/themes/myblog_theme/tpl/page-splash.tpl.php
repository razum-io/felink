<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title ?></title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <?php print $head ?>
  <?php print $styles ?>
  <!--[if lt IE 8]>
      <link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/myblog_theme/css/ie7.css" >
  <![endif]-->
  <?php print $scripts ?>
</head>
<body class="<?php print $body_classes?>">
<?php if (isset($one_admin)) print $one_admin; ?>
  <div id="body-inner">
      <div id="page" class="container-12 clear-block">
        <div id="splash" class="clear-block">
          <div id="splash-left">
          <?php if ($title): print '<h1 class="title">' . $title . '</h1>'; endif;?>
          <?php print $content; ?>
        </div> <!--  #splash-left -->

        <div id="splash-right">
           <?php if ($placeholder_logo) : ?>
                <div id="logo">
                 <?php print($placeholder_logo) ?>
                </div>
            <?php endif; ?>
        </div><!--/#splash-right -->
      </div><!--/#splash -->
    </div><!--/#page -->
  </div><!--/#body-inner -->
<?php print $closure;?>
</body>
</html>