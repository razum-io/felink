
window.onload=function(){
  /* only for IE < 9 */
  if ($.browser.msie) {
    if (parseInt(jQuery.browser.version) < 9) {

      smallborders = {
        tl: { radius: 3 },
        tr: { radius: 3 },
        bl: { radius: 3 },
        br: { radius: 3 }
      }
      
      mediumborders = {
        tl: { radius: 5 },
        tr: { radius: 5 },
        bl: { radius: 5 },
        br: { radius: 5 }
      }
      
      bigborders = {
        tl: { radius: 10 },
        tr: { radius: 10 },
        bl: { radius: 10 },
        br: { radius: 10 }
      }

      /* from style.css */
      //$('.sidebar .block').corner(mediumborders);
      $('div.messages').corner(mediumborders);
      $('#help').corner(mediumborders);
      $('#content-middle form').corner(mediumborders);
      $('.button a').corner(mediumborders);
      $('.pager').corner(mediumborders);
      $('.pager li span').corner(mediumborders);
      $('.not-front #content-middle-inner').corner(bigborders);
      $('.buildmode-text_list .node').corner(bigborders);
      $('.highlight-1').corner(mediumborders);
      $('.highlight-2').corner(mediumborders);

      /* from menu.css */

      /* from features.css */
      $('#splash').corner(mediumborders);
      $('.comment').corner(bigborders);
      $('.field-gallery-slideshow').corner(bigborders);
      //$('#content-middle .panel-panel').corner(bigborders);
      $('a.apachesolr-unclick').corner(mediumborders);
      $('#search-theme-form').corner(smallborders);
      $('#content-middle #search-form').corner(mediumborders);
      $('.search-wrapper').corner(mediumborders);
      
      /* give errors, try when jquery with drupal is updated */
      /*
       * submenus are weird because of padding problems
       * $('#search-theme-form .form-text').corner(smallborders);
       * $('input.button').corner(mediumborders);
       */
      
    }
  }
}
