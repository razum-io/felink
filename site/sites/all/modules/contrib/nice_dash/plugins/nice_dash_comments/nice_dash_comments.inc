<?php

/**
 * @file
 * Comments plugin for Nice dashboard.
 */

class nice_dash_comments {

  /**
   * Views api method.
   */
  function views_api() {
    return TRUE;
  }

  /**
   * Forms
   */
  function forms(&$forms) {
    $forms['nice_dash_comment'] = array(
      'callback' => 'nice_dash_menu_callback_form',
      'callback arguments' => array('nice_dash_comments', 'nice_dash_comment_form'),
    );
  }

  /**
   * Widgets method.
   */
  function widgets() {
    $widgets = array(
      'comment_configuration' => t('Configure comments'),
      'latest_comments' => t('Latest comments'),
    );
    return $widgets;
  }

  /**
   * Latests comments.
   */
  function nice_dash_comments_widget_latest_comments() {
    if (module_exists('comment')) {
      return array(
        'title' => t('Latest comments'),
        'description' => t('An overview of the latest comments'),
        'content' => views_embed_view('nice_dash_comments', 'block_1')
      );
    }
  }

  /**
   * Comment configuration.
   */
  function nice_dash_comments_widget_comment_configuration() {
    if (module_exists('comment')) {
      $content = drupal_get_form('nice_dash_comment');
      return array(
        'title' => ('Configure comments'),
        'content' => $content,
      );
    }
    return array();
  }

  function nice_dash_comment_form() {
    $form = $this->comment_configure_form();
    $form['#submit'][] = 'nice_dash_menu_callback_form_submit';

    return $form;
  }

  /**
   * Comment form configuration.
   */
  function comment_configure_form() {
    $form = array();

    $node_types = node_get_types();
    foreach ($node_types as $type => $node_type) {
      $form['comment_'. $type] = array(
        '#type' => 'select',
        '#options' => array(t('Disabled'), t('Read only'), t('Read/Write')),
        '#default_value' => variable_get('comment_'. $type, COMMENT_NODE_DISABLED),
      );
      $form['comment_anonymous_'. $type] = array(
        '#title' => t('Anonymous commenting'),
        '#type' => 'radios',
        '#options' => array(
          COMMENT_ANONYMOUS_MAYNOT_CONTACT => t('Anonymous posters may not enter their contact information'),
          COMMENT_ANONYMOUS_MAY_CONTACT => t('Anonymous posters may leave their contact information'),
          COMMENT_ANONYMOUS_MUST_CONTACT => t('Anonymous posters must leave their contact information'),
        ),
        '#default_value' => variable_get('comment_anonymous_'. $type, COMMENT_ANONYMOUS_MAYNOT_CONTACT),
      );
      $form['comment_subject_field_'. $type] = array(
        '#type' => 'radios',
        '#title' => t('Comment subject field'),
        '#default_value' => variable_get('comment_subject_field_'. $type, 1),
        '#options' => array(t('Disabled'), t('Enabled')),
        //'#description' => t('Can users provide a unique subject for their comments?'),
      );
      $form['comment_preview_'. $type] = array(
        '#type' => 'radios',
        '#title' => t('Preview comment'),
        '#default_value' => variable_get('comment_preview_'. $type, COMMENT_PREVIEW_REQUIRED),
        '#options' => array(t('Optional'), t('Required')),
        '#description' => t("Forces a user to look at their comment by clicking on a 'Preview' button before they can actually add the comment"),
      );
    }

    $form['#plugin_theme'] = 'theme_comment_configure_form';
    $form = system_settings_form($form);
    $form['buttons']['reset']['#access'] = FALSE;
    return $form;
  }

  /**
   * Theme comment form configuration.
   */
  function theme_comment_configure_form($form) {
    $output = '';
    $output .= drupal_render($form['enable']);

    $header = array(
      t('Content type'),
      t('Status'),
      t('Settings'),
    );

    $rows = array();
    $node_types = node_get_types();
    foreach ($node_types as $type => $node_type) {
      $settings = drupal_render($form['comment_anonymous_'. $type]) .
                  drupal_render($form['comment_subject_field_'. $type]) .
                  drupal_render($form['comment_preview_'. $type]);

      $settings_handler = '<div class="ctools-no-container ctools-collapsible-container ctools-collapsed ctools-collapsible-remember" id="comment-settings-'. $type .'">'. t('Manage settings') . '</div>';
      $settings_content = '<div id="comment-settings-'. $type .'-content">'. $settings .'</div>';

      // Basic settings.
      $row = array();
      $row[] = check_plain(t($node_types[$type]->name));
      $row[] = array('data' => drupal_render($form['comment_'. $type]));
      $row[] = array('data' => $settings_handler);
      $rows[] = $row;

      // Additional settings.
      $row = array(array('data' => '<div class="nice-dash-collapsible">'. $settings_content .'</div>', 'colspan' => '3', 'class' => 'nice-dash-collapsible-td'));
      $rows[] = $row;
    }

    if ($rows) {
      $output .= theme('table', $header, $rows, array('id' => 'configure-comments-overview', 'class' => 'nice-dash-table'));
    }

    $output .= drupal_render($form);

    return $output;
  }

  /**
   * Submit callback; settings for comment widget.
   */
  function nice_dash_comment_form_submit($form, &$form_state) {
    $node_types = node_get_types();
    foreach ($node_types as $type => $node_type) {
      if (variable_get('comment_' . $type) == COMMENT_NODE_READ_WRITE ||
          variable_get('comment_' . $type) == COMMENT_NODE_READ_ONLY) {
        variable_set('comment_form_location_'. $type, COMMENT_FORM_BELOW);
      }
    }
    drupal_flush_all_caches();
  }

}
