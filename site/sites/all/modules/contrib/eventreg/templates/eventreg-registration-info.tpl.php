	<h3 class="registration-info-title"><?php print t("Registration information") ?>:</h3>
	<div class="er-info">
		<table>
				<col width="30%" />
				<col width="70%" />
			<tbody>
				<tr>
					<td><strong><?php print t("Id") ?></strong></td>
					<td><?php print $registration->regid ?></td>
				</tr>
				<tr>
					<td><strong><?php print t("User") ?></strong></td>
					<td><?php $reg_user->uid > 0 ? print l($reg_user->name, 'user/'.$reg_user->uid) : print $reg_user->name ?></td>
				</tr>
				<tr>
					<td><strong><?php print t("Status") ?></strong></td>
					<td class="<?php print strtolower($registration->status) ?>"><?php print $registration->status ?></td>
				</tr>
				<tr>
					<td><strong><?php print t("Registration date") ?></strong></td>
					<td><?php $registration->registration_date > 0 ? print format_date($registration->registration_date, 'small') : print '-' ?></td>
				</tr>
				<tr>
					<td><strong><?php print t("Validation date") ?></strong></td>
					<td><?php $registration->validation_date > 0 ? print format_date($registration->validation_date, 'small') : print '-' ?></td>
				</tr>
				<tr>
					<td><strong><?php print t("Confirmation date") ?></strong></td>
					<td><?php $registration->confirmation_date > 0 ? print format_date($registration->confirmation_date, 'small') : print '-' ?></td>
				</tr>
				<tr>
					<td><strong><?php print t("Last confirmation send date") ?></strong></td>
					<td><?php $registration->last_confirmation_send_date > 0 ? print format_date($registration->last_confirmation_send_date, 'small') : print t("never") ?></td>
				</tr>
				<?php if ($status_form) : ?>
				<tr>
					<td><strong><?php print t("Update status") ?></strong></td>
					<td><?php print $status_form ?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
