<div class="er-info">
	<table>
			<col width="30%" />
			<col width="70%" />
		<tbody>
			<tr>
				<td><strong><?php print t("Total registrations") ?></strong></td>
				<td><?php print $stats['total_registrations_cnt'] ?></td>
			</tr>
			<tr>
				<td class="registered"><strong><?php print t("Registered") ?></strong></td>
				<td><?php print $stats['registered_cnt'] ?> (<?php print number_format($stats['registered_pct'], 1) ?>%)</td>
			</tr>
			<tr>
				<td class="validated"><strong><?php print t("Validated") ?></strong></td>
				<td><?php print $stats['validated_cnt'] ?> (<?php print number_format($stats['validated_pct'], 1) ?>%)</td>
			</tr>
			<tr>
				<td class="confirmed"><strong><?php print t("Confirmed") ?></strong></td>
				<td><?php print $stats['confirmed_cnt'] ?> (<?php print number_format($stats['confirmed_pct'], 1) ?>%)</td>
			</tr>
			<tr>
				<td><strong><?php print t("Total reminders sent") ?></strong></td>
				<td><?php print $stats['reminders_sent'] ?></td>
			</tr>
		</tbody>
	</table>
	<!--ul>
		<li><strong><?php print t("Total registrations") ?></strong>: <?php print $stats['total_registrations_cnt'] ?></li>
		<li>
			<ul>
				<li class="registered"><strong><?php print t("Registered") ?></strong>: <?php print $stats['registered_cnt'] ?> (<?php print number_format($stats['registered_pct'], 1) ?>%)</li>
				<li class="validated"><strong><?php print t("Validated") ?></strong>: <?php print $stats['validated_cnt'] ?> (<?php print number_format($stats['validated_pct'], 1) ?>%)</li>
				<li class="confirmed"><strong><?php print t("Confirmed") ?></strong>: <?php print $stats['confirmed_cnt'] ?> (<?php print number_format($stats['confirmed_pct'], 1) ?>%)</li>
			</ul>
		</li>
		<li><strong><?php print t("Total reminders sent") ?></strong>: <?php print $stats['reminders_sent'] ?></li>
	</ul-->
</div>