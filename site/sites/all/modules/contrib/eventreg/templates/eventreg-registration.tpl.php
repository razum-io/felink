<?php
//print_r($event);
//print_r($registration);
?>
	<div class="er-pager">
		<ul>
			<?php if ($back_link) { print '<li>'.$back_link.'</li>'.PHP_EOL; } ?>
			<?php if ($previous_link) { print '<li>'.$previous_link.'</li>'.PHP_EOL; } ?>
			<?php if ($next_link) { print '<li>'.$next_link.'</li>'.PHP_EOL; } ?>
		</ul>
	</div>

	<?php if ($is_draft) { print $is_draft; } ?>
	<?php print '<div class="registration-info">'.$registration_info.'</div>'; ?>

	<?php if ($submitted_values) : ?>
	<h3 class="registration-info-title"><?php print t("Registration data") ?>:</h3>
	<div class="submitted-values"><?php print $submitted_values ?></div>
	<?php endif; ?>

	<div class="er-pager">
		<ul>
			<?php if ($back_link) { print '<li>'.$back_link.'</li>'.PHP_EOL; } ?>
			<?php if ($previous_link) { print '<li>'.$previous_link.'</li>'.PHP_EOL; } ?>
			<?php if ($next_link) { print '<li>'.$next_link.'</li>'.PHP_EOL; } ?>
		</ul>
	</div>