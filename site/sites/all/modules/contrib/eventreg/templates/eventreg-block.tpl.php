<div class="eventreg-block item-list">

<?php foreach($events as $event):?>

	<div class="item-list">
		<ul class="clearfix">
			<li class="<?php echo ($i % 2 == 0) ? "even" : "odd";?>"><div class="buildmode-block">
					<div class="node node-type-news ">
						<div class="node-inner clear-block">
							<div class="nd-region-header clear-block ">
								<div class="field field-title">
									<p>
										<?php echo l($event->title,'node/'.$event->nid); ?>
									</p>
								</div>
							</div>
							<div class="nd-region-middle-wrapper nd-no-sidebars">
								<div class="nd-region-middle">
									<div class="field field-post-date field-inline-small"><?php echo format_date($event->evt_date, 'custom','d/m/Y'); ?></div>
								</div>
							</div>
						</div>
					</div>
				</div> </li>
			</li>
		</ul>
	</div>
	
  
<?php endforeach;?>
</div>
