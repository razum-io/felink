<div class="eventreg-sessions">
<h3><?php echo t('Sessions')?></h3>
<?php foreach($sessions as $session):?>
  <div class="session">
    <div class="time"><?php echo _eventreg_format_time($session->start_date); ?> - <?php echo _eventreg_format_time($session->end_date); ?></div>
    <div class="title"><?php echo $session->title; ?></div>
    <div class="desc"><?php echo $session->body; ?></div>
  </div>
<?php endforeach;?>
</div>