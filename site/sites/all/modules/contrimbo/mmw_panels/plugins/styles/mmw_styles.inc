<?php
/**
 * @file
 * Definition of the MMW panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('MMW styles'),
  'description' => t('Apply the predefined MMW styles to a panel region.'),
  'render region' => 'panels_mmw_style_render_region',
  'settings form' => 'panels_mmw_style_settings_form',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_mmw_style_render_region($display, $region_id, $panes, $settings) {
  $output = '';

  if (isset($settings['style'])) {
		$output .= '<div class="mod complex flow ' . $settings['style'] . '">';
		$output .= '<b class="top"><b class="tl"></b><b class="tr"></b></b>';
  }

  foreach ($panes as $pane_id => $pane_output) {
    if ($pane_output) {
      $output .= '<div class="in">' . $pane_output . '</div>';
    }
  }

  if (isset($settings['style'])) {
  	$output .= '<b class="bottom"><b class="bl"></b><b class="br"></b></b>';
    $output .= '</div>';
  }

  return $output;
}

/**
 * Settings form callback.
 */
function panels_mmw_style_settings_form($style_settings) {
  // Style settings
  $theme_key = variable_get('theme_default', 'clean_theme');
  $info = db_result(db_query("SELECT info FROM {system} WHERE name = '%s' AND type ='theme'", $theme_key));
  $theme_info = unserialize($info);

  //$options = array('' => 'No style');

  if ($theme_info['block_styles']) {
    foreach ($theme_info['block_styles'] as $key => $value) {
      $options[$value['class']] = $value['label'];
    }
  }

	$options['grey'] = 'Grey with shadow';
	$options['light'] = 'Light color without shadow'; 

  $form['style']['#type'] = 'select';
  $form['style']['#options'] = $options;

  $default_style = variable_get('block_styles', array());
  $form['style']['#default_value'] = isset($default_style[$form['module']['#value'] .'_'. $form['delta']['#value']]) ? $default_style[$form['module']['#value'] .'_'. $form['delta']['#value']] : '';

  return $form;
}


