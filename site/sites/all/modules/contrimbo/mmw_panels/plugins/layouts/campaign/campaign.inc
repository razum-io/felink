<?php
// Plugin definition
$plugin = array(
  'title' => t('One Three Two'),
  'icon' => 'campaign.png',
  'theme' => 'panels_campaign',
  'theme arguments' => array('id', 'content'),
  'css' => 'campaign.css',
  'panels' => array(
    'first_row' => t('First row'),
    'second_row_first' => t('Second row first pane'),
    'second_row_middle' => t('Second row middle pane'),
    'second_row_last' => t('Second row last pane'),
    'third_row_first' => t('Third row first pane'),
    'third_row_last' => t('Third row last pane'),
  ),
);
