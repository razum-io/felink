Drupal.one_actions = Drupal.one_actions || {};
Drupal.one_actions.overlay = '<div class="admin-border admin-border-top"></div><div class="admin-border admin-border-right"></div><div class="admin-border admin-border-bottom"></div><div class="admin-border admin-border-left"></div>';
Drupal.one_actions.active = '';


Drupal.behaviors.one_actions = function(context) {

  $("div.one-actions-enabled").hover(function(event) {
    // get the right actions from the closure region
    var $this = $(this);
    var classes = ($this.attr("class"));
    var identifier = '#one-actions-'+ classes.replace(/([^ ]+[ ]+)*one-actions-([^ ]+)([ ]+[^ ]+)*/, '$2');
    $this.prepend($(identifier));
     
    // hide parent actions  
    $(Drupal.one_actions.active + ' .one-actions-links:first').hide();
    $(Drupal.one_actions.active).css('display', 'none');
    $(Drupal.one_actions.active + ' .admin-border').remove();
    $(Drupal.one_actions.active + ' a.one-actions-toggler:first').removeClass('one-actions-toggler-active');

    Drupal.one_actions.active = identifier;

    // show current actions
    $('.one-actions:first', this).append(Drupal.one_actions.overlay);
    $('.admin-border-top', this).css({'width': $this.outerWidth() - 26});
    $('.admin-border-bottom', this).css({'width': $this.outerWidth()});    
    $('.admin-border-left', this).css({'height': $this.outerHeight() + 20});
    $('.admin-border-right', this).css({'height': $this.outerHeight()});    
    var offset = $this.position();
    var left = 0;
    var top = 0;
    if ($this.css("position") == 'static') {
      left = offset.left;
      top = offset.top;
    }
    
    $('.one-actions-wrapper:first', this).css({'top': top});
    $('.one-actions-wrapper:first', this).css({'left': left});    
    $('.admin-border-top:first', this).css({'top': top - 10});
    $('.admin-border-top:first', this).css({'left': left});   
    $('.admin-border-bottom:first', this).css({'top': top + $this.outerHeight()});
    $('.admin-border-bottom:first', this).css({'left': left});  
    $('.admin-border-left:first', this).css({'top': top - 10});
    $('.admin-border-left:first', this).css({'left': left - 10});
    $('.admin-border-right:first', this).css({'top': top + 10});   
    $('.admin-border-right:first', this).css({'left': left + $this.outerWidth()});    
    $('.one-actions:first', this).fadeIn('normal');
  }, function() {
    $('.one-actions:first', this).css('display', 'none');
    $('.admin-border', this).remove();   
    $('a.one-actions-toggler:first', this).removeClass('one-actions-toggler-active');
  });
}


/**
 * toggles visibility on an element
 */
Drupal.one_actions.toggleVis = function(element) {
  var element = $('#' + element);
  if (element.is(":hidden")) {
    element.slideDown('fast');
  } else {
    element.hide();
  }
  $('a.one-actions-toggler', element.parent()).toggleClass('one-actions-toggler-active');
}