<?php
/**
 * CCK defintion for agenda content type + extra agenda date field
 */
function _mmw_agenda_cck_export() {

  $content['type']  = array (
    'name' => 'Agenda',
    'type' => 'agenda',
    'description' => 'Agenda item ( simple agenda )',
    'title_label' => 'Title',
    'body_label' => 'Body',
    'min_word_count' => '0',
    'help' => '',
    'node_options' =>
  array (
      'status' => true,
      'promote' => true,
      'sticky' => false,
      'revision' => false,
  ),
    'language_content_type' => '0',
    'old_type' => 'agenda',
    'orig_type' => '',
    'module' => 'node',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'nodewords_edit_metatags' => 1,
    'nodewords_metatags_generation_method' => '0',
    'nodewords_metatags_generation_source' => '2',
    'nodewords_use_alt_attribute' => 1,
    'nodewords_filter_modules_output' =>
  array (
      'imagebrowser' => false,
      'img_assist' => false,
  ),
    'nodewords_filter_regexp' => '',
    'page_title' =>
  array (
      'show_field' =>
  array (
        'show_field' => true,
  ),
      'pattern' => '',
  ),
    'i18n_newnode_current' => 0,
    'i18n_required_node' => 0,
    'i18n_lock_node' => 0,
    'i18n_node' => 1,
  );
  $content['fields']  = array (
  0 =>
  array (
      'label' => 'Agenda date',
      'field_name' => 'field_agenda_date',
      'type' => 'date',
      'widget_type' => 'date_text',
      'change' => 'Change basic information',
      'weight' => '-4',
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'input_format' => 'd/m/Y - H:i:s',
      'input_format_custom' => 'd/m/Y',
      'advanced' =>
  array (
        'label_position' => 'above',
        'text_parts' =>
  array (
          'year' => 0,
          'month' => 0,
          'day' => 0,
          'hour' => 0,
          'minute' => 0,
          'second' => 0,
  ),
  ),
      'increment' => 1,
      'year_range' => '-3:+3',
      'label_position' => 'above',
      'text_parts' =>
  array (
  ),
      'description' => 'Enter the date in dd/mm/yyyy format',
      'required' => 1,
      'multiple' => '0',
      'repeat' => 0,
      'todate' => '',
      'granularity' =>
  array (
        'year' => 'year',
        'month' => 'month',
        'day' => 'day',
  ),
      'default_format' => 'long',
      'tz_handling' => 'none',
      'timezone_db' => '',
      'op' => 'Save field settings',
      'module' => 'date',
      'widget_module' => 'date',
      'columns' =>
  array (
        'value' =>
  array (
          'type' => 'varchar',
          'length' => 20,
          'not null' => false,
          'sortable' => true,
          'views' => true,
  ),
  ),
      'display_settings' =>
  array (
        'label' =>
  array (
          'format' => 'above',
          'exclude' => 0,
  ),
        'teaser' =>
  array (
          'format' => 'default',
          'exclude' => 0,
  ),
        'full' =>
  array (
          'format' => 'default',
          'exclude' => 0,
  ),
  4 =>
  array (
          'format' => 'default',
          'exclude' => 0,
  ),
  2 =>
  array (
          'format' => 'default',
          'exclude' => 0,
  ),
  3 =>
  array (
          'format' => 'default',
          'exclude' => 0,
  ),
        'sticky' =>
  array (
          'format' => 'default',
          'exclude' => 0,
  ),
        'block' =>
  array (
          'format' => 'default',
          'exclude' => 0,
  ),
        'text_list' =>
  array (
          'format' => 'default',
          'exclude' => 0,
  ),
        'token' =>
  array (
          'format' => 'default',
          'exclude' => 0,
  ),
  ),
  ),
  );
  $content['extra']  = array (
    'title' => '-5',
    'body_field' => '-1',
    'revision_information' => '3',
    'author' => '2',
    'options' => '4',
    'menu' => '-2',
    'path' => '6',
    'detailed_question' => '1',
    'page_title' => '-3',
    'xmlsitemap' => '5',
    'nodewords' => '0',
  );

  return $content;
}