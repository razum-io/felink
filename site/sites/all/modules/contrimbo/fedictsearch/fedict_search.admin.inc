<?php
/**
 * @file
 *
 */
function fedict_search_admin_settings() {

  $form = array();

  $form['fedict_search_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Fedict search key'),
    '#default_value' => variable_get('fedict_search_key', ''),
    '#description' => t('Website key in the fedict service, typically the name of the website'),
    '#required' => TRUE,
  );

  $form['fedict_search_wsdl_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Fedict search wsdl url'),
    '#default_value' => variable_get('fedict_search_wsdl_url', 'http://78.47.40.222/searchservice-ws/services/SearchService3?wsdl'),
    '#description' => t('URL for Fedict service definition'),
    '#required' => TRUE,
  );

  $form['fedict_search_location_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Fedict search location url'),
    '#default_value' => variable_get('fedict_search_location_url', 'http://78.47.40.222/searchservice-ws/services/SearchService3'),
    '#description' => t('Location URL for service definition'),
    '#required' => TRUE,
  );

  $form['fedict_search_results_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Results per page'),
    '#default_value' => variable_get('fedict_search_results_per_page', 10),
    '#description' => t('Number of search results per page'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}