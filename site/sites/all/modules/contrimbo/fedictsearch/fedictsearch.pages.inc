<?php

/**
 * @file
 * User page callbacks for the fedict search module.
 */
function fedict_search_view($type = '') {

  // the form has been submitted
  if(!isset($_POST['form_id']))
  {
    $keys = fedict_search_get_keys();

    $results = '';
    if (trim($keys)) {

      // Collect the search results:
      $results = fedict_search_data($keys);

      if ($results) {
        $results = theme('box', t('Search results'), $results);
      }
      else {
        $results = theme('box', t('Your search yielded no results'), fedict_search_help('fedict_search#noresults', drupal_help_arg()));
      }
    }
  }

  return drupal_get_form('fedict_search_form', NULL, empty($keys) ? '' : $keys, $type).$results;
}

/**
 * Process a search form submission.
 */
function fedict_search_form_submit($form, &$form_state) {

  $keys = $form_state['values']['keys'];
  if ($keys == '') {
    form_set_error('keys', t('Please enter some keywords.'));
    // Fall through to the drupal_goto() call.
  }

  // prevent the form to be redirected to fedictsearch which is the default behavior
  //	$form_state['redirect'] = false;

  $form_state['redirect'] = 'fedictsearch/'.$keys;
}

/**
 * Process variables for search-results.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $results
 * - $type
 *
 * @see search-results.tpl.php
 */
function template_preprocess_fedict_search_results(&$variables) {

  $variables['search_results'] = '';

  if($variables['results'] && count($variables['results']) > 0)
  {
    // if there is only one result, we do not need to loop on $variables['results']
    if(count($variables['results']) > 1)
    {
      foreach ($variables['results'] as $result)
      {
        $result = (array) $result;
        $variables['search_results'] .= theme('fedict_search_result', $result, $variables['type']);
      }
    }
    else
    {
      $result = (array) $variables['results'];
      $variables['search_results'] .= theme('fedict_search_result', $result, $variables['type']);
    }
  }

  $variables['pager'] = theme('pager', NULL, 10, 0);
}

/**
 * Process variables for fedict_search-result.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $result
 *
 * @see fedict_search-result.tpl.php
 */
function template_preprocess_fedict_search_result(&$variables) {

  $result = $variables['result'];
  $variables['url'] = $result['RealUrl'];
  $variables['title'] = $result['Title'];
  $variables['snippet'] = $result['AbstractText'];
}