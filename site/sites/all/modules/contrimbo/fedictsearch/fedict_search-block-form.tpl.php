<?php
/**
 * @file search-block-form.tpl.php
 * Default theme implementation for displaying a search form within a block region.
 *
 * Available variables:
 * - $fedict_search_form: The complete search form ready for print.
 * - $fedict_search: Array of keyed search elements. Can be used to print each form
 *   element separately.
 *
 * Default keys within $fedict_search:
 * - $fedict_search['fedict_search_form']: Text input area wrapped in a div.
 * - $fedict_search['submit']: Form submit button.
 * - $fedict_search['hidden']: Hidden form elements. Used to validate forms when submitted.
 *
 *
 * @see template_preprocess_fedict_search_block_form()
 */
?>

<div class="container-inline">
	<?php print $fedict_search_form; ?>
</div>
