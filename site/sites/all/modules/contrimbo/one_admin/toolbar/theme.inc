<?php

/**
 * Preprocessor for theme('one_admin_toolbar').
 */
function admin_menu_preprocess_one_admin_toolbar(&$vars) {
  $vars['collapsed'] = TRUE;

  $node_add = FALSE;
  if (arg(0) == 'node' && (arg(1) == 'add' || arg(2) == 'edit')) {
    $node_add = TRUE;
  }

  foreach ($vars['tree'] as $depth => $menus) {
    foreach ($menus as $href => $links) {
      $class = ($depth > 0) ? 'collapsed' : '';

      // Special case for node/add
      if ($node_add && $href == 'admin/content') {
        $class = '';
      }

      if ($depth > 0 && one_admin_in_active_trail($href)) {
        $class = '';
        $vars['collapsed'] = FALSE;
      }
      $id = str_replace('/', '-', $href);

      // If we aren't on the top level menu, provide a way to get to the top level page.
      /*
      if ($depth > 0 && !empty($links)) {
        $links['view-all'] = array(
          'title' => t('View all'),
          'href' => $href,
        );
      }
      */
      $vars["tree_{$depth}"][$id] = theme('links', $links, array('class' => "links clear-block $class", 'id' => "one-admin-toolbar-{$id}"));
    }
  }

  // Active.
  if ($node_add) {
    $string = $vars['tree_0']['admin'];
    $string = str_replace('admin-link-admin-content', 'admin-link-admin-content" class="active"', $string);
    $vars['tree_0']['admin'] = $string;
  }
}

/**
 * Theme function for contextual popups.
 */
function admin_menu_preprocess_one_admin_links(&$vars) {
  if (!empty($vars['links']) && is_array($vars['links'])) {
    $links = '';
    foreach ($vars['links'] as $link) {
      $links .= l($link['title'], $link['href'], $link);
    }
    $vars['links'] = $links;
  }
}
