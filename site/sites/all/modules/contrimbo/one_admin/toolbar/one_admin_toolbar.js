// $Id: admin_toolbar.js,v 1.1.2.2 2009/06/08 00:52:46 yhahn Exp $

Drupal.one_admin = Drupal.one_admin || {};
Drupal.one_admin.toolbar = Drupal.one_admin.toolbar || {};

Drupal.one_admin.toolbar.setActive = function(toolbar_id) {

  // Show the right toolbar
  $('#one-admin-toolbar .depth-1 ul.links').addClass('collapsed');
  $(toolbar_id).removeClass('collapsed');
  $('div#one-admin-toolbar, div#one-admin-toolbar .depth-1').removeClass('collapsed');

  // Switch link active class to corresponding menu item
  var link_id = toolbar_id.replace('one-admin-toolbar', 'admin-link');
  $('#one-admin-toolbar .depth-0 ul.links a').removeClass('active');
  $(link_id).addClass('active');
}

Drupal.behaviors.one_admin_toolbar = function(context) {

  // Primary menus
  $('#one-admin-toolbar .depth-0 ul.links a:not(.processed)').each(function() {
    var target = $(this).attr('id');
    if (target) {
      target = '#'+ target.replace('admin-link', 'one-admin-toolbar');
      if ($(target, '#one-admin-toolbar').size() > 0) {
        // If this link is active show this toolbar on setup
        if ($(this).parent().is('.active-trail')) {
          Drupal.one_admin.toolbar.setActive(target);
        }
        // Add click handler
        $(this).click(function() {
          Drupal.one_admin.toolbar.setActive(target);
          return false;
        });
      }
    }
    $(this).addClass('processed');
  });

  $('#one-admin-toolbar .depth-1 span.close:not(.processed)').each(function() {
    $(this).click(function() {
      $('#one-admin-toolbar .depth-1').addClass('collapsed');
      return false;
    });
    $(this).addClass('processed');
  });

  // Secondary menus
  $('#one-admin-toolbar .depth-1 ul.links:not(.processed)').each(function() {
    $(this).addClass('processed');
  });
}
