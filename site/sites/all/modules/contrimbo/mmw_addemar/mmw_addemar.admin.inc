<?php
/**
 * @file
 * Admin form.
 */

/**
 * Module admin settings form.
 */
function mmw_addemar_admin_settings() {
  $form = array();

  $form['mww_addemar_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Addemar token'),
    '#default_value' => variable_get('mww_addemar_token', ''),
    '#description' => t('Fedict Addemar application newsletter token'),
    '#required' => TRUE,
  );
  $form['mww_addemar_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Addemar url'),
    '#default_value' => variable_get('mww_addemar_url', MMW_ADDEMAR_URL),
    '#description' => t('Fedict Addemar application url'),
    '#required' => TRUE,
  );

  $form['mww_addemar_profile'] = array(
    '#type' => 'textfield',
    '#title' => t('Addemar profile'),
    '#default_value' => variable_get('mww_addemar_profile', 'FormWS'),
    '#description' => t('Fedict Addemar application newsletter profile'),
    '#required' => FALSE,
  );

  $form['mww_addemar_method'] = array(
    '#type' => 'textfield',
    '#title' => t('Addemar method'),
    '#default_value' => variable_get('mww_addemar_method', 'formOptin'),
    '#description' => t('Fedict Addemar application newsletter method'),
    '#required' => FALSE,
  );

  $form['mww_addemar_log_msg'] = array(
    '#type' => 'textfield',
    '#title' => t('log message'),
    '#default_value' => variable_get('mww_addemar_log_msg', 'Contact Form Website'),
    '#description' => t('Fedict Addemar application newsletter log message'),
    '#required' => FALSE,
  );

  $form['mww_addemar_message_id'] = array(
    '#type' => 'textfield',
    '#title' => t('message id'),
    '#default_value' => variable_get('mww_addemar_message_id', ''),
    '#description' => t('Fedict Addemar application newsletter message id'),
    '#required' => FALSE,
  );

  $form['mww_addemar_extra_fields'] = array(
    '#type' => 'textarea',
    '#title' => t('Addemar extra fields'),
    '#default_value' => variable_get('mww_addemar_extra_fields', ''),
    '#description' => t('Extra fields ( eg firstname, lastname, ... ) for subscription. Comma separated, currently only textfields'),
    '#required' => FALSE,
  );

  $form['mww_addemar_center'] = array(
    '#type' => 'textarea',
    '#title' => t('Valid_center'),
    '#default_value' => variable_get('mww_addemar_center', ''),
    '#description' => t('Add key | value pairs for newsletters with categories, where key contains the category name and value contains the Drupal node url. Each value requires a newline, e.g.: <br/> volontaire | node/5<br/>donation | node/74<br/>...'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
