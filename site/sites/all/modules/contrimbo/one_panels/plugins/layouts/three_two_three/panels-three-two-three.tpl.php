<?php
/**
 * @file
 * Template for the 3-2-3 layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['middle']: Content in the middle column.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="panel clear-block " <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

<?php if($content['top_left'] || $content['top_middle'] ||$content['top_right']) {?>
<div class="panel-row panel-row-first clear-block">
    <div class="panel-col panel-col-first">
      <?php if ($content['top_left']) { ?>
        <div class="panel-panel"><?php print $content['top_left']; ?></div>
      <?php } ?>
    </div>

    <div class="panel-col">
      <?php if ($content['top_middle']) { ?>
        <div class="panel-panel"><?php print $content['top_middle']; ?></div>
      <?php } ?>
    </div>

    <div class="panel-col panel-col-last">
      <?php if ($content['top_right']) { ?>
        <div class="panel-panel"><?php print $content['top_right']; ?></div>
      <?php } ?>
    </div>
</div>
<?php } ?>

<?php if($content['middle_left'] || $content['middle_right']) {?>
<div class="panel-row clear-block">
  <div class="panel-col panel-col-first">
    <?php if ($content['middle_left']) { ?>
      <div class="panel-panel"><?php print $content['middle_left']; ?></div>
    <?php } ?>
  </div>

  <div class="panel-col panel-col-last">
    <?php if ($content['middle_right']) { ?>
      <div class="panel-panel"><?php print $content['middle_right']; ?></div>
    <?php } ?>
  </div>
</div>
<?php } ?>

<?php if($content['bottom_left'] || $content['bottom_middle'] ||$content['bottom_right']) {?>
<div class="panel-row panel-row-last clear-block">
  <div class="panel-col panel-col-first">
    <?php if ($content['bottom_left']) { ?>
      <div class="panel-panel"><?php print $content['bottom_left']; ?></div>
    <?php } ?>
  </div>

  <div class="panel-col">
    <?php if ($content['bottom_middle']) { ?>
      <div class="panel-panel"><?php print $content['bottom_middle']; ?></div>
    <?php } ?>
  </div>

  <div class="panel-col panel-col-last">
    <?php if ($content['bottom_right']) { ?>
      <div class="panel-panel"><?php print $content['bottom_right']; ?></div>
    <?php } ?>
  </div>
</div>
<?php } ?>
</div>
