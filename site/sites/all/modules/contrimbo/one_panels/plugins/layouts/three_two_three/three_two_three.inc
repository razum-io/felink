<?php

// Plugin definition
$plugin = array(
  'title' => t('Three two three'),
  'icon' => 'three_two_three.png',
  'theme' => 'panels_three_two_three',
  'theme arguments' => array('id', 'content'),
  'css' => 'three_two_three.css',
  'panels' => array(
    'top_left' => t('Top left side'),
    'top_middle' => t('Top middle column'),
    'top_right' => t('Top right side'),
    'middle_left' => t('Middle left side'),
    'middle_right' => t('Middle right side'),
    'bottom_left' => t('Bottom left side'),
    'bottom_middle' => t('Bottom middle column'),
    'bottom_right' => t('Bottom right side')
  ),
);

