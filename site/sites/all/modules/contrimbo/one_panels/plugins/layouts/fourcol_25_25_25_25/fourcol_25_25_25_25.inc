<?php
// Plugin definition
$plugin = array(
  'title' => t('Four column equal width'),
  'icon' => 'fourcol_25_25_25_25.png',
  'theme' => 'panels_fourcol_25_25_25_25',
  'css' => 'fourcol_25_25_25_25.css',
  'panels' => array(
    'first' => t('First'),
    'second' => t('Second'),
    'third' => t('Third'),
    'fourth' => t('Fourth')
  ),
);
