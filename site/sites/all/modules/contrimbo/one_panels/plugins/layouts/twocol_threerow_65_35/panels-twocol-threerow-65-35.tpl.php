<?php
/**
 * @file
 * Template for the 3-2-3 layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['first_row']: Content in the first row.
 *   - $content['second_row_first']: Content in the first column of the second row.
 *   - $content['second_row_second']: Content in the second column of the second row.
 *   - $content['second_row_third']: Content in the second column of the second row, beneath the previous region.
 *   - $content['third_row_first']: Content in the first column of the third row.
 *   - $content['third_row_second']: Content in the second column of the third row.
 */
?>
<div class="panel clear-block panel-2col-3row-65-35 " <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if($content['first_row']) {?>
  <div class="panel-row panel-row-one clear-block">
      <div class="panel-panel"><?php print $content['first_row']; ?></div>
  </div>
  <?php } ?>

  <?php if($content['second_row_first'] || $content['second_row_last']) {?>
  <div class="panel-row panel-row-two clear-block">
    <div class="panel-col panel-col-first">
      <div class="panel-panel"><?php print $content['second_row_first']; ?></div>
    </div>

    <div class="panel-col panel-col-last">
      <div class="panel-panel"><?php print $content['second_row_last']; ?></div>
    </div>
  </div>
  <?php } ?>

  <?php if($content['third_row_first'] || $content['third_row_last']) {?>
  <div class="panel-row panel-row-last clear-block">
    <div class="panel-col panel-col-first">
      <div class="panel-panel"><?php print $content['third_row_first']; ?></div>
    </div>

    <div class="panel-col panel-col-last">
      <div class="panel-panel"><?php print $content['third_row_last']; ?></div>
    </div>
  </div>
  <?php } ?>
</div>
