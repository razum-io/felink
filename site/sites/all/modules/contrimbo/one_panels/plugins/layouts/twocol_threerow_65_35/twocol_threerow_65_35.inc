<?php
// Plugin definition
$plugin = array(
  'title' => t('Two columns three rows 65/35'),
  'icon' => 'twocol_threerow_65_35.png',
  'theme' => 'panels_twocol_threerow_65_35',
  'theme arguments' => array('id', 'content'),
  'css' => 'twocol_threerow_65_35.css',
  'panels' => array(
    'first_row' => t('First row'),
    'second_row_first' => t('Second row first pane'),
    'second_row_last' => t('Second row last pane'),
    'third_row_first' => t('Third row first pane'),
    'third_row_last' => t('Third row last pane'),
  ),
);
