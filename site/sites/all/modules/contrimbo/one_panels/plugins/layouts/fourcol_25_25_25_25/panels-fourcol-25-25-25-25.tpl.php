<?php
/**
 * @file
 * Template for a 4 column panel layout.
 *
 * This template provides a four column panel display layout, with
 * each column equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['first']: Content in the first column.
 *   - $content['second']: Content in the second column.
 *   - $content['third']: Content in the third column.
 *   - $content['fourth']: Content in the fourth column.
 */
?>

<div class="panel panel-4col-25 clear-block " <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-row panel-row-last clear-block">
    <div class="panel-col panel-col-first">
      <div class="panel-panel"><?php print $content['first']; ?></div>
    </div>

    <div class="panel-col">
      <div class="panel-panel"><?php print $content['second']; ?></div>
    </div>

    <div class="panel-col">
      <div class="panel-panel"><?php print $content['third']; ?></div>
    </div>

    <div class="panel-col panel-col-last">
      <div class="panel-panel"><?php print $content['fourth']; ?></div>
    </div>
  </div>
</div>
