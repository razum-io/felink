<?php
/**
 * @file
 * Definition of the Conimbo panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Conimbo styles'),
  'description' => t('Apply the predefined Conimbo styles to a panel region.'),
  'render region' => 'panels_conimbo_style_render_region',
  'settings form' => 'panels_conimbo_style_settings_form',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_conimbo_style_render_region($display, $region_id, $panes, $settings) {
  $output = '';

  if (isset($settings['style'])) {
    $output .= '<div class="'. $settings['style'] .'">';
  }

  foreach ($panes as $pane_id => $pane_output) {
    if ($pane_output) {
      $output .= $pane_output;
    }
  }

  if (isset($settings['style'])) {
    $output .= '</div>';
  }

  return $output;
}

/**
 * Settings form callback.
 */
function panels_conimbo_style_settings_form($style_settings) {
  // Style settings
  $theme_key = variable_get('theme_default', 'clean_theme');
  $info = db_result(db_query("SELECT info FROM {system} WHERE name = '%s' AND type ='theme'", $theme_key));
  $theme_info = unserialize($info);

  //$options = array('' => 'No style');

  if ($theme_info['block_styles']) {
    foreach ($theme_info['block_styles'] as $key => $value) {
      $options[$value['class']] = $value['label'];
    }
  }

  $form['style']['#type'] = 'select';
  $form['style']['#options'] = $options;

  $default_style = variable_get('block_styles', array());
  $form['style']['#default_value'] = isset($default_style[$form['module']['#value'] .'_'. $form['delta']['#value']]) ? $default_style[$form['module']['#value'] .'_'. $form['delta']['#value']] : '';

  return $form;
}

