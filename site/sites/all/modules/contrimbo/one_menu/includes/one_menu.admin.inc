<?php

/**
 * @file
 * Administrative Menu functions.
 */

/**
 * Menu callback: menu overview page.
 */
function one_menu_menu_overview_page() {

  $ignore_menus = array('primary-links', 'secondary-links');

  $result = db_query("SELECT * FROM {menu_custom} ORDER BY title");
  $content = array();
  while ($menu = db_fetch_array($result)) {

    if (in_array($menu['menu_name'], $ignore_menus)) {
      continue;
    }

    $menu['href'] = 'admin/build/menu-customize/'. $menu['menu_name'];
    $menu['localized_options'] = array();
    $content[] = $menu;
  }
  return theme('admin_block_content', $content);
}

/**
 * Taxonomy settings screen.
 */
function one_menu_taxonomy_settings($form_state) {
  $form = array();
  $menus = array();
  $selected_menus = variable_get('one_menu_taxonomy_menus', array());

  // Get all menu items.
  $result = db_query("SELECT * FROM {menu_custom} ORDER BY title");
  while ($row = db_fetch_object($result)) {
    $menus[$row->menu_name] = $row->title;
  }
  $form['#menus'] = $menus;

  // Select menus.
  $selected_menu_keys = array();
  foreach ($selected_menus as $key => $value) {
    $selected_menu_keys[$key] = $key;
  }
  $form['menus'] = array(
    '#title' => t('Menu \'s'),
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => $menus,
    '#default_value' => $selected_menu_keys,
    '#description' => t('Select primary menu\'s in which terms can add their menu items.'),
  );

  // Select taxonomy, only if $selected_menus is not empty.
  if (!empty($selected_menus)) {

    $taxonomy = array();
    $result = db_query("SELECT vid, name FROM {vocabulary} ORDER by name ASC");
    while ($row = db_fetch_object($result)) {
      $taxonomy[$row->vid] = $row->name;
    }

    foreach ($selected_menus as $key => $menu) {

      $form['fieldset-'. $key] = array(
        '#title' => 'Menu '. $menus[$key],
        '#type' => 'fieldset',
      );

      // Taxonomy.
      $form['fieldset-'. $key][$key] = array(
        '#title' => 'Taxonomy',
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => $taxonomy,
        '#default_value' => isset($selected_menus[$key]['taxonomy']) ? $selected_menus[$key]['taxonomy'] : array(),
      );

      $form['fieldset-'. $key][$key .'_menutree'] = array(
        '#type' => 'value',
        '#value' => 0,
      );


      // Multilingual support.
      if (module_exists('i18ntaxonomy')) {
        $options = array();
        $languages = language_list();
        foreach ($languages as $lang => $language) {
          $options[$lang] = $language->name;
        }

        $form['fieldset-'. $key][$key .'_lang'] = array(
          '#title' => 'Language',
          '#type' => 'select',
          '#options' => $options,
          '#default_value' => isset($selected_menus[$key]['lang']) ? $selected_menus[$key]['lang'] : array(),
        );
      }
    }

  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Taxonomy submit function.
 */
function one_menu_taxonomy_settings_submit($form, &$form_state) {
  $selected_menus = array();

  // Selected menu's.
  foreach ($form_state['values']['menus'] as $key) {
    $selected_menus[$key] = array('key' => $key, 'name' => $form['#menus'][$key]);
  }

  foreach ($selected_menus as $key => $value) {
    if ($form_state['values'][$key]) {

      // Taxonomy.
      foreach ($form_state['values'][$key] as $vid) {
        $selected_menus[$key]['taxonomy'][$vid] = $vid;
      }

      // Menu tree.
      $selected_menus[$key]['menu_tree'] = $form_state['values'][$key .'_menutree'];

      // i18n support.
      if (module_exists('i18ntaxonomy')) {
        $selected_menus[$key]['lang'] = $form_state['values'][$key .'_lang'];
      }
    }
  }

  // Save all.
  variable_set('one_menu_taxonomy_menus', $selected_menus);
}
