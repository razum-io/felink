<?php

/**
 * @file
 * Administrative functions for splash module.
 */

function splash_settings(&$form_state) {
  $form = array();

  // Add jquery.
  drupal_add_js(drupal_get_path('module', 'splash') .'/splash.js');

  // General settings.
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('Type'),
  );
  $form['general']['splash_type'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('splash_type', SPLASH_PAGE_AUTO),
    '#options' => array(
      SPLASH_PAGE_NONE => t('No page. It will redirect to the default frontpage depending on either database settings or a cookie.'),
      SPLASH_PAGE_AUTO => t('Show default page.'),
      SPLASH_PAGE_PAGE => t('Use existing page.'),
    ),
  );

  // Cookie settings.
  $form['cookie'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cookie settings'),
    '#attributes' => array(
      'class' => 'cookie',
    ),
  );
  $period = array('0' => t('Never expire'), 'close' => t('On browser close')) + drupal_map_assoc(array(3600, 10800, 21600, 32400, 43200, 86400, 172800, 259200, 604800, 1209600, 2419200, 4838400, 9676800), 'format_interval');
  $form['cookie']['splash_cookielifetime'] = array(
    '#type' => 'select',
    '#title' => t('Cookie lifetime'),
    '#options' => $period,
    '#default_value' => variable_get('splash_cookielifetime', 0),
  );

  // Auto generated page.
  $form['generate'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default page'),
    '#description' => t('By default, the auto generated page will create links to all the language sections in your site. You can override this easily by changing the title and intro texts per language.'),
    '#attributes' => array(
      'class' => 'default',
    ),
  );
  $form['generate']['splash_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title'),
    '#default_value' => variable_get('splash_title', 'Language choice'),
  );
  $languages = language_list();
  foreach ($languages as $lang => $language) {
    if ($language->enabled) {
      $form['generate']['one_fieldet_lang_'. $lang] = array(
        '#type' => 'fieldset',
        '#title' => t('Settings for @lang', array('@lang' => $language->name)),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $form['generate']['one_fieldet_lang_'. $lang]['splash_language_ignore_'. $lang] = array(
        '#type' => 'select',
        '#title' => t('Language setting'),
        '#default_value' => variable_get('splash_language_ignore_'. $lang, SPLASH_SHOW_LANGUAGE),
        '#options' => array(
          SPLASH_SHOW_LANGUAGE => t('Show language'),
          SPLASH_DONOTSHOW_LANGUAGE => t('Do not show language'),
        ),
      );
      $form['generate']['one_fieldet_lang_'. $lang]['splash_language_link_'. $lang] = array(
        '#type' => 'textfield',
        '#title' => t('Link text'),
        '#default_value' => variable_get('splash_language_link_'. $lang, ''),
        '#description' => t('Optional: if you leave this empty, the name of the language will be used as link. The path will be automatically derived depending on language settings (one ore more languages with prefixes or not).'),
      );
      $form['generate']['one_fieldet_lang_'. $lang]['splash_text_'. $lang] = array(
        '#type' => 'textarea',
        '#title' => t('Guiding text'),
        '#default_value' => variable_get('splash_text_'. $lang, ''),
        '#description' => t('Optional: interesting if you have a choice between more languages or only one language is available.'),
      );
    }
  }

  // Existing page.
  $form['existing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Existing page'),
    '#attributes' => array(
      'class' => 'existing',
    ),
  );
  $form['existing']['splash_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Page path'),
    '#default_value' => variable_get('splash_page', 'node'),
    '#description' => t('You are responsible for setting the cookie yourself if the page also needs to redirect.<br />This can be done by using a link like splash/switch/cookievalue where cookievalue is the path to redirect to.'),
  );

  $form = system_settings_form($form);
  return $form;
}

