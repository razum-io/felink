/* $Id$ */

/**
 * @file
 * Manipulate fieldsets depending on Splash type.
 */

Drupal.behaviors.SpwesjAdmin = function(context) {
  $("input[name='splash_type']").change(function(){
    SplashToggle();
  });
}

$(document).ready(function() {
  SplashToggle();
});

function SplashToggle() {
  var spwesj_type = $("input[@name='splash_type']:checked").val();

  if (spwesj_type == '1') {
    $('.default').hide();
    $('.existing').hide();
  }
  else if (spwesj_type == '2' || spwesj_type == '10') {
    $('.default').show();
    $('.existing').hide();
  }
  else if (spwesj_type == '4' || spwesj_type == '12') {
    $('.default').hide();
    $('.existing').show();
  }
};