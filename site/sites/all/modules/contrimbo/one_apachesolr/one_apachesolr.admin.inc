<?php

/**
 * @file
 * Administrative functions for One apachesolr module.
 */

/**
 * Fields for one apache solr.
 */
function one_apache_solr_fields() {
  $fields = array(
    'node_type' =>  array(
      'description' => t('The content type that will be searched. This cannot be modified after creation.'),
      'required' => TRUE,
    ),
    'lang_code' =>  array(
      'description' => t('The language for which this path is being configured. This cannot be modified after creation.'),
      'required' => TRUE,
    ),
    'use_key' =>  array(
      'description' => t('The function of this path. This cannot be modified after creation.'),
      'required' => TRUE,
    ),
    'callback' =>  array(
      'description' => t('Drupal\'s internal path for this search.'),
      'required' => TRUE,
    ),
    'title' =>  array(
      'description' => t('Title of the menu item for this search.'),
      'required' => FALSE,
    ),
    'facet' =>  array(
      'description' => t('The kind of object that will be searched.'),
      'required' => FALSE,
    ),
    'facet_value' =>  array(
      'description' => t('The value of the object that will be searched.'),
      'required' => FALSE,
    ),
    'build_mode' =>  array(
      'description' => t('The manner of presentation of the result.'),
      'required' => FALSE,
    ),
    'unset_keys' =>  array(
      'description' => '',
      'required' => FALSE,
    ),
    'show_sort' =>  array(
      'description' => '',
      'required' => FALSE,
    ),
    'show_rss_link' => array(
      'description' => t('To show or not to show a link to RSS feed on search result page. Only meaningful if a RSS view exists.'),
      'required' => FALSE,
    ),
  );

  return $fields;
}

/**
 * Menu callback: show overview screen.
 */
function one_apache_solr_overview() {
  $output = l('Add new path', 'admin/settings/apachesolr/one_apachesolr/edit');
  $conimbo_search = variable_get('conimbo_search', array());

  if (!empty($conimbo_search)) {
    $rows = array();
    $header = array(
      'Title', 'Content type', 'Language', 'Functionality', 'Callback', 'Operations',
    );
 		$languages = language_list('enabled');
 		$lang_names = array();
  	foreach ($languages[1] as $language_link) {
  		$lang_names[$language_link->language] = $language_link->name;
  	}
  	$node_types = node_get_types('names');
     
    foreach ($conimbo_search as $node_type => $lang) {
    	$node_name = $node_types[$node_type];
      foreach ($lang as $lang_code => $use) {
      	if (isset($lang_names[$lang_code])) {
	        foreach ($use as $use_key => $value) {
	          $row = array();
	          $row[] = $value['title'];
	          $row[] = $node_name;
	          $row[] = $lang_names[$lang_code];
	          $row[] = ucfirst($use_key);
	          $row[] = $value['callback'];
	          $operations = l('Edit', 'admin/settings/apachesolr/one_apachesolr/edit/'. $node_type .'/'. $lang_code .'/'. $use_key);
	          $operations .= ' - '. l('Delete', 'admin/settings/apachesolr/one_apachesolr/delete/'. $node_type .'/'. $lang_code .'/'. $use_key);
	          $row[] = $operations;
	          $rows[] = $row;
	       	}
      	}
    	}
   }
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= '<p>'. t('No paths configured for conimbo apachesolr settings.') .'</p>';
  }

  return $output;
}

/**
 * Menu callback: add or edit a path.
 */
function one_apache_solr_edit(&$form_state, $node_type = '', $lang_code = '', $use_key = '') {

  $fields = one_apache_solr_fields();

  // Default values.
  $values = array(
    'title' => '',
    'facet' => '',
    'facet_value' => '',
    'build_mode' => '',
    'show_sort' => FALSE,
    'unset_keys' => '',
    'show_rss_link' => FALSE,
  );

  // Create the form.
  $form = array();
  
  // Determin edit mode.
  $conimbo_search = variable_get('conimbo_search', array());
  $node_types = node_get_types('names');
  if (!empty($node_type) && !empty($lang_code) && !empty($use_key)) {
    if (isset($conimbo_search[$node_type][$lang_code][$use_key])) {
      $values = $conimbo_search[$node_type][$lang_code][$use_key];
    } 
    $node_name = $node_types[$node_type]; 
  	unset($fields['node_type'], $fields['use_key'], $fields['lang_code']);
  	$languages = language_list('enabled');
  	$language = NULL;
  	foreach ($languages[1] as $language_link) {
  		if ($language_link->language == $lang_code) {
  			$language = $language_link->name;
  			break;
  		}
  	}
  	$form['intro'] = array(
    '#type' => 'markup',
    '#value' => '<div>'.
    						'<p>' . t('Here you can edit the apache solr search menu path settings for the !type content type.', array('!type' => $node_name)) . '</p> ' . 
  							'<p>' . t('The function of this menu path is \'@use_key\'.', array('@use_key' => $use_key)) . '</p> ' .
  							'<p>' . t('The language of this search is \'@lang\'.', array('@lang' => $language)) . '</p> ' .
  							'<p>' . l(t('Add another path'), 'admin/settings/apachesolr/one_apachesolr/edit') . 
  							t(' if you need a menu path for another combination of content type, function and language.') . '</p>' .
                '</div>',
  	);
  } else {
  	$options = array();
  	$excluded_types = variable_get('apachesolr_search_excluded_types', array());
	  foreach($node_types as $type_key => $type_value) {
	  	if (!$excluded_types[$type_key]) {
	  		$options[$type_key] = $type_key;
	  	}
	  }
		$field = $fields['node_type'];

	  $form['node_type'] = array(
	    '#type' => 'select',
	    '#title' => t('Content type'),
	    '#options' => $options,
      '#default_value' => 0,
      '#description' => isset($field['description']) ? $field['description'] : '',
      '#required' => $field['required'],
	  );
	  unset($fields['node_type']);
	  
	  $field = $fields['lang_code'];
	  $options = array();
	  $languages = language_list('enabled');
    foreach ($languages[1] as $language_link) {
    	$options[$language_link->language] = $language_link->name; 
    }
    
    $form['lang_code'] = array(
	    '#type' => 'select',
	    '#title' => t('Language'),
	    '#options' => $options,
      '#default_value' => 0,
      '#description' => isset($field['description']) ? $field['description'] : '',
      '#required' => $field['required'],
	  );
    unset($fields['lang_code']);
  }
  
  foreach ($fields as $field => $field_properties) {
     $form[$field] = array(
      '#type' => 'textfield',
      '#title' => str_replace('_', ' ', ucfirst($field)),
      '#default_value' => isset(${$field}) ? ${$field} : $values[$field],
      '#description' => isset($field_properties['description']) ? $field_properties['description'] : '',
      '#required' => $field_properties['required'],
    );

    // Special case for show_short and show_rss_link
    if ($field == 'show_sort' ||
        $field == 'show_rss_link') {
      $form[$field]['#type'] = 'checkbox';
    }
    if ($field == 'show_rss_link') {
      $form[$field]['#title'] .=  t(' (Only meaningful if a corresponding RSS view exists.)');
    }

  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 *  Submit callback: save a path.
 */
function one_apache_solr_edit_submit($form, &$form_state) {
	$values = $form_state['values'];
  $conimbo_search = variable_get('conimbo_search', array());
  
  $fields = one_apache_solr_fields();
	
  $node_type 	= arg(5);
  $lang_code 	= arg(6);
  $use_key 		= arg(7);
		
	// The combination content type (node_type)/language code (lang_code)/use of path (use_key) is unique.
	// So edit the conimbo_search variable based on these keys.  
	foreach ($fields as $field => $field_properties) {
		// Content type, language and use key are keys in the conimbo_search variable, not values.
		if ($field != 'node_type' && $field != 'lang_code' && $field != 'use_key') {
			// Edit existing menu path.
			if (!empty($node_type) && !empty($lang_code) && !empty($use_key)) {
				$conimbo_search[$node_type][$lang_code][$use_key][$field] = $values[$field];
			} else {
	    	$conimbo_search[$values['node_type']][$values['lang_code']][$values['use_key']][$field] = $values[$field];   
	  	}
		}
	}
	
  // Save and rebuild the menu.[$field]
  variable_set('conimbo_search', $conimbo_search);
  menu_rebuild();

  $form_state['redirect'] = 'admin/settings/apachesolr/one_apachesolr';
  drupal_set_message('Path has been saved');
}

/**
 * Menu callback: return confirmation form to delete a path.
 */
function one_apache_solr_delete(&$form_state, $node_type = '', $lang_code = '', $use_key = '') {

  $redirect = TRUE;
  $conimbo_search = variable_get('conimbo_search', array());
  if (!empty($node_type) && !empty($lang_code) && !empty($use_key)) {
    if (isset($conimbo_search[$node_type][$lang_code][$use_key])) {
      $redirect = FALSE;
    }
  }

  // Show confirm form.
  if (!$redirect) {

    $form = array();
    $form['node_type'] = array(
      '#type' => 'value',
      '#value' => $node_type,
    );
    $form['lang_code'] = array(
      '#type' => 'value',
      '#value' => $lang_code,
    );
    $form['use_key'] = array(
      '#type' => 'value',
      '#value' => $use_key,
    );

    return confirm_form($form,
      t('Are you sure you want to delete %path?', array('%path' => "$node_type - $lang_code - $use_key")),
      'admin/settings/apachesolr/one_apachesolr',
      t('This action cannot be undone.'),
      t('Delete'),
      t('Cancel')
    );
  }

  // Redirect on illegal path.
  drupal_set_message('Illegal path');
  drupal_goto('admin/settings/apachesolr/one_apachesolr');
}

/**
 * Delete
 */
function one_apache_solr_delete_submit($form, &$form_state) {

  // Unset the path.
  $values = $form_state['values'];
  $conimbo_search = variable_get('conimbo_search', array());
  unset($conimbo_search[$values['node_type']][$values['lang_code']][$values['use_key']]);

  // Remove lower keys.
  if (empty($conimbo_search[$values['node_type']][$values['lang_code']])) {
    unset($conimbo_search[$values['node_type']][$values['lang_code']]);
  }
  if (empty($conimbo_search[$values['node_type']])) {
    unset($conimbo_search[$values['node_type']]);
  }

  // Save and rebuild the menu.
  variable_set('conimbo_search', $conimbo_search);
  menu_rebuild();

  $form_state['redirect'] = 'admin/settings/apachesolr/one_apachesolr';
  drupal_set_message('Path has been removed');
}
