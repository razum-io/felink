<?php
/**
 * @file
 * Theme functions for apache solr results.
 */

/**
 * Theme the event results overview.
 */
function theme_event_search_results($results, $build_mode) {

  $output = '';
  $ongoing_events = '';
  $total_ongoing = 0;
  $previous_month = '';

  foreach ($results as $key => $result) {

    $node = _solr_document_to_node($result);
    $node->build_mode = $build_mode;
    $node_view = node_view($node);

    // add to ongoing events also, when event is busy.
    if ($node->field_event_date[0]['value2'] > $_SERVER['REQUEST_TIME'] && $node->field_event_date[0]['value'] < $_SERVER['REQUEST_TIME']) {
      $ongoing_events .= $node_view;
      $total_ongoing++;
    }
    else {
      $month = date('F', $node->field_event_date[0]['value']);
      if ($month !== $previous_month) {
        $previous_month = $month;
        $output .= '<h2 class="category-title">'. t($month) .' '. date('Y', $node->field_event_date[0]['value']) .'</h2>';
      }
      $output .= $node_view;
    }
  }

  if ($total_ongoing > 0) {

    $ongoing_events = '<h2 class="category-title">'. t('Currently busy') .'</h2>' . $ongoing_events;
    $ongoing_events = '<div id="ongoing-events" class="clear-block">'. $ongoing_events;
    $ongoing_events .= '</div>';

  }
  return '<div class="events">'. $ongoing_events . $output .'</div>';
}

/**
 * Theme the event results overview.
 */
function theme_event_archive_search_results($results, $build_mode) {
  $previous_month = '';
  $previous_year = '';

  foreach ($results as $key => $result) {
    $node = _solr_document_to_node($result);
    $node->build_mode = $build_mode;
    $node_view = node_view($node);
    $year = date('Y', $node->field_event_date[0]['value']);
    $month = date('F', $node->field_event_date[0]['value']);

    if ($year !== $previous_year) {
      $previous_year = $year;
      $output .= '<h2>'. t($year) .'</h2>';
    }

    if ($month !== $previous_month) {
      $previous_month = $month;
      $output .= '<h3>'. t($month) . '</h3>';
    }
    $output .= $node_view;
  }
  return '<div class="events">'. $output .'</div>';;
}
