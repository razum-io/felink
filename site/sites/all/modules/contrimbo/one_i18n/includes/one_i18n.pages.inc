<?php

/**
 * @file
 * Pages for i18n.
 */

/**
 * Replacement for default node add page when i18n is enabled.
 */
function one_i18n_node_add_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);
  return theme('one_i18n_node_add_list', $content);
}

/**
 * Display the list of available node types for node creation. With language context.
 */
function theme_one_i18n_node_add_list($content) {
  $output = '';

  $languages = language_list();

  if ($content) {
    $output = '<ul class="admin-list">';
    foreach ($content as $item) {

      $explode = explode('/', $item['href']);
      $content_type = str_replace('-', '_', $explode[2]);

      $language_type = variable_get('i18n_node_'. $content_type, LANGUAGE_SUPPORT_NORMAL);

      // Do not consider admin language as an enabled languages as it (Drupal English) is hidden on the language overview page
      if ($language_type == LANGUAGE_SUPPORT_NORMAL && variable_get('language_count', 1) > 1) {

        $output .= '<li class="path-node-add path-node-add-' . $content_type . '">';
        $output .= '<span class="icon"></span>';
        $output .= $item['title'];
        $output .= '<div class="description">'. filter_xss_admin($item['description']) .'</div>';
        $langs = array();
        foreach ($languages as $key => $lang) {

          // Hide admin language if necessary.
          if (variable_get('admin_language_hide', 0) && variable_get('admin_language_default', 'en') == $key){
            continue;
          }

          if ($lang->enabled == TRUE) {
            $langs[] = l($lang->name, $item['link_path'], array('language' => $lang, 'query' => array('language' => $lang->language)));
          }
        }
        if (!empty($langs)) {
          $output .= '<div class="description">'. implode(' ', $langs) .'</div>';
        }
        $output .= '</li>';
      }
      else {
        $output .= '<li class="path-node-add path-node-add-' . $content_type . '">';
        $output .= '<span class="icon"></span>';
        $output .= l($item['title'], $item['href'], $item['localized_options']);
        $output .= '<div class="description">'. filter_xss_admin($item['description']) .'</div>';
        $output .= '</li>';
      }
    }
    $output .= '</ul>';
  }
  return $output;
}
