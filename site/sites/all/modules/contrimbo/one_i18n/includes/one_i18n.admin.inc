<?php

/**
 * @file
 * i18n.
 */


/**
 * Alter i18n admin settings form.
 */
function one_i18n_admin_settings_form_alter(&$form) {
  // Do not allow another content selection mode, because Conimbo uses Drupal English as admin language.
  // User does not want to see content in Drupal English but in enabled 'front-end' languages.
  $form['selection']['i18n_selection_mode']['#disabled'] = TRUE;
  $form['selection']['i18n_selection_mode']['#description'] = t('Conimbo shows content in current page\'s language by default, independant of admin language!');
}


