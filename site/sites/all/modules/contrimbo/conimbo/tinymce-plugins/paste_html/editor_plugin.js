(function() {
  
  var each = tinymce.each;
  
  // Load plugin specific language pack
  tinymce.PluginManager.requireLangPack('paste_html');

  tinymce.create('tinymce.plugins.paste_html', {
    
    /**
    * Returns information about the plugin as a name/value array.
    *
    * @return {Object} Name/value array containing information about the plugin.
    */
   getInfo : function() {
     return {
       longname : 'Paste Html plugin',
       author : 'Zuuperman',
       authorurl : 'http://www.one-agency.be',
       infourl : 'http://www.one-agency.be',
       version : "1.0"
     };
   },    
    
    /**
     * Check if the selected element belongs to this plugin.
     */
     isPasteHtmlElement : function(n) {
       return /^(mceIframe)$/.test(n.className);
    },
    
    /**
     * Initializes the plugin, this will be executed after the plugin has been created.
     * This call is done before the editor instance has finished it's initialization so use the onInit event
     * of the editor instance to intercept that event.
     *
     * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
     * @param {string} url Absolute URL to where the plugin is located.
     */
    init : function(ed, url) {
      
      this.editor = ed;
      this.url = url;       
      var parent = this;
       
      // Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceExample');
      ed.addCommand('paste_html', function() { parent.pasteHtmlPopup(ed, url) });

      // Register the paste_html button
      ed.addButton('paste_html', {
        title : ed.getLang('pastehtml.text_title', 'Insert / edit HTML'),
        cmd : 'paste_html',
        image : url + '/img/paste_html.gif'
      });
      
      // Add a node change handler, selects the button in the UI when an image is selected, and it has correct class
      ed.onNodeChange.add(function(ed, cm, n) {
        cm.setActive('paste_html', n.nodeName == 'IMG' && parent.isPasteHtmlElement(n));
      });
      
      // Attach: Show formatted versions of html
      ed.onSetContent.add(parent.prepareFormattedContent, parent);
      
      // Detach: Show original html
      ed.onPreProcess.add(parent.prepareOriginalContent, parent);      
      
    },

    /**
     * Creates control instances based in the incomming name. This method is normally not
     * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
     * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
     * method can be used to create those.
     *
     * @param {String} n Name of the control to create.
     * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
     * @return {tinymce.ui.Control} New control instance or null if no control was created.
     */
    createControl : function(n, cm) {
      return null;
    },

    /**
     * Command called when clicking the paste_html button, open a popup for pasting html.
     */
    pasteHtmlPopup : function(ed, url) {
      
      var data = ed.selection.getContent();
      
      
      ed.windowManager.open({
        file : url + '/popup.html',
        width : 500,
        height : 250,
        inline : 1
      }, {
        plugin_url : url // Plugin absolute URL
      });
      
    },
    
    /**
     * Prepare the content for tinyMCE: change iframes into image.
     * @param ed Current editor
     * @param data Tiny MCE data object
     */
     prepareFormattedContent : function(ed, data) {

       ed.dom.loadCSS(this.url + "/css/paste_html.css");
       
       var parent = this;
       
       each(ed.dom.select('iframe'), function(domNode) {
         ed.dom.replace(parent._createImg('mceIframe', domNode), domNode);
       });
      
    },
    
    /**
     * Create an image from a given dom object.
     * @param imageClass CSS Class to set
     * @param domNode Node to change
     */
    _createImg : function(imageClass, domNode) {
      
      var dom = this.editor.dom, img, args, params = {};
      
      // all allowed iframe tags
      args = ['frameborder', 'scrolling', 'marginheight', 'marginwidth', 'src'];
      
      // Create image
      img = dom.create('img', {
        src : this.url + '/img/spacer.gif',
        width : dom.getAttrib(domNode, 'width') || 100,
        height : dom.getAttrib(domNode, 'height') || 100,
        style : dom.getAttrib(domNode, 'style'),
        'class' : imageClass
      });
      
      // add all params from iframe to object
      each(args, function(arg) {
        var argValue = dom.getAttrib(domNode, arg);
        if (argValue) {
          params[arg] = argValue + ""; 
        }
      });
      
      img.title = tinymce.util.JSON.serialize(params);
      
      return img;
      
    },
    
    /**
    * Prepare the content for tinyMCE: change iframes into image.
    * @param ed Current editor
    * @param o Object containing things like the current node, set or get content ... 
    */
    prepareOriginalContent : function(ed, o) {
      
      var parent = this;
      
      if (o.get) {
        each(ed.dom.select('IMG', o.node), function(domNode) {
          if (parent.isPasteHtmlElement(domNode)) {
            ed.dom.replace(parent._createIframe(domNode), domNode);
          }
        }); 
      }
     
   },    
   
   /**
    * Create an iframe from a given dom object.
    * @param domNode Node to change
    */
   _createIframe : function(domNode) {
     
     var dom = this.editor.dom, iframe, args, params = {};
     
     // Create iframe
     iframe = dom.create('iframe', {
       width : dom.getAttrib(domNode, 'width') || 100,
       height : dom.getAttrib(domNode, 'height') || 100,
       style : dom.getAttrib(domNode, 'style')
     });
     
     var attributes = tinymce.util.JSON.parse(dom.getAttrib(domNode, 'title'));
     for (var attribute in attributes) {
       iframe.setAttribute(attribute, attributes[attribute]);  
     }
     
     return iframe;
     
   }   
    
  });

  // Register plugin
  tinymce.PluginManager.add('paste_html', tinymce.plugins.paste_html);
})();
