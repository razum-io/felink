tinyMCEPopup.requireLangPack();

var editNode;
var pluginTextarea;

var PasteHtmlDialog = {
  
  /**
   * Init the paste HTML form.
   */  
  init : function() {
    
    this.resize();
    editNode = tinyMCEPopup.editor.selection.getNode();
    if (/^(mceIframe)$/.test(tinyMCEPopup.editor.dom.getAttrib(editNode, 'class'))) {
      this.initIframeEdit();
    }
    
  },

  /**
   * Insert the given html into the editor.
   * Force a cleanup, so html is taken over by the plugins (ex. embed => media plugin)
   */
  insert : function() {
    tinyMCEPopup.editor.execCommand('mceInsertClipboardContent', false, {content : document.getElementById('content').value});
    tinyMCEPopup.execCommand('mceCleanup');
    tinyMCEPopup.close();
  },
  
  /**
   * An iframe is beïng edited, load the original iframe code.
   */
  initIframeEdit : function() {

    var iframeHtml = '<iframe', dom;
    dom = tinyMCEPopup.editor.dom;
    
    iframeHtml += ' width="' + dom.getAttrib(editNode, "width") + '"';
    iframeHtml += ' height="' + dom.getAttrib(editNode, "height") + '"';
    
    var style = dom.getAttrib(editNode, "style");
    if (style) {
      iframeHtml += style;
    }
    
    var attributes = tinymce.util.JSON.parse(dom.getAttrib(editNode, 'title'));
    
    for (var attribute in attributes) {
      iframeHtml += ' ' + attribute + '="' + attributes[attribute] + '"'; 
    }    

    iframeHtml += '></iframe>';
    pluginTextarea.innerHTML = iframeHtml;
    
  },

  /**
   * Resize the textarea to the size from the popup.
   */
  resize : function() {
    
    var vp = tinyMCEPopup.dom.getViewPort(window);

    pluginTextarea = document.getElementById('content');

    pluginTextarea.style.width  = (vp.w - 20) + 'px';
    pluginTextarea.style.height = (vp.h - 90) + 'px';
    
  }
};

tinyMCEPopup.onInit.add(PasteHtmlDialog.init, PasteHtmlDialog);