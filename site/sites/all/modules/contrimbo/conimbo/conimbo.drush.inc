<?php

/**
 * Implements hook_drush_command().
 */
function conimbo_drush_command() {
  $items = array();

  $items['conimbo-install'] = array(
    'description' => dt('General conimbo settings on install'),
    'arguments' => array(),
  );

  $items['conimbo-disable'] = array(
    'description' => dt('General conimbo settings on disable'),
    'arguments' => array(),
  );

  $items['conimbo-enable'] = array(
    'description' => dt('General conimbo settings on enable'),
    'arguments' => array(),
  );
  return $items;
}

/**
 * Install general stuff on installing a new site.
 */
function drush_conimbo_install() {
  drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_DATABASE);
  $site_uri = drush_get_context('DRUSH_URI');

  // Save the site uri.
  variable_set('conimbo_site_uri', $site_uri);
}

/**
 * Implements hook_post_hosting_install_task().
 *
 * Perform Conimbo specific tasks when Aegir's install task finishes.
 */
function conimbo_post_hosting_install_task($task, $data) {
  if ($task->ref->type == 'site') {
    $options = conimbo_get_site_context($task);
    drush_backend_invoke_args('conimbo-install', array(), $options, 'GET', TRUE);

    // Run cron on the newly installed site.
    drush_backend_invoke_args('core-cron', array(), $options, 'GET', TRUE);
  }
}

/**
 * Implementation of hook_post_hosting_disable_task().
 */
function conimbo_post_hosting_disable_task($task, $data) {
  if ($task->ref->type == 'site') {
    $options = conimbo_get_site_context($task);
    // Enable settings on site.
    drush_backend_invoke_args('conimbo-disable', array(), $options, 'GET', TRUE);
  }
}

/**
 * Implementation of hook_post_hosting_enable_task().
 */
function conimbo_post_hosting_enable_task($task, $data) {
  if ($task->ref->type == 'site') {
    $options = conimbo_get_site_context($task);
    // Enable settings on site.
    drush_backend_invoke_args('conimbo-enable', array(), $options, 'GET', TRUE);
  }
}

/**
 * Determines the site context for a given task.
 *
 * @return
 *   An associatieve array which can be passed as the options array to
 *   drush_backend_invoke_* to ensure drush uses to correct context.
 */
function conimbo_get_site_context($task) {
  // Trying to get the root by d(platform_alias)->root fails (returns the root
  // of the hostmaster profile instead of the platform's root), so load the
  // platform via it's node id.
  $platform = node_load($task->ref->platform);
  drush_log(print_r($task, TRUE), 'info');

  //$options['uri'] = $task->context_options['uri'];
  // TODO: Relying on node title for URI works but is fragile.
  $options['uri'] = $task->ref->title;
  $options['root'] = $platform->publish_path;

  return $options;
}
