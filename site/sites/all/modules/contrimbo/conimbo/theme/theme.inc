<?php

/**
 * @file
 * Theming functions.
 */

/**
 * Use label in link.
 */
function theme_conimbo_read_more($field) {
  $display_settings = variable_get('nd_display_settings_'. $field['object']->type, array());
  $label_value = ds_default_value($display_settings, $field['object']->build_mode, 'fields', 'read_more', 'label_value', '');
  if (!empty($label_value)) {
    $text = check_plain(t($label_value));
  }
  else {
    $text = $field['title'];
  }

  return l($text, 'node/'. $field['object']->nid);
}

/**
 * Run through filter url.
 */
function theme_conimbo_formatter_filter_url($element) {
  return _filter_url($element['#item']['safe'], 'plain');
}

/**
 * Linebreak
 */
function theme_conimbo_formatter_nl2br($element) {
  return nl2br($element['#item']['safe']);
}

/**
 * Theme body element through teaser.
 */
function theme_conimbo_teaser($field) {
  $data = $field['object']->content['body']['#value'];
  return _filter_htmlcorrector(trim(truncate_utf8(str_replace('<p>', '', str_replace('</p>', '<br />', strip_tags($data, '<p>'))), 200, TRUE, TRUE)));
}

/**
 * Fill empty cover image with type dependant default images .
 */
function theme_conimbo_formatter_cover_image($element) {
  // If the field_content_type_cover_image is empty,
  // show the default image for the file type of the field_content_type_file.
  $content_type = $element['#node']->type;
  $field_cover_image = 'field_'. $content_type .'_cover_image';
  $field_file = 'field_'. $content_type .'_file';

  if (empty($element['#node']->{$field_cover_image}['0'])) {
    if (empty($element['#node']->{$field_file}['0'])) {
      $file = array('filemime' => 'text-html');
    }
    else {
      $file = $element['#node']->{$field_file}['0'];
    }

    // We switch to the conimbo icon directory (in filefile icons directory).
    global $conf;
    $conf['filefield_icon_theme'] = 'conimbo';
    if ($element['#node']->build_mode != 'full') {
      return l(theme('filefield_icon', $file), 'node/'. $element['#node']->nid, array('html' => TRUE));
    }
    else {
      return theme('filefield_icon', $file);
    }
  }
  else {

    // We cannot call theme_imagecache_formatter_default directly as imagecache does not know our formatter.
    $item = $element['#item'];
    $item['data']['alt'] = isset($item['data']['alt']) ? $item['data']['alt'] : '';
    $item['data']['title'] = isset($item['data']['title']) ? $item['data']['title'] : NULL;

    // If the cover image is present, we switch to imagecache Teaser image preset.
    if ($element['#node']->build_mode == 'teaser' || $element['#node']->build_mode == 'block' || $element['#node']->build_mode == '3') {
      $presetname = 'small';
      return l(theme('imagecache', $presetname, $item['filepath'], $item['data']['alt'], $item['data']['title']), 'node/'. $element['#node']->nid, array('html' => TRUE)) ;
    }
    else {
      $presetname = 'medium';
      return l(theme('imagecache', $presetname, $item['filepath'], $item['data']['alt'], $item['data']['title']), file_create_url($item['filepath']), array('html' => TRUE, 'attributes' => array('rel' => 'lightbox[]['. $item['data']['description'] .']')));
    }
  }
}

/**
 * Render the 'download document' link.
 */
function theme_conimbo_formatter_file_download($field) {
  if (isset($field['#item']['filepath']) && !empty($field['#item']['filepath'])) {
    return l(t('Download document'), file_create_url($field['#item']['filepath']), array('attributes' => array('class' => 'target-blank')));
  }
}

/**
 * Render the 'download preview' link.
 */
function theme_conimbo_formatter_file_preview($field) {
  if (isset($field['#item']['filepath']) && !empty($field['#item']['filepath'])) {
    return l(t('Download preview'), file_create_url($field['#item']['filepath']), array('attributes' => array('class' => 'target-blank')));
  }
}

/**
* Only show the first image with a specific image cache.
*/
function theme_conimbo_formatter_first_image($element) {

  static $images = array();

  $nid = $element['#node']->nid;

  // Stop after the first one.
  if (isset($images[$nid])) {
    return;
  }

  if (empty($element['#item']['fid'])) {
    return '';
  }

  $preset = str_replace('conimbo_formatter_first_image_', '', $element['#theme']);
  $images[$nid] = TRUE;
  $item = $element['#item'];

  $alt = isset($item['data']['alt']) ? $item['data']['alt'] : '';
  $title = isset($item['data']['title']) ? $item['data']['title'] : NULL;

  //return theme('imagecache', 'uc_category', $item['filepath'], $alt, $title);
  return l(theme('imagecache', $preset, $item['filepath'], $alt, $title), 'node/'. $nid, array('html' => TRUE)) ;
}

/**
 * Render a date with conimbo date formats.
 */
function theme_conimbo_formatter_multilingual_date($field) {
  $output = '';
  $formatter = isset($field['#theme']) ? $field['#theme'] : $field['formatter'];
  // Get stored date format type.
  $date_type = isset($field['#item']['date_type']) ? $field['#item']['date_type'] : DATE_DATETIME;
  // Get stored date timezone.
  $date_timezone = isset($field['#item']['date_timezone_db']) ? $field['#item']['date_timezone_db'] : date_default_timezone_name();
  // What date to format?
  if (!isset($field['properties']['function'])) {
    if (isset($field['object_type']) &&
        $field['object_type'] == 'node' &&
        $field['key'] == 'post_date') {
      $date = $field['object']->created;
      $date_type = DATE_UNIX;
    }
    elseif (isset($field['#node']) &&
            isset($field['#item']['value'])) {
    	$date = $field['#item']['value'];
      if (isset($field['#item']['value2'])) {
        $date2 = $field['#item']['value2'];
      }
    }
  }
  else {
  	$date = $field['properties']['function']($field);
  }

  if (isset($date) &&
      isset($formatter)) {
    static $conimbo_date_formats = NULL;
    $conimbo_date_formats = isset($conimbo_date_formats) ? $conimbo_date_formats : variable_get('conimbo_date_formats', array());

    // What language to format in?
    global $language;
    $lang = isset($_REQUEST['language']) ? $_REQUEST['language'] : '';
    if ($lang == '') {
      $lang = $language->language;
    }

    $conimbo_date_format_key = str_replace('conimbo_formatter_', '', $formatter);
    $conimbo_date_format = $conimbo_date_formats[$conimbo_date_format_key][$lang]['value'];
    $supplementary_format_functions = isset($conimbo_date_formats[$conimbo_date_format_key][$lang]['functions']) ? $conimbo_date_formats[$conimbo_date_format_key][$lang]['functions'] : array();
    // Format date in a readable way.
    if (module_exists('date_api')) {
      $datetime = date_make_date($date, $date_timezone, $date_type);
      $output = date_format_date($datetime, 'custom', $conimbo_date_format);
      // Show "date to" only if it's different of the "date from".
      if (isset($date2) && $date2 != $date) {
        $datetime = date_make_date($date2, $date_timezone, $date_type);
        $output .= ' - ' . date_format_date($datetime, 'custom', $conimbo_date_format);
      }
    }
    else {
      $output = format_date($date, $conimbo_date_format);
       if (isset($date2)) {
         $output .= ' - ' . format_date($date2, $conimbo_date_format);
       }
    }
    foreach($supplementary_format_functions as $key => $function) {
      $output = $function($output);
    }
  }

  return $output;
}

/**
 * output a flash file through swfobject
 */
function theme_flash($file) {
  static $flash_added = FALSE;

  if (!$flash_added) {
    $flash_added = TRUE;
    drupal_add_js('swfobject.registerObject("myId", "9", "'. drupal_get_path('module', 'conimbo') .'/js/swfobject/expressInstall.swf");', 'inline');
    drupal_add_js(drupal_get_path('module', 'conimbo') . '/js/swfobject/swfobject.js');
  }

  $size = getimagesize($file);
  $width = $size[0];
  $height = $size[1];
  $file = base_path() . $file;

  $output = '<object id="myId" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' . $width . '" height="' . $height . '">
              <param name="movie" value="' . $file . '" />
              <param name="wmode" value="transparent" />
              <!--[if !IE]>-->
              <object type="application/x-shockwave-flash" data="' . $file . '" width="' . $width . '" height="' . $height . '" wmode="transparent">
              <!--<![endif]-->
              <!--[if !IE]>-->
              </object>
              <!--<![endif]-->
            </object>';

  return $output;
}


/**
 * Theme function for the system themes form.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @ingroup themeable
 */
function theme_conimbo_system_theme_settings_form($form) {
  global $user;

  $config = conf_path();

  foreach (element_children($form) as $key) {
    // Only look for themes
    if (!isset($form[$key]['info'])) {
      continue;
    }

    // Fetch info
    $info = $form[$key]['info']['#value'];

    // Localize theme description.
    $description = t($info['description']);

    // Make sure it is compatible and render the checkbox if so.
    if (isset($form['status']['#incompatible_themes_core'][$key])) {
      unset($form['status'][$key]);
      $status = theme('image', 'misc/watchdog-error.png', t('incompatible'), t('Incompatible with this version of Drupal core'));
      $description .= '<div class="incompatible">'. t('This version is incompatible with the !core_version version of Drupal core.', array('!core_version' => VERSION)) .'</div>';
    }
    elseif (isset($form['status']['#incompatible_themes_php'][$key])) {
      unset($form['status'][$key]);
      $status = theme('image', 'misc/watchdog-error.png', t('incompatible'), t('Incompatible with this version of PHP'));
      $php_required = $form['status']['#incompatible_themes_php'][$key];
      if (substr_count($php_required, '.') < 2) {
        $php_required .= '.*';
      }
      $description .= '<div class="incompatible">'. t('This theme requires PHP version @php_required and is incompatible with PHP version !php_version.', array('@php_required' => $php_required, '!php_version' => phpversion())) .'</div>';
    }
    else {
      $status = drupal_render($form['status'][$key]);
    }

    // Style theme info.
    $theme = '<div class="theme-info"><h2>'. $info['name'] .'</h2><div class="description">'. $description .'</div></div>';

    // Build rows.
    $row = array();
    $row[] = drupal_render($form[$key]['screenshot']);
    $row[] = $theme;

    // Hide columns for not admins.
    if ($user->uid == 1) {
      $row[] = isset($info['version']) ? $info['version'] : '';
      $row[] = array('data' => $status, 'align' => 'center');
    }

    if ($form['theme_default']) {
      $row[] = array('data' => drupal_render($form['theme_default'][$key]));
      if (user_access('export themes')) {
        $form[$key]['operations']['#value'] .= '<br /><br />'. l(t('export'), 'admin/build/themes/export/'. $form[$key]['#parents'][0]);
      }
      if (user_access('delete themes') && is_dir($config .'/themes/'. $key)) {
        $form[$key]['operations']['#value'] .= '<br /><br />'. l(t('delete'), 'admin/build/themes/delete/'. $form[$key]['#parents'][0]);
      }
      $row[] = array('data' => drupal_render($form[$key]['operations']));
    }
    $rows[] = $row;
  }

  // Hide columns for not admins.
  if ($user->uid > 1) {
    $header = array(t('Screenshot'), t('Name'), t('Active'), t('Operations'));
  }
  else {
    $header = array(t('Screenshot'), t('Name'), t('Version'), t('Enabled'), t('Active'), t('Operations'));
  }

  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}
/**
 * Theme a views summary as editable link.
 */
function theme_conimbo_view_summary_editable_link($view, $options, $rows) {
  global $language;
  $lang = isset($_REQUEST['language']) ? $_REQUEST['language'] : '';
  if ($lang == '') {
    $lang = $language->language;
  }
  $argument = $view->argument[$view->build_info['summary_level']];
  $output = '';
  foreach ($rows as $id => $row) {
    $class = '';
    //$rows[$id]->link = $options['link'] ? $options['link'] . '?q=' . $argument->summary_name($row) : $argument->summary_name($row);
    if ($options['link']) {
    	$url = url(NULL, array('absolute' => TRUE)) . '/' . drupal_get_path_alias($options['link'], $lang) . '&year=' . $argument->summary_name($row);
    } else {
      $url_options = array();
      if (!empty($view->exposed_raw_input)) {
        $url_options['query'] = $view->exposed_raw_input;
      }
      $args = $view->args;
      $args[$argument->position] = $argument->summary_argument($row);
      $url =  url($view->get_url($args), $url_options);
    }
    $count = intval($row->{$argument->count_alias});
    if ($url == base_path() . $_GET['q'] || $url == base_path() . drupal_get_path_alias($_GET['q'])) {
      $class = ' class="active"';
    }
    if ($count > 0) {
      $output .= '<li><a href="' . $url . '"'. $class . '>' . $argument->summary_name($row) . ' (' . $count . ')' . '</a></li>';
    }
  }
  if ($output) {
    $output = '<div class="item-list"><ul class="views-summary">' . $output . '</ul></div>';
  }
  return $output;
}

/**
 * Render the media player.
 */
function theme_conimbo_formatter_media_player($field) {
	$output = '';

	// Only render if ...
	// (1) video is enabled and ...
	// (2) there are indeed video fields and ...
	// (3) there is a skin for the player.
	$nice_dash_video = nice_dash_get_plugin('nice_dash_video');

	$conimbo_media_player_skin = variable_get('conimbo_media_player_skin', array());
	if ($nice_dash_video->video_feature_enabled() &&
			isset($field['object']) &&
			(isset($field['object']->{field_video_embedded}['0']['embed']) ||
			 isset($field['object']->{field_video_uploaded}['0']['filepath'])) &&
			 isset($conimbo_media_player_skin['path'])) {
		// Obtain theme.
		$theme = variable_get('theme_default', 'clean_theme');
		// Theme path
		$theme_path = drupal_get_path('theme', $theme);
		$theme_settings = theme_get_settings($theme);

    // Path to player: not using url() because of possible prefix with language negotiation.
		$player = base_path() . drupal_get_path('module', 'conimbo') . '/plugins/jwplayer/player.swf';

		// Path to skin xml file.
		$skin = $conimbo_media_player_skin['path'];

		// Do not use version of swfobject in js/conimbo, not compatible.
		drupal_add_js(drupal_get_path('module', 'conimbo') . '/plugins/jwplayer/swfobject.js');
		$use_playlist = variable_get('conimbo_' . $theme . '_media_player_playlist', FALSE);
	 	$object = $field['object'];
	 	$presetname = NULL;

	  // Default sizes.
   	$width = '640';
    $height = '360';

    // Default sizes can be overriden by imagecache preset sizes in pixels.
    if (isset($field['formatter']) &&
   			function_exists('imagecache_preset_by_name')) {
    	$presetname = str_replace('conimbo_formatter_media_player_', '', $field['formatter']);
   		$preset = imagecache_preset_by_name($presetname);
   		if (isset($preset)) {
	   		module_load_include('inc', 'conimbo', 'includes/conimbo.media_player');
	   		$sizes = _conimbo_media_player_preset_to_format($preset);
	   		if ($sizes) {
	   		 $width = $sizes['width'];
	   		 $height = $sizes['height'];
	   		}
   		}
   	}

   	static $video_id;

   	foreach ($object as $property_key => $property_value) {
   		if ($property_key == 'field_video_embedded' || $property_key == 'field_video_uploaded') {

   			// Embed each video field in a media player.
   			foreach($property_value as $field_index => $field_value) {
   				// Only embed if there actually is a value for the field.
   				if (($property_key == 'field_video_embedded' &&
   						isset($field_value['embed'])) ||
   						($property_key == 'field_video_uploaded' &&
   						isset($field_value['filepath']))) {
	   				// Initialize variables (of previous video)
	   				$variables = array();
	   				$addVariables = '';

	   				$video_id++;
	   				$so = 'so'.$video_id;

	   				// Add corresponding image if found
	   				$stillframe = NULL;
			 			$stillframe_path = NULL;
		   			if (isset($object->{$property_key . '_image'}[$field_index]['filepath'])) {
			      	$stillframe_path = $object->{$property_key . '_image'}[$field_index]['filepath'];
			      	$stillframe = imagecache_create_url($presetname, $stillframe_path);
		   				if (isset($stillframe)) {
				  			$variables['image'] = $stillframe;
							}
				  	}

				  	// Path to video.
				  	$variables['file'] = $property_key == 'field_video_embedded' ? url($field_value['embed'], array('absolute' => true)) : base_path() . '/' . $field_value['filepath'];

				  	// Path to skin.
				  	$variables['skin'] = $skin;

				  	// Remaining variables (allow configuration in dashboard in the future?)
						$variables['controlbar'] = 'over';
						$variables['dock'] = 'false';
						$variables['stretching'] = 'fill';

				  	foreach ($variables as $key => $value) {
				  		$addVariables .= $so . '.addVariable("' . $key . '","' . $value . '");' . PHP_EOL;
						}

						// Create the actual markup for the embedded player.
						$output .= '<div id="' . $theme . '-video-' . $video_id . '"></div>' . PHP_EOL;
						$output .= '<script type="text/javascript">';
						$output .= 'var ' . $so . ' = new SWFObject("'. $player . '","mpl","' . $width .'","' . $height . '","9");' . PHP_EOL;
						$output .= $so . '.addParam("allowfullscreen","true");' . PHP_EOL;
						$output .= $so . '.addParam("allowscriptaccess","always");' . PHP_EOL;
						$output .= $so . '.addParam("wmode","opaque");' . PHP_EOL;
						$output .= $addVariables;
						$output .= $so . '.write("' . $theme . '-video-' . $video_id . '");' . PHP_EOL;
						$output .= '</script>';
	   			}
   			}
	 		}
   	}
	}
  return $output;
}
