<?php

/**
 * @file
 * Conimbo token extensions.
 */

function conimbo_tokens($arguments) {

  static $conimbo_tokens = array();
  if (empty($conimbo_tokens)) {
    $conimbo_tokens = _get_conimbo_tokens();
  }

  $tokens = token_prepare_tokens($conimbo_tokens['tokens'], TOKEN_PREFIX, TOKEN_SUFFIX);
  return str_replace($tokens, $conimbo_tokens['values'], $arguments['string']);
}

/**
 * Helper function to return conimbo tokens.
 */
function _get_conimbo_tokens() {
  $tokens = array();

  $single_term = '';
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));

    // conimbo-single-term
    if ($node->taxonomy) {
      foreach ($node->taxonomy as $vid => $tids) {
        $tids = (array) $tids;
        foreach ($tids as $tid) {
          $term = taxonomy_get_term($tid);
          if (!empty($term)) {
            $single_term = check_plain($term->name);
            break;
          }
        }
      }
    }

    // conimbo-node-title
    $node_title = $node->title;
  }

  $tokens['tokens'][] = 'conimbo-single-term';
  $tokens['values'][] = $single_term;
  $tokens['tokens'][] = 'conimbo-single-term-strtolower';
  $tokens['values'][] = strtolower($single_term);
  $tokens['tokens'][] = 'conimbo-node-title';
  $tokens['values'][] = $node_title;

  return $tokens;
}
