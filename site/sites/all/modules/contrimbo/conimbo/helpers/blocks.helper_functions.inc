<?php

/**
 * @file
 * PHP helpder functions you can use in block visibility settings. Makes the code much more scalable.
 */

function blocks_content_type_validation($arguments) {
  $show = FALSE;
  if ($node = menu_get_object()) {
    if (in_array($node->type, $arguments)) {
      $show = TRUE;
    }
  }

  return $show;
}
/**
 * Return true if this is a node search result and not a node page.
 */
function blocks_content_type_search_validation($arguments) {
  $show = FALSE;

  $path = arg(0);
  if (isset($arguments['callbacks'][$path])) {
    return TRUE;
  }

  return $show;
}