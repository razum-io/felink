<?php

/**
 * @file
 * PHP helpder functions you can use in views php code. Makes the code much more scalable.
 */

/**
 * Basic views argument validation.
 */
function views_basic_argument_validation($arguments) {
  $argument = $arguments['argument'];
  if (empty($argument)) {
      return FALSE;
    }
    else {
      return TRUE;
  }
}

/**
 * Return the taxonomies on the current node as arguments.
 */
function views_node_terms_as_argument($arguments) {

  $arg0 = arg(0);
  $arg1 = arg(1);

  if ($arg0 != 'node' || !is_numeric($arg1)) {
    return FALSE;
  }

  $viewed_node = node_load($arg1);

  $argument = '';
  foreach ($viewed_node->taxonomy as $tid => $term) {
    $argument .= $tid . '+';
  }

  $argument = substr($argument, 0, -1);

  return $argument;
}

/**
 * Return the node references on the current node as arguments.
 */
function views_argument_node_reference($arguments) {

  $arg0 = arg(0);
  $arg1 = arg(1);

  if ($arg0 != 'node' || !is_numeric($arg1)) {
    return FALSE;
  }

  $viewed_node = node_load($arg1);
  $field = $arguments['field'];

  $argument = '';
  if (isset($viewed_node->{$field}[0]['nid'])) {
    $argument = $viewed_node->{$field}[0]['nid'];
  }

  return $argument;
}
/**
 * Return the node creation year from GET parameter as argument.
 * For language independant use.
 */
function views_date_as_argument($arguments) {
  $arg0 = arg(0);
  $arg1 = arg(1);

  if ($arg0 != 'blogs' ||
      $arg1 != 'archive' ||
      !isset($_GET['year']) ||
      !is_numeric($_GET['year'])) {
    return FALSE;
  }
  return $_GET['year'];
}
