<?php

/**
 * @file
 * Workflow.
 */
/**
 * Modify node edit form for workflow.
 */
function conimbo_workflow_node_form_alter(&$form) {
  // Modify editable publishing options, according to workflow settings.
  $type = $form['type']['#value'];
  // Remove publishing checkbox for node types which have a workflow.
  $result = db_result(db_query("SELECT type FROM {workflow_type_map} WHERE type = '%s' and wid > 0", $type));
  if ($result !== FALSE) {
    unset($form['options']['status']);
  }

  // Some editing of workflow scheduling time.
  $form['workflow']['workflow_scheduled_hour']['#title'] = t('Time');
  $form['workflow']['workflow_scheduled_hour']['#size'] = t('5');
  $form['workflow']['workflow_scheduled_hour']['#maxlength'] = t('5');
}