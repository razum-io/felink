<?php
/**
 * Check whether a preset has pixel sizes which can serve as sizes for
 * the JW Player formatter.
 */
function _conimbo_media_player_preset_to_format($preset) {
	
	// Retrieve eventual sizes of images in this preset
  $action_weight = NULL;
  $width = NULL;
  $height = NULL;
	// Retreive size of last imagecache rule.
	// Only consider sizes in pixels.
	foreach ($preset['actions'] as $action_key => $action_value) {
		if ((!isset($action_weight) ||
				$action_value['weight'] > $action_weight) &&
				isset($action_value['data']['width']) &&
				isset($action_value['data']['height']) &&
				!strpos($action_value['data']['width'], '%') &&
				!strpos($action_value['data']['height'], '%')) {
			$action_weight = $action_value['weight'];
			$width = $action_value['data']['width'];
			$height = $action_value['data']['height'];		
		}
	}

	if (isset($width) && 
		  isset($height)) {
		return array('width' => $width, 'height' => $height);
	} else {
	  return FALSE;
	}
}

/**
 * Alter form to disable default embedded video player settings.
 */
function conimbo_media_player_field_form_alter(&$form) {  
	$form['widget']['video']['#collapsed'] = TRUE;
	$form['widget']['video']['#title'] .= ' ' . t('(Disable video functionality in Dashboard if you prefer to set embedded video display settings here.)');
	unset($form['widget']['video']['#description']);
	foreach (element_children($form['widget']['video']) as $key) {
    unset($form['widget']['video'][$key]);
  }
  $form['widget']['preview']['#collapsed'] = TRUE;
	$form['widget']['preview']['#title'] .= '  ' . t('(Disable video functionality in Dashboard if you prefer to set embedded video preview settings here.)');
	unset($form['widget']['preview']['#description']);
	foreach (element_children($form['widget']['preview']) as $key) {
    unset($form['widget']['preview'][$key]);
  }
	$form['widget']['tn']['#collapsed'] = TRUE;
	$form['widget']['tn']['#title'] .= '  ' . t('(Disable video functionality in Dashboard if you prefer to set embedded video preview settings here.)');
	unset($form['widget']['tn']['#description']);
	foreach (element_children($form['widget']['tn']) as $key) {
    unset($form['widget']['tn'][$key]);
  }
}

/**
 * Alter form to allow choice of playlist
 */
function conimbo_media_player_type_form_alter(&$form) {

	$form['conimbo_media_player'] = array(
    '#type' => 'fieldset',
    '#title' => t('Media player settings'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
  );
  $default_theme = variable_get('theme_default', 'clean_theme');
  $form['conimbo_media_player']['conimbo_media_player_playlist'] = array(
   	'#type' => 'checkbox',
    '#title' => t('Use playlist to show multiple video\'s on a !news item instead of multiple players.', array('!news' => strtolower($form['#node_type']->name))),
    '#default_value' => variable_get('conimbo_' . $default_theme . '_media_player_playlist', FALSE),
  );

	$form['#submit'][] = 'conimbo_media_player_type_form_alter_submit';
}

/**
 * Save form alter of field.
 */
function conimbo_media_player_type_form_alter_submit($form, &$form_state) {
	$default_theme = variable_get('theme_default', 'clean_theme');
  variable_set('conimbo_' . $default_theme . '_media_player_playlist', $form_state['values']['conimbo_media_player_playlist']);
}
