<?php

/**
 * @file
 * Views text.
 */

/**
 * Alter views_text node form.
 */
function conimbo_views_text_node_form_alter(&$form) {
  $form['#validate'][] = 'conimbo_views_text_node_form_validate';
}

/**
 * Change views text node form
 */
function conimbo_views_text_node_form_validate($form, &$form_state) {
  // Views title field hidden in nodeformscols. Views title field filled with views text title.
  if (isset($form_state['values']['field_views_text_title']) &&
      isset($form_state['values']['title'])) {
    $form_state['values']['field_views_text_title'][0]['value'] = $form_state['values']['title'];
  }
}
