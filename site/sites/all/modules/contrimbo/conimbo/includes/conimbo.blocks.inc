<?php

/**
 * @file
 * Conimbo block functions.
 */

/**
 * Show a block, looking at the delta.
 */
function conimbo_show_block($delta) {
  if (function_exists($delta)) {
    return $delta();
  }
}

/**
 * Return a list of product categories.
 */
function conimbo_product_categories() {
  $content = '';
  $block = array();

  $result = db_query(db_rewrite_sql("SELECT title, nid FROM {node} n WHERE n.type = 'product_category' ORDER BY title ASC"));
  while ($row = db_fetch_object($result)) {
    $content .= '<li>'. l($row->title, 'node/'. $row->nid);

    $products = db_result(db_query('SELECT count(field_product_category_reference_nid) as total FROM {content_field_product_category_reference} WHERE field_product_category_reference_nid = %d', $row->nid));
    if ($products != 0) {
      $content .= '<span class="item-counter">('. $products .')</span>';
    }
    $content .= '</li>';
  }

  if (!empty($content)) {
    $content = '<ul>'. $content .'</ul>';
  }


  $block['subject'] = 'Categories';
  $block['content'] = $content;
  return $block;
}

/**
 * Return user info
 */
function conimbo_user_info() {
  $content = '';
  $block = array();
  global $user;
  global $language;
  $i = 0;

  // Links to views of flagged items of all flags for current user.
  if ($user->uid > 0 && module_exists('flag')) {
    $flags = flag_get_flags('node');
    foreach ($flags as $flag_name => $flagged_content) {
      $content[$i] = conimbo_flag_item('node', $user->uid, $flag_name);
      $i++;
    }
  }
  $account_navigation = menu_tree_all_data('menu-account-'. $language->language);
  foreach ($account_navigation as $key => $value) {
    $content[$i] = l($value['link']['title'], $value['link']['link_path']);
    $i++;
  }

  if (!empty($content)) {
    $content = theme('item_list', $content);
  }
  $block['subject'] = 'User info';
  $block['content'] = $content;
  return $block;
}

/**
 * Return links to feeds
 */
function conimbo_feeds() {
  $content = theme('feed_icon', 'rss.xml', t('feed'));

  if (!empty($content)) {
    $content = theme('item_list', (array) $content);
  }
  $block['subject'] = 'Feeds';
  $block['content'] = $content;

  return $block;
}
