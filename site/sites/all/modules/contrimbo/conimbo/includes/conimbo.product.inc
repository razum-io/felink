<?php

/**
 * Alter form to accept default product category.
 */
function conimbo_product_node_form_alter(&$form) {
  if (isset($_GET['product_category']) && is_numeric($_GET['product_category'])) {
    $form['field_product_category_reference']['#default_value'][0]['nid'] = $_GET['product_category'];
  }
}
