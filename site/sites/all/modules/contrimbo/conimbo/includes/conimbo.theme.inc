<?php

/**
 * @file
 * Administrative functions for theming in Conimbo.
 */

/**
 * Plugin options.
 */
function conimbo_theme_plugin_options() {
	
  $options = array(
    'placeholders' => t('Placeholders'),
    'menus' => t('Navigation model'),
    'conimbo_layouts' => t('Page layout'),
    'breadcrumb' => t('Breadcrumb'),
    'rssfeed' => t('RSS feeds'),
  	'media_player_skins' => t ('Video player skins'),
  );
  return $options;
}

/**
 * Menu callback: plugin settings screen.
 */
function conimbo_theme_plugin_settings($form_state) {
  $form = array();
  $nice_dash_video = nice_dash_get_plugin('nice_dash_video');
  $conimbo_theme_plugins = variable_get('conimbo_theme_plugins', array());
  $conimbo_theme_plugins['media_player_skins'] = $nice_dash_video->video_feature_enabled() ? 'media_player_skins' : FALSE;  
  $form['conimbo_theme_plugins'] = array(
    '#title' => 'plugins',
    '#type' => 'checkboxes',
    '#options' => conimbo_theme_plugin_options(),
    '#description' => t('Select which plugins you want to enable on this site'),
    '#default_value' => $conimbo_theme_plugins,
  );

  $form['submit'] = array(
    '#value' => t('Save configuration'),
    '#type' => 'submit'
  );

  return $form;
}

/**
 * Save theme plugin settings.
 */
function conimbo_theme_plugin_settings_submit($form, &$form_state) {
  $plugin_values = $form_state['values']['conimbo_theme_plugins'];

  // Save each plugin individualy.
  $options = conimbo_theme_plugin_options();
  foreach ($options as $key => $option) {
    $value = ($plugin_values[$key] === $key) ? TRUE : FALSE;
    variable_set('conimbo_'. $key, $value);
  }

  // Save theme plugin settings.
  variable_set('conimbo_theme_plugins', $plugin_values);

  drupal_set_message('The theme plugin settings have been saved');
}

/**
 * Add extra plugins to the theme settings form.
 */
function conimbo_theme_plugins(&$form) {

  $theme_key = arg(4);
  // fallback when on default tab
  if (empty($theme_key)) {
    $theme_key = $form['var']['#value'];
    $theme_parts = explode('_', $theme_key);
    unset($theme_parts[0]);
    unset($theme_parts[count($theme_parts)]);
    $theme_key = implode('_', $theme_parts);
  }

  $theme_settings = theme_get_settings($theme_key);

  $info = db_result(db_query("SELECT info FROM {system} WHERE name = '%s' AND type ='theme'", $theme_key));
  $theme_info = unserialize($info);

  $extra_settings = variable_get('conimbo_theme_plugins', array());
  if (!empty($extra_settings)) {
    foreach ($extra_settings as $setting_key => $setting_value) {
      if ($setting_key === $setting_value)
      _conimbo_theme_plugins($form, $theme_key, $theme_settings, $theme_info, $setting_key);
    }
  }
}

/**
 * Helper function that loads a plugin for a theme.
 *
 * @param string $theme_key The name of the theme.
 * @param array $theme_settings The settings of this theme.
 * @param array $theme_info The info for this theme.
 * @param string $type The function to load.
 */
function _conimbo_theme_plugins(&$form, $theme_key, $theme_settings, $theme_info, $type) {
  module_load_include('inc', 'conimbo', 'plugins/'. $type);
  $function = '_'. $type;
  $function($form, $theme_key, $theme_settings, $theme_info);
}

/**
 * System theme form.
 */
function conimbo_system_theme_form(&$form) {
  global $user;
  // Only show enabled themes to admin.
  if ($user->uid > 1) {
    foreach ($form['status']['#options'] as $key => $value) {
      if (!in_array($key, $form['status']['#default_value'])) {
        unset($form['status']['#options'][$key]);
        unset($form['theme_default']['#options'][$key]);
        unset($form[$key]);
      }
      unset($form[$key]['info']['#value']['version']);
    }
    $form['status']['#access'] = FALSE;
    unset($form['buttons']['reset']);
  }

  $form['#theme'] = 'conimbo_system_theme_settings_form';

  $form['#submit'][] = 'conimbo_flush_imagecache';
  $form['#submit'][] = 'conimbo_save_wysiwyg_settings';
  $form['#submit'][] = 'conimbo_save_maintenance_theme';
}

/**
 * Theme settings form.
 */
function conimbo_theme_settings(&$form) {
  drupal_add_js(drupal_get_path('module', 'conimbo') .'/js/theme_config.js');

  // Save active stylesheet in WYSIWYG settings.
  $form['#submit'][] = 'conimbo_save_wysiwyg_settings';

  // Remove the possibility to reset theme configuration for enterprise basic.
  $form['buttons']['reset']['#access'] = FALSE;

  // Theme the favicon in a nicer way.
  $form['theme_settings']['#access'] = FALSE;
  $form['favicon']['#collapsible'] = TRUE;
  $form['favicon']['#collapsed'] = TRUE;

  // Hide logo.
  unset($form['logo']);
}

/**
 * Submit callback: alter wysiwyg settings based on default front / end theme.
 */
function conimbo_save_wysiwyg_settings($form, &$form_state) {
  $add_paths = FALSE;
  $paths_string = '';
  $all_paths = array();
  $values = $form_state['values'];

  $default_theme = variable_get('theme_default', 'garland');
  $administration_theme = variable_get('admin_theme', '0');
  $node_admin_theme = variable_get('node_admin_theme', FALSE);
  if ($default_theme != $administration_theme) {
    $add_paths = TRUE;
    if (!$node_admin_theme) {
      $add_paths = FALSE;
    }
  }

  // Get the generated css file from site-elements.less (if exists) from that theme.
  if ($add_paths == TRUE) {
    $paths_string = '';
    $theme_data = db_result(db_query("SELECT info FROM {system} WHERE name = '%s'", $default_theme));
    if (!empty($theme_data)) {
      $theme_data_unserialized = unserialize($theme_data);
      foreach ($theme_data_unserialized['stylesheets'] as $type => $values) {
        if ($type != 'print') {
          foreach ($values as $key => $stylesheet) {
            if (substr($stylesheet, -18) == 'site-elements.less') {
              $css_path = file_create_path('less');
              $base_path = $_SERVER['DOCUMENT_ROOT'] . base_path();
              $output_file = $css_path .'/'. 'less_'. md5_file($stylesheet) .'.css';
              $paths_string = '%b'. $output_file;
              break;
            }
          }
        }
      }
    }
  }

  // Get all wysiwyg profiles.
  $query = "SELECT format, editor, settings FROM {wysiwyg}";
  $result = db_query($query);
  while ($row = db_fetch_object($result)) {
    if (!empty($row->editor)) {
      $settings = unserialize($row->settings);
      $settings['css_setting'] = 'self';
      $settings['css_path'] = $paths_string;
      db_query("UPDATE {wysiwyg} set settings = '%s' WHERE format = %d", serialize($settings), $row->format);
    }
  }

  // Sync block settings to new theme
  if ($form['theme_default']['#value'] != $form['theme_default']['#default_value']) {
    conimbo_sync_blocks($form['theme_default']['#value'], $form['theme_default']['#default_value']);
  }
}

/**
 * Export a theme and its Conimbo files.
 */
function conimbo_export_theme($theme) {
  global $base_path;

  // Add theme folder.
  $src = drupal_get_path('theme', $theme);
  $subfolder = 'themes';

  $filename = $theme;
  if (substr($src,-1) === '/') {
    $src = substr($src, 0, -1);
  }
  $arr_src = explode('/', $src);
  unset($arr_src[count($arr_src)-1]);
  $path_length = strlen(implode('/', $arr_src) .'/');
  $f = explode('/', $filename);
  $filename = (($filename=='') ? 'backup.zip' : $filename .'_'. time() .'.zip');

  $zip = new ZipArchive;
  $res = $zip->open(file_directory_temp() .'/'. $filename, ZipArchive::CREATE);
  if ($res !== TRUE) {
    return 'Error: Unable to create zip file';
  }
  if (is_file($src)) {
    $zip->addFile($src, substr($src, $path_length));
  }
  else {
    if (!is_dir($src)) {
      $zip->close();
      @unlink($filename);
      return 'Error: File not found';
    }
    recurse_zip($src, $zip, $path_length, $subfolder);
  }

  // Add files if they exist.
  $theme_settings = theme_get_settings($theme);
  $theme_info = unserialize(db_result(db_query("SELECT info FROM {system} WHERE name = '%s'", $theme)));

  // Add theme settings.
  $zip->addFromString('theme-settings.txt', serialize($theme_settings));

  // Close and stream zip to download.
  $zip->close();
  header("Content-type: application/zip");
  header("Content-Disposition: attachment; filename=$filename");
  header("Pragma: no-cache");
  header("Expires: 0");
  readfile(file_directory_temp() .'/'. $filename);
  @unlink(file_directory_temp() .'/'. $filename);
  exit();
}

/**
 * Helper function to loop through a folder to add it to a zip file.
 */
function recurse_zip($src, &$zip, $path_length, $subfolder) {
  $dir = opendir($src);
  while (false !== ($file = readdir($dir))) {
    if (($file != '.' ) && ($file != '..') && ($file != '.svn')) {
      if (is_dir($src . '/' . $file)) {
        recurse_zip($src . '/' . $file, $zip, $path_length, $subfolder);
      }
      else {
        $file_to_add = $src .'/'. $file;
        $dest = $subfolder .'/'. substr($src .'/'. $file, $path_length);
        $zip->addFile($file_to_add, $dest);
      }
    }
  }
  closedir($dir);
}

/**
 * Import a conimbo theme.
 */
function conimbo_import_theme(&$form_state) {
  $form = array();

  $form['theme_upload'] = array(
    '#type' => 'file',
    '#title' => 'File',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload theme'),
  );
  $form['#attributes'] = array(
    'enctype' => 'multipart/form-data'
  );
  return $form;
}

/**
 * Submit theme upload.
 */
function conimbo_import_theme_validate(&$form, &$form_state) {

  $config = conf_path();

  $command = "/usr/bin/sudo /bin/chgrp www-data /home/conimbo/WWW/drupal/$config/themes";
  system($command);

  // Validate the file.
  // NOTE: admin will always pass this test.
  $validators = array('file_validate_extensions' => array('zip'));
  $file = file_save_upload('theme_upload', $validators);

  if ($file) {

    // Unzip the file.
    $temporary_path = file_directory_temp() .'/theme_upload';
    exec("unzip ". $file->filepath ." -d ". $temporary_path);

    // Check on themes directory.
    if (is_dir($temporary_path .'/themes')) {
      $themes = file_scan_directory($temporary_path .'/themes', 'info');
      foreach ($themes as $key => $theme) {

        // Check theme name.
        $theme_name = db_result(db_query("SELECT name FROM {system} WHERE name = '%s' AND type = 'theme'", $theme->name));

        // Let's move the theme.
        if ($theme_name === FALSE && is_dir($temporary_path .'/themes/'. $theme->name)) {

          // Move themes folder.
          $src = $temporary_path .'/themes/'. $theme->name;
          $destination = $config .'/themes/';
          exec("mv ". $src ." ". $destination);

          // Enable the theme.
          system_theme_data();
          db_query("UPDATE {system} set status = 1 WHERE name = '%s' AND type = 'theme'", $theme->name);

          // Friendly message.
          drupal_set_message(t("%theme_name moved to themes directory.", array('%theme_name' => $theme->name)));
        }
        else {
          if ($theme_name == TRUE) {
            drupal_set_message(t('A theme with name %theme_name already exists. Please rename your theme.', array('%theme_name' => $theme->name)), 'error');
          }
          else if (!is_dir($temporary_path .'/themes/'. $theme->name)){
            drupal_set_message(t('The file structure is wrong. Please refer to the documentation.'), 'error');
          }
        }
      }
    }
    else {
      drupal_set_message(t('Themes folder in zip file not found.', 'error'));
    }

    // Remove obsolete stuff.
    exec("rm -rf ". $temporary_path);
    file_delete($file->filepath);
    db_query("DELETE FROM {files} WHERE fid = %d", $file->fid);
  }
  else {
     drupal_set_message(t('The uploaded theme is not in a valid format. Please refer to the documentation.'), 'error');
  }

  $command = "/usr/bin/sudo /bin/chgrp conimbo /home/conimbo/WWW/drupal/$config/themes";
  system($command);

}

/**
 * Delete theme.
 */
function conimbo_delete_theme(&$form_state, $theme) {
  if (is_dir(conf_path() .'/themes/'. $theme)) {

    $form['theme'] = array(
      '#type' => 'value',
      '#value' => $theme
    );

    return confirm_form($form,
      t('Are you sure you want to delete theme %theme?', array('%theme' => $theme)),
      'admin/build/themes',
      t('This action cannot be undone.'),
      t('Delete'),
      t('Cancel')
    );
  }
  else {
    drupal_goto('admin/build/themes');
  }
}

/**
 * Delete theme.
 */
function conimbo_delete_theme_submit($form, &$form_state) {
  $config = conf_path();
  $command = "/usr/bin/sudo /bin/chgrp www-data /home/conimbo/WWW/drupal/$config/themes";
  system($command);

  $theme = $form_state['values']['theme'];
  exec("rm -rf ". conf_path() .'/themes/'. $theme);

  drupal_set_message(t('%theme has been deleted.', array('%theme' => $theme)));
  $form_state['redirect'] = 'admin/build/themes';

  $command = "/usr/bin/sudo /bin/chgrp conimbo /home/conimbo/WWW/drupal/$config/themes";
  system($command);
}

/**
 * Submit callback: save maintenance theme in settings.php
 */
function conimbo_save_maintenance_theme($form, &$form_state) {

  $theme = $form_state['values']['theme_default'];

  $folder = file_directory_path() . '/settings.d/';

  file_check_directory($folder, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

  $file = $folder . "/maintenance_theme.conf";
  if ($handle = fopen($file, 'w')) {
    $config = '<?php' . "\n";
    $config .= '$conf[\'maintenance_theme\'] = \'' . $theme . '\';' . "\n";
    $config .= '?>';
    fwrite($handle, $config);
    fclose($handle);
    if (isset($form_state['group']) && $form_state['group'] != '') {
      chgrp($file, $form_state['group']);
    }
  }
  else {
    watchdog('theme_settings', 'Unable to write maintenance theme settings.');
    drupal_set_message(t('Unable to write maintenance settings.'));
  }
}

/**
 * Flush imagecache after theme switch.
 */
function conimbo_flush_imagecache($form, &$form_state) {
  $presets = imagecache_presets();
  foreach ($presets as $preset) {
    imagecache_preset_flush($preset);
  }
}
