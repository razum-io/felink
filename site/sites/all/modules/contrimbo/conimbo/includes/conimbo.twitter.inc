<?php

/**
 * @file
 * Conimbo twitter functions.
 */

/**
 * Submit twitter settings
 */
function conimbo_twitter_settings_submit($form, &$form_state) {

  module_load_include('inc', 'twitter');

  $account = array();
  $account['uid'] = 1;
  $account['screen_name'] = $form_state['values']['twitter_global_name'];
  $account['password'] = $form_state['values']['twitter_global_password'];
  $account['import'] = 1;

  variable_set('tweetmeme_source', $account['screen_name']);

  twitter_user_save($account);
}