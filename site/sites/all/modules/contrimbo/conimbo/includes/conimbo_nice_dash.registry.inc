<?php

/**
 * @file
 * Registry file for Nice dashboard.
 * Contains functions which are only called after a cache clear.
 */

/**
 * Return plugins.
 */
function _conimbo_nice_dash_plugins() {
  $plugins = array();

  $plugins['nice_dash_video'] = array(
    'title' => t('Video'),
    'handler' => array(
      'path' => drupal_get_path('module', 'conimbo') .'/plugins/nice_dash/nice_dash_video',
      'file' => 'nice_dash_video.inc',
      'class' => 'nice_dash_video',
    ),
  );
  $plugins['nice_dash_fivestar'] = array(
    'title' => t('Fivestar'),
    'handler' => array(
      'path' => drupal_get_path('module', 'conimbo') .'/plugins/nice_dash/nice_dash_fivestar',
      'file' => 'nice_dash_fivestar.inc',
      'class' => 'nice_dash_fivestar',
    ),
  );
  $plugins['nice_dash_features'] = array(
    'title' => t('Features'),
    'handler' => array(
      'path' => drupal_get_path('module', 'conimbo') .'/plugins/nice_dash/nice_dash_features',
      'file' => 'nice_dash_features.inc',
      'class' => 'nice_dash_features',
    ),
  );
  return $plugins;
}

