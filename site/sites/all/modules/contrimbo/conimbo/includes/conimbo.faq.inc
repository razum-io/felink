<?php

/**
 * @file
 * Faq.
 */

function conimbo_faq_settings(&$form) {
  $conimbo_faq = variable_get('conimbo_faq', array());
  foreach($conimbo_faq as $key => $field)
  $form[$key] = array(
    '#type' => $field['type'],
    '#title' => $field['title'],
    '#default_value' => isset($field['value']) ? $field['value'] : TRUE,
    '#description' => t($field['description']),
   );
  $form['#submit'][] = 'conimbo_faq_settings_submit';
}

/**
 * Submit faq settings.
 */
function conimbo_faq_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
  $conimbo_faq = variable_get('conimbo_faq', array());

  foreach ($values as $field => $value) {
    if (isset($conimbo_faq[$field])) {
      $conimbo_faq[$field]['value'] = $value;
    }
  }
  variable_set('conimbo_faq', $conimbo_faq);
}

/**
 * Alter form to redirect after saving faq node.
 */
function conimbo_faq_node_form_alter(&$form) {
	 $form['#submit'][] = 'conimbo_faq_node_form_submit';
}

/**
 * Redirect to faq page after editing a faq.
 */
function conimbo_faq_node_form_submit($form, &$form_state) {
  $options = '';
  if (isset($_GET['language'])) {
    $options = 'language='.  $_GET['language'];
  }
  unset($_REQUEST['destination'], $_REQUEST['edit']['destination']);
  $form_state['redirect'] = array('faq', $options);
}
