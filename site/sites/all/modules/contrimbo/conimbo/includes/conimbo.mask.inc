<?php

/**
 * @file
 * Modules page.
 */

/**
 * Mask Views PHP.
 */
function conimbo_mask_views_form_alter(&$form) {
  global $conf, $user;

  // It's possible to ignore this as user one if this
  // is set in settings.php.
  if (isset($conf['ignore_user_1']) && $conf['ignore_user_1'] && $user->uid == 1) {
    return;
  }

  $form['options']['default_argument_php']['#access'] = FALSE;
  $form['options']['validate_argument_php']['#access'] = FALSE;
}

/**
 * Mask user permissions.
 */
function conimbo_mask_permissions_form_alter(&$form) {
  global $conf, $user;

  // It's possible to ignore this as user one if this
  // is set in settings.php.
  if (isset($conf['ignore_user_1']) && $conf['ignore_user_1'] && $user->uid == 1) {
    return;
  }

  $hide_permissions = conimbo_mask_permissions();
  foreach ($hide_permissions as $hidden) {
    unset($form['permission'][$hidden]);
    foreach (element_children($form['checkboxes']) as $rid) {
      unset($form['checkboxes'][$rid]['#options'][$hidden]);
    }
  }
}

function conimbo_mask_permissions() {
  return array(
    // block module
    'use PHP for block visibility',
    // content module (in CCK)
    'Use PHP input for field settings (dangerous - grant with care)',
    // webform module
    'use PHP for additional processing',
    // display suite
    'use PHP in custom fields',
    // Google analytics.
    'use PHP for tracking visibility',
    // Devel
    'execute php code',
    // Custom formatters.
    'administer custom formatters',
    // 'Fivestar',
    'use PHP for fivestar target',
    // 'Mediafront'
    'use PHP for mediafront parameters',
    // 'Hansel'
    'use PHP for hansel configuration',
  );
}

/**
 * Alters the system modules form to hide (mask) certain modules.
 *
 * Profiles can define which modules are installed by default, which modules
 * are required and which modules are optional. Modules not in this list are
 * hidden from view on the modules page, except for users with the 'view all
 * modules'-permission.
 *
 * @see <profile>_profile_modules().
 * @see <profile>_profile_optional_modules().
 * @see <profile>_profile_required_modules().
 */
function conimbo_mask_modules_form_alter(&$form) {
  // Users with the 'view all modules' permissions will see the unrestricted
  // module list.
  if (!user_access('view all modules') && !(in_array($install_profile, array('base', 'default', 'hostmaster')))) {
    // Load the profile for this site.
    $install_profile = variable_get('install_profile', 'enterprise_basic');
    require_once('./profiles/'. $install_profile .'/'. $install_profile .'.profile');

    // User our theme functions.
    $form['#theme'] = 'conimbo_system_modules';

    $masked = array();

    // Modules when installed.
    $modules_available_function = $install_profile .'_profile_modules';
    $available = $modules_available_function();

    // Modules which are optional.
    $modules_optional_function = $install_profile .'_profile_optional_modules';
    $optional = $modules_optional_function();

    // Modules which are required.
    $modules_required_function = $install_profile .'_profile_required_modules';
    $required = $modules_required_function();

    $masked['available'] = array_merge($available, $optional);
    $masked['required'] = $required;

    // Remove Apache Solr.
    if (!variable_get('apachesolr_installed', FALSE)) {
      $as = array(
        'apachesolr',
        'apachesolr_search',
        'apachesolr_date',
        'apachesolr_attachments',
        'apachesolr_nodeaccess',
      );
      foreach ($as as $as_module) {
        $key = array_search($as_module, $masked['available']);
        if ($key) {
          unset($masked['available'][$key]);
        }
      }
    }

    foreach (element_children($form['name']) as $module) {

      // Make version a value.
      $form['version'][$module]['#type'] = 'value';

      // If module is not available, remove it.
      if (is_array($masked['available']) && !in_array($module, $masked['available'])) {
        $value = (in_array($module, $form['status']['#default_value'])) ? 1 : 0;
        $form['status'][$module] = array('#type' => 'value', '#value' => $value);
        foreach (array('name', 'version', 'description', 'throttle') as $key) {
          unset($form[$key][$module]);
        }
        unset($form['throttle'][$module], $form['throttle']['#options'][$module]);
      }
      // If module is required, pass it as a value and hide it.
      elseif (is_array($masked['required']) && in_array($module, $masked['required'])) {
        $form['status'][$module] = array('#type' => 'value', '#value' => 1);
        foreach (array('name', 'version', 'description', 'throttle') as $key) {
          unset($form[$key][$module]);
        }
        unset($form['throttle']['#options'][$module]);
      }
    }
  }
}

/**
 * Theme the modules overview page.
 */
function theme_conimbo_system_modules($form) {

  if (isset($form['confirm'])) {
    return drupal_render($form);
  }

  // Individual table headers.
  $header = array();
  $header[] = array('data' => t('Enabled'), 'class' => 'checkbox');
  $header[] = t('Name');
  $header[] = t('Description');

  $social = array('twitter', 'comment', 'tagadelic', 'twitter_actions', 'mollom', 'comment', 'contact', 'emf', 'emf_campaign_monitor', 'tweetmeme', 'votingapi', 'fivestar', 'advpoll');

  // Pull package information from module list and start grouping modules.
  $modules = $form['validation_modules']['#value'];
  foreach ($modules as $module) {
    if (!isset($module->info['package']) || !$module->info['package']) {
      $package = t('Other');
    }
    else {
      $package = $module->info['package'];
    }
    if ($package) {
      if (in_array($module->name, $social)) {
        $package = t('Social');
      }
      elseif ($package == 'One' || $module->name == 'Splash') {
        $package = 'Conimbo';
      }
      elseif ($package == t('Views')) {
        $package = t('Views');
      }
      elseif ($package == t('Display suite')) {
        $package = t('Display Suite');
      }
      elseif ($package == t('ImageCache')) {
        $package = t('ImageCache');
      }
      elseif ($package == t('Multilanguage') || $module->name == 'locale' || $module->name == 'translation' || $module->name == 'admin_language') {
        $package = t('Multilanguage');
      }
      elseif ($package == t('CCK') || $package == t('Panels') ||  $package == t('Chaos tool suite') || $module->name == 'faq') {
        $package = t('Content');
      }
      elseif (strpos($package, 'Ui') !== FALSE) {
        $package = t('User interface');
      }
      elseif ($package == t('Apache Solr') || $module->name == 'search') {
        $package = t('Search');
      }
      else {
        $package = t('Other');
      }
    }

    $packages[$package][$module->name] = $module->info;
  }
  ksort($packages);

  // Display packages.
  $output = '';
  foreach ($packages as $package => $modules) {
    $rows = array();
    foreach ($modules as $key => $module) {
      $row = array();

      // Do not render stuff which isn't available.
      if (!isset($form['name'][$key])) {
        continue;
      }

      // Change name of one to Conimbo.
      if ($package == t('Conimbo') || $package == t('Wiki')) {
        $form['name'][$key]['#value'] = str_replace('One', 'Conimbo', $form['name'][$key]['#value']);
        $form['description'][$key]['#value'] = str_replace('One', 'Conimbo', $form['description'][$key]['#value']);
      }

      // Checkbox.
      $status = drupal_render($form['status'][$key]);
      $row[] = array('data' => $status, 'class' => 'checkbox');

      // Label and description.
      unset($form['description'][$key]['required']);
      unset($form['description'][$key]['dependencies']);

      $description = drupal_render($form['description'][$key]);
      if (isset($form['status'][$key])) {
        $row[] = '<strong><label for="'. $form['status'][$key]['#id'] .'">'. drupal_render($form['name'][$key]) .'</label></strong>';
      }
      else {
        $row[] = '<strong>'. drupal_render($form['name'][$key]) .'</strong>';
      }
      $row[] = array('data' => $description, 'class' => 'description');

      $rows[] = $row;
    }

    $fieldset = array(
      '#title' => t($package),
      '#collapsible' => TRUE,
      '#collapsed' => $package == t('Conimbo') ? FALSE : TRUE,
      '#value' => theme('table', $header, $rows, array('class' => 'package')),
    );
    $output .= theme('fieldset', $fieldset);
  }

  $output .= drupal_render($form);
  return $output;
}
