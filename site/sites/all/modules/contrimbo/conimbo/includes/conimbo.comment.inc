<?php

/**
 * @file
 * Comment.
 */

/**
 * Alter the comment form.
 */
function conimbo_comment_form_alter(&$form) {
  $form['#after_build'][] = 'conimbo_comment_form_remove_node_on_preview';
  /*if ($form['_author'] || $form['name']) {
    if ($form['_author']) {
      $form['_author']['#prefix'] = '<div class="comment-left">';
    } else {
      $form['name']['#prefix'] = '<div class="comment-left">';
    }
    if ($form['homepage']) {
      $form['homepage']['#default_value'] = 'http://';
    }
    if ($form['subject']) {
      $form['subject']['#prefix'] = '</div><div class="comment-right">';
    } else {
      $form['comment_filter']['comment']['#prefix'] = '</div><div class="comment-right">';
    }
    $form['preview']['#suffix'] = '</div>';
  }*/
  $form['comment_filter']['comment']['#resizable'] = FALSE;
  array_unshift($form['#validate'], 'conimbo_comment_form_validate');
}

/**
 * Custom comment form validation.
 */
function conimbo_comment_form_validate($form, &$form_state) {
  if (trim($form_state['values']['homepage']) == 'http://') {
  	$form_state['values']['homepage'] = '';
  }
}

/**
 * Remove the node on the comment preview.
 */
function conimbo_comment_form_remove_node_on_preview($form) {
  unset($form['#suffix']);
  return $form;
}