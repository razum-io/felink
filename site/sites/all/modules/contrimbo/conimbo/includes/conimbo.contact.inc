<?php

/**
 * @file
 * Contact settings.
 */

function conimbo_contact_settings(&$form) {
  global $language;

  $lang = isset($_REQUEST['language']) ? $_REQUEST['language'] : '';
  if ($lang == '') {
    $lang = $language->language;
  }

  $form['contact_mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => t('[!category] !subject', array('!category' => '!category', '!subject' => '!subject'), $lang),
    '#description' => t('Subject of the contact email. Available variables are: !category and !subject')
  );

  $form['contact_mail_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => t('!name sent a message using the contact form at !form.', array('!name' => '!name', '!form' => '!form'), $lang),
    '#description' => t('Body of the contact email. Available variables are: !name and !form')
  );

  $form['contact_mail_thanks'] = array(
    '#type' => 'textfield',
    '#title' => t('Thank you message'),
    '#default_value' => t('Your message has been sent.', array(), $lang),
    '#description' => t('Thank you message shown to user after sending a contact mail.')
  );

  $form['#submit'][] = 'conimbo_contact_settings_submit';
}

/**
 * Submit contact settings.
 */
function conimbo_contact_settings_submit($form, &$form_state) {

  global $language;

  $lang = isset($_REQUEST['language']) ? $_REQUEST['language'] : '';
  if ($lang == '') {
    $lang = $language->language;
  }

  include 'includes/locale.inc';

  $subject_source = '[!category] !subject';
  $body_source = '!name sent a message using the contact form at !form.';
  $thx_source = 'Your message has been sent.';

  $subject_lid = db_result(db_query("SELECT lid FROM {locales_source} WHERE source = '%s'", $subject_source));
  $body_lid = db_result(db_query("SELECT lid FROM {locales_source} WHERE source = '%s'", $body_source));
  $thx_lid = db_result(db_query("SELECT lid FROM {locales_source} WHERE source = '%s'", $thx_source));

  $translate_subject = array();
  $translate_subject['values']['lid'] = $subject_lid;
  $translate_subject['values']['translations'][$lang] = $form_state['values']['contact_mail_subject'];
  locale_translate_edit_form_submit(NULL, $translate_subject);

  $translate_body = array();
  $translate_body['values']['lid'] = $body_lid;
  $translate_body['values']['translations'][$lang] = $form_state['values']['contact_mail_body'];
  locale_translate_edit_form_submit(NULL, $translate_body);

  $translate_thx = array();
  $translate_thx['values']['lid'] = $thx_lid;
  $translate_thx['values']['translations'][$lang] = $form_state['values']['contact_mail_thanks'];
  locale_translate_edit_form_submit(NULL, $translate_thx);
}
