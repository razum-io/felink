<?php

/**
 * @file
 * Conimbo registry file.
 */

/**
 * Menu items.
 */
function _conimbo_menu() {
  $items = array();

  // Theming items.
  $items['admin/build/themes/export'] = array(
    'title' => 'Export theme',
    'page callback' => 'conimbo_export_theme',
    'access arguments' => array('export themes'),
    'file' => 'includes/conimbo.theme.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/build/themes/delete'] = array(
    'title' => 'Delete theme',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conimbo_delete_theme'),
    'access arguments' => array('delete themes'),
    'file' => 'includes/conimbo.theme.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/build/themes/import'] = array(
    'title' => 'Import theme',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conimbo_import_theme'),
    'access arguments' => array('import themes'),
    'file' => 'includes/conimbo.theme.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/build/themes/plugins'] = array(
    'title' => 'Plugins',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conimbo_theme_plugin_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/conimbo.theme.inc',
    'type' => MENU_LOCAL_TASK,
  );

  if (module_exists('flag')) {
    $flags = flag_get_flags('node');
    foreach ($flags as $key => $flag) {
      $items['flag/' . $flag->fid . '/unflag-all'] = array(
        'title' => 'Unflag flagged items',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('conimbo_flag_unflag_all_confirm', 1),
        'access callback' => 'user_is_logged_in',
        'type' => MENU_CALLBACK,
      );
    }
    $items['ajax/getflagcounter'] = array(
      'title' => 'Ajax call flag counter',
      'page callback' => 'conimbo_ajax_flag_counter',
      'access callback' => 'user_is_logged_in',
      'type' => MENU_CALLBACK,
    );
  }

  $items['robots.txt'] = array(
    'page callback' => 'conimbo_robotstxt_robots',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  // Callback for handling access denied redirection.
  $items['conimbo/denied'] = array(
    'access callback' => TRUE,
    'page callback' => 'conimbo_denied',
    'title' => 'Access denied',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Conimbo menu alter.
 */
function _conimbo_menu_alter(&$items) {
  $items['admin/content/node/overview']['title'] = t('All');

  // set default theme to default configure tab.
  $default_theme = variable_get('theme_default', 'clean_theme');
  $items['admin/build/themes/settings/'. $default_theme]['type'] = 136;
  $items['admin/build/themes/settings/'. $default_theme]['weight'] = -1;
  $items['admin/build/themes/settings']['page arguments'] = $items['admin/build/themes/settings/'. $default_theme]['page arguments'];
  $items['admin/build/themes/settings']['access arguments'] = $items['admin/build/themes/settings/'. $default_theme]['access arguments'];
  $items['admin/build/themes/settings']['access callback'] = $items['admin/build/themes/settings/'. $default_theme]['access callback'];

  // only real admin can see the global settings + backend theme.
  $items['admin/build/themes/settings/global'] += $items['admin/build/themes/settings'];
  $items['admin/build/themes/settings/global']['weight'] = 20;
  $items['admin/build/themes/settings/global']['type'] = 128;
  $items['admin/build/themes/settings/global']['access callback'] = 'user_is_admin';
  $backend_theme = variable_get('admin_theme', 'rubik');
  $items['admin/build/themes/settings/'. $backend_theme]['access callback'] = 'user_is_admin';

  // Overwrite default node page.
  $items['node']['page callback'] = 'conimbo_page_default';

  // Remove block administration pages for themes, as our blocks are theme independant.
  foreach (list_themes() as $key => $theme) {
    unset($items['admin/build/block/list/'. $key]);
  }
}

/**
 * Theme registry.
 */
function _conimbo_theme() {
  $path = drupal_get_path('module', 'conimbo');

  $theme_functions = array(
    'conimbo_formatter_filter_url' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme.inc',
      'path' => $path .'/theme',
    ),
    'conimbo_formatter_nl2br' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme.inc',
      'path' => $path .'/theme',
    ),
    'conimbo_formatter_cover_image' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme.inc',
      'path' => $path .'/theme',
    ),
    'conimbo_teaser' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme.inc',
      'path' => $path .'/theme',
    ),
    'conimbo_formatter_file_download' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme.inc',
      'path' => $path .'/theme',
    ),
    'conimbo_formatter_file_preview' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme.inc',
      'path' => $path .'/theme',
    ),
    'conimbo_read_more' => array(
      'arguments' => array('field' => NULL),
      'file' => 'theme.inc',
      'path' => $path .'/theme',
    ),
    'conimbo_system_theme_settings_form' => array(
      'arguments' => array('form' => NULL),
      'file' => 'theme.inc',
      'path' => $path .'/theme',
    ),
    'flash' => array(
      'arguments' => array('file' => ''),
      'file' => 'theme.inc',
      'path' => $path .'/theme',
    ),
    'designkit_colorpicker' => array(
      'arguments' => array(),
    ),
    'colors' => array(
      'template' => 'colors',
      'arguments' => array('color' => array()),
      'path' => $path .'/plugins',
    ),
    'conimbo_system_modules' => array(
      'arguments' => array('form' => NULL),
    ),
    'conimbo_addthis_buttons' => array(
      'arguments' => array(
        'buttons' => NULL,
        'button_compact' => NULL,
        'url' => NULL,
      ),
      'template' => 'conimbo_addthis_buttons',
      'path' => drupal_get_path('module', 'conimbo') . '/templates'
    ),
  );

  if (function_exists('imagecache_presets')) {
  	module_load_include('inc', 'conimbo', 'includes/conimbo.media_player');
  	$nice_dash_video = nice_dash_get_plugin('nice_dash_video');
    foreach (imagecache_presets() as $preset) {
      $theme_functions['conimbo_formatter_first_image_' . $preset['presetname']] = array(
        'arguments' => array('element' => NULL),
        'file' => 'theme.inc',
        'path' => $path .'/theme',
        'function' => 'theme_conimbo_formatter_first_image',
      );
      
      // Register media player theme implementation if video functionality is enabled.
   		if ($nice_dash_video->video_feature_enabled() &&
   				_conimbo_media_player_preset_to_format($preset)) {
	      $theme_functions['conimbo_formatter_media_player_' . $preset['presetname']] = array(
	        'arguments' => array('element' => NULL),
	        'file' => 'theme.inc',
	        'path' => $path .'/theme',
	        'function' => 'theme_conimbo_formatter_media_player',
	      );
   		}
    }
  }

  // Table isn't available first time date_theme() is called in update.php.
  if (db_table_exists('date_format_types')) {
    $format_types = date_get_format_types('', TRUE);
    if (!empty($format_types)) {
      foreach ($format_types as $type => $type_info) {
        $theme_functions['conimbo_formatter_multilingual_' . $type] = array(
          'file' => 'theme.inc',
          'path' => $path .'/theme',
          'arguments' => array('element' => NULL),
          'function' => 'theme_conimbo_formatter_multilingual_date',
        );
      }
    }
  }
  return $theme_functions;
}

/**
 * Field formatters info.
 */
function _conimbo_field_formatter_info() {
  $formatters = array(
    'filter_url' => array(
      'label' => t('URL filter'),
      'field types' => array('text'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'nl2br' => array(
      'label' => t('Linebreak'),
      'field types' => array('text'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'cover_image' => array(
      'label' => t('Cover image'),
      'field types' => array('image', 'filefield'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'file_download' => array(
      'label' => t('Download link'),
      'field types' => array('image', 'filefield'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'file_preview' => array(
      'label' => t('Preview link'),
      'field types' => array('image', 'filefield'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );

  // First image (of multiple images) formatter for every imagecache preset.
  if (function_exists('imagecache_presets')) {
    foreach (imagecache_presets() as $preset) {
      $formatters['first_image_' . $preset['presetname']] = array(
        'label' => t('First @preset image of multiple', array('@preset' => $preset['presetname'])),
        'field types' => array('image', 'filefield'),
        'description' => t('Displays first of multiple images.'),
        'multiple values' => CONTENT_HANDLE_CORE,
      );
    }
  }

  $format_types = date_get_format_types('', TRUE);
  if (!empty($format_types)) {
    foreach ($format_types as $type => $type_info) {
      $formatters['multilingual_' . $type] = array(
        //'label' => $type_info['title'],
        'label' => t('Multilingual @type date', array('@type' => $type)),
        'field types' => array('date', 'datestamp', 'datetime'),
        'multiple values' => CONTENT_HANDLE_CORE,
      );
    }
  }
  return $formatters;
}

/**
 * Default views alter.
 */
function _conimbo_views_default_views_alter(&$views) {
  foreach($views as $key => $view) {
    // Node Picker.
    // Hide our back-end node-types.
    if ($key == 'nodepicker_nodes') {
      if (isset($view->display['default']->display_options['filters']['type'])) {
        unset($view->display['default']->display_options['filters']['type']);
        $view->display['default']->display_options['filters']['type_1'] =  array(
          'operator' => 'not in',
          'value' => array(
            'error' => 'error',
            'views_text' => 'views_text',
            'call_to_action' => 'call_to_action',
          ),
          'group' => '0',
          'exposed' => FALSE,
          'expose' => array(
            'operator' => FALSE,
            'label' => '',
          ),
          'id' => 'type_1',
          'table' => 'node',
          'field' => 'type',
          'relationship' => 'none',
        );
        $view->display['default']->display_options['filters']['type'] = array(
          'operator' => 'in',
          'value' => array(
            'news' => 'news',
            'page' => 'page',
            'job' => 'job',
            'document' => 'document',
            'location' => 'location',
            'product' => 'product',
            'product_category' => 'product_category',
            'event' => 'event',
            'panel' => 'panel',
            'faq' => 'faq',
          ),
          'group' => '0',
          'exposed' => TRUE,
          'expose' => array(
            'use_operator' => 0,
            'operator' => 'type_op',
            'identifier' => 'type',
            'label' => 'Node: Type',
            'optional' => 1,
            'single' => 1,
            'remember' => 0,
            'reduce' => 1,
          ),
          'id' => 'type',
          'table' => 'node',
          'field' => 'type',
          'relationship' => 'none',
        );
      }
      // Remove fields to reset them after language field for correct order in Nodepicker popup window.
      if (isset($view->display['default']->display_options['fields']['title_1'])) {
      	$title1 = $view->display['default']->display_options['fields']['title_1'];
        unset($view->display['default']->display_options['fields']['title_1']);
      }
      if (isset($view->display['default']->display_options['fields']['type'])) {
        $type = $view->display['default']->display_options['fields']['type'];
        unset($view->display['default']->display_options['fields']['type']);
      }
      if (isset($view->display['default']->display_options['fields']['nothing'])) {
        $nothing = $view->display['default']->display_options['fields']['nothing'];
        unset($view->display['default']->display_options['fields']['nothing']);
      }

      // Add field language
      $view->display['default']->display_options['fields']['language'] = array(
        'label' => 'Language',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
         ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_node' => 0,
        'exclude' => 0,
        'id' => 'language',
        'table' => 'node',
        'field' => 'language',
        'relationship' => 'none',
      );
      // Reset fields after language field.
      if (isset($title1)) {
      	$view->display['default']->display_options['fields']['title_1'] = $title1;
      }
      if (isset($type)) {
        $view->display['default']->display_options['fields']['type'] = $type;
      }
      if (isset($nothing)) {
        $view->display['default']->display_options['fields']['nothing'] = $nothing;
      }
      if (isset($view->display['default']->display_options['style_options']['info'])) {
        $view->display['default']->display_options['style_options']['info']['language']['sortable'] = 1;
        $view->display['default']->display_options['style_options']['info']['language']['separator'] = '';
      }
    }
    // Node picker taxonomy. Have nodepicker insert source path, not alias
    // to prevent language prefix appearing twice in the path.
    if ($key == 'nodepicker_taxonomy') {
      if (isset($view->display['default']->display_options['fields']['nothing'])) {
        $custom_field = $view->display['default']->display_options['fields']['nothing'];
        unset($view->display['default']->display_options['fields']['nothing']);
        $view->display['default']->display_options['fields']['name_2'] = array(
          'label' => '',
          'alter' => array(
            'alter_text' => 0,
            'text' => '',
            'make_link' => 0,
            'path' => '',
            'link_class' => '',
            'alt' => '',
            'prefix' => '',
            'suffix' => '',
            'target' => '',
            'help' => '',
            'trim' => 0,
            'max_length' => '',
            'word_boundary' => 1,
            'ellipsis' => 1,
            'html' => 0,
            'strip_tags' => 0,
          ),
          'empty' => '',
          'hide_empty' => 0,
          'empty_zero' => 0,
          'link_to_taxonomy' => 0,
          'exclude' => 1,
          'id' => 'name_2',
          'table' => 'term_data',
          'field' => 'name',
          'relationship' => 'none',
        );
        $view->display['default']->display_options['fields']['nothing'] = $custom_field;
        $view->display['default']->display_options['fields']['nothing']['alter']['text'] = '<a href="taxonomy/term/[tid]" class="nodepicker-insert" title="[name_2]">Insert link</a>';
        $view->display['default']->display_options['fields']['nothing']['alter']['make_link'] = 0;
      }
    }

    // Some modifications to nodequeues.
    if ($view->tag == 'nodequeue') {
      if (isset($view->display['default']->display_options['sorts']['position'])) {
        // New nodes on top.
        $view->display['default']->display_options['sorts']['position']['order'] = 'DESC';
        // Only show nodes in current language.
        $view->display['default']->display_options['filters']['language']= array(
          'operator' => 'in',
          'value' => array(
             '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
          ),
          'group' => '0',
          'exposed' => FALSE,
          'expose' => array(
            'operator' => FALSE,
            'label' => '',
          ),
          'id' => 'language',
          'table' => 'node',
          'field' => 'language',
          'relationship' => 'none',
        );
        // No limit to nr. of items
        $view->display['default']->display_options['items_per_page'] = 0;
        // No pager.
        $view->display['default']->display_options['use_pager'] = 0;
        // No 'Queue' mentioned before title please, and no single quotes.
        $view->display['default']->display_options['title'] = str_replace('Queue', '', $view->display['default']->display_options['title']);
      }
      if (isset($view->display['block'])) {
        $view->display['block']->display_options['defaults']['items_per_page'] = TRUE;
        $view->display['block']->display_options['defaults']['use_more'] = FALSE;
        $view->display['block']->display_options['row_plugin'] = 'ds';
        $view->display['block']->display_options['row_options'] = array(
          'build_mode' => 'block',
          'advanced' => '0',
        );
        $view->display['block']->display_options['use_more'] = 0;
      }
    }
  }
 }
