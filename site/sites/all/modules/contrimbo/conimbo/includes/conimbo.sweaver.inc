<?php

/**
 * Implementation of hook_default_sweaver_property().
 */
function _conimbo_sweaver_selectors() {
  $selectors = array();

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'toolbar-outer';
  $selector->description = 'the toolbar wrapper';
  $selector->selector_selector = '#toolbar-outer'; // css selector.
  $selector->selector_highlight = FALSE; // Whether to highlight the selector in the active path.
  $selectors['toolbar-outer'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'toolbar';
  $selector->description = 'the toolbar';
  $selector->selector_selector = '#toolbar'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['toolbar'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'header-outer';
  $selector->description = 'the header wrapper';
  $selector->selector_selector = '#header-outer'; // css selector.
  $selector->selector_highlight = FALSE; // Whether to highlight the selector in the active path.
  $selectors['header-outer'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'header';
  $selector->description = 'the header';
  $selector->selector_selector = '#header'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['header'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'service-navigation';
  $selector->description = 'the service navigation';
  $selector->selector_selector = '#service-navigation'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['service-navigation'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'logo';
  $selector->description = 'the logo';
  $selector->selector_selector = '#logo'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['logo'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'navigation-outer';
  $selector->description = 'the navigation wrapper';
  $selector->selector_selector = '#navigation-outer'; // css selector.
  $selector->selector_highlight = FALSE; // Whether to highlight the selector in the active path.
  $selectors['navigation-outer'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'menu-horizontal';
  $selector->description = 'the horizontal navigation';
  $selector->selector_selector = '.menu-horizontal'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['menu-horizontal'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'menu-vertical';
  $selector->description = 'the vertical navigation';
  $selector->selector_selector = '.menu-vertical'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['menu-vertical'] = $selector;


  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'subheader-outer';
  $selector->description = 'the subheader wrapper';
  $selector->selector_selector = '#subheader-outer'; // css selector.
  $selector->selector_highlight = FALSE; // Whether to highlight the selector in the active path.
  $selectors['subheader-outer'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'subheader';
  $selector->description = 'the subheader';
  $selector->selector_selector = '#subheader'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['subheader'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'content-outer';
  $selector->description = 'the content wrapper';
  $selector->selector_selector = '#content-outer'; // css selector.
  $selector->selector_highlight = FALSE; // Whether to highlight the selector in the active path.
  $selectors['content-outer'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'content';
  $selector->description = 'the content region';
  $selector->selector_selector = '#content'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['content'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'content-middle-grid';
  $selector->description = 'the content middle region';
  $selector->selector_selector = '#content-middle-grid'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['content-middle-grid'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'sidebar';
  $selector->description = 'a sidebar';
  $selector->selector_selector = '.sidebar'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['sidebar'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'doormat-outer';
  $selector->description = 'the doormat wrapper';
  $selector->selector_selector = '#doormat-outer'; // css selector.
  $selector->selector_highlight = FALSE; // Whether to highlight the selector in the active path.
  $selectors['doormat-outer'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'doormat';
  $selector->description = 'the doormat';
  $selector->selector_selector = '#doormat'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['doormat'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'footer-outer';
  $selector->description = 'the footer wrapper';
  $selector->selector_selector = '#footer-outer'; // css selector.
  $selector->selector_highlight = FALSE; // Whether to highlight the selector in the active path.
  $selectors['footer-outer'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'footer';
  $selector->description = 'the footer';
  $selector->selector_selector = '#footer'; // css selector.
  $selector->selector_highlight = TRUE; // Whether to highlight the selector in the active path.
  $selectors['footer'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'body_inner';
  $selector->description = 'the body inner region';
  $selector->selector_selector = '#body-inner';
  $selector->selector_highlight = FALSE;
  $selectors['body_inner'] = $selector;

  $selector = new stdClass;
  $selector->api_version = 1;
  $selector->disabled = FALSE; // Set this to true if you want to disable this by default.
  $selector->name = 'breadcrumb-outer';
  $selector->description = 'the breadcrumb wrapper';
  $selector->selector_selector = '#breadcrumb-outer'; // css selector.
  $selector->selector_highlight = FALSE; // Whether to highlight the selector in the active path.
  $selectors['breadcrumb-outer'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'breadcrumb';
  $selector->description = 'the breadcrumb';
  $selector->selector_selector = '#breadcrumb';
  $selector->selector_highlight = TRUE;
  $selectors['breadcrumb'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'status';
  $selector->description = 'a status message';
  $selector->selector_selector = '.status';
  $selector->selector_highlight = TRUE;
  $selectors['status'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'help';
  $selector->description = 'the help region';
  $selector->selector_selector = '#help';
  $selector->selector_highlight = TRUE;
  $selectors['help'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'warning';
  $selector->description = 'a warning';
  $selector->selector_selector = '.warning';
  $selector->selector_highlight = TRUE;
  $selectors['warning'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'error';
  $selector->description = 'an error message';
  $selector->selector_selector = '.error';
  $selector->selector_highlight = TRUE;
  $selectors['error'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'the_block';
  $selector->description = 'a block';
  $selector->selector_selector = '.block';
  $selector->selector_highlight = TRUE;
  $selectors['the_block'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'block_title';
  $selector->description = 'a block title';
  $selector->selector_selector = '.block-title';
  $selector->selector_highlight = TRUE;
  $selectors['block_title'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'panel_panel';
  $selector->description = 'a panel region';
  $selector->selector_selector = '.panel-panel';
  $selector->selector_highlight = FALSE;
  $selectors['panel_panel'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'panel_pane';
  $selector->description = 'a pane';
  $selector->selector_selector = '.panel-pane';
  $selector->selector_highlight = TRUE;
  $selectors['panel_pane'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'pane_title';
  $selector->description = 'a pane title';
  $selector->selector_selector = '.pane-title';
  $selector->selector_highlight = TRUE;
  $selectors['pane_title'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'panel_col';
  $selector->description = 'a panel column';
  $selector->selector_selector = '.panel-col';
  $selector->selector_highlight = FALSE;
  $selectors['panel_col'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'panel_col_first';
  $selector->description = 'the first panel column';
  $selector->selector_selector = '.panel-col-first';
  $selector->selector_highlight = FALSE;
  $selectors['panel_col_first'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'panel_col_last';
  $selector->description = 'the last panel column';
  $selector->selector_selector = '.panel-col-last';
  $selector->selector_highlight = FALSE;
  $selectors['panel_col_last'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'pager';
  $selector->description = 'a pager';
  $selector->selector_selector = '.pager';
  $selector->selector_highlight = TRUE;
  $selectors['pager'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'searchform';
  $selector->description = 'the search form';
  $selector->selector_selector = '#search-theme-form';
  $selector->selector_highlight = TRUE;
  $selectors['searchform'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'read_more_button';
  $selector->description = 'a read more button';
  $selector->selector_selector = '.field-read-more';
  $selector->selector_highlight = TRUE;
  $selectors['read_more_button'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'small_button';
  $selector->description = 'a small button';
  $selector->selector_selector = '.small_button';
  $selector->selector_highlight = TRUE;
  $selectors['small_button'] = $selector;

  $selector = new stdClass;
  $selector->disabled = FALSE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'front';
  $selector->description = 'the homepage';
  $selector->selector_selector = '.front';
  $selector->selector_highlight = FALSE;
  $selectors['front'] = $selector;

  $selector = new stdClass;
  $selector->disabled = TRUE; /* Edit this to true to make a default selector disabled initially */
  $selector->api_version = 1;
  $selector->name = 'div';
  $selector->description = 'a region';
  $selector->selector_selector = 'div';
  $selector->selector_highlight = FALSE;
  $selectors['div'] = $selector;

  return $selectors;
}