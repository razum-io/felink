<?php

/**
 * @file
 * Generate imagecache file on submit if wanted.
 */

/**
 * Form alter to add checkbox to CCK file field.
 */
function conimbo_imagecache_generate_field_form_alter(&$form) {

  if ($form['widget_module']['#value'] == 'imagefield' || $form['widget_module']['#value'] == 'swfupload') {

    $imagefield = $form['field_name']['#value'];

    $form['conimbo_imagecache_fieldname'] = array(
      '#type' => 'value',
      '#value' => $imagefield,
    );

    $form['conimbo_imagecache'] = array(
      '#type' => 'fieldset',
      '#title' => t('Conimbo field settings'),
      '#weight' => 0,
    );
    $form['conimbo_imagecache']['conimbo_generate_imagecache'] = array(
      '#type' => 'checkbox',
      '#title' => t('Generate imagecache on submit'),
      '#default_value' => variable_get('cgi_'. $imagefield, FALSE),
    );
    $form['#submit'][] = 'conimbo_imagecache_generate_field_form_alter_submit';
  }
}

/**
 * Save form alter of field.
 */
function conimbo_imagecache_generate_field_form_alter_submit($form, &$form_state) {
  variable_set('cgi_'. $form_state['values']['conimbo_imagecache_fieldname'], $form_state['values']['conimbo_generate_imagecache']);
}
