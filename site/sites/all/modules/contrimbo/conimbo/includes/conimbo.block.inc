<?php

/**
 * @file
 * Block settings.
 */

function conimbo_block_settings(&$form) {

  // Content type based visibility settings.
  $default_content_types = array();
  if (isset($form['module']) &&
      isset($form['delta'])) {
    $result = db_query("SELECT type FROM {conimbo_block_node_type} WHERE module = '%s' and delta = '%s'", $form['module']['#value'], $form['delta']['#value']);
    while ($block_type = db_fetch_object($result)) {
      $default_content_types[] = $block_type->type;
    }
  }

  $nodes = node_get_types('names');
  $form['node_type_vis_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Content type specific visibility settings'),
      '#collapsible' => TRUE,
  );
  $form['node_type_vis_settings']['types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show block for specific content types'),
    '#default_value' => $default_content_types,
    '#options' => $nodes,
    '#description' => t('Show this block only on pages that display content of the given type(s). If you select no types, there will be no type-specific limitation.'),
  );

  // Style settings
  $theme_key = variable_get('theme_default', 'clean_theme');
  $info = db_result(db_query("SELECT info FROM {system} WHERE name = '%s' AND type ='theme'", $theme_key));
  $theme_info = unserialize($info);

  $options = array('' => 'No style');

  if ($theme_info['block_styles']) {
    foreach ($theme_info['block_styles'] as $key => $value) {
      $options[$value['class']] = $value['label'];
    }
  }

  $form['styles']['#type'] = 'fieldset';
  $form['styles']['#title'] = 'Choose a style';
  $form['styles']['#collapsible'] = TRUE;
  $form['styles']['style']['#type'] = 'select';
  $form['styles']['style']['#options'] = $options;

  $default_style = variable_get('block_styles', array());
  $form['styles']['style']['#default_value'] = isset($default_style[$form['module']['#value'] .'_'. $form['delta']['#value']]) ? $default_style[$form['module']['#value'] .'_'. $form['delta']['#value']] : '';

  $form['#submit'][] = 'conimbo_save_block_settings';
}

/**
 * Submit callback: save the block style in a variable
 */
function conimbo_save_block_settings($form, &$form_state) {
  // Content type based visibility settings.
  if (isset($form_state['values']['module']) &&
      isset($form_state['values']['delta']) &&
      isset($form_state['values']['types'])) {
    db_query("DELETE FROM {conimbo_block_node_type} WHERE module = '%s' and delta = '%s'", $form_state['values']['module'], $form_state['values']['delta']);
    foreach($form_state['values']['types'] as $key => $type) {
    	if ($type) {
    		db_query("INSERT INTO {conimbo_block_node_type} (module, delta, type) VALUES ('%s', '%s', '%s')", $form_state['values']['module'], $form_state['values']['delta'], $type);
    	}
    }
  }

  // Style settings
  $block_styles = variable_get('block_styles', array());
  if (isset($form_state['values']['style'])) {
    $block_styles[$form_state['values']['module'] .'_'. $form_state['values']['delta']] = $form_state['values']['style'];
  }
  else {
    unset($block_styles[$form_state['values']['module'] .'_'. $form_state['values']['delta']]);
  }
  variable_set('block_styles', $block_styles);
}
