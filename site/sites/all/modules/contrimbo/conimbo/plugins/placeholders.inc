<?php

/**
 * @file
 * Adds placeholders to the form.
 */

function _placeholders(&$form, $theme_key, $theme_settings, $theme_info) {

  if (isset($theme_info['placeholders'])) {

    $keys = array();

    drupal_add_js(drupal_get_path('module', 'conimbo') .'/plugins/placeholders.js');

    $form['placeholders'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('Visuals'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#prefix' => '<div id="placeholders">',
      '#suffix' => '</div>',
    );

    $form['#placeholders'] = $theme_info['placeholders'];
    foreach ($theme_info['placeholders'] as $key => $value) {

      // Uploads.
      $dir = file_directory_path();
      if ($file = file_save_upload('placeholder_upload_'. $key, array('file_validate_extensions' => array('jpg png gif swf')), $dir, FILE_EXISTS_REPLACE)) {
        $parts = pathinfo($file->filepath);
        $filename = file_directory_path() . '/placeholder_'. $theme_key .'_'. $key .'.'. $parts['extension'];
        // The image was saved using file_save_upload() and was added to the
        // files table as a temporary file. We'll make a copy and let the garbage
        // collector delete the original upload.
        if (file_copy($file, $filename, FILE_EXISTS_REPLACE)) {
          $_POST['placeholder_default_'. $key] = 0;
          $_POST['placeholder_path_'. $key] = $filename;
          drupal_set_message($message = t('Your new visual placeholder has been saved at: ').$file->filepath, $type = 'status', $repeat = TRUE);
        }
      } else {
        if ($theme_settings['placeholder_default_'. $key]) {
          $_POST['placeholder_path_'. $key] = '';
          $theme_settings['placeholder_path_'. $key] = '';
        }
      }

      // Form.
      $form['placeholders']['placeholder_container_'. $key] = array(
        '#type'          => 'fieldset',
        '#title'         => $value['title'],
        '#description'         => $value['description'],
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      if (isset($value['background']) && isset($value['background-repeat'])){
        $keys[] = 'placeholder_background_repeat_'. $key;
        $form['placeholders']['placeholder_container_'. $key]['placeholder_background_repeat_'. $key] = array(
          '#type'          => 'select',
          '#title'         => 'Background repeat',
          '#default_value' => isset($theme_settings['placeholder_background_repeat_'. $key]) ? $theme_settings['placeholder_background_repeat_'. $key] : $theme_info['placeholders'][$key]['background-repeat'],
          '#options'       => array(
            'default' => 'Default',
            'no-repeat' => 'No repeat',
            'repeat-x' => 'Repeat horizontally',
            'repeat-y' => 'Repeat vertically',
            'repeat' => 'Repeat horizontally and vertically',
          ),
        );
      }

      $keys[] = 'placeholder_default_'. $key;
      $form['placeholders']['placeholder_container_'. $key]['placeholder_default_'. $key] = array(
        '#type' => 'checkbox',
        '#title' => 'Use the default placeholder image or animation',
        '#default_value' => isset($theme_settings['placeholder_default_'. $key]) ? $theme_settings['placeholder_default_'. $key] : TRUE,
        '#tree' => FALSE,
        '#description' => t('Check here if you want the theme to use the placeholder supplied with it.')
      );

      $keys[] = 'placeholder_path_'. $key;
      $form['placeholders']['placeholder_container_'. $key]['placeholder_path_'. $key] = array(
        '#type' => 'textfield',
        '#title' => 'Placeholder image path',
        '#default_value' => $theme_settings['placeholder_path_'. $key],
      );

      $form['placeholders']['placeholder_container_'. $key]['placeholder_upload_'. $key] = array(
        '#type' => 'file',
        '#title' => 'Upload new placeholder image or animation',
      );

      $keys[] = 'placeholder_alt_'. $key;
      $form['placeholders']['placeholder_container_'. $key]['placeholder_alt_'. $key] = array(
        '#type' => 'textfield',
        '#title' => 'Placeholder alternative text',
        '#default_value' => $theme_settings['placeholder_alt_'. $key],
      );

    }
    $form['#submit'][] = '_placeholders_submit';

    if (isset($form['#sweaver_other_themesettings'])) {
      $form['#sweaver_other_themesettings'] += $keys;
    }
  }
}


/**
 * Submit callback. Save the settings for the placeholders.
 */
function _placeholders_submit($form, &$form_state) {
  $theme_key = arg(4);
  // fallback when on default tab
  if (empty($theme_key)) {
    $theme_key = $form['var']['#value'];
    $theme_parts = explode('_', $theme_key);
    unset($theme_parts[0]);
    unset($theme_parts[count($theme_parts)]);
    $theme_key = implode('_', $theme_parts);
  }

  $values = $form_state['values'];
  $css = '';
  $placeholders = array();

  $info = db_result(db_query("SELECT info FROM {system} WHERE name = '%s' AND type ='theme'", $theme_key));
  $theme_info = unserialize($info);

  foreach ($theme_info['placeholders'] as $name => $placeholder_info) {
    if (isset($placeholder_info['background']) && isset($values['placeholder_path_'. $name]) && !$values['placeholder_default_'. $name]){
      $placeholders[$name] = base_path() . $values['placeholder_path_'. $name];
    }
    if (isset($values['placeholder_background_repeat_'. $name]) && $values['placeholder_background_repeat_'. $name] != 'default' && $values['placeholder_background_repeat_'. $name]){
      $placeholders[$name .'-repeat'] = $values['placeholder_background_repeat_'. $name];
    }
  }

  // write or delete css file
  $cssFile = file_directory_path() .'/conimbo-'. $theme_key .'-placeholders.css';

  if (!empty($placeholders)) {
    $theme_placeholder_file = drupal_get_path('theme', $theme_key) .'/tpl/placeholders.tpl.php';
    if (!file_exists($theme_placeholder_file)) {
      $theme_placeholder_file = 'placeholders.tpl.php';
    }

    ob_start();
    include_once($theme_placeholder_file);
    $output = ob_get_contents();
    ob_end_clean();

    // Construct CSS file
    $fh = fopen($cssFile, 'w');
    if ($fh) {
      fwrite($fh, $output); // write css to file
    } else {
      drupal_set_message(t('Cannot write placeholder css file, please check your file system. (Visit status report page)'), 'error');
    }
    fclose($fh);
  }
  else {
    if (file_exists($cssFile)) {
      unlink($cssFile);
    }
  }

  // Clear css cache.
  drupal_clear_css_cache();
}
