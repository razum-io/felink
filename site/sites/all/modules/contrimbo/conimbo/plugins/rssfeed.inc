<?php

/**
 * @file
 * Alters the form to rss feeds options.
 */

function _rssfeed(&$form, $theme_key, $theme_settings, $theme_info) {

  $keys = array('rss_feed_type', 'rss_icon_default', 'rss_icon_path');
  if (isset($form['#sweaver_other_themesettings'])) {
    $form['#sweaver_other_themesettings'] = array_merge($keys, $form['#sweaver_other_themesettings']);
  }

  // Save RSS icon upload.
  $dir = file_directory_path();
  if ($file = file_save_upload('rss_icon_upload', array('file_validate_extensions' => array('jpg png gif')), $dir, FILE_EXISTS_REPLACE)) {
    $parts = pathinfo($file->filepath);
    $filename = file_directory_path() . '/rssicon.'. $parts['extension'];
    // The image was saved using file_save_upload() and was added to the
    // files table as a temporary file. We'll make a copy and let the garbage
    // collector delete the original upload.
    if (file_copy($file, $filename, FILE_EXISTS_REPLACE)) {
      $_POST['rss_icon_default'] = 0;
      $_POST['rss_icon_path'] = $filename;
      drupal_set_message($message = t('Your new rss placeholder has been saved at: '). $file->filepath, $type = 'status', $repeat = TRUE);
    }
  }
  else {
    if ($theme_settings['rss_icon_default']) {
      $_POST['rss_icon_path'] = '';
      $theme_settings['rss_icon_path'] = '';
    }
  }

  // Form.
  $form['rssfeed'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('RSS feeds'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['rssfeed']['rss_feed_type'] = array(
    '#type'          => 'select',
    '#title'         => t('RSS feed type'),
    '#description'   => t('This setting affects all links to rss feeds in the entire site.'),
    '#default_value' => isset($theme_settings['rss_feed_type']) ? $theme_settings['rss_feed_type'] : 3,
    '#options'       => array(
      1   => t('Icon only'),
      2   => t('Link only'),
      3   => t('Icon + link'),
    )
  );

  $form['rssfeed']['rss_icon_default'] = array(
    '#type' => 'checkbox',
    '#title' => 'Use the default rss icon',
    '#default_value' => isset($theme_settings['rss_icon_default']) ? $theme_settings['rss_icon_default'] : TRUE,
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the rss icon supplied with it.')
  );

  $form['rssfeed']['rss_icon_path'] = array(
    '#type' => 'textfield',
    '#title' => 'RSS icon path',
    '#default_value' => $theme_settings['rss_icon_path'],
  );

  $form['rssfeed']['rss_icon_upload'] = array(
    '#type' => 'file',
    '#title' => 'Upload new rss icon',
  );
}

