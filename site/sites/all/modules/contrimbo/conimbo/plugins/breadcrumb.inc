<?php

/**
 * @file
 * Alters the form to add breadcrumb.
 */

function _breadcrumb(&$form, $theme_key, $theme_settings, $theme_info) {

  $keys = array('breadcrumb_on', 'breadcrumb_separator', 'breadcrumb_home');
  if (isset($form['#sweaver_other_themesettings'])) {
    $form['#sweaver_other_themesettings'] = array_merge($keys, $form['#sweaver_other_themesettings']);
  }

  // Form.
  $form['breadcrumb'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breadcrumb'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['breadcrumb']['breadcrumb_on'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => isset($theme_settings['breadcrumb_on']) ? $theme_settings['breadcrumb_on'] : FALSE,
  );

  $form['breadcrumb']['breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show breadcrumb on homepage'),
    '#default_value' => isset($theme_settings['breadcrumb_home']) ? $theme_settings['breadcrumb_home'] : FALSE,
  );

  $form['breadcrumb']['breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#default_value' => isset($theme_settings['breadcrumb_separator']) ? $theme_settings['breadcrumb_separator'] : '&gt;',
    '#size'          => 5,
    '#maxlength'     => 10,
  );
}

