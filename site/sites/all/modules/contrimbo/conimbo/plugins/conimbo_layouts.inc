<?php

/**
 * @file
 * Theme settings around context and layouts.
 */

/**
 * Alters the form to add existing contexts and the ability
 * to change the template for that context.
 */
function _conimbo_layouts(&$form, $theme_key, $theme_settings, $theme_info) {
  if (isset($theme_info['conimbo']['certified']) && $theme_info['conimbo']['certified'] == 'true') {
    $layouts = array(
      'one_column' => 'No sidebars',
      'left_sidebar' => 'Left sidebar',
      'right_sidebar' =>'Right sidebar',
      'sidebars_left_and_right' => 'Sidebars left and right',
      'sidebars_left' => 'Sidebars left',
      'sidebars_right' => 'Sidebars right',
    );

    $form['conimbo_layout'] = array(
      '#type' => 'fieldset',
      '#title' => t('Layouts'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t("Choose the position of your sidebars."),
      '#prefix' => '<div class="conimbo-layouts clear-block">',
      '#suffix' => '</div>',
    );

    // Create nice radio buttons :)
    $options = array();
    foreach ($layouts as $key => $layout) {
      $image = conimbo_get_layout_image($key);
      $options[$key] = $layout .'<br />'. $image;
    }

    $form['conimbo_layout']['layout'] = array(
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => isset($theme_settings['layout']) ? $theme_settings['layout'] : 'left_sidebar',
    );

    $keys = array('layout');
    if (isset($form['#sweaver_other_themesettings'])) {
      $form['#sweaver_other_themesettings'] = array_merge($keys, $form['#sweaver_other_themesettings']);
    }
  }
}

function _conimbo_get_layouts($theme_key, $options_or_default) {
  $result = db_query("SELECT info FROM {system} WHERE type = 'theme' and name = '%s'", $theme_key);
  while ($row = db_fetch_object($result)) {
    $info = unserialize($row->info);
  }
  return $info['layouts'][$options_or_default];
}

function _conimbo_get_layout($theme_key = NULL, $theme_settings = NULL, $theme_info = NULL) {
	$theme = isset($theme_key) ? $theme_key : variable_get('theme_default', '');
  $settings = isset($theme_settings) ? $theme_settings : variable_get(str_replace('/', '_', 'theme_'. $theme .'_settings'), array());
  $layout = isset($settings['layout']) ? $settings['layout'] : (isset($theme_info['layouts']['default']) ? $theme_info['layouts']['default'] : _conimbo_get_layouts($theme, 'default'));
  return $layout;
}

function conimbo_get_layout_image($layout = NULL) {
  $layout_key = isset($layout) ? $layout : _conimbo_get_layout($theme);
  $path = drupal_get_path('module', 'conimbo');
  return theme('image', $path . '/plugins/conimbo_layouts/'. $layout_key .'.png', '', '', array('class' => 'conimbo-layout-img'));
}