Drupal.behaviors.placeholder_config = function (context) {
  
  // check if 'default' is checked
  $("#placeholders .form-checkbox").each(function() {
    if ($(this).is(':checked')){
      $(this).parent().parent().nextAll().hide();
    }
  });
  
  // hide empty path fields
  $("#placeholders .form-text").each(function() {
    if ($(this).val() == ''){
      //$(this).hide();
      //$(this).siblings().eq(0).hide();      
    }
  }); 
  
  // toggle display on every check
  $("#placeholders .form-checkbox").change(function(){
    $(this).parent().parent().nextAll().toggle();    
  });
  
};