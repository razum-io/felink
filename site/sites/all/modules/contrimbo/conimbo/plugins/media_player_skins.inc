<?php

/**
 * @file
 * Theme settings concerning player skins.
 */

/**
 * Alters the form to allow choice theme video player skin.
 */
function _media_player_skins(&$form, $theme_key, $theme_settings, $theme_info) {
	$conimbo_media_player_skin = variable_get('conimbo_media_player_skin', array());
	// If theme skins are missing, check out eventual base theme skins.
	if (isset($theme_info['media_player_skins'])) {
		$conimbo_media_player_skin['theme'] = $theme_key;
	} 
	elseif (isset($theme_info['base theme'])) {
		$base_info = db_result(db_query("SELECT info FROM {system} WHERE name = '%s' AND type ='theme'", $theme_info['base theme']));
  	$base_theme_info = unserialize($base_info);
  	if (isset($base_theme_info['media_player_skins'])) {
  		$theme_info['media_player_skins'] = $base_theme_info['media_player_skins'];
  		$conimbo_media_player_skin['theme'] = $theme_info['base theme'];
  	}
	}
	variable_set('conimbo_media_player_skin', $conimbo_media_player_skin);
	
	if (isset($theme_info['media_player_skins'])) {
    $keys = array('media_player_skins');

    $options = $theme_info['media_player_skins'];

    $form['media_player_skins_container'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('Video player skins'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Choose the video player skin for your website.'),
    );

    $form['media_player_skins_container']['media_player_skin'] = array(
      '#type' => 'select',
      '#default_value' => $theme_settings['media_player_skin'],
      '#options' => $options,
    );
    
    $form['#submit'][] = '_media_player_skins_submit';
  }

}

/**
 * Submit callback. Save the settings for the media player skin.
 */
function _media_player_skins_submit($form, &$form_state) {
  $theme_key = arg(4);
  // fallback when on default tab
  if (empty($theme_key)) {
    $theme_key = $form['var']['#value'];
    $theme_parts = explode('_', $theme_key);
    unset($theme_parts[0]);
    unset($theme_parts[count($theme_parts)]);
    $theme_key = implode('_', $theme_parts);
  }
  
  if (isset($form_state['values']['media_player_skin'])) {
  	$conimbo_media_player_skin = variable_get('conimbo_media_player_skin', array());
  	if (!isset($conimbo_media_player_skin['theme'])) {
  		$conimbo_media_player_skin['theme'] = $theme_key;
  	}
  	$conimbo_media_player_skin['name'] = $form_state['values']['media_player_skin'];
  	$skin = base_path() . drupal_get_path('theme', $conimbo_media_player_skin['theme']) . '/skins/' . $conimbo_media_player_skin['name'] . '/' . $conimbo_media_player_skin['name'] . '.xml';
  	$conimbo_media_player_skin['path'] = $skin;
  	variable_set('conimbo_media_player_skin', $conimbo_media_player_skin);
  }  
}