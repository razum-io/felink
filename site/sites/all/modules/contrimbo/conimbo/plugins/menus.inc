<?php

/**
 * @file
 * Adds 4 menu options to the form.
 */
function _menus(&$form, $theme_key, $theme_settings, $theme_info) {

  if (isset($theme_info['menu'])) {
    $keys = array('menus');
    if (isset($form['#sweaver_other_themesettings'])) {
      $form['#sweaver_other_themesettings'] = array_merge($keys, $form['#sweaver_other_themesettings']);
    }

    $options = $theme_info['menu'];

    $form['menu_container'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('Navigation model'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Choose a navigation model for your website.<br />If you include a vertical navigation, you need to select a layout with at least one sidebar to control the location of your vertical navigation.'),
    );

    $form['menu_container']['menus'] = array(
      '#type' => 'select',
      '#default_value' => $theme_settings['menus'],
      '#options' => $options,
    );
  }
}
