<?php

/**
 * @file
 * Features plugin for Nice dashboard.
 */

class nice_dash_features {

  /**
   * Forms
   */
  function forms(&$forms) {
    $forms['nice_dash_features'] = array(
      'callback' => 'nice_dash_menu_callback_form',
      'callback arguments' => array('nice_dash_features', 'nice_dash_features_form'),
    );
  }

  /**
   * Widgets method.
   */
  function widgets() {
    $widgets = array(
      'features_configuration' => t('Configure features'),
    );
    return $widgets;
  }

  /**
   * Features configuration.
   */
  function nice_dash_features_widget_features_configuration() {

    $content = $this->nice_dash_features_overview();
    return array(
      'title' => ('Configure features'),
      'content' => $content,
    );
  }

  /**
   * Features
   */
  function features_list() {
    static $features_list = array();
    $features_definition = array(
      'news' => array(
        'title' => 'News',
        'operations' => array(
          'views' => array('News'),
          'content_type' => array('news'),
        ),
        'more_info' => 'Create news items on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/news',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/news',
            'title' => 'Content type configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'blogs' => array(
        'title' => 'Blogs',
        'operations' => array(
          'views' => array('Blogs'),
          'modules' => array('comment'),
          'dep_check' => array('comments'),
          'content_type' => array('blog'),
        ),
        'more_info' => 'Create blog items on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/blog',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/blog',
            'title' => 'Content type configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'twitter' => array(
        'title' => 'Twitter',
        'operations' => array(
          'modules' => array('twitter', 'tweetmeme'),
          'views' => array('twitter', 'tweets'),
        ),
        'more_info' => 'Import twitters and post articles to twitter',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/twitter',
            'title' => 'Twitter configuration'
          ),
          1 => array(
            'href' => 'admin/settings/tweetmeme',
            'title' => 'TweetMeme configuration'
          ),
        ),
        'group' => 'Social',
      ),
      'comments' => array(
        'title' => 'Comment',
        'operations' => array(
          'modules' => array('comment', 'cd'),
          'dep_check' => array('blog'),
        ),
        'more_info' => 'Enable comments on your website',
        'group' => 'Social',
      ),
      'voting' => array(
        'title' => 'Voting',
        'operations' => array(
          'modules' => array('votingapi', 'fivestar', 'nd_fivestar'),
          'dep_check' => array('poll'),
          'dep_check_true' => array('fivestar', 'nd_fivestar'),
          'views' => array('conimbo_voting'),
        ),
        'more_info' => 'Enable voting on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/fivestar',
            'title' => 'Voting configuration'
          ),
        ),
        'group' => 'Social',
      ),
      'faq' => array(
        'title' => 'FAQ',
        'operations' => array(
          'modules' => array('faq'),
        ),
        'more_info' => 'Create a FAQ on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/faq',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/faq',
            'title' => 'Content type configuration'
          ),
          2 => array(
            'href' => 'admin/settings/faq',
            'title' => 'FAQ configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'aggregator' => array(
        'title' => 'Simple RSS import',
        'operations' => array(
          'modules' => array('aggregator'),
        ),
        'more_info' => 'Import feeds into your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/content/aggregator',
            'title' => 'Aggregator configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'feeds' => array(
        'title' => 'Advanced RSS import',
        'operations' => array(
          'modules' => array('feeds', 'xml_parser', 'feeds_xpathparser'),
        ),
        'more_info' => 'Import advanvaced feeds into your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/feeds',
            'title' => 'Feeds configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'breadcrumb' => array(
        'title' => 'Breadcrumbs',
        'operations' => array(
          'modules' => array('hansel', 'hansel_taxonomy'),
          'theme_variables' => array(
            'breadcrumb_on',
            'breadcrumb_home',
          ),
        ),
        'more_info' => 'Enable breadcrumbs on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/hansel',
            'title' => 'Breadcrumbs configuration'
          ),
        ),
        'group' => 'Other',
      ),
      'sitemap' => array(
        'title' => 'Site map',
        'operations' => array(
          'modules' => array('site_map')
        ),
        'more_info' => 'Enable a sitemap on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/sitemap',
            'title' => 'Sitemap configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'webform' => array(
        'title' => t('Advanced forms'),
        'operations' => array(
          'modules' => array('webform'),
        ),
        'more_info' => 'Enable advanced forms on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/content/node-type/webform',
            'title' => 'Webform configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'newsletter' => array(
        'title' => t('Newsletter'),
        'operations' => array(
          'modules' => array('emf_campaign_monitor', 'emf'),
        ),
        'more_info' => 'Enable newsletters on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/emf',
            'title' => 'Newsletter configuration'
          ),
          1 => array(
            'href' => 'admin/settings/emf_campaign_monitor',
            'title' => 'Campaign monitor configuration'
          ),
        ),
        'group' => 'Social',
      ),
      'ga' => array(
        'title' => t('Google Analytics'),
        'operations' => array(
          'modules' => array('googleanalytics'),
        ),
        'more_info' => 'Enable Google Analytics on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/googleanalytics',
            'title' => 'Google analytics configuration'
          ),
        ),
        'group' => 'Other',
      ),
      'tagcloud' => array(
        'title' => t('Tagcloud'),
        'operations' => array(
          'modules' => array('tagadelic'),

        ),
        'more_info' => 'Enable tag clouds on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/tagadelic',
            'title' => 'Tag cloud configuration'
          ),
        ),
        'group' => 'Social',
      ),
      'mollom' => array(
        'title' => t('Mollom'),
        'operations' => array(
          'modules' => array('mollom'),
        ),
        'more_info' => 'Enable Mollom on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/mollom',
            'title' => 'Mollom configuration'
          ),
        ),
        'group' => 'Social',
      ),
      'poll' => array(
        'title' => t('Polls'),
        'operations' => array(
          'modules' => array('votingapi', 'advpoll'),
          'dep_check' => array('voting'),
          'dep_check_true' => array('advpoll'),
        ),
        'more_info' => 'Create polls on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/content/node-type/advpoll-binary',
            'title' => 'Binary poll configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/advpoll-ranking',
            'title' => 'Ranking poll configuration'
          ),
        ),
        'group' => 'Social',
      ),
      'products' => array(
        'title' => t('Products'),
        'operations' => array(
          'views' => array('Products'),
          'content_type' => array('product', 'product_category', 'viewsdisplaytabs'),
        ),
        'more_info' => 'Create products on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/product',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/product',
            'title' => 'Product content type configuration'
          ),
          2 => array(
            'href' => 'admin/build/ds/layout/product-category',
            'title' => 'Layout configuration'
          ),
          3 => array(
            'href' => 'admin/content/node-type/product-category',
            'title' => 'Product category content type configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'error' => array(
        'title' => 'Error pages',
        'operations' => array(
          'content_type' => array('error'),
        ),
        'more_info' => 'Create error pages on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/error',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/error',
            'title' => 'Content type configuration'
          ),
          2 => array(
            'href' => 'admin/settings/error-reporting',
            'title' => 'Error reporting',
          ),
        ),
        'group' => 'Content',
      ),
      'workflow' => array(
        'title' => 'Workflow',
        'operations' => array(
          'modules' => array('workflow', 'workflow_access'),
        ),
        'more_info' => 'Enable workflow on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/workflow',
            'title' => 'Workflow configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'calltoaction' => array(
        'title' => 'Call to action',
        'operations' => array(
          'content_type' => array('call_to_action'),
          'views' => array('nodequeue_1'),
        ),
        'more_info' => 'Create call to actions on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/call-to-action',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/call-to-action',
            'title' => 'Content type configuration'
          ),
        ),
        'group' => 'Social',
      ),
      'jobs' => array(
        'title' => 'Jobs',
        'operations' => array(
          'content_type' => array('job'),
          'views' => array('Jobs'),
        ),
        'more_info' => 'Create jobs on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/job',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/job',
            'title' => 'Content type configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'documents' => array(
        'title' => 'Documents',
        'operations' => array(
          'content_type' => array('document'),
          'views' => array('Documents'),
          'modules' => array('apachesolr', 'apachesolr_search', 'apachesolr_attachments', 'one_apachesolr','nd_search', 'search'),
          'dep_check' => array('events', 'search'),
          'dep_check_true' => array('apachesolr_attachments'),
        ),
        'more_info' => 'Create documents on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/document',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/document',
            'title' => 'Content type configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'events' => array(
        'title' => 'Events',
        'operations' => array(
          'content_type' => array('event'),
          'views' => array('Events'),
          'modules' => array('apachesolr', 'apachesolr_search', 'apachesolr_attachments', 'one_apachesolr', 'apachesolr_date', 'nd_search', 'search'),
          'dep_check' => array('events', 'search'),
          'dep_check_true' => array('apachesolr_date'),
        ),
        'more_info' => 'Create events on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/event',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/event',
            'title' => 'Content type configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'sweaver' => array(
        'title' => 'Theme styler',
        'operations' => array(
          'modules' => array('sweaver', 'jquery_ui'),
        ),
        'more_info' => 'Enable a theme builder on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/sweaver',
            'title' => 'Sweaver configuration'
          ),
        ),
        'group' => 'Other',
      ),
      'splash' => array(
        'title' => 'Splash',
        'operations' => array(
          'modules' => array('splash'),
        ),
        'more_info' => 'Enable splash on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/splash',
            'title' => 'Splash configuration'
          ),
        ),
        'group' => 'Other',
      ),
      'contact' => array(
        'title' => 'Contact',
        'operations' => array(
          'modules' => array('contact'),
        ),
        'more_info' => 'Enable contact form on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/contact',
            'title' => 'Contact configuration'
          ),
        ),
        'group' => 'Social',
      ),
      'video' => array(
        'title' => 'Video',
        'operations' => array(
          'enable_disable' => array(
            'plugin' => 'nice_dash_video',
            'enable' => 'video_enable',
            'disable' => 'video_disable',
          )
        ),
        'more_info' => 'Enable video on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/mediafront',
            'title' => 'Video preset configuration',
          ),
        ),
        'group' => 'Content',
      ),
      'search' => array(
        'title' => 'Search',
        'operations' => array(
          'modules' => array('apachesolr', 'apachesolr_search', 'nd_search', 'search'),
          'dep_check' => array('events', 'documents', 'search'),
        ),
        'more_info' => 'Enable search on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/settings/apachesolr',
            'title' => 'Search configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'flag' => array(
        'title' => 'Flags',
        'operations' => array(
          'modules' => array('flag')
        ),
        'more_info' => 'Enable flags on your website',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/flag',
            'title' => 'Flags configuration'
          ),
        ),
        'group' => 'Social',
      ),
      'gallery' => array(
        'title' => 'Gallery',
        'operations' => array(
          'modules' => array('views_slideshow'),
          'views' => array('Gallery'),
          'content_type' => array('gallery'),
        ),
        'more_info' => 'Create galleries on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/gallery',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/gallery',
            'title' => 'Content type configuration'
          ),
        ),
        'group' => 'Content',
      ),
      'poi' => array(
        'title' => 'Location',
        'operations' => array(
          'views' => array('Locations'),
          'modules' => array('gmap', 'location', 'nd_location'),
          'content_type' => array('location'),
        ),
        'more_info' => 'Create points of interest on your website.',
        'more_links' => array(
          0 => array(
            'href' => 'admin/build/ds/layout/location',
            'title' => 'Layout configuration'
          ),
          1 => array(
            'href' => 'admin/content/node-type/location',
            'title' => 'Content type configuration'
          ),
          2 => array(
            'href' => 'admin/settings/gmap',
            'title' => 'Google maps configuration'
          ),
          3 => array(
            'href' => 'admin/settings/location',
            'title' => 'Location configuration'
          ),
        ),
        'group' => 'Content',
      ),
    );

    // Reset the list if apachesolr is not installed.
    if (!variable_get('apachesolr_installed', FALSE)) {
      unset($features_definition['events']['operations']['dep_check_true']);
      unset($features_definition['documents']['operations']['dep_check_true']);
      $features_definition['events']['operations']['modules'] = array('nd_search', 'search');
      $features_definition['documents']['operations']['modules'] = array('nd_search', 'search');
      $features_definition['search']['operations']['modules'] = array('nd_search', 'search');
      $features_definition['search']['more_links'][0]['href'] = 'admin/settings/search';
    }


    if (empty($features_list)) {
      $features = module_invoke_all('conimbo_features');
      foreach ($features as $feature) {
        $features_list[$feature] = $features_definition[$feature];
      }
    }

    return $features_list;
  }

  /**
   * Theme features configuration.
   */
  function nice_dash_features_overview() {
    $features = $this->features_list();

    drupal_add_js(drupal_get_path('module', 'conimbo') . '/plugins/nice_dash/nice_dash.js');

    // Catch URL settings.
    if (isset($_GET['feature']) && isset($features[$_GET['feature']])) {
      $this->nice_dash_features_configure($_GET['feature'], $features[$_GET['feature']]);
      drupal_flush_all_caches();
      drupal_goto();
    }

    $destination = urlencode($_GET['q']);

    $groups = array();
    $features = $this->features_list();
    foreach ($features as $feature_key => $feature_value) {

      $status = $this->nice_dash_features_status($feature_key, $feature_value);
      $title = ($status) ? t('Disable') : t('Enable');

      $class = ($status) ? '' : t('disabled');
      $feature = '<div class="form-item '. $class. '">'. theme('image', drupal_get_path('module', 'conimbo') .'/plugins/nice_dash/nice_dash_features/images/'. $feature_key .'.png', $feature_value['title'], $feature_value['title'], array('width' => '68', 'height' => '68'), FALSE);
      $feature .= '<div class="title">'. $feature_value['title'] ."</div>";

      $info = '<h3>Status</h3>'. l($title, $_GET['q'], array('query' => array('feature' => $feature_key, 'destination' => $destination)));
      if (!empty($feature_value['more_info'])) {
        $info .= '<h3>Info</h3>'. $feature_value['more_info'];
      }
      if (!empty($feature_value['more_links']) && $this->nice_dash_features_status($feature_key, $feature_value)) {
        $info .= '<h3>Links</h3>'. theme('links', $feature_value['more_links']);
      }
      if (!empty($info)) {
        $feature .= '<div class="description">'. $info .'</div>';
      }
      $feature .= '</div>';

      $groups[$feature_value['group']][] = $feature;
    }

    $output = '';
    if (!empty($groups)) {
      foreach ($groups as $key => $group_value) {
        if (!empty($group_value)) {
          $output .= '<div id="'. strtolower(str_replace(' ', '_', $key)) .'" class="dashboard-component admin-panel ctools-collapsible-container ctools-collapsible-remember">
            <div class="ctools-collapsible-handle">
              <h3><strong>'. $key .'</strong></h3>
            </div>

            <div class="content ctools-collapsible-content">
              '. implode('', $group_value) .'
              <div style="clear: both;"></div>
            </div>
          </div>';
        }
      }

      $output .= '<div style="clear: both;"></div>';
    }
    else {
      $outpur = t('No features found.');
    }
    return $output;
  }

  /**
   * Configure the feature.
   *
   * @param string $feature_key The key of the feature.
   * @param array $feature_value The value of the feature.
   */
  function nice_dash_features_configure($feature_key, $feature_value) {
    $status = $this->nice_dash_features_status($feature_key, $feature_value);
    $conimbo_status = ($status) ? FALSE : TRUE;
    variable_set('conimbo_feature_'. $feature_key, $conimbo_status);
    $statused = ($conimbo_status) ? 'enabled' : 'disabled';
    $message = t('@conimbo_feature has been @statused.', array('@conimbo_feature' => $feature_value['title'], '@statused' => $statused));
    drupal_set_message($message);

    // Perform operations if necessary.
    if (isset($feature_value['operations'])) {
      foreach ($feature_value['operations'] as $method => $method_value) {
        $method = 'nice_dash_features_'. $method;
        if (method_exists($this, $method)) {
          $this->$method($feature_key, $feature_value, $conimbo_status, $method_value);
        }
      }
    }
  }

  /**
   * Return the status of the feature.
   *
   * @param string $feature_key The key of the feature.
   * @param array $feature_value The value of the feature.
   */
  function nice_dash_features_status($feature_key, $feature_value) {

    $status = variable_get('conimbo_feature_'. $feature_key, FALSE);
    if (isset($feature['operations']['modules'])) {
      foreach ($feature['operations']['modules'] as $module) {
        if (!module_exists($module)) {
          $status = FALSE;
          break;
        }
      }
    }

    return $status;
  }

  /**
   * Enable a plugin by calling the plugin & it's methods.
   *
   * @param string $feature The name of the feature.
   * @param array $feature_value The definition of the feature.
   * @param boolean $status Whether the feature is to be enabled or disabled.
   * @param array $plugin Definition of the plugin with enabled & disabled methods.
   */
  function nice_dash_features_enable_disable($feature, $feature_value, $status, $plugin) {
    $nice_dash_plugin = nice_dash_get_plugin($plugin['plugin']);
    if ($status) {
      $nice_dash_plugin->{$plugin['enable']}();
    }
    else {
      $nice_dash_plugin->{$plugin['disable']}();
    }
  }

  /**
   * Enable or disable modules.
   *
   * @param string $feature The name of the feature.
   * @param array $feature_value The definition of the feature.
   * @param boolean $status Whether the feature is to be enabled or disabled.
   * @param array $modules A collection of modules.
   */
  function nice_dash_features_modules($feature, $feature_value, $status, $modules) {
    if ($status) {
      include_once './includes/install.inc';
      drupal_install_modules($modules);
    }
    else {
      $disable = TRUE;
      if (isset($feature_value['operations']['dep_check'])) {
        foreach ($feature_value['operations']['dep_check'] as $feature_dep) {
          if (variable_get('conimbo_feature_'. $feature_dep, FALSE)) {
            $disable = FALSE;
            break;
          }
        }
      }

      if (!$disable && isset($feature_value['operations']['dep_check_true'])) {
        $disable = TRUE;
        $modules = $feature_value['operations']['dep_check_true'];
      }

      if ($disable) {
        module_disable($modules);
      }
    }
  }

  /**
   * Enable or disable settings for the default theme.
   *
   * @param string $feature The name of the feature.
   * @param array $feature_value The definition of the feature.
   * @param boolean $status Whether the feature is to be enabled or disabled.
   * @param array $theme_variables A collection of theme_variables.
   */
  function nice_dash_features_theme_variables($feature, $feature_value, $status, $theme_variables) {

    $theme_key = variable_get('theme_default', 'garland');
    $theme_settings_key = str_replace('/', '_', 'theme_'. $theme_key .'_settings');
    $theme_settings = theme_get_settings($theme_key);

    foreach ($theme_variables as $key => $theme_variable) {
      $theme_settings[$theme_variable] = $status;
    }

    variable_set($theme_settings_key, $theme_settings);
  }

  /**
   * Enable or disable content types.
   *
   * @param string $feature The name of the feature.
   * @param array $feature_value The definition of the feature.
   * @param boolean $status Whether the feature is to be enabled or disabled.
   * @param array $content_types A collection of content types.
   */
  function nice_dash_features_content_type($feature, $feature_value, $status, $content_types) {
    foreach ($content_types as $content_type) {
      $conimbo_ctypes_disabled = variable_get('conimbo_ctypes_disabled', array());
      if ($status) {
        unset($conimbo_ctypes_disabled[$content_type]);
      }
      else {
        $conimbo_ctypes_disabled[$content_type] = $content_type;
      }
    }
    variable_set('conimbo_ctypes_disabled', $conimbo_ctypes_disabled);
  }

  /**
   * Enable or disable a view.
   *
   * @param string $feature The name of the feature.
   * @param array $feature_value The definition of the feature.
   * @param boolean $status Whether the feature is to be enabled or disabled.
   * @param array $views A collection of views.
   */
  function nice_dash_features_views($feature, $feature_value, $status, $views) {
    // Note that setting a default view to FALSE actually enables it.
    $views_defaults = variable_get('views_defaults', array());
    foreach ($views as $view) {
      if ($status) {
        $views_defaults[$view] = FALSE;
      }
      else {
        $views_defaults[$view] = TRUE;
      }
    }
    variable_set('views_defaults', $views_defaults);
  }
}
