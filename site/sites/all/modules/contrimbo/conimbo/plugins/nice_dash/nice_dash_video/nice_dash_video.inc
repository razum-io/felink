<?php

/**
 * @file
 * Video plugin for Nice dashboard.
 */

/**
 * Video for this node type are disabled.
 */
define('VIDEO_NODE_DISABLED', 0);

/**
 * Comments for this node type are enabled.
 */
define('VIDEO_NODE_ENABLED', 1);


class nice_dash_video {
  var $modules = array(
      0 => 'fieldgroup',
      1 => 'imagefield',
  		2 => 'filefield',
      3 => 'emfield',
      4 => 'emvideo',
      5 => 'media_vimeo',
      6 => 'media_youtube',
    );

  /**
   * Video feature enabled method.
   */
  function video_feature_enabled() {
  	$enabled = TRUE;
  	
  	foreach ($this->modules as $key => $module) {
  		$enabled = module_exists($module);
  		if (!$enabled) {
  		  break;
  		}
  	}

    return $enabled;
  }

  /**
   * Forms
   */
  function forms(&$forms) {
    $forms['nice_dash_video'] = array(
      'callback' => 'nice_dash_menu_callback_form',
      'callback arguments' => array('nice_dash_video', 'nice_dash_video_form'),
    );
  }

  /**
   * Widgets method.
   */
  function widgets() {
    $widgets = array(
      'video_configuration' => t('Configure video'),
      // 'video_latest' => t('Latest videos'),
    );
    return $widgets;
  }

  /**
   * Video configuration.
   */
  function nice_dash_video_widget_video_configuration() {
    if (!$this->video_feature_enabled()) {
      return;
    }
    $content = drupal_get_form('nice_dash_video', 'nice_dash_video', 'video_configure_form');
    return array(
      'title' => ('Configure video'),
      'content' => $content,
    );
  }

  function nice_dash_video_form() {
    if ($this->video_feature_enabled()) {
      $form = $this->video_configure_form();
      $form['#submit'][] = 'nice_dash_menu_callback_form_submit';
      return $form;
    }
  }

  /**
   * Video form configuration.
   */
  function video_configure_form() {
    $form = array();

    $node_types = node_get_types();
    foreach ($node_types as $type => $node_type) {
      $form['conimbo_video_'. $type] = array(
        '#type' => 'select',
         '#options' => array(
          VIDEO_NODE_DISABLED => t('Disabled '),
          VIDEO_NODE_ENABLED => t('Enabled'),
        ),
        '#default_value' => variable_get('conimbo_video_'. $type, VIDEO_NODE_DISABLED),
      );
    }
    $form['#plugin_theme'] = 'theme_video_configure_form';
    $form = system_settings_form($form);
    $form['buttons']['reset']['#access'] = FALSE;
    return $form;
  }

  function theme_video_configure_form($form) {
    $output = '';
    $output .= drupal_render($form['enable']);

    $header = array(
      t('Content type'),
      t('Video status'),
      '',
    );

    $rows = array();
    $node_types = node_get_types();
    foreach ($node_types as $type => $node_type) {
      $row = array();
      $row[] = t($node_types[$type]->name);
      $row[] = array('data' => drupal_render($form['conimbo_video_' . $type]));
      $row[] = array('data' => l('Display settings', 'admin/build/ds/layout/'. $type));
      $rows[] = $row;
    }

    if ($rows) {
      $output .= theme('table', $header, $rows, array('id' => 'configure-video-overview'));
    }

    $output .= drupal_render($form);

    return $output;
  }

  /**
   * Submit callback; enable or disable the video functionality.
   */
  function nice_dash_video_form_submit($form, &$form_state) {
    // Create, disable or reactivate the content fields.
    $types = array();
    foreach($form_state['values'] as $key => $enabled) {
    	$types[str_replace('conimbo_video_', '', $key)] = $enabled;
    }
   	$this->video_content_fields($types);
    drupal_flush_all_caches();
  }

  function video_enable() {
    // Check if needed Conimbo (optional) modules are enabled.
    foreach($this->modules as $sequence => $module) {
      if (!module_exists($module)) {
        $modules[] = $module;
      }
    }
    // Make sure the installation API is available
    include_once './includes/install.inc';
    drupal_install_modules($modules);
    
    // Exclude video fields to be rendered as DS fields.
    module_load_include('inc', 'conimbo', 'plugins/nice_dash/nice_dash_video/nice_dash_video_fields');
    $excluded_fields = variable_get('ds_excluded_fields', array());
    $fields = video_content_default_fields();
   	foreach ($fields as $field) {
    	if (!isset($excluded_fields[$field['field_name']])) {
     		$excluded_fields[] = $field['field_name'];
    	}
  	}
    variable_set('ds_excluded_fields', $excluded_fields);
  }

  function video_disable() {
  	// Risky to disable modules like emvideo, and certainly don't disable filefield, nor fieldgroup.
  	// Disabling takes it effect by testing if the functionality is enabled 
  	// at time of rendering the media player through DS. 
  	module_disable(array('media_vimeo', 'media_youtube'));
  }

  /**
   * Helper function to load the default content fields.
   */
  function video_content_fields($types) {
    module_load_include('inc', 'conimbo', 'plugins/nice_dash/nice_dash_video/nice_dash_video_fields');
    $fields = video_content_default_fields();
    if (!empty($fields)) {
      module_load_include('inc', 'content', 'includes/content.crud');
      content_clear_type_cache(TRUE);
      $node_types = node_get_types();
      foreach ($node_types as $type => $node_type) {
        foreach ($fields as $field) {
          $field['type_name'] = $type; 
          $existing_field = content_fields($field['field_name']);
          $existing_instance = content_fields($field['field_name'], $field['type_name']);
          // If video feature is disabled alltogether or
          // if video feature is disabled for this content type and
          // there exists a field, disactivate it.
          if ((!$types[$type] || !variable_get('conimbo_video_'. $type) || variable_get('conimbo_video_'. $type) == VIDEO_NODE_DISABLED) &&
                !empty($existing_field) && !empty($existing_instance)) {
              $field['widget_active'] = 0;
              content_field_instance_update($field, FALSE);
          // If the video feature is enabled and
          // if video feature is enabled for this content type.
          }
          elseif ($types[$type] && variable_get('conimbo_video_'. $type) == VIDEO_NODE_ENABLED) {
            // Retrieve the eventual (inactive or active) content instance fields.
            $existing_instances = content_field_instance_read(array('field_name' => $field['field_name'], 'type_name' => $field['type_name']), TRUE);
          	if (!empty($existing_instances)) {
              foreach($existing_instances as $key => $existing_instance) {
                if ($existing_instance['widget_active'] == 0) {
                  // Set the fields active again.
                  db_query("UPDATE {content_node_field_instance} SET widget_active = 1 WHERE field_name = '%s' AND type_name = '%s'", $existing_instance['field_name'], $existing_instance['type_name']);
                }
                content_field_instance_update($field, FALSE);
              }
            }
            else {
            	// Create missing fields for content types where video is enabled.
              content_field_instance_create($field, FALSE);
            }
          }
        }
        $this->video_content_fieldgroup($types[$type], video_content_group(), $type, $fields);
      }
    }
    variable_set('menu_rebuild_needed', TRUE);
  }
  /**
   * Helper function to manage video fieldgroups.
   */
  function video_content_fieldgroup($enabled, $group, $type, $fields) {
  	// Does the field group exist?
  	$exists = db_result(db_query("SELECT count(*) FROM {content_group} WHERE group_name = '%s' AND type_name ='%s'", array($group['group_name'], $type)));
  	
  	// If content type is enabled and the field group does not exist, create it.
  	if ($enabled && !$exists) {
  		db_query("INSERT INTO {content_group} (group_type, type_name, group_name, label, settings, weight)".
      " VALUES ('%s', '%s', '%s', '%s', '%s', %d)", $group['group_type'], $type, $group['group_name'], $group['label'], serialize($group['settings']), $group['weight']);
  		foreach($fields as $field) {
	  		db_query("INSERT INTO {content_group_fields} (type_name, group_name, field_name)".
	      " VALUES ('%s', '%s', '%s')",  $type,  $group['group_name'], $field['field_name']);
  		}
  	}
  	// If content type is disabled and the field group exists, terminate it.
  	elseif (!$enabled && $exists) {
  		db_query("DELETE FROM {content_group_fields} WHERE group_name = '%s' AND type_name ='%s'", array($group['group_name'], $type));
  		db_query("DELETE FROM {content_group} WHERE group_name = '%s' AND type_name ='%s'", array($group['group_name'], $type));  
  	}
  }
}
