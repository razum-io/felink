<?php

/**
 * @file
 * Fivestar plugin for Nice dashboard.
 */

class nice_dash_fivestar {

  /**
   * Forms
   */
  function forms(&$forms) {
    $forms['nice_dash_fivestar'] = array(
      'callback' => 'nice_dash_menu_callback_form',
      'callback arguments' => array('nice_dash_fivestar', 'nice_dash_fivestar_form'),
    );
  }

  /**
   * Widgets method.
   */
  function widgets() {
    $widgets = array(
      'fivestar_configuration' => t('Configure fivestar'),
    );
    return $widgets;
  }

  /**
   * Comment configuration.
   */
  function nice_dash_fivestar_widget_fivestar_configuration() {
    if (module_exists('fivestar') && module_exists('nd_fivestar')) {
      $content = drupal_get_form('nice_dash_fivestar');
      return array(
        'title' => ('Configure fivestar'),
        'content' => $content,
      );
    }
    return array();
  }

  function nice_dash_fivestar_form() {
    $form = $this->fivestar_configure_form();
    $form['#submit'][] = 'nice_dash_menu_callback_form_submit';
    return $form;
  }

  /**
   * Fivestar form configuration.
   */
  function fivestar_configure_form() {
    $form = array();

    $node_types = node_get_types();
    foreach ($node_types as $type => $node_type) {
      $form['nd_fivestar_'. $type] = array(
        '#type' => 'select',
        '#options' => array(t('Disabled'), t('Enabled')),
        '#default_value' => variable_get('nd_fivestar_'. $type, 0),
      );
      $form['fivestar_stars_'. $type] = array(
        '#type' => 'select',
        '#options' => drupal_map_assoc(range(1, 10)),
        '#default_value' => variable_get('fivestar_stars_'. $type, 5),
        '#weight' => -4,
      );
    }

    $form['#plugin_theme'] = 'theme_fivestar_configure_form';
    $form = system_settings_form($form);
    $form['buttons']['reset']['#access'] = FALSE;
    return $form;
  }

  /**
   * Theme fivestar form configuration.
   */
  function theme_fivestar_configure_form($form) {
    $output = '';
    $output .= drupal_render($form['enable']);

    $header = array(
      t('Content type'),
      t('Status'),
      t('Stars'),
    );

    $rows = array();
    $node_types = node_get_types();
    foreach ($node_types as $type => $node_type) {
      $row = array();
      $row[] = check_plain(t($node_types[$type]->name));
      $row[] = array('data' => drupal_render($form['nd_fivestar_'. $type]));
      $row[] = array('data' => drupal_render($form['fivestar_stars_'. $type]));
      $rows[] = $row;
    }

    if ($rows) {
      $output .= theme('table', $header, $rows, array('id' => 'configure-comments-overview', 'class' => 'nice-dash-table'));
    }

    $output .= drupal_render($form);

    return $output;
  }
}
