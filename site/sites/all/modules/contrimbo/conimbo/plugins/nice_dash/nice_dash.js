Drupal.dash = Drupal.dash || {};

$(document).ready(function() {
  // Fade in the disabled items when hovering.
  $('#nice-dashboard .configure_features .disabled').hover(
    function() {
      $('img, .title', this).css({'opacity' : '1'});
    },
    function () {
      $('img, .title', this).css({'opacity' : '0.4'});
    }    
  );
  
  // Make sure the description does not fall outside the viewport.
  $('#nice-dashboard .configure_features .form-item').mouseover(function() {
    var desc = $('.description', this);
    if (desc) {
	    var offset = desc.offset();
	    if (offset.left < 0) {
	      desc.css({'right' : '-125px'});
	    };
      if (offset.left + desc.outerWidth() > $(window).width()) {
        desc.css({'right' : '-75px'});
      };	    
	  }
  });
  
  // Add a 'loading' overlay after enabling/disabling a feature.
  $('<div id="loader-overlay"></div>').appendTo('body');
  $('<div id="loader-icon"></div>').appendTo('body');
    
  $('#nice-dashboard .configure_features a').click(function(event) {
    $('#loader-overlay, #loader-icon').show();
    Drupal.dash.overlaySize();
	  $(window).resize(function() {
	    Drupal.dash.overlaySize();
	  });   
  });
});

/**
 * Calculate the width and height of the overlay.
 */
Drupal.dash.overlaySize = function () {
  if ($('#loader-overlay').is(':visible')) {
    var w = $(window).width();
    var h = $(window).height();
    $('#loader-overlay').css({'width' : w, 'height' : h});
    $('#loader-icon').css({'width' : w, 'height' : h});
  }
}