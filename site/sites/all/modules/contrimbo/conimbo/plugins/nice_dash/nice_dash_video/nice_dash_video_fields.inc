<?php

/**
 * Helper for creating content fields.
 */
function video_content_default_fields() {
  return array(
    0 => array (
	    'label' => 'Embedded video',
	    'field_name' => 'field_video_embedded',
	    'type' => 'emvideo',
	    'widget_type' => 'emvideo_textfields',
	    'change' => 'Change basic information',
	    'weight' => '35',
	    'providers' => 
	    array (
	      'conimbo' => true,
	      'vimeo' => true,
	      'youtube' => true,
	    ),
	    'description' => '',
	    'default_value' => 
	    array (
	      0 => 
	      array (
	        'embed' => '',
	        'value' => '',
	      ),
	    ),
	    'default_value_php' => '',
	    'default_value_widget' => NULL,
	    'group' => 'group_video',
	    'required' => 0,
	    'multiple' => '1',
	    'op' => 'Save field settings',
	    'module' => 'emvideo',
	    'widget_module' => 'emvideo',
	    'columns' => 
	    array (
	      'embed' => 
	      array (
	        'type' => 'text',
	        'size' => 'big',
	        'not null' => false,
	        'sortable' => true,
	      ),
	      'value' => 
	      array (
	        'type' => 'varchar',
	        'length' => 255,
	        'not null' => false,
	        'sortable' => true,
	      ),
	      'provider' => 
	      array (
	        'type' => 'varchar',
	        'length' => 255,
	        'not null' => false,
	        'sortable' => true,
	      ),
	      'data' => 
	      array (
	        'type' => 'text',
	        'size' => 'big',
	        'not null' => false,
	        'sortable' => false,
	      ),
	      'status' => 
	      array (
	        'description' => 'The availability status of this media.',
	        'type' => 'int',
	        'unsigned' => 'TRUE',
	        'not null' => true,
	        'default' => 1,
	      ),
	      'version' => 
	      array (
	        'description' => 'The version of the provider\'s data.',
	        'type' => 'int',
	        'unsigned' => true,
	        'not null' => true,
	        'default' => 0,
	      ),
	      'duration' => 
	      array (
	        'description' => 'Store the duration of a video in seconds.',
	        'type' => 'int',
	        'unsigned' => true,
	        'not null' => true,
	        'default' => 0,
	      ),
	    ),
	    'display_settings' => 
	    array (
	      'label' => 
	      array (
	        'format' => 'above',
	        'exclude' => 0,
	      ),
	      'teaser' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'full' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      4 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      2 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      3 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'list' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'bubble' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'block' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'text_list' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'flag_list' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'search' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'token' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	    ),
	  ),
	  1 => array (
	    'label' => 'Embedded video image',
	    'field_name' => 'field_video_embedded_image',
	    'type' => 'filefield',
	    'widget_type' => 'imagefield_widget',
	    'change' => 'Change basic information',
	    'weight' => '36',
	    'file_extensions' => 'png gif jpg jpeg',
	    'progress_indicator' => 'bar',
	    'file_path' => '',
	    'max_filesize_per_file' => '',
	    'max_filesize_per_node' => '',
	    'max_resolution' => 0,
	    'min_resolution' => 0,
	    'custom_alt' => 0,
	    'alt' => '',
	    'custom_title' => 0,
	    'title_type' => 'textfield',
	    'title' => '',
	    'use_default_image' => 0,
	    'default_image_upload' => '',
	    'default_image' => NULL,
	    'description' => '',
	    'group' => 'group_video',
	    'required' => 0,
	    'multiple' => '1',
	    'list_field' => '0',
	    'list_default' => 1,
	    'description_field' => '0',
	    'op' => 'Save field settings',
	    'module' => 'filefield',
	    'widget_module' => 'imagefield',
	    'columns' => 
	    array (
	      'fid' => 
	      array (
	        'type' => 'int',
	        'not null' => false,
	        'views' => true,
	      ),
	      'list' => 
	      array (
	        'type' => 'int',
	        'size' => 'tiny',
	        'not null' => false,
	        'views' => true,
	      ),
	      'data' => 
	      array (
	        'type' => 'text',
	        'serialize' => true,
	        'views' => true,
	      ),
	    ),
	    'conimbo_imagecache_fieldname' => 'field_video_embedded_image',
	    'conimbo_generate_imagecache' => 1,
	    'display_settings' => 
	    array (
	      'label' => 
	      array (
	        'format' => 'above',
	        'exclude' => 0,
	      ),
	      'teaser' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'full' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      4 => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      2 => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      3 => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'list' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'bubble' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'block' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'text_list' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'flag_list' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'search' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'token' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	    ),
	  ),
	  2 => array (
	    'label' => 'Uploaded video',
	    'field_name' => 'field_video_uploaded',
	    'type' => 'filefield',
	    'widget_type' => 'filefield_widget',
	    'change' => 'Change basic information',
	    'weight' => '37',
	    'file_extensions' => 'mp4 mpg m4v flv f4v mov',
	    'progress_indicator' => 'bar',
	    'file_path' => '',
	    'max_filesize_per_file' => '',
	    'max_filesize_per_node' => '',
	    'description' => '',
	    'group' => 'group_video',
	    'required' => 0,
	    'multiple' => '1',
	    'list_field' => '0',
	    'list_default' => 1,
	    'description_field' => '0',
	    'op' => 'Save field settings',
	    'module' => 'filefield',
	    'widget_module' => 'filefield',
	    'columns' => 
	    array (
	      'fid' => 
	      array (
	        'type' => 'int',
	        'not null' => false,
	        'views' => true,
	      ),
	      'list' => 
	      array (
	        'type' => 'int',
	        'size' => 'tiny',
	        'not null' => false,
	        'views' => true,
	      ),
	      'data' => 
	      array (
	        'type' => 'text',
	        'serialize' => true,
	        'views' => true,
	      ),
	    ),
	    'display_settings' => 
	    array (
	      'label' => 
	      array (
	        'format' => 'above',
	        'exclude' => 0,
	      ),
	      'teaser' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'full' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      4 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      2 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      3 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'list' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'bubble' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'block' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'text_list' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'flag_list' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'search' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'token' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	    ),
	  ),
	  3 => array (
	    'label' => 'Uploaded video image',
	    'field_name' => 'field_video_uploaded_image',
	    'type' => 'filefield',
	    'widget_type' => 'imagefield_widget',
	    'change' => 'Change basic information',
	    'weight' => '38',
	    'file_extensions' => 'png gif jpg jpeg',
	    'progress_indicator' => 'bar',
	    'file_path' => '',
	    'max_filesize_per_file' => '',
	    'max_filesize_per_node' => '',
	    'max_resolution' => 0,
	    'min_resolution' => 0,
	    'custom_alt' => 0,
	    'alt' => '',
	    'custom_title' => 0,
	    'title_type' => 'textfield',
	    'title' => '',
	    'use_default_image' => 0,
	    'default_image_upload' => '',
	    'default_image' => NULL,
	    'description' => '',
	    'group' => 'group_video',
	    'required' => 0,
	    'multiple' => '1',
	    'list_field' => '0',
	    'list_default' => 1,
	    'description_field' => '0',
	    'op' => 'Save field settings',
	    'module' => 'filefield',
	    'widget_module' => 'imagefield',
	    'columns' => 
	    array (
	      'fid' => 
	      array (
	        'type' => 'int',
	        'not null' => false,
	        'views' => true,
	      ),
	      'list' => 
	      array (
	        'type' => 'int',
	        'size' => 'tiny',
	        'not null' => false,
	        'views' => true,
	      ),
	      'data' => 
	      array (
	        'type' => 'text',
	        'serialize' => true,
	        'views' => true,
	      ),
	    ),
	    'conimbo_imagecache_fieldname' => 'field_video_uploaded_image',
	    'conimbo_generate_imagecache' => 1,
	    'display_settings' => 
	    array (
	      'label' => 
	      array (
	        'format' => 'above',
	        'exclude' => 0,
	      ),
	      'teaser' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'full' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      4 => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      2 => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      3 => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'list' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'bubble' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'block' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'text_list' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'flag_list' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'search' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	      'token' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	    ),
	  ),
	);
}

function video_content_group() {
	return array(
		'group_type' => 'standard',
		'group_name' => 'group_video',
		'label' => 'Video',
		'settings' => array(
			'form' => array(
				'style' => 'fieldset',
				'description' => '',
			),
			'display' => array(
				'description' => '',
				'label' => above,
				'teaser' => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				'full' => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				4 => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				2 => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				3 => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				'list' => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				'bubble' => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				'block' => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				'text_list' => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				'flag_list' => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				'search' => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
				'token' => array(
					'format' => 'fieldset',
					'exclude' => 0,
				),
			),
		),
		'weight' => 99
	);
}