<?php
/**
 * @file
 * Contains the summary editable link style plugin, which displays items in an HTML list as links to an editable path.
 */

/**
 * The default style plugin for summaries.
 *
 * @ingroup views_style_plugins
 */

class conimbo_plugin_style_summary_editable_link extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    $options['link'] = array('default' => TRUE);
    $options['count'] = array('default' => TRUE);
    $options['override'] = array('default' => FALSE);
    $options['items_per_page'] = array('default' => 25);

    return $options;
  }

  function query() {
    if (!empty($this->options['override'])) {
      $this->view->set_items_per_page(intval($this->options['items_per_page']));
    }
  }

  function options_form(&$form, &$form_state) {
    $link = $this->options['link'] ? $this->options['link'] : ($this->view->display['page_1']->display_options['path'] ? $this->view->display['page_1']->display_options['path'] : '');
    $form['link'] = array(
      '#title' => t('Link path'),
      '#type' => 'textfield',
      '#default_value' => $link,
      '#field_prefix' => '<span dir="ltr">' . url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
      '#field_suffix' => '</span>&lrm;',
      '#attributes' => array('dir'=>'ltr'),
      '#description' => t('The Drupal path or absolute URL for this summary as link. Arguments will be appended.'),
      '#maxlength' => 255,
    );
    $form['count'] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['count']),
      '#title' => t('Display record count with link'),
    );
    $form['override'] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['override']),
      '#title' => t('Override number of items to display'),
    );
    $form['items_per_page'] = array(
      '#type' => 'textfield',
      '#title' => t('Items to display'),
      '#default_value' => $this->options['items_per_page'],
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-style-options-override' => array(TRUE)),
    );
  }

  function render() {
    $rows = array();
    foreach ($this->view->result as $row) {
      // @todo: Include separator as an option.
      $rows[] = $row;
    }
    return theme($this->theme_functions(), $this->view, $this->options, $rows);
  }
}



