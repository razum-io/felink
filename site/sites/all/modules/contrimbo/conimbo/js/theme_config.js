Drupal.behaviors.theme_config = function (context) {
  // Check if 'default' is checked.
  if ($('#edit-default-favicon-wrapper input.form-checkbox').is(':checked')){
    $('#edit-default-favicon-wrapper').nextAll().hide();
  }

  // Hide empty path field.
  if ($('#edit-favicon-path-wrapper input.form-text').val() == ''){
    $('#edit-favicon-path-wrapper input.form-text').hide();
    $('#edit-favicon-path-wrapper input.form-text').siblings().hide();
  }

  // Toggle display on every check.
  $("#edit-default-favicon-wrapper input.form-checkbox").change(function(){
    $(this).parent().parent().nextAll().toggle();
  });
};