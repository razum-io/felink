/**
* Conimbo behaviors
*/

Drupal.behaviors.Conimbo = function(context) {
  // Add attribute 'target' for document preview and download links.
  // the attribute was removed from the them.inc for AnySurfer compatibility.
  $('.target-blank').click(function(){
    window.open($(this).attr('href'));
    return false;
  });
  
  // Show/hide hidden regions with the class show_region_on_hover.
  if($('#content-middle .show_region_on_hover').length > 0) {
	  $('#content-middle .show_region_on_hover').parents('.node').hover(
		  function() {
		    $(this).find('.show_region_on_hover').show();
		  },
		  function() {
		    $(this).find('.show_region_on_hover').hide();
		  }
	  );
  }

};

/**
 * Conimbo flag object
 */
$(document).bind('flagGlobalAfterLinkUpdate', function(event, data) {
  if (!Drupal.settings.conimboFlag.exclude) {

    Drupal.conimboFlag.getFlagCounter(event, data);
  }
  else {
    var contentHolder = $('#flag-' + data.contentId).parents('.buildmode-flag_list');
    
    var search = '.nd-region-right';    
    
    var undo = false;
    if (data.flagStatus == "unflagged") {
      search = '.nd-region-footer';
      undo = true;
      var smallButton = contentHolder.find(search + ' #hidden-flag-' + data.contentId);  
    } else {
      var smallButton = contentHolder.find(search + ' #flag-' + data.contentId);
    }

    
    smallButton.html(data.newLink);
    Drupal.attachBehaviors($(document));
  
    if (undo) {
      smallButton.find('a').html(Drupal.t('Undo'));
      contentHolder.children().addClass('disabled');
	    contentHolder.find('.nd-region-right,.nd-region-middle').hide();
	    contentHolder.find('.nd-region-footer').fadeIn('slow');   
    } else {
      contentHolder.children().removeClass('disabled');
	    contentHolder.find('.nd-region-right,.nd-region-middle').fadeIn('slow');
	    contentHolder.find('.nd-region-footer').hide();   
    }    
  
 
        
  }

});

Drupal.conimboFlag = Drupal.conimboFlag || {};

Drupal.conimboFlag.getFlagCounter = function(event, data) {

  var element = this;
  $.ajax({
    type: "GET",
    url: Drupal.settings.conimboFlag.ajax_path + '?flag=' + data.flagName,
    cache: false,
    dataType: "json",
    success: function(response) {
      $('#' + response.element_id).html(response.data);
    }
  })
  
}

