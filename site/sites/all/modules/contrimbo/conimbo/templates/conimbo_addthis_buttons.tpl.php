<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style">
  <?php foreach ($buttons as $button): ?>
    <a <?php print drupal_attributes($button); ?>></a>
  <?php endforeach; ?>
  <span class="addthis_separator">|</span>
  <?php print l($button_compact['title'], $button_compact['path'], $button_compact['options']) ?>
</div>
<script type="text/javascript" src="<?php print $url; ?>/js/250/addthis_widget.js#username=xa-4c64e99b74ed89eb"></script>
<!-- AddThis Button END -->