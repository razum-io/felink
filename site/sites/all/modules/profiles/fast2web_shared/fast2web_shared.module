<?php

/**
 * Implementation of hook_conimbo_features().
 */
function fast2web_shared_conimbo_features() {
  return array(
    'news',
    'blogs',
    'comments',
    'faq',
    'aggregator',
    'breadcrumb',
    'sitemap',
    'ga',
    'tagcloud',
    'mollom',
    'error',
    'calltoaction',
    'jobs',
    'events',
    'contact',
    'search',
    'gallery',
  );
}

/**
 * Implementation of hook_form().
 */
function fast2web_shared_form($node, $form_state) {
  return node_content_form($node, $form_state);
}

/**
 * Implementation of hook_access()
 */
function fast2web_shared_access($op, $node, $account) {
  return node_content_access($op, $node, $account);
}

/**
 * Implementation of hook_perm().
 */
function fast2web_shared_perm() {
  // Generate permissions for any default node types provided by fast2web
  // modules.

  foreach (node_get_types() as $type) {
    if (in_array($type->module, _fast2web_shared_modules())) {
      $name = check_plain($type->type);
      $perms[] = 'create '. $name .' content';
      $perms[] = 'delete own '. $name .' content';
      $perms[] = 'delete any '. $name .' content';
      $perms[] = 'edit own '. $name .' content';
      $perms[] = 'edit any '. $name .' content';
    }
  }
  return $perms;
}

/**
 * Helper function to return a settings function.
 *
 * @param string $function The name of the function.
 */
function fast2web_shared_default_settings_function($function) {
  $theme = variable_get('theme_default', 'garland');
  if (function_exists($function .'_'. $theme)) {
    return $function .'_'. $theme;
  }
  else {
    return $function .'_default';
  }
}

/**
 * Helper function to load the default content fields.
 */
function fast2web_shared_build_content_fields() {
  $fields = module_invoke_all('content_default_fields');
  if (!empty($fields)) {
    module_load_include('inc', 'content', 'includes/content.crud');
    content_clear_type_cache(TRUE);

    foreach ($fields as $field) {
      $existing_field = content_fields($field['field_name']);
      $existing_instance = content_fields($field['field_name'], $field['type_name']);
      if ($existing_field && $existing_instance) {
        content_field_instance_update($field, FALSE);
      }
      else {
        content_field_instance_create($field, FALSE);
      }
      variable_set('menu_rebuild_needed', TRUE);
    }
  }
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function fast2web_shared_imagecache_default_presets() {
  module_load_include('inc', 'fast2web_shared', 'fast2web_shared.imagecache');
  return _fast2web_shared_imagecache_default_presets();
}

/**
 * Implementation of hook_node_info().
 */
function fast2web_shared_node_info() {
  module_load_include('inc', 'fast2web_shared', 'fast2web_shared.node');
  return _fast2web_shared_node_info();
}

/**
 * Implementation of hook_ds_default_settings().
 */
function fast2web_shared_ds_default_settings() {
  module_load_include('inc', 'fast2web_shared', 'fast2web_shared.ds');
  return _fast2web_shared_ds_default_settings();
}

/**
 * Implementation of hook_views_default_views().
 */
function fast2web_shared_views_default_views() {
  module_load_include('inc', 'fast2web_shared', 'fast2web_shared.views');
  return _fast2web_shared_views_default_views();
}

/**
 * Implementation of hook_content_default_fields().
 */
function fast2web_shared_content_default_fields() {
  module_load_include('inc', 'fast2web_shared', 'fast2web_shared.defaults');
  return _fast2web_shared_content_default_fields();
}

/**
 * Returns a list of Fast2Web modules used for profile installation.
 */
function _fast2web_shared_modules() {
  return array(
    'fast2web_shared',
    'fast2web_event',
  );
}
/**
 * Implementation of hook_ds_fields().
 */
function fast2web_shared_ds_fields() {
  $data = array(
    'nd' => array(
      'gallery_thumbnails' => array(
        'type' => 6,
        'status' => 2,
        'title' => 'Gallery thumbnails',
        'exclude' => array(
          'error' => 'error',
          'news' => 'news',
          'page' => 'page',
          'views_text' => 'views_text',
          'gallery' => 0,
          'blog' => 'blog',
          'faq' => 'faq',
        ),
        'properties' => array(
          'block' => 'views|Gallery-block_1',
          'render' => '1',
        ),
      ),
      'gallery_slideshow' => array(
        'type' => 6,
        'status' => 2,
        'title' => 'Gallery slideshow',
        'exclude' => array(
          'error' => 'error',
          'news' => 'news',
          'page' => 'page',
          'views_text' => 'views_text',
          'gallery' => 0,
          'blog' => 'blog',
          'faq' => 'faq',
        ),
        'properties' => array(
          'block' => 'views|Gallery-block_2',
          'render' => '1',
        ),
      ),
      'related_blog_posts' => array(
        'type' => 6,
        'status' => 2,
        'title' => 'Related blog posts',
        'exclude' => array(
          'error' => 'error',
          'news' => 'news',
          'page' => 'page',
          'views_text' => 'views_text',
          'gallery' => 'gallery',
          'blog' => 0,
        ),
        'properties' => array(
          'block' => 'views|Blogs-block_2',
          'render' => '1',
        ),
      ),
    ),
  );
  return $data;
}
