<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _fast2web_shared_node_info() {

  $items = array(
    'error' => array(
      'name' => t('Error pages'),
      'module' => 'fast2web_shared',
      'description' => t('An error page'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'news' => array(
      'name' => t('News'),
      'module' => 'fast2web_shared',
      'description' => t('A news item is a simple method for creating and displaying news information. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page'),
      'module' => 'fast2web_shared',
      'description' => t('A <em>page</em> is a simple method for creating and displaying information that rarely changes, such as an "About us" section of a website. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'views_text' => array(
      'name' => t('Views text'),
      'module' => 'fast2web_shared',
      'description' => t('Views text content which makes it possible to configure the views title, header, footer and empty text per views, per language.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
    'gallery' => array(
      'name' => t('Gallery'),
      'module' => 'fast2web_shared',
      'description' => t('A gallery allows uploading and displaying images in an overview or slideshow.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'blog' => array(
      'name' => t('Blog entry'),
      'module' => 'fast2web_shared',
      'description' => t('A blog is an online diary.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'files' => array(
      'name' => t('Downloads'),
      'module' => 'fast2web_shared',
      'description' => t('A list of files available for download.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );

  $conimbo_ctypes_disabled = variable_get('conimbo_ctypes_disabled', array());

  foreach ($items as $key => $item) {
    if (isset($conimbo_ctypes_disabled[$key])) {
      unset($items[$key]);
    }
  }

  return $items;
}
