<?php

/**
 * Panels installation.
 */

$panels_tables = array(
  'display' => array(
    0 => array(
      'did' => 1,
      'layout' => 'twocol_threerow_65_35',
      'layout_settings' => 'a:0:{}',
      'panel_settings' => 'a:0:{}',
      'cache' => 'a:0:{}',
      'title' => '',
      'hide_title' => 0,
      'title_pane' => 0,
    ),
    1 => array(
      'did' => 2,
      'layout' => 'twocol_threerow_65_35',
      'layout_settings' => 'a:0:{}',
      'panel_settings' => 'a:0:{}',
      'cache' => 'a:0:{}',
      'title' => '',
      'hide_title' => 0,
      'title_pane' => 0,
    ),
    2 => array(
      'did' => 3,
      'layout' => 'fourcol_25_25_25_25',
      'layout_settings' => 'a:0:{}',
      'panel_settings' => 'a:0:{}',
      'cache' => 'a:0:{}',
      'title' => '',
      'hide_title' => 0,
      'title_pane' => 0,
    ),
    3 => array(
      'did' => 4,
      'layout' => 'fourcol_25_25_25_25',
      'layout_settings' => 'a:0:{}',
      'panel_settings' => 'a:0:{}',
      'cache' => 'a:0:{}',
      'title' => '',
      'hide_title' => 0,
      'title_pane' => 0,
    ),
    4 => array(
      'did' => 5,
      'layout' => 'twocol_threerow_65_35',
      'layout_settings' => 'a:0:{}',
      'panel_settings' => 'a:1:{s:14:"style_settings";a:6:{s:7:"default";N;s:9:"first_row";N;s:16:"second_row_first";N;s:15:"second_row_last";N;s:15:"third_row_first";N;s:14:"third_row_last";N;}}',
      'cache' => 'a:0:{}',
      'title' => '',
      'hide_title' => 0,
      'title_pane' => 0,
    ),
    5 => array(
      'did' => 6,
      'layout' => 'campaign',
      'layout_settings' => 'a:0:{}',
      'panel_settings' => 'a:1:{s:14:"style_settings";a:16:{s:7:"default";N;s:3:"top";N;s:10:"left_above";N;s:11:"right_above";N;s:6:"middle";N;s:10:"left_below";N;s:11:"right_below";N;s:6:"bottom";N;s:4:"left";N;s:5:"right";N;s:9:"first_row";N;s:16:"second_row_first";N;s:15:"second_row_last";N;s:15:"third_row_first";N;s:14:"third_row_last";N;s:17:"second_row_middle";N;}}',
      'cache' => 'a:0:{}',
      'title' => '',
      'hide_title' => 0,
      'title_pane' => 0,
    ),
    6 => array(
      'did' => 7,
      'layout' => 'fourcol_25_25_25_25',
      'layout_settings' => 'a:0:{}',
      'panel_settings' => 'a:1:{s:14:"style_settings";a:6:{s:7:"default";N;s:5:"first";N;s:6:"second";N;s:5:"third";N;s:6:"fourth";N;s:6:"middle";N;}}',
      'cache' => 'a:0:{}',
      'title' => '',
      'hide_title' => 0,
      'title_pane' => 0,
    ),
    7 => array(
      'did' => 8,
      'layout' => '	threecol_33_34_33',
      'layout_settings' => 'a:0:{}',
      'panel_settings' => 'a:1:{s:14:"style_settings";a:4:{s:7:"default";N;s:4:"left";N;s:6:"middle";N;s:5:"right";N;}}',
      'cache' => 'a:0:{}',
      'title' => '',
      'hide_title' => 1,
      'title_pane' => 0,
    ),
  ),
  'node' => array(
    0 => array(
      'nid' => $nids_by_type['panel']['en-us'][0],
      'css_id' => '',
      'did' => 1,
    ),
    1 => array(
      'nid' => $nids_by_type['panel']['nl'][0],
      'css_id' => '',
      'did' => 2,
    ),
    2 => array(
      'nid' => $nids_by_type['panel']['fr'][0],
      'css_id' => '',
      'did' => 5,
    ),
    3 => array(
      'nid' => $nids_by_type['panel']['fr'][1],
      'css_id' => '',
      'did' => 6,
    ),
  ),
  'mini' => array(
    0 => array(
      'pid' => 1,
      'name' => 'doormat_en_us',
      'category' => '',
      'did' => 3,
      'admin_title' => 'Doormat English',
      'admin_description' => '',
      'requiredcontexts' => 'b:0;',
      'contexts' => 'b:0;',
      'relationships' => 'b:0;',
    ),
    1 => array(
      'pid' => 2,
      'name' => 'doormat_nl',
      'category' => '',
      'did' => 4,
      'admin_title' => 'Doormat Nederlands',
      'admin_description' => '',
      'requiredcontexts' => 'b:0;',
      'contexts' => 'b:0;',
      'relationships' => 'b:0;',
    ),
    2 => array(
      'pid' => 3,
      'name' => 'doormat_fr',
      'category' => '',
      'did' => 7,
      'admin_title' => 'Doormat Français',
      'admin_description' => '',
      'requiredcontexts' => 'b:0;',
      'contexts' => 'b:0;',
      'relationships' => 'b:0;',
    ),
  ),
  'pane' => array(array('pid'=>1,'did'=>1,'panel'=>'first_row','type'=>'custom','subtype'=>'custom','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:5:{s:11:"admin_title";s:0:"";s:5:"title";s:7:"Welcome";s:4:"body";s:70:"Welcome on your homepage. You can easily add new content on this page.";s:6:"format";s:1:"2";s:10:"substitute";i:1;}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>2,'did'=>2,'panel'=>'first_row','type'=>'custom','subtype'=>'custom','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:5:{s:11:"admin_title";s:0:"";s:5:"title";s:6:"Welkom";s:4:"body";s:77:"Welkom op je voorpagina. Je kan gemakkelijk inhoud toevoegen aan deze pagina.";s:6:"format";s:1:"2";s:10:"substitute";i:1;}','cache'=>'a:0:{}','style'=>'a:0:{}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>22,'did'=>5,'panel'=>'second_row_first','type'=>'block','subtype'=>'views-News-block_3','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>4,'did'=>2,'panel'=>'second_row_first','type'=>'block','subtype'=>'views-News-block_1','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:0:{}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>12,'did'=>3,'panel'=>'third','type'=>'block','subtype'=>'menu-menu-footer-en-us','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>11,'did'=>3,'panel'=>'fourth','type'=>'block','subtype'=>'node-0','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>6,'did'=>4,'panel'=>'first','type'=>'custom','subtype'=>'custom','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:5:{s:11:"admin_title";s:0:"";s:5:"title";s:12:"Doormat item";s:4:"body";s:68:"U kan gemakkelijk de structuur en inhoud van deze doormat aanpassen.";s:6:"format";s:1:"2";s:10:"substitute";b:1;}','cache'=>'a:0:{}','style'=>'a:0:{}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>14,'did'=>3,'panel'=>'first','type'=>'block','subtype'=>'menu-secondary-links','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>10,'did'=>3,'panel'=>'second','type'=>'menu_tree','subtype'=>'menu-main-en-us','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:11:{s:9:"menu_name";s:15:"menu-main-en-us";s:11:"parent_mlid";i:0;s:10:"title_link";i:0;s:11:"admin_title";s:0:"";s:5:"level";s:1:"1";s:6:"follow";i:0;s:5:"depth";i:0;s:8:"expanded";i:0;s:4:"sort";i:0;s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>15,'did'=>1,'panel'=>'first_row','type'=>'block','subtype'=>'views-filelist-block_1','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>3),
    array('pid'=>16,'did'=>1,'panel'=>'first_row','type'=>'block','subtype'=>'views-Downloads-block_1','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>2),
    array('pid'=>17,'did'=>5,'panel'=>'first_row','type'=>'custom','subtype'=>'custom','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:5:{s:11:"admin_title";s:7:"Accueil";s:5:"title";s:38:"Bienvenue sur le site Fast2Web Starter";s:4:"body";s:157:"Le profil Starter de Fast2Web vous permet de rapidement mettre en place un site web avec toutes les fonctionalités habituelles ( pages, news, contact, ... )";s:6:"format";s:1:"2";s:10:"substitute";b:1;}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>19,'did'=>5,'panel'=>'second_row_last','type'=>'block','subtype'=>'views-Downloads-block_1','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>20,'did'=>1,'panel'=>'first_row','type'=>'block','subtype'=>'views-News-block_3','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>1),
    array('pid'=>39,'did'=>7,'panel'=>'second','type'=>'block','subtype'=>'views-Blogs-block_4','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>38,'did'=>7,'panel'=>'first','type'=>'block','subtype'=>'views-News-block_4','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>30,'did'=>5,'panel'=>'second_row_last','type'=>'block','subtype'=>'views-Polls-block_1','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:15:"Dernier Sondage";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>1),
    array('pid'=>48,'did'=>1,'panel'=>'first_row','type'=>'custom','subtype'=>'custom','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:5:{s:11:"admin_title";s:0:"";s:5:"title";s:4:"test";s:4:"body";s:4:"test";s:6:"format";s:1:"1";s:10:"substitute";b:1;}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>4),
    array('pid'=>32,'did'=>7,'panel'=>'fourth','type'=>'block','subtype'=>'node-0','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>36,'did'=>6,'panel'=>'first_row','type'=>'custom','subtype'=>'custom','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:5:{s:11:"admin_title";s:14:"Campagne Intro";s:5:"title";s:17:"Fast2Web Campagne";s:4:"body";s:432:"Le profil Fast2Web Campagne est disponible pour rapidement mettre en place un site de campagne de communication. Ce type de site est orienté vers la diffusion autour d\'un thème précis ( par ex une campagne de communication autour d\'une élection, une nouvelle loi, un nouveau label écologique, ... ). Idéalement la première page de ce type de site doit présenter le thème et orienter l\'utilisateur vers les points d\'actions.";s:6:"format";s:1:"1";s:10:"substitute";b:1;}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>35,'did'=>7,'panel'=>'third','type'=>'block','subtype'=>'menu-menu-main-fr','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>44,'did'=>6,'panel'=>'third_row_last','type'=>'block','subtype'=>'views-twitter-block2','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:1;s:19:"override_title_text";s:16:"Tweets en direct";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>46,'did'=>6,'panel'=>'third_row_first','type'=>'block','subtype'=>'views-News-block_3','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>50,'did'=>6,'panel'=>'second_row_first','type'=>'custom','subtype'=>'custom','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:5:{s:11:"admin_title";s:6:"test1a";s:5:"title";s:6:"test1b";s:4:"body";s:22:"<img src="/ex1.jpg" />";s:6:"format";s:1:"2";s:10:"substitute";b:1;}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>51,'did'=>6,'panel'=>'second_row_middle','type'=>'custom','subtype'=>'custom','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:5:{s:11:"admin_title";s:5:"test2";s:5:"title";s:5:"test2";s:4:"body";s:22:"<img src="/ex2.jpg" />";s:6:"format";s:1:"2";s:10:"substitute";b:1;}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0),
    array('pid'=>52,'did'=>6,'panel'=>'second_row_last','type'=>'custom','subtype'=>'custom','shown'=>1,'access'=>'a:0:{}','configuration'=>'a:5:{s:11:"admin_title";s:6:"test3a";s:5:"title";s:6:"test3b";s:4:"body";s:22:"<img src="/ex3.jpg" />";s:6:"format";s:1:"2";s:10:"substitute";b:1;}','cache'=>'a:0:{}','style'=>'a:1:{s:8:"settings";N;}','css'=>'a:0:{}','extras'=>'a:0:{}','position'=>0)
  ),
);

db_query("TRUNCATE {ctools_object_cache}");

foreach ($panels_tables as $table => $rows) {
  db_query("TRUNCATE {panels_$table}");

  foreach ($rows as $row => $fields) {
    $field_names = implode(',', array_keys($fields));
    $field_types_array = array();
    foreach ($fields as $field_name => $field_value) {
      if (is_numeric($field_value)) {
        $field_types_array[] = '%d';
      } else {
        $field_types_array[] = '\'%s\'';
      }
    }
    $field_values = array_values($fields);
    $field_types = implode(',', array_values($field_types_array));
    db_query("INSERT INTO {panels_$table} ($field_names) VALUES ($field_types)", $field_values);
  }
}