<?php

function taxonomy_get_blog() {
  $taxonomy_export = array (
    'vocabulary' => array(
      'name' => 'Blog category',
      'description' => '',
      'help' => 'Choose a blog category.',
      'relations' => '1',
      'hierarchy' => '1',
      'multiple' => '0',
      'required' => '0',
      'tags' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => '',
      'nodes' =>
     		array (
        	'blog' => 'blog',
      	),
      'i18nmode' => '3',
    ),
  );

  return $taxonomy_export;
}