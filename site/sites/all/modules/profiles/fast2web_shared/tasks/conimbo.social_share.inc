<?php

// Set default values for social_share module.

// Content types.
$node_types = array(
  'blog' => 'blog',
  'news' => 'news',
  'files' => 0,
  'error' => 0,
  'faq' => 0,
  'gallery' => 0,
  'page' => 0,
  'panel' => 0,
  'pool' => 0,
  'views_text' => 0,
  'webform' => 0,
);
variable_set('social_share_node_types', $node_types);

// Display in teaser view.
variable_set('social_share_teaser', TRUE);

// Open links in new window.
variable_set('social_share_new_window', FALSE);

// Makes links available as a block.
variable_set('social_share_block', FALSE);

// Share label.
variable_set('social_share_label', 'Share to');

// Weight.
variable_set('social_share_weight', '0');

// Enabled share links.
$social_sites = array(
  'facebook' => 'facebook',
  'twitter' => 'twitter',
  'googleplus' => 'googleplus',
  'linkedin' => 'linkedin',
  'delicious' => 'delicious',
  'myspace' => 0,
  'msnlive' => 0,
  'yahoo' => 0,
  'orkut' => 0,
  'digg' => 0,
);
variable_set('social_share_sites',  $social_sites);

// Twitter share method.
variable_set('social_share_twitter_method', 'new');

// Truncate titles when sharing to twitter.
variable_set('social_share_twitter_truncate', FALSE);

// Max description length.
variable_set('social_share_max_desc_length', '50');
