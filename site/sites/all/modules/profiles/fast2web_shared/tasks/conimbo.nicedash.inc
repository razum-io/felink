<?php

  // Configure the first dashboard.
  cache_clear_all('nice_dash_objects', 'cache');
  cache_clear_all('plugins:nice_dash:plugins', 'cache');
  db_query("INSERT INTO {nice_dash_dashboards} (name) VALUES ('%s')", 'Features');
  db_query("TRUNCATE {nice_dash_config}");
  db_query("TRUNCATE {nice_dash_widgets}");

  db_query("INSERT INTO `nice_dash_widgets` (`wid`, `widget_key`, `widget_plugin`, `title`, `description`, `custom`) VALUES
(1, 'content_add', 'nice_dash_content', 'Add content', '', 0),
(2, 'content_latest', 'nice_dash_content', 'Latest content', '', 0),
(3, 'content_sticky', 'nice_dash_content', 'Sticky content', '', 0),
(4, 'content_promoted', 'nice_dash_content', 'Promoted content', '', 0),
(5, 'comment_configuration', 'nice_dash_comments', 'Configure comments', '', 0),
(6, 'latest_comments', 'nice_dash_comments', 'Latest comments', '', 0),
(7, 'ga_visits', 'nice_dash_ga', 'Visit statistics from Google Analytics', '', 0),
(8, 'scheduler_unpublish', 'nice_dash_scheduler', 'Content to be unpublished', '', 0),
(9, 'scheduler_publish', 'nice_dash_scheduler', 'Content to be published', '', 0),
(10, 'user_latest', 'nice_dash_user', 'Latest registered users', '', 0),
(11, 'user_last_login', 'nice_dash_user', 'Latest logged in users', '', 0),
(12, 'video_configuration', 'nice_dash_video', 'Configure video', '', 0),
(13, 'fivestar_configuration', 'nice_dash_fivestar', 'Configure fivestar', '', 0),
(14, 'features_configuration', 'nice_dash_features', 'Configure features', '', 0);");

  db_query("INSERT INTO `nice_dash_config` (`wid`, `did`, `weight`, `region`) VALUES
(1, 1, 1, 'disabled'),
(2, 1, 1, 'disabled'),
(3, 1, 1, 'disabled'),
(4, 1, 1, 'disabled'),
(5, 1, 0, 'left'),
(6, 1, 1, 'disabled'),
(7, 1, 1, 'disabled'),
(8, 1, 1, 'disabled'),
(9, 1, 1, 'disabled'),
(10, 1, 0, 'disabled'),
(11, 1, 1, 'disabled'),
(12, 1, 0, 'right'),
(13, 1, 1, 'left'),
(14, 1, -1, 'header');");
