<?php
  $key = 0;
  $aliases = array(
    $key++ => array(
      'src' => 'news',
      'dest' => 'nieuws',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'news/rss.xml',
      'dest' => 'nieuws/rss.xml',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'galleries',
      'dest' => 'galerijen',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'blogs/archive',
      'dest' => 'blogs/archief',
      'language' => 'nl',
    ),
  );

  foreach ($aliases as $key => $alias) {
    path_set_alias($alias['src'], $alias['dest'], NULL, $alias['language']);
  }