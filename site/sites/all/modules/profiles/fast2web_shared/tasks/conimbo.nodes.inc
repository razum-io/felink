<?php
  //Beware for tnid's of translated nodes!
  //Beware of nid values of nodereference fields!

  $nodes = array();
  //We need to save nids for some content types for further use. F.i. referencing in panels and nodequeues.
  $nids_by_type = array();
  $key = 0;
  $nodes[++$key] = array(
      'type' => 'error',
      'title' => 'Page not found',
      'body' => 'The page you requested was not found.',
      'language' => 'en-us',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key,
  );
  $nodes[++$key] = array(
      'type' => 'error',
      'title' => 'Pagina niet gevonden',
      'body' => 'De pagina die u opzocht werd niet gevonden.',
      'language' => 'nl',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key-1,
  );
  $nodes[++$key] = array(
      'type' => 'views_text',
      'title' => 'News in this category',
      'body' => '',
      'language' => 'en-us',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key,
      'one_views_text' => array('News|block_2' => 'News|block_2'),
       'field_views_text_empty' => array(
        0 => array(
          'value' => '<p><a href="en/node/add/news?language=en-us">Click here to add your first news item.</a></p>',
          'format' => 2,
        ),
      ),
      'field_views_text_title' => array(
        0 => array('value' => 'News in [conimbo-single-term]')
      ),
  );
  $nodes[++$key] = array(
      'type' => 'views_text',
      'title' => 'Nieuws in deze categorie',
      'body' => '',
      'language' => 'nl',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key-1,
      'one_views_text' => array('News|block_2' => 'News|block_2'),
      'field_views_text_empty' => array(
        0 => array(
          'value' => '<p><a href="en/node/add/news?language=nl">Klik hier om je eerste nieuws item toe te voegen.</a></p>',
          'format' => 2,
        ),
      ),
      'field_views_text_title' => array(
        0 => array('value' => 'Nieuws in [conimbo-single-term]')
      ),
  );
  $nodes[++$key] = array(
    'type' => 'panel',
    'title' => 'Homepage',
    'body' => 'Homepage english',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']][0] = $key;
  $nodes[++$key] = array(
    'type' => 'panel',
    'title' => 'homepage',
    'body' => 'Homepage nederlands',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']][0] = $key;
  $nodes[++$key] = array(
      'type' => 'views_text',
      'title' => 'News',
      'body' => '',
      'language' => 'en-us',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key,
      'one_views_text' => array('News|page_1' => 'News|page_1'),
      'field_views_text_empty' => array(
        0 => array(
          'value' => '<p><a href="node/add/news?language=en-us">Click here to add your first news item.</a></p>',
          'format' => 2,
        ),
      ),
      'field_views_text_title' => array(
        0 => array('value' => 'News')
      ),
  );
  $nodes[++$key] = array(
      'type' => 'views_text',
      'title' => 'Nieuws',
      'body' => '',
      'language' => 'nl',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key-1,
      'one_views_text' => array('News|page_1' => 'News|page_1'),
      'field_views_text_empty' => array(
        0 => array(
          'value' => '<p><a href="node/add/news?language=nl">Klik hier om je eerste nieuws item toe te voegen.</a></p>',
          'format' => 2,
        ),
      ),
      'field_views_text_title' => array(
        0 => array('value' => 'Nieuws')
      ),
  );
  $nodes[++$key] = array(
      'type' => 'news',
      'title' => 'This is the title of your first news item',
      'field_news_teaser' => array(
        0 => array('value' => 'This is the teaser content for this news item in overviews.')
      ),
      'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This
style will used throughout the whole website as a default, non formatted style
for textual content.</p>',
      'field_news_source' => array(
        0 => array('value' => 'This is the source of the news item')
      ),
      'language' => 'en-us',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key,
  );
  $nodes[++$key] = array(
      'type' => 'news',
      'title' => 'Dit is de titel van uw eerste nieuwsbericht',
      'field_news_teaser' => array(
        0 => array('value' => 'Dit is de intro van dit nieuwsbericht in overzichten.')
      ),
      'body' => 'Dit is uw eerste nieuwsbericht.',
      'language' => 'nl',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key-1,
  );
  $nodes[++$key] = array(
      'type' => 'page',
      'title' => 'Disclaimer',
      'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more
prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h3>This is a level 3 subtitle.</h3>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h4>This is a level 4 subtitle.</h4>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h5>This is a level 5 subtitle.</h5>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h6>This is a level 6 subtitle.</h6>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<ul>
  <li>Unordered list first level.</li>
  <li>Unordered list first level.
    <ul>
      <li>Unordered list second level.</li>
      <li>Unordered list second level.</li>
    </ul>
  </li>
  <li>Unordered list first level.</li>
</ul>
<ol>
  <li>Ordered list first level.</li>
  <li>Ordered list first level.
    <ol>
      <li>Ordered list second level.</li>
      <li>Ordered list second level.</li>
    </ol>
  </li>
  <li>Ordered list first level.</li>
</ol>
<p class="highlight1">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<p class="highlight2">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<div class="messages warning">
This is a warning text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages status">
This is a status text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages error">
This is an error text. This paragraph style is used to give feedback to the user.
</div>
<blockquote>
This is a quote. This paragraph style is commonly used to point out some quoted
text lines.
</blockquote>
<p>This is a <a href="">link tag</a> example</a><br />
This is an <i>italic tag</i> example<br />
This is a <strike>strike tag</strike> example<br />
This is a <strong>strong</strong> tag example<br />
This is <sub>a sub tag</sub> example<br />
This is a <sup>sup tag</sup> example</p>',
      'language' => 'en-us',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key,
  );
  $nodes[++$key] = array(
      'type' => 'page',
      'title' => 'Disclaimer',
      'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more
prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h3>This is a level 3 subtitle.</h3>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h4>This is a level 4 subtitle.</h4>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h5>This is a level 5 subtitle.</h5>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h6>This is a level 6 subtitle.</h6>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<ul>
  <li>Unordered list first level.</li>
  <li>Unordered list first level.
    <ul>
      <li>Unordered list second level.</li>
      <li>Unordered list second level.</li>
    </ul>
  </li>
  <li>Unordered list first level.</li>
</ul>
<ol>
  <li>Ordered list first level.</li>
  <li>Ordered list first level.
    <ol>
      <li>Ordered list second level.</li>
      <li>Ordered list second level.</li>
    </ol>
  </li>
  <li>Ordered list first level.</li>
</ol>
<p class="highlight1">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<p class="highlight2">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<div class="messages warning">
This is a warning text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages status">
This is a status text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages error">
This is an error text. This paragraph style is used to give feedback to the user.
</div>
<blockquote>
This is a quote. This paragraph style is commonly used to point out some quoted
text lines.
</blockquote>
<p>This is a <a href="">link tag</a> example</a><br />
This is an <i>italic tag</i> example<br />
This is a <strike>strike tag</strike> example<br />
This is a <strong>strong</strong> tag example<br />
This is <sub>a sub tag</sub> example<br />
This is a <sup>sup tag</sup> example</p>.',
      'language' => 'nl',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key-1,
  );
  $nodes[++$key] = array(
      'type' => 'page',
      'title' => 'Content page',
      'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more
prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h3>This is a level 3 subtitle.</h3>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h4>This is a level 4 subtitle.</h4>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h5>This is a level 5 subtitle.</h5>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h6>This is a level 6 subtitle.</h6>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<ul>
  <li>Unordered list first level.</li>
  <li>Unordered list first level.
    <ul>
      <li>Unordered list second level.</li>
      <li>Unordered list second level.</li>
    </ul>
  </li>
  <li>Unordered list first level.</li>
</ul>
<ol>
  <li>Ordered list first level.</li>
  <li>Ordered list first level.
    <ol>
      <li>Ordered list second level.</li>
      <li>Ordered list second level.</li>
    </ol>
  </li>
  <li>Ordered list first level.</li>
</ol>
<p class="highlight1">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<p class="highlight2">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<div class="messages warning">
This is a warning text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages status">
This is a status text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages error">
This is an error text. This paragraph style is used to give feedback to the user.
</div>
<blockquote>
This is a quote. This paragraph style is commonly used to point out some quoted
text lines.
</blockquote>
<p>This is a <a href="">link tag</a> example</a><br />
This is an <i>italic tag</i> example<br />
This is a <strike>strike tag</strike> example<br />
This is a <strong>strong</strong> tag example<br />
This is <sub>a sub tag</sub> example<br />
This is a <sup>sup tag</sup> example</p>.',
      'language' => 'en-us',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key,
  );
  $nodes[++$key] = array(
      'type' => 'page',
      'title' => 'Pagina',
      'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more
prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h3>This is a level 3 subtitle.</h3>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h4>This is a level 4 subtitle.</h4>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h5>This is a level 5 subtitle.</h5>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h6>This is a level 6 subtitle.</h6>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<ul>
  <li>Unordered list first level.</li>
  <li>Unordered list first level.
    <ul>
      <li>Unordered list second level.</li>
      <li>Unordered list second level.</li>
    </ul>
  </li>
  <li>Unordered list first level.</li>
</ul>
<ol>
  <li>Ordered list first level.</li>
  <li>Ordered list first level.
    <ol>
      <li>Ordered list second level.</li>
      <li>Ordered list second level.</li>
    </ol>
  </li>
  <li>Ordered list first level.</li>
</ol>
<p class="highlight1">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<p class="highlight2">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<div class="messages warning">
This is a warning text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages status">
This is a status text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages error">
This is an error text. This paragraph style is used to give feedback to the user.
</div>
<blockquote>
This is a quote. This paragraph style is commonly used to point out some quoted
text lines.
</blockquote>
<p>This is a <a href="">link tag</a> example</a><br />
This is an <i>italic tag</i> example<br />
This is a <strike>strike tag</strike> example<br />
This is a <strong>strong</strong> tag example<br />
This is <sub>a sub tag</sub> example<br />
This is a <sup>sup tag</sup> example</p>.',
      'language' => 'nl',
      'uid' => 1,
      'status' => 1,
      'tnid' => $key-1,
    );
  $nodes[++$key] = array(
    'type' => 'panel',
    'title' => 'Accueil',
    'body' => 'Accueil',
    'language' => 'fr',
    'uid' => 1,
    'status' => 1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']][0] = $key;
  $nodes[++$key] = array(
    'type' => 'panel',
    'title' => 'Fast2Web Campagne',
    'body' => '<p>Front page panel for campaign sites</p>',
    'language' => 'fr',
    'uid' => 1,
    'status' => 1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']][1] = $key;

  $nids = array();
  foreach ($nodes as $key => $data) {
    $node = (object) $data;
    $node->format = 2;
    node_save($node);
  }