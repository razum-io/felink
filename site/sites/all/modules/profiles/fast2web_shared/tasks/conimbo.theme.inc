<?php

  /**
   * Maintence theme settings.
   */
  require_once('sites/all/modules/contrimbo/conimbo/includes/conimbo.theme.inc');
  $themes = system_theme_data();

  db_query("UPDATE {system} set status = 1 WHERE type ='theme' AND name = 'cloud_theme'");
  db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' AND name = 'fast2web'");
  // Configure maintenance theme
  if (function_exists('drush_get_option')) {
    $form_state = array();
    $form_state['values']['theme_default'] = 'fast2web';
    $form_state['group'] = drush_get_option('web_group', 'www-data');

    conimbo_save_maintenance_theme(array(), $form_state);
  }