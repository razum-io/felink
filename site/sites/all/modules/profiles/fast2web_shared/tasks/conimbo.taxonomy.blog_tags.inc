<?php
function taxonomy_get_blog_tags() {
  $taxonomy_export = array (
    'vocabulary' => array(
      'name' => 'Blog free tagging',
      'description' => '',
      'help' => 'Add tags to your blog posts.',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '0',
      'required' => '0',
      'tags' => '1',
      'module' => 'taxonomy',
      'weight' => '2',
      'language' => '',
      'nodes' =>
     		array (
        	'blog' => 'blog',
      	),
      'i18nmode' => '3',
    ),
  );

  return $taxonomy_export;
}