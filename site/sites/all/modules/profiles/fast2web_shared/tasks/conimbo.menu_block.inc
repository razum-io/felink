<?php
/**
 * Blocks are created in the conimbo.blocks.inc. Here are saved only menu_block
 * specific settings.
 */

$menu_blocks = array(
  array(
    'delta' => 1,
    'title_link' => '',
    'admin_title' => 'French (level 2)',
    'parent'      => 'menu-main-fr:0',
    'level'       => 2,
    'follow'      => 0,
    'depth'       => 0,
    'expanded'    => 0,
    'sort'        => 0
  ),
  array(
    'delta' => 2,
    'title_link' => '',
    'admin_title' => 'Dutch (level 2)',
    'parent'      => 'menu-main-nl:0',
    'level'       => 2,
    'follow'      => 0,
    'depth'       => 0,
    'expanded'    => 0,
    'sort'        => 0
  ),
);
$ids = array();
foreach ($menu_blocks as $menu_block) {
  // Get block delta.
  $delta = $menu_block['delta'];
  unset($menu_block['delta']);
  $ids[] = $delta;
  foreach ($menu_block as $variable => $value) {
    // Save menu block setting for a specific delta.
    variable_set('menu_block_' . $delta . '_' . $variable, $value);
  }
}
// Save ids.
variable_set('menu_block_ids', $ids);