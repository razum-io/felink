<?php

$standard_roles = array(
  array('rid' => '1','name' => 'anonymous user'),
  array('rid' => '2','name' => 'authenticated user'),
);

$extra_roles = array(
  array('rid' => '3','name' => 'editor'),
  array('rid' => '4','name' => 'builder'),
  array('rid' => '5','name' => 'configurator'),
  array('rid' => '6','name' => 'hoofdredacteur'),
  array('rid' => '8','name' => 'dummyrole')
);

$permissions = array(
  array('rid' => '1','perm' => 'access comments, post comments, access site-wide contact form, view faq page, fedict search content, view imagecache gallery_thumbnail, view imagecache large, view imagecache medium, view imagecache slide_show, view imagecache small, access content, cancel own vote, vote on polls, search content, access site map','tid' => '0'),
  array('rid' => '2','perm' => 'access comments, post comments, post comments without approval, access site-wide contact form, view faq page, fedict search content, view imagecache gallery_thumbnail, view imagecache large, view imagecache medium, view imagecache slide_show, view imagecache small, access content, restrictive menu permissions, cancel own vote, vote on polls, search content, access site map','tid' => '0'),
  array('rid' => '4','perm' => 'display admin pages in another language, access admin menu, access administration menu, administer blocks, access comments, administer comments, post comments, post comments without approval, administer clean-urls, administer content node settings, administer date-time, administer error reporting, administer file system, administer modules, administer site information, administer themes, display site building menu, display site configuration menu, access tinymce filemanager, access tinymce imagemanager, view all modules, access site-wide contact form, administer site-wide contact form, administer dc storm, access display suite, administer nd, administer styles, administer vd, configure layout for nd, configure layout for vd, export and import settings, revert overridden settings, administer faq, administer faq order, create faq, delete faq content, delete own faq content, edit faq, edit own faq, view faq page, fedict search content, administer @font-your-face, administer all languages, administer translations, view imagecache gallery_thumbnail, view imagecache large, view imagecache medium, view imagecache slide_show, view imagecache small, submit translations to localization server, use on-page translation, administer languages, administer menu, access content, administer content types, administer nodes, create feed content, create feed_item content, create rss_importer content, create rss_item content, create webform content, delete any feed content, delete any feed_item content, delete any rss_importer content, delete any rss_item content, delete any webform content, delete own feed content, delete own feed_item content, delete own rss_importer content, delete own rss_item content, delete own webform content, delete revisions, edit any feed content, edit any feed_item content, edit any rss_importer content, edit any rss_item content, edit any webform content, edit own feed content, edit own feed_item content, edit own rss_importer content, edit own rss_item content, edit own webform content, revert revisions, view revisions, administer meta tags, edit meta tag DESCRIPTION, edit meta tag KEYWORDS, access actions, restrictive menu permissions, set page title, administer advanced pane settings, administer pane access, administer pane visibility, administer panels layouts, use panels caching features, use panels dashboard, use panels in place editing, view all panes, view pane admin links, administer panel-nodes, create panel-nodes, delete any panel-nodes, delete own panel-nodes, edit any panel-nodes, edit own panel-nodes, create url aliases, cancel own vote, create poll content, delete any poll content, delete own poll content, edit any poll content, edit own poll content, inspect all votes, vote on polls, schedule (un)publishing of nodes, search content, access site map, upload files with swfupload, access administration pages, access site reports, administer actions, administer files, administer site configuration, select different theme, administer taxonomy, translate content, manage fr translation overview priorities, manage nl translation overview priorities, view translation overview assigments, add twitter accounts, use global twitter account, administer permissions, administer users','tid' => '0'),
  array('rid' => '6','perm' => 'display admin pages in another language, access admin menu, access comments, administer comments, post comments, post comments without approval, access tinymce filemanager, access tinymce imagemanager, access site-wide contact form, configure layout for nd, configure layout for vd, administer faq, administer faq order, create faq, delete faq content, delete own faq content, edit faq, edit own faq, view faq page, create blog content, create files content, create gallery content, create news content, create page content, delete any blog content, delete any files content, delete any gallery content, delete any news content, delete any page content, delete own blog content, delete own files content, delete own gallery content, delete own news content, delete own page content, edit any blog content, edit any error content, edit any files content, edit any gallery content, edit any news content, edit any page content, edit own blog content, edit own files content, edit own gallery content, edit own news content, edit own page content, fedict search content, use advanced fedict search, administer feeds, administer all languages, administer translations, administer imagecache, flush imagecache, view imagecache gallery_thumbnail, view imagecache large, view imagecache medium, view imagecache slide_show, view imagecache small, access content, administer nodes, revert revisions, view revisions, administer meta tags, edit meta tag DESCRIPTION, edit meta tag KEYWORDS, access actions, restrictive menu permissions, set page title, use panels caching features, use panels dashboard, view all panes, create url aliases, cancel own vote, create poll content, delete any poll content, delete own poll content, edit any poll content, edit own poll content, vote on polls, schedule (un)publishing of nodes, search content, access site map, upload files with swfupload, access administration pages, translate content, manage fr translation overview priorities, manage nl translation overview priorities, view translation overview assigments, add twitter accounts, use global twitter account','tid' => '0'),
  array('rid' => '3','perm' => 'display admin pages in another language, access admin menu, access comments, administer comments, post comments, post comments without approval, access tinymce filemanager, access tinymce imagemanager, access site-wide contact form, administer faq, administer faq order, create faq, delete faq content, delete own faq content, edit faq, edit own faq, view faq page, create blog content, create files content, create gallery content, create news content, create page content, delete any blog content, delete any files content, delete any gallery content, delete any news content, delete any page content, delete own blog content, delete own files content, delete own gallery content, delete own news content, delete own page content, edit any blog content, edit any files content, edit any gallery content, edit any news content, edit any page content, edit own blog content, edit own files content, edit own gallery content, edit own news content, edit own page content, fedict search content, use advanced fedict search, administer all languages, administer translations, administer imagecache, flush imagecache, view imagecache gallery_thumbnail, view imagecache large, view imagecache medium, view imagecache slide_show, view imagecache small, access content, administer nodes, delete revisions, revert revisions, view revisions, administer meta tags, edit meta tag DESCRIPTION, edit meta tag KEYWORDS, access actions, restrictive menu permissions, set page title, use panels caching features, use panels dashboard, view all panes, create url aliases, cancel own vote, create poll content, delete any poll content, delete own poll content, edit any poll content, edit own poll content, vote on polls, schedule (un)publishing of nodes, search content, access site map, upload files with swfupload, access administration pages, translate content, view translation overview assigments, add twitter accounts, use global twitter account','tid' => '0'),
  array('rid' => '5','perm' => 'display admin pages in another language, access admin menu, access administration menu, administer blocks, access comments, administer comments, post comments, post comments without approval, administer clean-urls, administer content node settings, administer date-time, administer error reporting, administer file system, administer modules, administer site information, administer themes, display site building menu, display site configuration menu, access tinymce filemanager, access tinymce imagemanager, view all modules, access site-wide contact form, administer site-wide contact form, administer dc storm, access display suite, administer nd, administer styles, administer vd, configure layout for nd, configure layout for vd, export and import settings, revert overridden settings, administer faq, administer faq order, create faq, delete faq content, delete own faq content, edit faq, edit own faq, view faq page, administer fedict search, fedict search content, use advanced fedict search, administer @font-your-face, administer hansel, test hansel, administer all languages, administer translations, administer inline form errors, administer imagecache, flush imagecache, view imagecache gallery_thumbnail, view imagecache large, view imagecache medium, view imagecache slide_show, view imagecache small, administer javascript libraries, submit translations to localization server, use on-page translation, administer lightbox2, download original image, administer languages, administer menu, access content, administer content types, administer nodes, create feed content, create feed_item content, create rss_importer content, create rss_item content, create webform content, delete any feed content, delete any feed_item content, delete any rss_importer content, delete any rss_item content, delete any webform content, delete own feed content, delete own feed_item content, delete own rss_importer content, delete own rss_item content, delete own webform content, delete revisions, edit any feed content, edit any feed_item content, edit any rss_importer content, edit any rss_item content, edit any webform content, edit own feed content, edit own feed_item content, edit own rss_importer content, edit own rss_item content, edit own webform content, revert revisions, view revisions, administer nodeaccess, grant deletable node permissions, grant editable node permissions, grant node permissions, grant own node permissions, administer meta tags, edit meta tag DESCRIPTION, edit meta tag KEYWORDS, access actions, restrictive menu permissions, set page title, administer advanced pane settings, administer pane access, administer pane visibility, use panels caching features, use panels dashboard, view all panes, create url aliases, administer pathauto, notify of path changes, cancel own vote, create poll content, delete any poll content, delete own poll content, edit any poll content, edit own poll content, inspect all votes, vote on polls, schedule (un)publishing of nodes, search content, use advanced search, access site map, upload files with swfupload, access administration pages, access site reports, administer actions, administer files, administer site configuration, select different theme, administer taxonomy, translate content, manage fr translation overview priorities, manage nl translation overview priorities, view translation overview assigments, add twitter accounts, use global twitter account, administer permissions, administer users, access all views, administer views','tid' => '0'),
  array('rid' => '8','perm' => 'display admin pages in another language, access admin menu, access comments, administer comments, post comments, post comments without approval, access tinymce filemanager, access tinymce imagemanager, access site-wide contact form, configure layout for nd, configure layout for vd, administer faq, administer faq order, create faq, delete faq content, delete own faq content, edit faq, edit own faq, view faq page, fedict search content, administer all languages, administer translations, view imagecache gallery_thumbnail, view imagecache large, view imagecache medium, view imagecache slide_show, view imagecache small, access content, delete revisions, revert revisions, view revisions, administer meta tags, edit meta tag DESCRIPTION, edit meta tag KEYWORDS, access actions, restrictive menu permissions, set page title, use panels caching features, use panels dashboard, view all panes, create url aliases, cancel own vote, create poll content, delete any poll content, delete own poll content, edit any poll content, edit own poll content, vote on polls, schedule (un)publishing of nodes, search content, access site map, upload files with swfupload, access administration pages, translate content, manage fr translation overview priorities, manage nl translation overview priorities, view translation overview assigments, add twitter accounts, use global twitter account','tid' => '0')
);

// First wipe-out standard Drupal permissions.
foreach ($standard_roles as $role) {
  db_query("DELETE FROM {permission} WHERE rid = %d", $role['rid']);
}

// Create new roles.
foreach ($extra_roles as $role) {
  db_query("INSERT INTO {role} (rid, name) VALUES (%d, '%s')", $role['rid'], $role['name']);
}

// Add permissions.
foreach ($permissions as $permission) {
  db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, '%s')", $permission['rid'], $permission['perm']);
}

// Filters & Better formats.

db_query("TRUNCATE {filters}");
db_query("TRUNCATE {filter_formats}");
db_query("TRUNCATE {better_formats_defaults}");

$filter_formats = array(
  0 => array(
    'format' => '1',
    'name' => 'Filtered HTML',
    'roles' => ',1,2,',
    'cache' => 1,
  ),
  1 => array(
    'format' => '2',
    'name' => 'Full HTML',
    'roles' => ',1,2,3,4,5,6',
    'cache' => 1,
  ),
);

foreach ($filter_formats as $key => $format) {
  db_query("INSERT INTO {filter_formats} (format, name, roles, cache) VALUES (%d, '%s', '%s', %d)", $format['format'], $format['name'], $format['roles'], $format['cache']);
}

$filters = array(
  0 => array('format' => 1, 'module' => 'filter', 'delta' => 2, 'weight' => 0),
  1 => array('format' => 1, 'module' => 'filter', 'delta' => 0, 'weight' => 1),
  2 => array('format' => 1, 'module' => 'filter', 'delta' => 1, 'weight' => 2),
  3 => array('format' => 1, 'module' => 'filter', 'delta' => 3, 'weight' => 10),
  4 => array('format' => 2, 'module' => 'filter', 'delta' => 2, 'weight' => 0),
  5 => array('format' => 2, 'module' => 'filter', 'delta' => 3, 'weight' => 10),
  6 => array('format' => 2, 'module' => 'nodepicker', 'delta' => 0, 'weight' => 10),
);

foreach ($filters as $key => $filter) {
  db_query("INSERT INTO {filters} (format, module, delta, weight) VALUES (%d, '%s', %d, %d)", $filter['format'], $filter['module'], $filter['delta'], $filter['weight']);
}

$better_formats = array(
  0 => array('rid' => 1, 'type' => 'node', 'format' => 1, 'type_weight' => 1, 'weight' => -22),
  1 => array('rid' => 2, 'type' => 'node', 'format' => 1, 'type_weight' => 1, 'weight' => -23),
  2 => array('rid' => 3, 'type' => 'node', 'format' => 2, 'type_weight' => 1, 'weight' => -24),
  3 => array('rid' => 4, 'type' => 'node', 'format' => 2, 'type_weight' => 1, 'weight' => -25),
  4 => array('rid' => 5, 'type' => 'node', 'format' => 2, 'type_weight' => 1, 'weight' => -26),
  5 => array('rid' => 1, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -22),
  6 => array('rid' => 2, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -23),
  7 => array('rid' => 3, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -24),
  8 => array('rid' => 4, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -25),
  9 => array('rid' => 5, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -26),
  10 => array('rid' => 1, 'type' => 'block', 'format' => 1, 'type_weight' => 1, 'weight' => -22),
  11 => array('rid' => 2, 'type' => 'block', 'format' => 1, 'type_weight' => 1, 'weight' => -23),
  12 => array('rid' => 3, 'type' => 'block', 'format' => 2, 'type_weight' => 1, 'weight' => -24),
  13 => array('rid' => 4, 'type' => 'block', 'format' => 2, 'type_weight' => 1, 'weight' => -25),
  14 => array('rid' => 5, 'type' => 'block', 'format' => 2, 'type_weight' => 1, 'weight' => -26),
);

foreach ($better_formats as $key => $better_format) {
  db_query("INSERT INTO {better_formats_defaults} (rid, type ,format, type_weight, weight) VALUES (%d, '%s', %d, %d, %d)",
    $better_format['rid'], $better_format['type'], $better_format['format'], $better_format['type_weight'], $better_format['weight']
  );
}

// Add user 1 with configurator role.
$user_roles = array(
  1 => 5
);

foreach ($user_roles as $uid => $rid) {
  db_query("INSERT INTO {users_roles} (uid, rid) VALUES (%d, %d)", $uid, $rid);
}
