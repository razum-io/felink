<?php

  // If you add new taxonomies, check the 'one_menu_taxonomy_menus'
  // variable and taxonomy_hierarchical_select_x in conimbo.variables.inc
  // to see if it needs an update.
  // Blocks also might need updates too.

  $taxonomies = array(
    'news', // 1
    'blog', // 2
    'blog_tags', // 3
    'faq', // 4
  );

  foreach ($taxonomies as $taxonomy) {

    _taxonomy_create($taxonomy);
  }

  cache_clear_all('*', 'cache');

function _taxonomy_create($type) {

  require_once('conimbo.taxonomy.'. $type .'.inc');

  $function = 'taxonomy_get_'.$type;
  if (function_exists($function)) {
    $type_array = $function();
    taxonomy_save_vocabulary($type_array['vocabulary']);

    $vid = db_result(db_query("SELECT MAX(vid) FROM {vocabulary}"));
  }
}