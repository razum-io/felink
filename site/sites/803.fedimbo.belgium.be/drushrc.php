<?php 


$options['db_type'] = 'mysqli';
$options['db_host'] = 'localhost';
$options['db_port'] = '3306';
$options['db_passwd'] = 'HdrqvDcv3T';
$options['db_name'] = '803bfedimbobelgi';
$options['db_user'] = '803bfedimbobelgi';
$options['packages'] = array (
  'platforms' => 
  array (
    'drupal' => 
    array (
      'short_name' => 'drupal',
      'version' => '6.31',
      'description' => 'This platform is running Drupal 6.31',
    ),
  ),
  'profiles' => 
  array (
    'enterprise_premium' => 
    array (
      'name' => 'enterprise_premium',
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/profiles/enterprise_premium/enterprise_premium.profile',
      'project' => '',
      'info' => 
      array (
        'name' => 'Enterprise Premium',
        'description' => 'Select this profile to enable the enterprise premium site.',
        'languages' => 
        array (
          0 => 'en',
        ),
        'version' => NULL,
      ),
      'version' => '6.31',
      'status' => 1,
    ),
  ),
  'modules' => 
  array (
    'aggregator' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/aggregator/aggregator.module',
      'name' => 'aggregator',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Aggregator',
        'description' => 'Aggregates syndicated content (RSS, RDF, and Atom feeds).',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'blog' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/blog/blog.module',
      'name' => 'blog',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Blog',
        'description' => 'Enables keeping easily and regularly updated user web pages or blogs.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'blogapi' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/blogapi/blogapi.module',
      'name' => 'blogapi',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Blog API',
        'description' => 'Allows users to post content using applications that support XML-RPC blog APIs.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'book' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/book/book.module',
      'name' => 'book',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Book',
        'description' => 'Allows users to structure site pages in a hierarchy or outline.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'color' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/color/color.module',
      'name' => 'color',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Color',
        'description' => 'Allows the user to change the color scheme of certain themes.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'cookie_cache_bypass' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/cookie_cache_bypass/cookie_cache_bypass.module',
      'name' => 'cookie_cache_bypass',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Cookie cache bypass',
        'description' => 'Sets a cookie on form submission directing a reverse proxy to temporarily not serve cached pages for an anonymous user that just submitted content.',
        'core' => '6.x',
        'package' => 'Pressflow',
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'forum' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/forum/forum.module',
      'name' => 'forum',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Forum',
        'description' => 'Enables threaded discussions about general topics.',
        'dependencies' => 
        array (
          0 => 'taxonomy',
          1 => 'comment',
        ),
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'help' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/help/help.module',
      'name' => 'help',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Help',
        'description' => 'Manages the display of online help.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'openid' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/openid/openid.module',
      'name' => 'openid',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'OpenID',
        'description' => 'Allows users to log into your site using OpenID.',
        'version' => '6.31',
        'package' => 'Core - optional',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'path_alias_cache' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/path_alias_cache/path_alias_cache.module',
      'name' => 'path_alias_cache',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Path alias cache',
        'description' => 'A path alias implementation which adds a cache to the core version.',
        'core' => '6.x',
        'package' => 'Pressflow',
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'php' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/php/php.module',
      'name' => 'php',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'PHP filter',
        'description' => 'Allows embedded PHP code/snippets to be evaluated.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'ping' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/ping/ping.module',
      'name' => 'ping',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Ping',
        'description' => 'Alerts other sites when your site has been updated.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'poll' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/poll/poll.module',
      'name' => 'poll',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Poll',
        'description' => 'Allows your site to capture votes on different topics in the form of multiple choice questions.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'simpletest' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/simpletest/simpletest.module',
      'name' => 'simpletest',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'SimpleTest',
        'description' => 'Provides a framework for unit and functional testing.',
        'package' => 'Development',
        'core' => '6.x',
        'php' => '5 ; Drupal 6',
        'files' => 
        array (
          0 => 'simpletest.module',
          1 => 'simpletest.pages.inc',
          2 => 'simpletest.install',
          3 => 'simpletest.test',
          4 => 'drupal_web_test_case.php',
          5 => 'tests/block.test',
        ),
        'version' => '6.x-2.10',
        'project' => 'simpletest',
        'datestamp' => '1262212859',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'simpletest',
      'version' => '6.x-2.10',
    ),
    'statistics' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/statistics/statistics.module',
      'name' => 'statistics',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Statistics',
        'description' => 'Logs access statistics for your site.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'syslog' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/syslog/syslog.module',
      'name' => 'syslog',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Syslog',
        'description' => 'Logs and records system events to syslog.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'throttle' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/throttle/throttle.module',
      'name' => 'throttle',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Throttle',
        'description' => 'Handles the auto-throttling mechanism, to control site congestion.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'tracker' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/tracker/tracker.module',
      'name' => 'tracker',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Tracker',
        'description' => 'Enables tracking of recent posts for users.',
        'dependencies' => 
        array (
          0 => 'comment',
        ),
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'update' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/update/update.module',
      'name' => 'update',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update status',
        'description' => 'Checks the status of available updates for Drupal and your installed modules and themes.',
        'version' => '6.31',
        'package' => 'Core - optional',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'upload' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/upload/upload.module',
      'name' => 'upload',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Upload',
        'description' => 'Allows users to upload and attach files to content.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'advagg' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/advagg/advagg.module',
      'name' => 'advagg',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'advagg_bundler' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/advagg/advagg_bundler/advagg_bundler.module',
      'name' => 'advagg_bundler',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'advagg_css_compress' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/advagg/advagg_css_compress/advagg_css_compress.module',
      'name' => 'advagg_css_compress',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'advagg_js_cdn' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/advagg/advagg_js_cdn/advagg_js_cdn.module',
      'name' => 'advagg_js_cdn',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'advagg_js_compress' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/advagg/advagg_js_compress/advagg_js_compress.module',
      'name' => 'advagg_js_compress',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'advpoll' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/advpoll/advpoll.module',
      'name' => 'advpoll',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Advanced Poll',
        'description' => 'An advanced polling module for voting, elections, and group decision-making.',
        'dependencies' => 
        array (
          0 => 'votingapi',
        ),
        'core' => '6.x',
        'package' => 'Voting',
        'version' => '6.x-1.x-dev',
        'project' => 'advpoll',
        'datestamp' => '1402910888',
        'php' => '4.3.5',
      ),
      'project' => 'advpoll',
      'version' => '6.x-1.x-dev',
    ),
    'advpoll_convert' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/advpoll/advpoll_convert.module',
      'name' => 'advpoll_convert',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Advanced Poll Converter',
        'description' => 'Converts a site\'s standard Drupal polls into Advanced Poll polls.',
        'dependencies' => 
        array (
          0 => 'votingapi',
          1 => 'advpoll',
        ),
        'package' => 'Voting',
        'core' => '6.x',
        'version' => '6.x-1.x-dev',
        'project' => 'advpoll',
        'datestamp' => '1402910888',
        'php' => '4.3.5',
      ),
      'project' => 'advpoll',
      'version' => '6.x-1.x-dev',
    ),
    'apachesolr_commentsearch' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/apachesolr/contrib/apachesolr_commentsearch/apachesolr_commentsearch.module',
      'name' => 'apachesolr_commentsearch',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Apache Solr comment search',
        'description' => 'Treats comments as a content type allowing you to search them separately.',
        'dependencies' => 
        array (
          0 => 'apachesolr_search',
          1 => 'comment',
        ),
        'package' => 'Apache Solr',
        'core' => '6.x',
        'php' => '5.1.4',
        'version' => '6.x-2.0-beta2',
        'project' => 'apachesolr',
        'datestamp' => '1270752624',
      ),
      'project' => 'apachesolr',
      'version' => '6.x-2.0-beta2',
    ),
    'apachesolr_image' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/apachesolr/contrib/apachesolr_image/apachesolr_image.module',
      'name' => 'apachesolr_image',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Apache Solr image module integration',
        'description' => 'Integrates the Apache Solr and Image modules',
        'dependencies' => 
        array (
          0 => 'image',
          1 => 'apachesolr',
        ),
        'package' => 'Apache Solr',
        'core' => '6.x',
        'version' => '6.x-2.0-beta2',
        'project' => 'apachesolr',
        'datestamp' => '1270752624',
        'php' => '4.3.5',
      ),
      'project' => 'apachesolr',
      'version' => '6.x-2.0-beta2',
    ),
    'apachesolr_og' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/apachesolr/contrib/apachesolr_og/apachesolr_og.module',
      'name' => 'apachesolr_og',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Apache Solr Organic Groups',
        'description' => 'Integrates Organic Groups and Apache Solr Search',
        'dependencies' => 
        array (
          0 => 'apachesolr',
          1 => 'og',
        ),
        'package' => 'Apache Solr',
        'core' => '6.x',
        'version' => '6.x-2.0-beta2',
        'project' => 'apachesolr',
        'datestamp' => '1270752624',
        'php' => '4.3.5',
      ),
      'project' => 'apachesolr',
      'version' => '6.x-2.0-beta2',
    ),
    'autoload' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/autoload/autoload.module',
      'name' => 'autoload',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Autoload',
        'description' => 'Helper module to autoload classes under PHP 5.',
        'php' => '5.1.2',
        'core' => '6.x',
        'version' => '6.x-2.1',
        'project' => 'autoload',
        'datestamp' => '1303486914',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'autoload',
      'version' => '6.x-2.1',
    ),
    'backup_migrate' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/backup_migrate/backup_migrate.module',
      'name' => 'backup_migrate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Backup and Migrate',
        'description' => 'Backup or migrate the Drupal Database quickly and without unnecessary data.',
        'core' => '6.x',
        'version' => '6.x-2.8',
        'project' => 'backup_migrate',
        'datestamp' => '1383686611',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'backup_migrate',
      'version' => '6.x-2.8',
    ),
    'beididp' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/beididp/beididp.module',
      'name' => 'beididp',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'beididp_block' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/beididp/modules/beididp_block/beididp_block.module',
      'name' => 'beididp_block',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'beididp_button' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/beididp/modules/beididp_button/beididp_button.module',
      'name' => 'beididp_button',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'beididp_checkrole' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/beididp/modules/beididp_checkrole/beididp_checkrole.module',
      'name' => 'beididp_checkrole',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'beididp_mail' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/beididp/modules/beididp_mail/beididp_mail.module',
      'name' => 'beididp_mail',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'beididp_migrate' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/beididp/modules/beididp_migrate/beididp_migrate.module',
      'name' => 'beididp_migrate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'beididp_pape' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/beididp/modules/beididp_pape/beididp_pape.module',
      'name' => 'beididp_pape',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'beididp_profile' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/beididp/modules/beididp_profile/beididp_profile.module',
      'name' => 'beididp_profile',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'boost' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/boost/boost.module',
      'name' => 'boost',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Boost',
        'description' => 'Caches text (html, ajax, xml) outputted by Drupal as static files for performance and scalability purposes.',
        'recommends' => 
        array (
          0 => 'nodereferrer',
        ),
        'package' => 'Performance and scalability',
        'core' => '6.x',
        'version' => '6.x-1.21',
        'project' => 'boost',
        'datestamp' => '1349667069',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'boost',
      'version' => '6.x-1.21',
    ),
    'cacheexclude' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cacheexclude/cacheexclude.module',
      'name' => 'cacheexclude',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Cache Exclude',
        'description' => 'Exclude certain pages from being cached.',
        'core' => '6.x',
        'version' => '6.x-2.2',
        'project' => 'cacheexclude',
        'datestamp' => '1357939519',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'cacheexclude',
      'version' => '6.x-2.2',
    ),
    'content_copy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cck/modules/content_copy/content_copy.module',
      'name' => 'content_copy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Copy',
        'description' => 'Enables ability to import/export field definitions.',
        'dependencies' => 
        array (
          0 => 'content',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-2.9',
        'project' => 'cck',
        'datestamp' => '1294407979',
        'php' => '4.3.5',
      ),
      'project' => 'cck',
      'version' => '6.x-2.9',
    ),
    'content_permissions' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cck/modules/content_permissions/content_permissions.module',
      'name' => 'content_permissions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Permissions',
        'description' => 'Set field-level permissions for CCK fields.',
        'package' => 'CCK',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'content',
        ),
        'version' => '6.x-2.9',
        'project' => 'cck',
        'datestamp' => '1294407979',
        'php' => '4.3.5',
      ),
      'project' => 'cck',
      'version' => '6.x-2.9',
    ),
    'fieldgroup' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cck/modules/fieldgroup/fieldgroup.module',
      'name' => 'fieldgroup',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fieldgroup',
        'description' => 'Create display groups for CCK fields.',
        'dependencies' => 
        array (
          0 => 'content',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-2.9',
        'project' => 'cck',
        'datestamp' => '1294407979',
        'php' => '4.3.5',
      ),
      'project' => 'cck',
      'version' => '6.x-2.9',
    ),
    'number' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cck/modules/number/number.module',
      'name' => 'number',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Number',
        'description' => 'Defines numeric field types.',
        'dependencies' => 
        array (
          0 => 'content',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-2.9',
        'project' => 'cck',
        'datestamp' => '1294407979',
        'php' => '4.3.5',
      ),
      'project' => 'cck',
      'version' => '6.x-2.9',
    ),
    'userreference' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cck/modules/userreference/userreference.module',
      'name' => 'userreference',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User Reference',
        'description' => 'Defines a field type for referencing a user from a node.',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'text',
          2 => 'optionwidgets',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-2.9',
        'project' => 'cck',
        'datestamp' => '1294407979',
        'php' => '4.3.5',
      ),
      'project' => 'cck',
      'version' => '6.x-2.9',
    ),
    'content_profile' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/content_profile/content_profile.module',
      'name' => 'content_profile',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Profile',
        'description' => 'Use content types for user profiles.',
        'package' => 'Content Profile',
        'core' => '6.x',
        'version' => '6.x-1.0',
        'project' => 'content_profile',
        'datestamp' => '1270662007',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'content_profile',
      'version' => '6.x-1.0',
    ),
    'content_profile_registration' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/content_profile/modules/content_profile_registration.module',
      'name' => 'content_profile_registration',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Profile User Registration',
        'description' => 'Enable content profile features during user registration',
        'package' => 'Content Profile',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'content_profile',
        ),
        'version' => '6.x-1.0',
        'project' => 'content_profile',
        'datestamp' => '1270662007',
        'php' => '4.3.5',
      ),
      'project' => 'content_profile',
      'version' => '6.x-1.0',
    ),
    'content_profile_tokens' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/content_profile/modules/content_profile_tokens.module',
      'name' => 'content_profile_tokens',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Profile Tokens',
        'description' => 'Add user tokens for content profiles.',
        'package' => 'Content Profile',
        'dependencies' => 
        array (
          0 => 'content_profile',
          1 => 'token',
          2 => 'content',
        ),
        'core' => '6.x',
        'version' => '6.x-1.0',
        'project' => 'content_profile',
        'datestamp' => '1270662007',
        'php' => '4.3.5',
      ),
      'project' => 'content_profile',
      'version' => '6.x-1.0',
    ),
    'content_taxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/content_taxonomy/content_taxonomy.module',
      'name' => 'content_taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Taxonomy',
        'description' => 'Defines a field type for taxonomy terms',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'taxonomy',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-1.0-rc2',
        'project' => 'content_taxonomy',
        'datestamp' => '1250688034',
        'php' => '4.3.5',
      ),
      'project' => 'content_taxonomy',
      'version' => '6.x-1.0-rc2',
    ),
    'content_taxonomy_autocomplete' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/content_taxonomy/content_taxonomy_autocomplete.module',
      'name' => 'content_taxonomy_autocomplete',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Taxonomy Autocomplete',
        'description' => 'Defines a autocomplete widget type for content_taxonomy',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'content_taxonomy',
          2 => 'taxonomy',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-1.0-rc2',
        'project' => 'content_taxonomy',
        'datestamp' => '1250688034',
        'php' => '4.3.5',
      ),
      'project' => 'content_taxonomy',
      'version' => '6.x-1.0-rc2',
    ),
    'content_taxonomy_options' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/content_taxonomy/content_taxonomy_options.module',
      'name' => 'content_taxonomy_options',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Taxonomy Options',
        'description' => 'Defines a option widget type for content_taxonomy for selects, radios/checkboxes',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'content_taxonomy',
          2 => 'taxonomy',
          3 => 'optionwidgets',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-1.0-rc2',
        'project' => 'content_taxonomy',
        'datestamp' => '1250688034',
        'php' => '4.3.5',
      ),
      'project' => 'content_taxonomy',
      'version' => '6.x-1.0-rc2',
    ),
    'content_taxonomy_tree' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/content_taxonomy/content_taxonomy_tree.module',
      'name' => 'content_taxonomy_tree',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Taxonomy Tree',
        'description' => 'Defines a dynamic tree widget for Content Taxonomy',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'content_taxonomy',
          2 => 'taxonomy',
          3 => 'taxonomy_manager',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-1.0-rc2',
        'project' => 'content_taxonomy',
        'datestamp' => '1250688034',
        'php' => '4.3.5',
      ),
      'project' => 'content_taxonomy',
      'version' => '6.x-1.0-rc2',
    ),
    'bulk_export' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ctools/bulk_export/bulk_export.module',
      'name' => 'bulk_export',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bulk Export',
        'description' => 'Performs bulk exporting of data objects known about by Chaos tools.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'package' => 'Chaos tool suite',
        'version' => '6.x-1.11',
        'project' => 'ctools',
        'datestamp' => '1392220726',
        'php' => '4.3.5',
      ),
      'project' => 'ctools',
      'version' => '6.x-1.11',
    ),
    'ctools_access_ruleset' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ctools/ctools_access_ruleset/ctools_access_ruleset.module',
      'name' => 'ctools_access_ruleset',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Custom rulesets',
        'description' => 'Create custom, exportable, reusable access rulesets for applications like Panels.',
        'core' => '6.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '6.x-1.11',
        'project' => 'ctools',
        'datestamp' => '1392220726',
        'php' => '4.3.5',
      ),
      'project' => 'ctools',
      'version' => '6.x-1.11',
    ),
    'ctools_ajax_sample' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ctools/ctools_ajax_sample/ctools_ajax_sample.module',
      'name' => 'ctools_ajax_sample',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos Tools (CTools) AJAX Example',
        'description' => 'Shows how to use the power of Chaos AJAX.',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '6.x-1.11',
        'core' => '6.x',
        'project' => 'ctools',
        'datestamp' => '1392220726',
        'php' => '4.3.5',
      ),
      'project' => 'ctools',
      'version' => '6.x-1.11',
    ),
    'ctools_custom_content' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ctools/ctools_custom_content/ctools_custom_content.module',
      'name' => 'ctools_custom_content',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Custom content panes',
        'description' => 'Create custom, exportable, reusable content panes for applications like Panels.',
        'core' => '6.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '6.x-1.11',
        'project' => 'ctools',
        'datestamp' => '1392220726',
        'php' => '4.3.5',
      ),
      'project' => 'ctools',
      'version' => '6.x-1.11',
    ),
    'ctools_plugin_example' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ctools/ctools_plugin_example/ctools_plugin_example.module',
      'name' => 'ctools_plugin_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos Tools (CTools) Plugin Example',
        'description' => 'Shows how an external module can provide ctools plugins (for Panels, etc.).',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'panels',
          2 => 'page_manager',
          3 => 'advanced_help',
        ),
        'core' => '6.x',
        'version' => '6.x-1.11',
        'project' => 'ctools',
        'datestamp' => '1392220726',
        'php' => '4.3.5',
      ),
      'project' => 'ctools',
      'version' => '6.x-1.11',
    ),
    'page_manager' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ctools/page_manager/page_manager.module',
      'name' => 'page_manager',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Page manager',
        'description' => 'Provides a UI and API to manage pages within the site.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'package' => 'Chaos tool suite',
        'version' => '6.x-1.11',
        'project' => 'ctools',
        'datestamp' => '1392220726',
        'php' => '4.3.5',
      ),
      'project' => 'ctools',
      'version' => '6.x-1.11',
    ),
    'stylizer' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ctools/stylizer/stylizer.module',
      'name' => 'stylizer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Stylizer',
        'description' => 'Create custom styles for applications such as Panels.',
        'core' => '6.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '6.x-1.11',
        'project' => 'ctools',
        'datestamp' => '1392220726',
        'php' => '4.3.5',
      ),
      'project' => 'ctools',
      'version' => '6.x-1.11',
    ),
    'views_content' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ctools/views_content/views_content.module',
      'name' => 'views_content',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views content panes',
        'description' => 'Allows Views content to be used in Panels, Dashboard and other modules which use the CTools Content API.',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'views',
        ),
        'core' => '6.x',
        'version' => '6.x-1.11',
        'project' => 'ctools',
        'datestamp' => '1392220726',
        'php' => '4.3.5',
      ),
      'project' => 'ctools',
      'version' => '6.x-1.11',
    ),
    'custom_formatters' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/custom_formatters/custom_formatters.module',
      'name' => 'custom_formatters',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Custom Formatters',
        'description' => 'Allows users to easily define custom CCK Formatters.',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'token',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-1.6',
        'project' => 'custom_formatters',
        'datestamp' => '1318069301',
        'php' => '4.3.5',
      ),
      'project' => 'custom_formatters',
      'version' => '6.x-1.6',
    ),
    'date_locale' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/date/date_locale/date_locale.module',
      'name' => 'date_locale',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Locale',
        'description' => 'Allows the site admin to configure multiple formats for date/time display to tailor dates for a specific locale or audience.',
        'package' => 'Date/Time',
        'dependencies' => 
        array (
          0 => 'date_api',
          1 => 'locale',
        ),
        'core' => '6.x',
        'version' => '6.x-2.10',
        'project' => 'date',
        'datestamp' => '1396284252',
        'php' => '4.3.5',
      ),
      'project' => 'date',
      'version' => '6.x-2.10',
    ),
    'date_php4' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/date/date_php4/date_php4.module',
      'name' => 'date_php4',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date PHP4',
        'description' => 'Emulate PHP 5.2 date functions in PHP 4.x, PHP 5.0, and PHP 5.1. Required when using the Date API with PHP versions less than PHP 5.2.',
        'package' => 'Date/Time',
        'dependencies' => 
        array (
          0 => 'date_api',
        ),
        'core' => '6.x',
        'version' => '6.x-2.10',
        'project' => 'date',
        'datestamp' => '1396284252',
        'php' => '4.3.5',
      ),
      'project' => 'date',
      'version' => '6.x-2.10',
    ),
    'date_repeat' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/date/date_repeat/date_repeat.module',
      'name' => 'date_repeat',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Repeat API',
        'description' => 'A Date Repeat API to calculate repeating dates and times from iCal rules.',
        'dependencies' => 
        array (
          0 => 'date_api',
        ),
        'package' => 'Date/Time',
        'core' => '6.x',
        'version' => '6.x-2.10',
        'project' => 'date',
        'datestamp' => '1396284252',
        'php' => '4.3.5',
      ),
      'project' => 'date',
      'version' => '6.x-2.10',
    ),
    'date_tools' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/date/date_tools/date_tools.module',
      'name' => 'date_tools',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Tools',
        'description' => 'Tools to import and auto-create dates and calendars.',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'date',
        ),
        'package' => 'Date/Time',
        'core' => '6.x',
        'version' => '6.x-2.10',
        'project' => 'date',
        'datestamp' => '1396284252',
        'php' => '4.3.5',
      ),
      'project' => 'date',
      'version' => '6.x-2.10',
    ),
    'deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/deploy.module',
      'name' => 'deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'comment_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/comment_deploy/comment_deploy.module',
      'name' => 'comment_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'content_copy_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/content_copy_deploy/content_copy_deploy.module',
      'name' => 'content_copy_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'date_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/date_deploy/date_deploy.module',
      'name' => 'date_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'deploy_uuid' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/deploy_uuid/deploy_uuid.module',
      'name' => 'deploy_uuid',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'deploy_uuid_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/deploy_uuid/deploy_uuid_service.module',
      'name' => 'deploy_uuid_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'filefield_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/filefield_deploy/filefield_deploy.module',
      'name' => 'filefield_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'nodereference_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/nodereference_deploy/nodereference_deploy.module',
      'name' => 'nodereference_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'node_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/node_deploy/node_deploy.module',
      'name' => 'node_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'system_settings_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/system_settings_deploy/system_settings_deploy.module',
      'name' => 'system_settings_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'taxonomy_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/taxonomy_deploy/taxonomy_deploy.module',
      'name' => 'taxonomy_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'userreference_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/userreference_deploy/userreference_deploy.module',
      'name' => 'userreference_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'user_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/user_deploy/user_deploy.module',
      'name' => 'user_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'views_deploy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/modules/views_deploy/views_deploy.module',
      'name' => 'views_deploy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'content_copy_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/services/content_copy_service/content_copy_service.module',
      'name' => 'content_copy_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'system_settings_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/deploy/services/system_settings_service/system_settings_service.module',
      'name' => 'system_settings_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'diff' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/diff/diff.module',
      'name' => 'diff',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'emf' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emf/emf.module',
      'name' => 'emf',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'E-mail Marketing Framework',
        'description' => 'Integration for list based e-mail marketing services like Campaign Monitor, MailChimp, ...',
        'core' => '6.x',
        'package' => 'E-mail Marketing Framework',
        'version' => '6.x-1.4',
        'project' => 'emf',
        'datestamp' => '1292059537',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'emf',
      'version' => '6.x-1.4',
    ),
    'emf_campaign_monitor' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emf/plugins/emf_campaign_monitor/emf_campaign_monitor.module',
      'name' => 'emf_campaign_monitor',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Campaign Monitor Plugin',
        'description' => 'E-mail Marketing Framework plugin for Campaign Monitor integration.',
        'core' => '6.x',
        'package' => 'E-mail Marketing Framework',
        'dependencies' => 
        array (
          0 => 'emf',
        ),
        'version' => '6.x-1.4',
        'project' => 'emf',
        'datestamp' => '1292059537',
        'php' => '4.3.5',
      ),
      'project' => 'emf',
      'version' => '6.x-1.4',
    ),
    'emf_mailchimp' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emf/plugins/emf_mailchimp/emf_mailchimp.module',
      'name' => 'emf_mailchimp',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'MailChimp Plugin',
        'description' => 'E-mail Marketing Framework plugin for MailChimp integration.',
        'core' => '6.x',
        'package' => 'E-mail Marketing Framework',
        'dependencies' => 
        array (
          0 => 'emf',
        ),
        'version' => '6.x-1.4',
        'project' => 'emf',
        'datestamp' => '1292059537',
        'php' => '4.3.5',
      ),
      'project' => 'emf',
      'version' => '6.x-1.4',
    ),
    'emaudio' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emfield/contrib/emaudio/emaudio.module',
      'name' => 'emaudio',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Embedded Audio Field',
        'description' => 'Defines a field type for displaying third party music, podcasts, and other audio, such as podOmatic and Odeo.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'emfield',
        ),
        'package' => 'Media',
        'version' => '6.x-2.6',
        'project' => 'emfield',
        'datestamp' => '1353445349',
        'php' => '4.3.5',
      ),
      'project' => 'emfield',
      'version' => '6.x-2.6',
    ),
    'embonus' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emfield/contrib/embonus/embonus.module',
      'name' => 'embonus',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Embedded Media Bonus Pack',
        'description' => 'Provides extra field information for embedded media fields.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'emfield',
        ),
        'package' => 'Media',
        'version' => '6.x-2.6',
        'project' => 'emfield',
        'datestamp' => '1353445349',
        'php' => '4.3.5',
      ),
      'project' => 'emfield',
      'version' => '6.x-2.6',
    ),
    'emimage' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emfield/contrib/emimage/emimage.module',
      'name' => 'emimage',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Embedded Image Field',
        'description' => 'Defines a field type for displaying images from third party providers, such as Flickr or Photobucket.',
        'dependencies' => 
        array (
          0 => 'emfield',
        ),
        'package' => 'Media',
        'core' => '6.x',
        'version' => '6.x-2.6',
        'project' => 'emfield',
        'datestamp' => '1353445349',
        'php' => '4.3.5',
      ),
      'project' => 'emfield',
      'version' => '6.x-2.6',
    ),
    'eminline' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emfield/contrib/eminline/eminline.module',
      'name' => 'eminline',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Embedded Inline Media',
        'description' => 'Allows users to embed media into text-areas.',
        'dependencies' => 
        array (
          0 => 'emfield',
          1 => 'emvideo',
        ),
        'core' => '6.x',
        'package' => 'Media',
        'version' => '6.x-2.6',
        'project' => 'emfield',
        'datestamp' => '1353445349',
        'php' => '4.3.5',
      ),
      'project' => 'emfield',
      'version' => '6.x-2.6',
    ),
    'emthumb' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emfield/contrib/emthumb/emthumb.module',
      'name' => 'emthumb',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Embedded Media Thumbnail',
        'description' => 'Allows custom thumbnails for Embedded Media Fields.',
        'dependencies' => 
        array (
          0 => 'emfield',
        ),
        'package' => 'Media',
        'core' => '6.x',
        'version' => '6.x-2.6',
        'project' => 'emfield',
        'datestamp' => '1353445349',
        'php' => '4.3.5',
      ),
      'project' => 'emfield',
      'version' => '6.x-2.6',
    ),
    'emvideo' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emfield/contrib/emvideo/emvideo.module',
      'name' => 'emvideo',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Embedded Video Field',
        'description' => 'Defines a field type for displaying third party videos, such as YouTube and Google Video.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'emfield',
        ),
        'package' => 'Media',
        'version' => '6.x-2.6',
        'project' => 'emfield',
        'datestamp' => '1353445349',
        'php' => '4.3.5',
      ),
      'project' => 'emfield',
      'version' => '6.x-2.6',
    ),
    'emwave' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emfield/contrib/emwave/emwave.module',
      'name' => 'emwave',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Embedded Wave Field',
        'description' => 'Defines a field type for displaying waves from third party providers, such as Google or ...',
        'dependencies' => 
        array (
          0 => 'emfield',
        ),
        'package' => 'Media',
        'core' => '6.x',
        'version' => '6.x-2.6',
        'project' => 'emfield',
        'datestamp' => '1353445349',
        'php' => '4.3.5',
      ),
      'project' => 'emfield',
      'version' => '6.x-2.6',
    ),
    'emfield' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/emfield/emfield.module',
      'name' => 'emfield',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Embedded Media Field',
        'description' => 'Provides an engine for modules to integrate various 3rd party media content providers.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'content',
        ),
        'package' => 'Media',
        'php' => '5.2',
        'version' => '6.x-2.6',
        'project' => 'emfield',
        'datestamp' => '1353445349',
      ),
      'project' => 'emfield',
      'version' => '6.x-2.6',
    ),
    'eventreg' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/eventreg/eventreg.module',
      'name' => 'eventreg',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Event Registration',
        'description' => 'Let users register to your events with a complete validation process.',
        'dependencies' => 
        array (
          0 => 'webform',
        ),
        'package' => 'Other',
        'core' => '6.x',
        'version' => '6.x-1.0',
        'project' => 'eventreg',
        'datestamp' => '1395131063',
        'php' => '4.3.5',
      ),
      'project' => 'eventreg',
      'version' => '6.x-1.0',
    ),
    'fbconnect' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/fbconnect/fbconnect.module',
      'name' => 'fbconnect',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'feeds' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/feeds/feeds.module',
      'name' => 'feeds',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds',
        'description' => 'Aggregates RSS/Atom/RDF feeds, imports CSV files and more.',
        'package' => 'Feeds',
        'core' => '6.x',
        'php' => '5.2',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'job_scheduler',
        ),
        'version' => '6.x-1.0-beta13',
        'project' => 'feeds',
        'datestamp' => '1355848351',
      ),
      'project' => 'feeds',
      'version' => '6.x-1.0-beta13',
    ),
    'feeds_defaults' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/feeds/feeds_defaults/feeds_defaults.module',
      'name' => 'feeds_defaults',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'feeds_fast_news' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/feeds/feeds_fast_news/feeds_fast_news.module',
      'name' => 'feeds_fast_news',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'data',
          1 => 'feeds',
        ),
        'description' => 'A fast news aggregator built with feeds, creates flat database records from imported feed items.',
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'data:data_default:1',
            1 => 'data:data_table:1',
            2 => 'feeds:feeds_importer_default:1',
          ),
          'data_tables' => 
          array (
            0 => 'feeds_data_feed_fast',
          ),
          'feeds_importer' => 
          array (
            0 => 'feed_fast',
          ),
          'node' => 
          array (
            0 => 'feed_fast',
          ),
        ),
        'name' => 'Feeds Fast News',
        'package' => 'Feeds',
        'version' => '6.x-1.0-beta13',
        'project' => 'feeds',
        'datestamp' => '1355848351',
        'php' => '4.3.5',
      ),
      'project' => 'feeds',
      'version' => '6.x-1.0-beta13',
    ),
    'feeds_import' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/feeds/feeds_import/feeds_import.module',
      'name' => 'feeds_import',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'feeds',
        ),
        'description' => 'An example of a node importer and a user importer.',
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'feeds:feeds_importer_default:1',
          ),
          'feeds_importer' => 
          array (
            0 => 'node',
            1 => 'user',
          ),
        ),
        'files' => 
        array (
          0 => 'feeds_import.test',
        ),
        'name' => 'Feeds Import',
        'package' => 'Feeds',
        'version' => '6.x-1.0-beta13',
        'project' => 'feeds',
        'datestamp' => '1355848351',
        'php' => '4.3.5',
      ),
      'project' => 'feeds',
      'version' => '6.x-1.0-beta13',
    ),
    'feeds_news' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/feeds/feeds_news/feeds_news.module',
      'name' => 'feeds_news',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'feeds',
          1 => 'views',
        ),
        'description' => 'A news aggregator built with feeds, creates nodes from imported feed items. With OPML import.',
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'feeds:feeds_importer_default:1',
          ),
          'feeds_importer' => 
          array (
            0 => 'feed',
            1 => 'opml',
          ),
          'node' => 
          array (
            0 => 'feed',
            1 => 'feed_item',
          ),
          'views' => 
          array (
            0 => 'feeds_defaults_feed_items',
          ),
          'views_api' => 
          array (
            0 => 'api:2',
          ),
        ),
        'name' => 'Feeds News',
        'package' => 'Feeds',
        'version' => '6.x-1.0-beta13',
        'project' => 'feeds',
        'datestamp' => '1355848351',
        'php' => '4.3.5',
      ),
      'project' => 'feeds',
      'version' => '6.x-1.0-beta13',
    ),
    'feeds_ui' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/feeds/feeds_ui/feeds_ui.module',
      'name' => 'feeds_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds Admin UI',
        'description' => 'Administrative UI for Feeds module.',
        'package' => 'Feeds',
        'dependencies' => 
        array (
          0 => 'feeds',
        ),
        'core' => '6.x',
        'php' => '5.2',
        'version' => '6.x-1.0-beta13',
        'project' => 'feeds',
        'datestamp' => '1355848351',
      ),
      'project' => 'feeds',
      'version' => '6.x-1.0-beta13',
    ),
    'feeds_xmlparser' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/feeds_xmlparser/feeds_xmlparser.module',
      'name' => 'feeds_xmlparser',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'feeds_xpathparser' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/feeds_xpathparser/feeds_xpathparser.module',
      'name' => 'feeds_xpathparser',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds XPath Parser',
        'description' => 'Parse an XML or HTML document using XPath.',
        'package' => 'Feeds',
        'dependencies' => 
        array (
          0 => 'feeds',
        ),
        'core' => '6.x',
        'version' => '6.x-1.12',
        'project' => 'feeds_xpathparser',
        'datestamp' => '1323977140',
        'php' => '4.3.5',
      ),
      'project' => 'feeds_xpathparser',
      'version' => '6.x-1.12',
    ),
    'filefield_meta' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/filefield/filefield_meta/filefield_meta.module',
      'name' => 'filefield_meta',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'FileField Meta',
        'description' => 'Add metadata gathering and storage to FileField.',
        'dependencies' => 
        array (
          0 => 'filefield',
          1 => 'getid3',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'php' => '5.0',
        'version' => '6.x-3.12',
        'project' => 'filefield',
        'datestamp' => '1392169125',
      ),
      'project' => 'filefield',
      'version' => '6.x-3.12',
    ),
    'fivestar' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/fivestar/fivestar.module',
      'name' => 'fivestar',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fivestar',
        'description' => 'A simple five-star voting widget for nodes.',
        'package' => 'Voting',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'votingapi',
        ),
        'version' => '6.x-1.20',
        'project' => 'fivestar',
        'datestamp' => '1334160048',
        'php' => '4.3.5',
      ),
      'project' => 'fivestar',
      'version' => '6.x-1.20',
    ),
    'fivestar_comment' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/fivestar/fivestar_comment.module',
      'name' => 'fivestar_comment',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fivestar Comments',
        'description' => 'Rate nodes by leaving comments.',
        'package' => 'Voting',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'votingapi',
          1 => 'fivestar',
        ),
        'version' => '6.x-1.20',
        'project' => 'fivestar',
        'datestamp' => '1334160048',
        'php' => '4.3.5',
      ),
      'project' => 'fivestar',
      'version' => '6.x-1.20',
    ),
    'flag_actions' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/flag/flag_actions.module',
      'name' => 'flag_actions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Flag actions',
        'description' => 'Execute actions on Flag events.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'flag',
        ),
        'package' => 'Flags',
        'version' => '6.x-2.1',
        'project' => 'flag',
        'datestamp' => '1354216010',
        'php' => '4.3.5',
      ),
      'project' => 'flag',
      'version' => '6.x-2.1',
    ),
    'common_fonts' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/fontyourface/modules/common_fonts/common_fonts.module',
      'name' => 'common_fonts',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Common fonts',
        'description' => '@font-your-face provider of fonts usually already available in browsers, with fallbacks.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '6.x',
        'version' => '6.x-1.4',
        'project' => 'fontyourface',
        'datestamp' => '1284262607',
        'php' => '4.3.5',
      ),
      'project' => 'fontyourface',
      'version' => '6.x-1.4',
    ),
    'google_fonts_api' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/fontyourface/modules/google_fonts_api/google_fonts_api.module',
      'name' => 'google_fonts_api',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Google Fonts API',
        'description' => '@font-your-face provider with Google fonts.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '6.x',
        'version' => '6.x-1.4',
        'project' => 'fontyourface',
        'datestamp' => '1284262607',
        'php' => '4.3.5',
      ),
      'project' => 'fontyourface',
      'version' => '6.x-1.4',
    ),
    'kernest' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/fontyourface/modules/kernest/kernest.module',
      'name' => 'kernest',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'KERNEST',
        'description' => '@font-your-face provider with KERNEST fonts.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '6.x',
        'version' => '6.x-1.4',
        'project' => 'fontyourface',
        'datestamp' => '1284262607',
        'php' => '4.3.5',
      ),
      'project' => 'fontyourface',
      'version' => '6.x-1.4',
    ),
    'typekit_api' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/fontyourface/modules/typekit_api/typekit_api.module',
      'name' => 'typekit_api',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Typekit API',
        'description' => '@font-your-face provider with Typekit.com fonts.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '6.x',
        'version' => '6.x-1.4',
        'project' => 'fontyourface',
        'datestamp' => '1284262607',
        'php' => '4.3.5',
      ),
      'project' => 'fontyourface',
      'version' => '6.x-1.4',
    ),
    'gmap_location' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/gmap/gmap_location.module',
      'name' => 'gmap_location',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'GMap Location',
        'description' => 'Display location.module information on Google Maps',
        'package' => 'Location',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'gmap',
          1 => 'location',
        ),
        'version' => '6.x-2.0-beta5',
        'project' => 'gmap',
        'datestamp' => '1378835214',
        'php' => '4.3.5',
      ),
      'project' => 'gmap',
      'version' => '6.x-2.0-beta5',
    ),
    'gmap_macro_builder' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/gmap/gmap_macro_builder.module',
      'name' => 'gmap_macro_builder',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'GMap Macro Builder',
        'description' => 'UI for building GMap macros.',
        'package' => 'Location',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'gmap',
        ),
        'version' => '6.x-2.0-beta5',
        'project' => 'gmap',
        'datestamp' => '1378835214',
        'php' => '4.3.5',
      ),
      'project' => 'gmap',
      'version' => '6.x-2.0-beta5',
    ),
    'gmap_taxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/gmap/gmap_taxonomy.module',
      'name' => 'gmap_taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'GMap Taxonomy Markers',
        'description' => 'Taxonomy based markers',
        'package' => 'Location',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'taxonomy',
          1 => 'gmap',
        ),
        'version' => '6.x-2.0-beta5',
        'project' => 'gmap',
        'datestamp' => '1378835214',
        'php' => '4.3.5',
      ),
      'project' => 'gmap',
      'version' => '6.x-2.0-beta5',
    ),
    'gvs' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/gvs/gvs.module',
      'name' => 'gvs',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'hansel_domain' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hansel/domain/hansel_domain.module',
      'name' => 'hansel_domain',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hansel Domain',
        'description' => 'Domain related switches and actions',
        'package' => 'Hansel',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'hansel',
          1 => 'domain',
        ),
        'version' => '6.x-1.3',
        'project' => 'hansel',
        'datestamp' => '1323296441',
        'php' => '4.3.5',
      ),
      'project' => 'hansel',
      'version' => '6.x-1.3',
    ),
    'hansel_export' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hansel/export/hansel_export.module',
      'name' => 'hansel_export',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hansel Export',
        'description' => 'Import and export functions',
        'package' => 'Hansel',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'hansel',
        ),
        'version' => '6.x-1.3',
        'project' => 'hansel',
        'datestamp' => '1323296441',
        'php' => '4.3.5',
      ),
      'project' => 'hansel',
      'version' => '6.x-1.3',
    ),
    'hansel_forum' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hansel/forum/hansel_forum.module',
      'name' => 'hansel_forum',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hansel Forum',
        'description' => 'Forum related switches and actions',
        'package' => 'Hansel',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'hansel',
          1 => 'forum',
        ),
        'version' => '6.x-1.3',
        'project' => 'hansel',
        'datestamp' => '1323296441',
        'php' => '4.3.5',
      ),
      'project' => 'hansel',
      'version' => '6.x-1.3',
    ),
    'hansel_og' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hansel/og/hansel_og.module',
      'name' => 'hansel_og',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hansel OG',
        'description' => 'Organic Groups related switches and actions',
        'package' => 'Hansel',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'hansel',
          1 => 'og',
        ),
        'version' => '6.x-1.3',
        'project' => 'hansel',
        'datestamp' => '1323296441',
        'php' => '4.3.5',
      ),
      'project' => 'hansel',
      'version' => '6.x-1.3',
    ),
    'hs_content_taxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hierarchical_select/modules/hs_content_taxonomy.module',
      'name' => 'hs_content_taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hierarchical Select Content Taxonomy',
        'description' => 'Use Hierarchical Select as the widget for Content Taxonomy CCK fields.',
        'dependencies' => 
        array (
          0 => 'hierarchical_select',
          1 => 'content_taxonomy',
          2 => 'hs_taxonomy',
        ),
        'package' => 'Form Elements',
        'core' => '6.x',
        'version' => '6.x-3.8',
        'project' => 'hierarchical_select',
        'datestamp' => '1330518354',
        'php' => '4.3.5',
      ),
      'project' => 'hierarchical_select',
      'version' => '6.x-3.8',
    ),
    'hs_flatlist' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hierarchical_select/modules/hs_flatlist.module',
      'name' => 'hs_flatlist',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hierarchical Select Flat List',
        'description' => 'Allows Hierarchical Select\'s dropbox to be used for selecting multiple items in a flat list of options.',
        'dependencies' => 
        array (
          0 => 'hierarchical_select',
        ),
        'package' => 'Form Elements',
        'core' => '6.x',
        'version' => '6.x-3.8',
        'project' => 'hierarchical_select',
        'datestamp' => '1330518354',
        'php' => '4.3.5',
      ),
      'project' => 'hierarchical_select',
      'version' => '6.x-3.8',
    ),
    'hs_menu' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hierarchical_select/modules/hs_menu.module',
      'name' => 'hs_menu',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hierarchical Select Menu',
        'description' => 'Use Hierarchical Select for menu parent selection.',
        'dependencies' => 
        array (
          0 => 'hierarchical_select',
          1 => 'menu',
        ),
        'package' => 'Form Elements',
        'core' => '6.x',
        'version' => '6.x-3.8',
        'project' => 'hierarchical_select',
        'datestamp' => '1330518354',
        'php' => '4.3.5',
      ),
      'project' => 'hierarchical_select',
      'version' => '6.x-3.8',
    ),
    'hs_smallhierarchy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hierarchical_select/modules/hs_smallhierarchy.module',
      'name' => 'hs_smallhierarchy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hierarchical Select Small Hierarchy',
        'description' => 'Allows Hierarchical Select to be used for a hardcoded hierarchy. When it becomes to slow, you should move the hierarchy into the database and write a proper implementation.',
        'dependencies' => 
        array (
          0 => 'hierarchical_select',
        ),
        'package' => 'Form Elements',
        'core' => '6.x',
        'version' => '6.x-3.8',
        'project' => 'hierarchical_select',
        'datestamp' => '1330518354',
        'php' => '4.3.5',
      ),
      'project' => 'hierarchical_select',
      'version' => '6.x-3.8',
    ),
    'hs_taxonomy_views' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hierarchical_select/modules/hs_taxonomy_views.module',
      'name' => 'hs_taxonomy_views',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hierarchical Select Taxonomy Views',
        'description' => 'Use Hierarchical Select for Taxonomy exposed filters in Views.',
        'dependencies' => 
        array (
          0 => 'hierarchical_select',
          1 => 'hs_taxonomy',
          2 => 'views',
        ),
        'package' => 'Form Elements',
        'core' => '6.x',
        'version' => '6.x-3.8',
        'project' => 'hierarchical_select',
        'datestamp' => '1330518354',
        'php' => '4.3.5',
      ),
      'project' => 'hierarchical_select',
      'version' => '6.x-3.8',
    ),
    'honeypot' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/honeypot/honeypot.module',
      'name' => 'honeypot',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Honeypot',
        'description' => 'Mitigates spam form submissions using the honeypot method.',
        'core' => '6.x',
        'package' => 'Spam control',
        'version' => '6.x-1.16',
        'project' => 'honeypot',
        'datestamp' => '1386997105',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'honeypot',
      'version' => '6.x-1.16',
    ),
    'i18ncck' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18ncck/i18ncck.module',
      'name' => 'i18ncck',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'CCK translation',
        'description' => 'Supports translatable custom CCK fields and fieldgroups.',
        'dependencies' => 
        array (
          0 => 'i18n',
          1 => 'content',
          2 => 'i18nstrings',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'i18ncontent' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18ncontent/i18ncontent.module',
      'name' => 'i18ncontent',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content type translation',
        'description' => 'Add multilingual options for content and translate related strings: name, description, help text...',
        'dependencies' => 
        array (
          0 => 'i18nstrings',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'i18nmenu' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18nmenu/i18nmenu.module',
      'name' => 'i18nmenu',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu translation',
        'description' => 'Supports translatable custom menu items.',
        'dependencies' => 
        array (
          0 => 'i18n',
          1 => 'menu',
          2 => 'i18nblocks',
          3 => 'i18nstrings',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'i18npoll' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18npoll/i18npoll.module',
      'name' => 'i18npoll',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Poll aggregate',
        'description' => 'Aggregates poll results for all translations.',
        'dependencies' => 
        array (
          0 => 'translation',
          1 => 'poll',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'i18nprofile' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18nprofile/i18nprofile.module',
      'name' => 'i18nprofile',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Profile translation',
        'description' => 'Enables multilingual profile fields.',
        'dependencies' => 
        array (
          0 => 'profile',
          1 => 'i18nstrings',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'i18nsync' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18nsync/i18nsync.module',
      'name' => 'i18nsync',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Synchronize translations',
        'description' => 'Synchronizes taxonomy and fields accross translations of the same content.',
        'dependencies' => 
        array (
          0 => 'i18n',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'i18n_test' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/tests/i18n_test.module',
      'name' => 'i18n_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Internationalization tests',
        'description' => 'Helper module for testing i18n (do not enable manually)',
        'dependencies' => 
        array (
          0 => 'locale',
          1 => 'translation',
          2 => 'i18n',
        ),
        'package' => 'Testing',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'i18nviews' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18nviews/i18nviews.module',
      'name' => 'i18nviews',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views translation',
        'description' => 'Translation of views strings and content selection for views. Requires Views 3.x',
        'dependencies' => 
        array (
          0 => 'views',
          1 => 'i18nstrings',
          2 => 'i18ntaxonomy',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-2.0',
        'project' => 'i18nviews',
        'datestamp' => '1276171508',
        'php' => '4.3.5',
      ),
      'project' => 'i18nviews',
      'version' => '6.x-2.0',
    ),
    'imageapi_imagemagick' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/imageapi/imageapi_imagemagick.module',
      'name' => 'imageapi_imagemagick',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ImageAPI ImageMagick',
        'description' => 'Command Line ImageMagick support.',
        'package' => 'ImageCache',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'imageapi',
        'datestamp' => '1305563215',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'imageapi',
      'version' => '6.x-1.10',
    ),
    'job_scheduler' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/job_scheduler/job_scheduler.module',
      'name' => 'job_scheduler',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Job Scheduler',
        'description' => 'Scheduler API',
        'core' => '6.x',
        'php' => '5.2',
        'version' => '6.x-1.0-beta3',
        'project' => 'job_scheduler',
        'datestamp' => '1284491510',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'job_scheduler',
      'version' => '6.x-1.0-beta3',
    ),
    'less' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/less/less.module',
      'name' => 'less',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'link' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/link/link.module',
      'name' => 'link',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Link',
        'description' => 'Defines simple link field types.',
        'dependencies' => 
        array (
          0 => 'content',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'files' => 
        array (
          0 => 'link.migrate.inc',
        ),
        'version' => '6.x-2.11',
        'project' => 'link',
        'datestamp' => '1393559923',
        'php' => '4.3.5',
      ),
      'project' => 'link',
      'version' => '6.x-2.11',
    ),
    'linkchecker' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/linkchecker/linkchecker.module',
      'name' => 'linkchecker',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Link checker',
        'description' => 'Periodically checks for broken links in node types, blocks and cck fields and reports the results.',
        'core' => '6.x',
        'version' => '6.x-2.8',
        'project' => 'linkchecker',
        'datestamp' => '1402132729',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'linkchecker',
      'version' => '6.x-2.8',
    ),
    'linkit_file' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/linkit/plugins/linkit_file/linkit_file.module',
      'name' => 'linkit_file',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Linkit File',
        'description' => 'Extend Linkit with support for file links.',
        'core' => '6.x',
        'package' => 'Linkit',
        'dependencies' => 
        array (
          0 => 'linkit',
        ),
        'version' => '6.x-1.12',
        'project' => 'linkit',
        'datestamp' => '1362876195',
        'php' => '4.3.5',
      ),
      'project' => 'linkit',
      'version' => '6.x-1.12',
    ),
    'linkit_permissions' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/linkit/plugins/linkit_permissions/linkit_permissions.module',
      'name' => 'linkit_permissions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Linkit Permissions',
        'description' => 'Extend Linkit with permissions for plugins.',
        'core' => '6.x',
        'package' => 'Linkit',
        'dependencies' => 
        array (
          0 => 'linkit',
        ),
        'version' => '6.x-1.12',
        'project' => 'linkit',
        'datestamp' => '1362876195',
        'php' => '4.3.5',
      ),
      'project' => 'linkit',
      'version' => '6.x-1.12',
    ),
    'linkit_taxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/linkit/plugins/linkit_taxonomy/linkit_taxonomy.module',
      'name' => 'linkit_taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Linkit Taxonomy',
        'description' => 'Extend Linkit with support for taxonomy links.',
        'core' => '6.x',
        'package' => 'Linkit',
        'dependencies' => 
        array (
          0 => 'linkit',
          1 => 'taxonomy',
        ),
        'version' => '6.x-1.12',
        'project' => 'linkit',
        'datestamp' => '1362876195',
        'php' => '4.3.5',
      ),
      'project' => 'linkit',
      'version' => '6.x-1.12',
    ),
    'linkit_user' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/linkit/plugins/linkit_user/linkit_user.module',
      'name' => 'linkit_user',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Linkit User',
        'description' => 'Extend Linkit with support for user links.',
        'core' => '6.x',
        'package' => 'Linkit',
        'dependencies' => 
        array (
          0 => 'linkit',
        ),
        'version' => '6.x-1.12',
        'project' => 'linkit',
        'datestamp' => '1362876195',
        'php' => '4.3.5',
      ),
      'project' => 'linkit',
      'version' => '6.x-1.12',
    ),
    'location_addanother' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/contrib/location_addanother/location_addanother.module',
      'name' => 'location_addanother',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Add Another',
        'description' => 'Allows you to quickly add locations directly from a node without having to click \'edit\' first.',
        'dependencies' => 
        array (
          0 => 'location',
          1 => 'location_node',
        ),
        'package' => 'Location',
        'core' => '6.x',
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'location_cck' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/contrib/location_cck/location_cck.module',
      'name' => 'location_cck',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location CCK',
        'description' => 'Defines a Location field type.',
        'core' => '6.x',
        'package' => 'CCK',
        'dependencies' => 
        array (
          0 => 'location',
          1 => 'content',
        ),
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'location_fax' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/contrib/location_fax/location_fax.module',
      'name' => 'location_fax',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Fax',
        'package' => 'Location',
        'description' => 'Allows you to add a fax number to a location.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'core' => '6.x',
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'location_generate' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/contrib/location_generate/location_generate.module',
      'name' => 'location_generate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Generate',
        'description' => 'Bulk assign random latitude and longitudes to nodes.',
        'package' => 'Development',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'devel_generate',
          1 => 'location',
        ),
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'location_phone' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/contrib/location_phone/location_phone.module',
      'name' => 'location_phone',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Phone',
        'package' => 'Location',
        'description' => 'Allows you to add a phone number to a location.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'core' => '6.x',
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'location_search' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/contrib/location_search/location_search.module',
      'name' => 'location_search',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Search',
        'package' => 'Location',
        'description' => 'Advanced search page for locations.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'search',
          1 => 'location',
        ),
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'location_taxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/contrib/location_taxonomy/location_taxonomy.module',
      'name' => 'location_taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Taxonomy',
        'description' => 'Associate locations with taxonomy terms.',
        'dependencies' => 
        array (
          0 => 'location',
          1 => 'taxonomy',
        ),
        'package' => 'Location',
        'core' => '6.x',
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'location_user' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/location_user.module',
      'name' => 'location_user',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User Locations',
        'description' => 'Associate locations with users.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'package' => 'Location',
        'core' => '6.x',
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'logintoboggan_content_access_integration' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/logintoboggan/contrib/logintoboggan_content_access_integration/logintoboggan_content_access_integration.module',
      'name' => 'logintoboggan_content_access_integration',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'LoginToboggan Content Access Integration',
        'description' => 'Integrates LoginToboggan with Content Access module, so that Non-validated users are handled correctly',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'logintoboggan',
          1 => 'content_access',
        ),
        'version' => '6.x-1.10',
        'project' => 'logintoboggan',
        'datestamp' => '1320873035',
        'php' => '4.3.5',
      ),
      'project' => 'logintoboggan',
      'version' => '6.x-1.10',
    ),
    'logintoboggan_rules' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/logintoboggan/contrib/logintoboggan_rules/logintoboggan_rules.module',
      'name' => 'logintoboggan_rules',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'LoginToboggan Rules Integration',
        'description' => 'Integrates LoginToboggan with Rules module',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'logintoboggan',
          1 => 'rules',
        ),
        'version' => '6.x-1.10',
        'project' => 'logintoboggan',
        'datestamp' => '1320873035',
        'php' => '4.3.5',
      ),
      'project' => 'logintoboggan',
      'version' => '6.x-1.10',
    ),
    'mediafront' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/mediafront.module',
      'name' => 'mediafront',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'osmplayer' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/players/osmplayer/osmplayer.module',
      'name' => 'osmplayer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'mediafront_audio' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/plugins/mediafront_audio/mediafront_audio.module',
      'name' => 'mediafront_audio',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'mediafront_cdn2' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/plugins/mediafront_cdn2/mediafront_cdn2.module',
      'name' => 'mediafront_cdn2',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'mediafront_emfield' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/plugins/mediafront_emfield/mediafront_emfield.module',
      'name' => 'mediafront_emfield',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'mediafront_s3' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/plugins/mediafront_s3/mediafront_s3.module',
      'name' => 'mediafront_s3',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'mediafront_statistics' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/plugins/mediafront_statistics/mediafront_statistics.module',
      'name' => 'mediafront_statistics',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'mediafront_tagging' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/plugins/mediafront_tagging/mediafront_tagging.module',
      'name' => 'mediafront_tagging',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'mediafront_user' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/plugins/mediafront_user/mediafront_user.module',
      'name' => 'mediafront_user',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'favorites_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/services/favorites_service/favorites_service.module',
      'name' => 'favorites_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'tagging_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/services/tagging_service/tagging_service.module',
      'name' => 'tagging_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'voting_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mediafront/services/voting_service/voting_service.module',
      'name' => 'voting_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'media_vimeo' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/media_vimeo/media_vimeo.module',
      'name' => 'media_vimeo',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Media: Vimeo',
        'description' => 'Provides Vimeo support to the Embedded Media Field module.',
        'package' => 'Media',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'emfield',
          1 => 'emvideo',
        ),
        'version' => '6.x-1.1',
        'project' => 'media_vimeo',
        'datestamp' => '1285276865',
        'php' => '4.3.5',
      ),
      'project' => 'media_vimeo',
      'version' => '6.x-1.1',
    ),
    'media_youtube' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/media_youtube/media_youtube.module',
      'name' => 'media_youtube',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Media: YouTube',
        'description' => 'Provides YouTube support to the Embedded Media Field module.',
        'package' => 'Media',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'emfield',
          1 => 'emvideo',
        ),
        'version' => '6.x-1.2',
        'project' => 'media_youtube',
        'datestamp' => '1289578575',
        'php' => '4.3.5',
      ),
      'project' => 'media_youtube',
      'version' => '6.x-1.2',
    ),
    'mollom' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mollom/mollom.module',
      'name' => 'mollom',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Mollom',
        'description' => 'Automatically moderates user-submitted content and protects your site from spam and profanity.',
        'core' => '6.x',
        'php' => '5.2.4',
        'version' => '6.x-2.10',
        'project' => 'mollom',
        'datestamp' => '1399472028',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'mollom',
      'version' => '6.x-2.10',
    ),
    'mollom_test' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mollom/tests/mollom_test.module',
      'name' => 'mollom_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Mollom Test',
        'description' => 'Testing module for Mollom functionality. Do not enable.',
        'core' => '6.x',
        'package' => 'Testing',
        'hidden' => true,
        'php' => '5',
        'version' => '6.x-2.10',
        'project' => 'mollom',
        'datestamp' => '1399472028',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'mollom',
      'version' => '6.x-2.10',
    ),
    'mollom_test_server' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/mollom/tests/mollom_test_server.module',
      'name' => 'mollom_test_server',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Mollom Test Server',
        'description' => 'Testing Server module for Mollom functionality. Do not enable.',
        'core' => '6.x',
        'package' => 'Testing',
        'hidden' => true,
        'php' => '5',
        'version' => '6.x-2.10',
        'project' => 'mollom',
        'datestamp' => '1399472028',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'mollom',
      'version' => '6.x-2.10',
    ),
    'ndtest' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd/tests/ndtest.module',
      'name' => 'ndtest',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node displays TEST',
        'description' => 'Test module for Node Displays, do not enable this on production sites.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'ds',
          1 => 'ds_ui',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.4',
        'project' => 'nd',
        'datestamp' => '1292540633',
        'php' => '4.3.5',
      ),
      'project' => 'nd',
      'version' => '6.x-2.4',
    ),
    'nd_context_bm' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd_contrib/nd_context_bm/nd_context_bm.module',
      'name' => 'nd_context_bm',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ND context',
        'dependencies' => 
        array (
          0 => 'context',
          1 => 'ds',
        ),
        'description' => 'Allow nd-build modes to act as conditions and triggers for contexts',
        'package' => 'Display suite',
        'core' => '6.x',
        'version' => '6.x-2.4',
        'project' => 'nd_contrib',
        'datestamp' => '1292540634',
        'php' => '4.3.5',
      ),
      'project' => 'nd_contrib',
      'version' => '6.x-2.4',
    ),
    'nd_faq' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd_contrib/nd_faq/nd_faq.module',
      'name' => 'nd_faq',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ND Faq',
        'description' => 'FAQ support for Node Displays',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'nd',
          1 => 'faq',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.4',
        'project' => 'nd_contrib',
        'datestamp' => '1292540634',
        'php' => '4.3.5',
      ),
      'project' => 'nd_contrib',
      'version' => '6.x-2.4',
    ),
    'nd_fivestar' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd_contrib/nd_fivestar/nd_fivestar.module',
      'name' => 'nd_fivestar',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ND Fivestar',
        'description' => 'Fivestar support for node displays',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'nd',
          1 => 'fivestar',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.4',
        'project' => 'nd_contrib',
        'datestamp' => '1292540634',
        'php' => '4.3.5',
      ),
      'project' => 'nd_contrib',
      'version' => '6.x-2.4',
    ),
    'nd_switch_bm' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd_contrib/nd_switch_bm/nd_switch_bm.module',
      'name' => 'nd_switch_bm',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ND switch build modes',
        'description' => 'Switch the full build mode on a per-node basis',
        'package' => 'Display suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'core' => '6.x',
        'version' => '6.x-2.4',
        'project' => 'nd_contrib',
        'datestamp' => '1292540634',
        'php' => '4.3.5',
      ),
      'project' => 'nd_contrib',
      'version' => '6.x-2.4',
    ),
    'ucd' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd_contrib/nd_ubercart/ucd.module',
      'name' => 'ucd',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Ubercart displays',
        'description' => 'Extended Ubercart node build and display modes',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'ds',
          1 => 'nd',
          2 => 'uc_product',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.4',
        'project' => 'nd_contrib',
        'datestamp' => '1292540634',
        'php' => '4.3.5',
      ),
      'project' => 'nd_contrib',
      'version' => '6.x-2.4',
    ),
    'nd_webform' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd_contrib/nd_webform/nd_webform.module',
      'name' => 'nd_webform',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ND Webform',
        'description' => 'Webform support for Node Displays',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'nd',
          1 => 'webform',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.4',
        'project' => 'nd_contrib',
        'datestamp' => '1292540634',
        'php' => '4.3.5',
      ),
      'project' => 'nd_contrib',
      'version' => '6.x-2.4',
    ),
    'nfcbiblio' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodeformcols/nfcbiblio.module',
      'name' => 'nfcbiblio',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Biblio compatability',
        'description' => 'Adds support for biblio fields',
        'dependencies' => 
        array (
          0 => 'nodeformcols',
          1 => 'biblio',
        ),
        'core' => '6.x',
        'package' => 'Node form columns',
        'version' => '6.x-1.7',
        'project' => 'nodeformcols',
        'datestamp' => '1389798524',
        'php' => '4.3.5',
      ),
      'project' => 'nodeformcols',
      'version' => '6.x-1.7',
    ),
    'nfccaptcha' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodeformcols/nfccaptcha.module',
      'name' => 'nfccaptcha',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'CAPTCHA compatability',
        'description' => 'Adds support for CAPTCHA in node forms',
        'dependencies' => 
        array (
          0 => 'nodeformcols',
          1 => 'captcha',
        ),
        'core' => '6.x',
        'package' => 'Node form columns',
        'version' => '6.x-1.7',
        'project' => 'nodeformcols',
        'datestamp' => '1389798524',
        'php' => '4.3.5',
      ),
      'project' => 'nodeformcols',
      'version' => '6.x-1.7',
    ),
    'nodepicker' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodepicker/nodepicker.module',
      'name' => 'nodepicker',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node Picker',
        'description' => 'A node picker for WYSIWYG editors that inserts links into content.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'views',
          1 => 'wysiwyg',
          2 => 'jquery_ui',
        ),
        'version' => '6.x-1.0-alpha4',
        'project' => 'nodepicker',
        'datestamp' => '1276060507',
        'php' => '4.3.5',
      ),
      'project' => 'nodepicker',
      'version' => '6.x-1.0-alpha4',
    ),
    'nodepicker_migrate' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodepicker_migrate/nodepicker_migrate.module',
      'name' => 'nodepicker_migrate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node Picker Migrate',
        'description' => 'Migrate links formatted in Node Picket syntax to Pathologic or static links.',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'nodequeue_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodequeue/addons/nodequeue_service/nodequeue_service.module',
      'name' => 'nodequeue_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'services',
          1 => 'nodequeue',
        ),
        'description' => 'Provides a nodequeue service.',
        'name' => 'Nodequeue Service',
        'package' => 'Nodequeue',
        'version' => '6.x-2.11',
        'project' => 'nodequeue',
        'datestamp' => '1316558105',
        'php' => '4.3.5',
      ),
      'project' => 'nodequeue',
      'version' => '6.x-2.11',
    ),
    'nodequeue_generate' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodequeue/nodequeue_generate.module',
      'name' => 'nodequeue_generate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nodequeue generate',
        'description' => 'Bulk assign nodes into queues for quickly populating a site.',
        'package' => 'Development',
        'dependencies' => 
        array (
          0 => 'nodequeue',
        ),
        'core' => '6.x',
        'version' => '6.x-2.11',
        'project' => 'nodequeue',
        'datestamp' => '1316558105',
        'php' => '4.3.5',
      ),
      'project' => 'nodequeue',
      'version' => '6.x-2.11',
    ),
    'smartqueue' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodequeue/smartqueue.module',
      'name' => 'smartqueue',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Smartqueue taxonomy',
        'description' => 'Creates a node queue for each taxonomy vocabulary',
        'package' => 'Nodequeue',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'nodequeue',
          1 => 'taxonomy',
        ),
        'version' => '6.x-2.11',
        'project' => 'nodequeue',
        'datestamp' => '1316558105',
        'php' => '4.3.5',
      ),
      'project' => 'nodequeue',
      'version' => '6.x-2.11',
    ),
    'nodewords_extra' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodewords/nodewords_extra/nodewords_extra.module',
      'name' => 'nodewords_extra',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nodewords extra meta tags',
        'description' => 'Add the Dublin Core, \'geo.placename\', \'geo.position\', \'geo.region\', \'icbm\' and \'shorturl\' meta tags.',
        'dependencies' => 
        array (
          0 => 'nodewords',
        ),
        'package' => 'Meta tags',
        'core' => '6.x',
        'version' => '6.x-1.14',
        'project' => 'nodewords',
        'datestamp' => '1354723721',
        'php' => '4.3.5',
      ),
      'project' => 'nodewords',
      'version' => '6.x-1.14',
    ),
    'nodewords_og' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodewords/nodewords_og/nodewords_og.module',
      'name' => 'nodewords_og',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nodewords Open Graph meta tags',
        'description' => 'Add the Open Graph (i.e. Facebook) meta tags. Note: the theme must be customized in order for these tags to work correctly, please see the README.txt file for full details.',
        'dependencies' => 
        array (
          0 => 'nodewords',
          1 => 'nodewords_basic',
        ),
        'package' => 'Meta tags',
        'core' => '6.x',
        'version' => '6.x-1.14',
        'project' => 'nodewords',
        'datestamp' => '1354723721',
        'php' => '4.3.5',
      ),
      'project' => 'nodewords',
      'version' => '6.x-1.14',
    ),
    'options_element' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/options_element/options_element.module',
      'name' => 'options_element',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Options element',
        'description' => 'A custom form element for entering the options in select lists, radios, or checkboxes.',
        'core' => '6.x',
        'php' => '5.0',
        'version' => '6.x-1.12',
        'project' => 'options_element',
        'datestamp' => '1397696071',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'options_element',
      'version' => '6.x-1.12',
    ),
    'page_title_views_test' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/page_title/tests/page_title_views_test.module',
      'name' => 'page_title_views_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Page Title Views SimpleTest',
        'description' => 'SimpleTest dependency modue for testing Views with Page Title',
        'core' => '6.x',
        'package' => 'Development',
        'dependencies' => 
        array (
          0 => 'page_title',
          1 => 'views',
        ),
        'version' => '6.x-2.7',
        'project' => 'page_title',
        'datestamp' => '1336556783',
        'php' => '4.3.5',
      ),
      'project' => 'page_title',
      'version' => '6.x-2.7',
    ),
    'panels_ipe' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/panels/panels_ipe/panels_ipe.module',
      'name' => 'panels_ipe',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Panels In-Place Editor',
        'description' => 'Provide a UI for managing some Panels directly on the frontend, instead of having to use the backend.',
        'package' => 'Panels',
        'dependencies' => 
        array (
          0 => 'panels',
          1 => 'jquery_ui',
        ),
        'core' => '6.x',
        'version' => '6.x-3.10',
        'project' => 'panels',
        'datestamp' => '1326917148',
        'php' => '4.3.5',
      ),
      'project' => 'panels',
      'version' => '6.x-3.10',
    ),
    'panels_stylizer' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/panels/panels_stylizer/panels_stylizer.module',
      'name' => 'panels_stylizer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'pearwiki_filter' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/pearwiki_filter/pearwiki_filter.module',
      'name' => 'pearwiki_filter',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'ajax_quiz' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/includes/ajax_quiz/ajax_quiz.module',
      'name' => 'ajax_quiz',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'AJAX Quiz',
        'description' => 'This module doesn\'t degrade gracefully, and we don\'t recommend using it until it does. This module attempts to provide an AJAX version of quiz. Successive quiz questions will be loaded in the same page without page reload, but it doesn\'t work flawlessly at the moment.',
        'dependencies' => 
        array (
          0 => 'quiz',
        ),
        'core' => '6.x',
        'package' => 'Quiz Addon',
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
        'php' => '4.3.5',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'quiz_stats' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/includes/quiz_stats/quiz_stats.module',
      'name' => 'quiz_stats',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Quiz Statistics',
        'description' => 'Creates a report to compare and analyse the results of quiz attendees.',
        'core' => '6.x',
        'php' => '5',
        'package' => 'Quiz Addon',
        'dependencies' => 
        array (
          0 => 'quiz',
          1 => 'chart',
        ),
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'long_answer' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/question_types/long_answer/long_answer.module',
      'name' => 'long_answer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Long Answer',
        'package' => 'Quiz Question',
        'description' => 'This provides long answer (essay, multi-paragraph) question types for use by the Quiz module.',
        'dependencies' => 
        array (
          0 => 'quiz',
          1 => 'quiz_question',
        ),
        'core' => '6.x',
        'php' => '5',
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'matching' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/question_types/matching/matching.module',
      'name' => 'matching',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Matching question',
        'package' => 'Quiz Question',
        'description' => 'Provide a way to create matching type of questions.',
        'core' => '6.x',
        'php' => '5',
        'dependencies' => 
        array (
          0 => 'quiz',
          1 => 'quiz_question',
        ),
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'multichoice' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/question_types/multichoice/multichoice.module',
      'name' => 'multichoice',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Quiz Multichoice',
        'package' => 'Quiz Question',
        'description' => 'Multiple choice question type for quiz.',
        'core' => '6.x',
        'php' => '5',
        'dependencies' => 
        array (
          0 => 'quiz',
          1 => 'quiz_question',
        ),
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'quiz_directions' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/question_types/quiz_directions/quiz_directions.module',
      'name' => 'quiz_directions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Quiz Direction',
        'package' => 'Quiz Question',
        'description' => 'Provide a slot to add directions for a quiz or some sub-portion of a quiz.',
        'core' => '6.x',
        'php' => '5',
        'dependencies' => 
        array (
          0 => 'quiz',
          1 => 'quiz_question',
        ),
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'quiz_question' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/question_types/quiz_question/quiz_question.module',
      'name' => 'quiz_question',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Quiz Question',
        'package' => 'Quiz Core',
        'description' => 'Helper module for all quiz question types',
        'core' => '6.x',
        'php' => '5.2',
        'dependencies' => 
        array (
          0 => 'autoload',
          1 => 'quiz',
        ),
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'scale' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/question_types/scale/scale.module',
      'name' => 'scale',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Scale',
        'package' => 'Quiz Question',
        'description' => 'Scale question type for quiz.',
        'core' => '6.x',
        'php' => '5',
        'dependencies' => 
        array (
          0 => 'quiz',
          1 => 'quiz_question',
        ),
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'short_answer' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/question_types/short_answer/short_answer.module',
      'name' => 'short_answer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Short Answer',
        'package' => 'Quiz Question',
        'description' => 'Short answer question type for Quiz.',
        'core' => '6.x',
        'php' => '5.1',
        'dependencies' => 
        array (
          0 => 'quiz',
          1 => 'quiz_question',
        ),
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'truefalse' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/question_types/truefalse/truefalse.module',
      'name' => 'truefalse',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'True False',
        'package' => 'Quiz Question',
        'description' => 'Module to create True or False Kind of Questions',
        'core' => '6.x',
        'php' => '5',
        'dependencies' => 
        array (
          0 => 'quiz',
          1 => 'quiz_question',
        ),
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'quiz' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/quiz/quiz.module',
      'name' => 'quiz',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Quiz',
        'package' => 'Quiz Core',
        'description' => 'Create interactive, multipage quizzes. This module must have at least one question-type module enabled in order to function properly. If in doubt, use the Multichoice module.',
        'dependencies' => 
        array (
          0 => 'taxonomy',
        ),
        'core' => '6.x',
        'php' => '5.1',
        'version' => '6.x-4.5',
        'project' => 'quiz',
        'datestamp' => '1383124558',
      ),
      'project' => 'quiz',
      'version' => '6.x-4.5',
    ),
    'recaptcha' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/recaptcha/recaptcha.module',
      'name' => 'recaptcha',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'recaptcha_mailhide' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/recaptcha/recaptcha_mailhide.module',
      'name' => 'recaptcha_mailhide',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'seckit' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/seckit/seckit.module',
      'name' => 'seckit',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Security Kit',
        'description' => 'Enhance security of your Drupal website',
        'package' => 'Security',
        'core' => '6.x',
        'version' => '6.x-1.7',
        'project' => 'seckit',
        'datestamp' => '1379512387',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'seckit',
      'version' => '6.x-1.7',
    ),
    'securesite' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/securesite/securesite.module',
      'name' => 'securesite',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Secure Site',
        'description' => 'Enables HTTP Auth security or an HTML form to restrict site access.',
        'core' => '6.x',
        'version' => '6.x-2.4',
        'project' => 'securesite',
        'datestamp' => '1254487917',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'securesite',
      'version' => '6.x-2.4',
    ),
    'services_keyauth' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/auth/services_keyauth/services_keyauth.module',
      'name' => 'services_keyauth',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'xmlrpc_server' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/servers/xmlrpc_server/xmlrpc_server.module',
      'name' => 'xmlrpc_server',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'services' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services.module',
      'name' => 'services',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'comment_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/comment_service/comment_service.module',
      'name' => 'comment_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'file_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/file_service/file_service.module',
      'name' => 'file_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'menu_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/menu_service/menu_service.module',
      'name' => 'menu_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'node_resource' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/node_service/node_resource.module',
      'name' => 'node_resource',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'node_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/node_service/node_service.module',
      'name' => 'node_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'search_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/search_service/search_service.module',
      'name' => 'search_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'system_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/system_service/system_service.module',
      'name' => 'system_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'taxonomy_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/taxonomy_service/taxonomy_service.module',
      'name' => 'taxonomy_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'user_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/user_service/user_service.module',
      'name' => 'user_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'views_service' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/services/services/views_service/views_service.module',
      'name' => 'views_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'social_share' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/social-share/social_share.module',
      'name' => 'social_share',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Social Share',
        'description' => 'Adds configurable social network share links to nodes.',
        'core' => '6.x',
        'version' => '6.x-1.12',
        'project' => 'social-share',
        'datestamp' => '1350669716',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'social-share',
      'version' => '6.x-1.12',
    ),
    'tableofcontents' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/tableofcontents/tableofcontents.module',
      'name' => 'tableofcontents',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Table of Contents',
        'description' => 'Adds a filter that generates Tables of contents for pages with  \'[toc ...]\' tags or that have a predetermined number of headers.',
        'package' => 'Input filters',
        'core' => '6.x',
        'version' => '6.x-3.8',
        'project' => 'tableofcontents',
        'datestamp' => '1352926978',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'tableofcontents',
      'version' => '6.x-3.8',
    ),
    'tableofcontents_block' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/tableofcontents/tableofcontents_block/tableofcontents_block.module',
      'name' => 'tableofcontents_block',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Table of Contents Block',
        'description' => 'Display the table of contents in a block.',
        'package' => 'Input filters',
        'dependencies' => 
        array (
          0 => 'tableofcontents',
        ),
        'core' => '6.x',
        'version' => '6.x-3.8',
        'project' => 'tableofcontents',
        'datestamp' => '1352926978',
        'php' => '4.3.5',
      ),
      'project' => 'tableofcontents',
      'version' => '6.x-3.8',
    ),
    'tagadelic' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/tagadelic/tagadelic.module',
      'name' => 'tagadelic',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Tagadelic',
        'description' => 'Tagadelic makes weighted tag clouds from your taxonomy terms.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'taxonomy',
        ),
        'package' => 'Taxonomy',
        'version' => '6.x-1.5',
        'project' => 'tagadelic',
        'datestamp' => '1391549611',
        'php' => '4.3.5',
      ),
      'project' => 'tagadelic',
      'version' => '6.x-1.5',
    ),
    'tcontact' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/tcontact/tcontact.module',
      'name' => 'tcontact',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'token_test' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/token/tests/token_test.module',
      'name' => 'token_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Token Test',
        'description' => 'Testing module for token functionality.',
        'package' => 'Testing',
        'core' => '6.x',
        'files' => 
        array (
          0 => 'token_test.module',
        ),
        'hidden' => true,
        'version' => '6.x-1.19',
        'project' => 'token',
        'datestamp' => '1347470077',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'token',
      'version' => '6.x-1.19',
    ),
    'tokenSTARTER' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/token/tokenSTARTER.module',
      'name' => 'tokenSTARTER',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'TokenSTARTER',
        'description' => 'Provides additional tokens and a base on which to build your own tokens.',
        'dependencies' => 
        array (
          0 => 'token',
        ),
        'core' => '6.x',
        'version' => '6.x-1.19',
        'project' => 'token',
        'datestamp' => '1347470077',
        'php' => '4.3.5',
      ),
      'project' => 'token',
      'version' => '6.x-1.19',
    ),
    'token_actions' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/token/token_actions.module',
      'name' => 'token_actions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Token actions',
        'description' => 'Provides enhanced versions of core Drupal actions using the Token module.',
        'dependencies' => 
        array (
          0 => 'token',
        ),
        'core' => '6.x',
        'version' => '6.x-1.19',
        'project' => 'token',
        'datestamp' => '1347470077',
        'php' => '4.3.5',
      ),
      'project' => 'token',
      'version' => '6.x-1.19',
    ),
    'tweetmeme' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/tweetmeme/tweetmeme.module',
      'name' => 'tweetmeme',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'TweetMeme',
        'description' => 'Allows tracking of links on Twitter, through the TweetMeme web service.',
        'core' => '6.x',
        'version' => '6.x-1.3',
        'project' => 'tweetmeme',
        'datestamp' => '1261259754',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'tweetmeme',
      'version' => '6.x-1.3',
    ),
    'twitter_actions' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/twitter/twitter_actions/twitter_actions.module',
      'name' => 'twitter_actions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Twitter actions',
        'description' => 'Exposes Drupal actions to send Twitter messages.',
        'dependencies' => 
        array (
          0 => 'twitter',
        ),
        'core' => '6.x',
        'version' => '6.x-2.6',
        'project' => 'twitter',
        'datestamp' => '1246548743',
        'php' => '4.3.5',
      ),
      'project' => 'twitter',
      'version' => '6.x-2.6',
    ),
    'varnish' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/varnish/varnish.module',
      'name' => 'varnish',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'views_export' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views/views_export/views_export.module',
      'name' => 'views_export',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views exporter',
        'description' => 'Allows exporting multiple views at once.',
        'package' => 'Views',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'core' => '6.x',
        'version' => '6.x-2.16',
        'project' => 'views',
        'datestamp' => '1321305946',
        'php' => '4.3.5',
      ),
      'project' => 'views',
      'version' => '6.x-2.16',
    ),
    'views_bonus_export' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views_bonus/export/views_bonus_export.module',
      'name' => 'views_bonus_export',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bonus: Views Export',
        'description' => 'Plugin to export views a couple of formats including Comma Separated Values(CSV), Doc or XML',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'package' => 'Views',
        'core' => '6.x',
        'version' => '6.x-1.1',
        'project' => 'views_bonus',
        'datestamp' => '1276701909',
        'php' => '4.3.5',
      ),
      'project' => 'views_bonus',
      'version' => '6.x-1.1',
    ),
    'views_bonus_paged_feed' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views_bonus/paged_feed/views_bonus_paged_feed.module',
      'name' => 'views_bonus_paged_feed',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bonus: Paged Feed',
        'description' => 'Like a feed display on it pages. How cool!',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'package' => 'Views',
        'core' => '6.x',
        'version' => '6.x-1.1',
        'project' => 'views_bonus',
        'datestamp' => '1276701909',
        'php' => '4.3.5',
      ),
      'project' => 'views_bonus',
      'version' => '6.x-1.1',
    ),
    'views_bonus_panels' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views_bonus/panels/views_bonus_panels.module',
      'name' => 'views_bonus_panels',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bonus: Panels',
        'description' => 'Various views style plugins to put views nodes in panels',
        'dependencies' => 
        array (
          0 => 'views',
          1 => 'panels',
        ),
        'package' => 'Views',
        'core' => '6.x',
        'version' => '6.x-1.1',
        'project' => 'views_bonus',
        'datestamp' => '1276701909',
        'php' => '4.3.5',
      ),
      'project' => 'views_bonus',
      'version' => '6.x-1.1',
    ),
    'actions_permissions' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views_bulk_operations/actions_permissions.module',
      'name' => 'actions_permissions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Actions permissions',
        'description' => 'Integrates actions with the permission system.',
        'package' => 'Administration',
        'core' => '6.x',
        'version' => '6.x-1.12',
        'project' => 'views_bulk_operations',
        'datestamp' => '1318585901',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'views_bulk_operations',
      'version' => '6.x-1.12',
    ),
    'votingapi' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/votingapi/votingapi.module',
      'name' => 'votingapi',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Voting API',
        'description' => 'Provides a shared voting API for other modules.',
        'package' => 'Voting',
        'core' => '6.x',
        'version' => '6.x-2.3',
        'project' => 'votingapi',
        'datestamp' => '1250359559',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'votingapi',
      'version' => '6.x-2.3',
    ),
    'wikitools' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/wikitools/wikitools.module',
      'name' => 'wikitools',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'workflow' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/workflow/workflow.module',
      'name' => 'workflow',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Workflow',
        'description' => 'Allows the creation and assignment of arbitrary workflows to node types.',
        'package' => 'Workflow',
        'core' => '6.x',
        'version' => '6.x-1.5',
        'project' => 'workflow',
        'datestamp' => '1288212335',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'workflow',
      'version' => '6.x-1.5',
    ),
    'workflow_access' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/workflow/workflow_access.module',
      'name' => 'workflow_access',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Workflow access',
        'description' => 'Content access control based on workflows and roles.',
        'dependencies' => 
        array (
          0 => 'workflow',
        ),
        'package' => 'Workflow',
        'core' => '6.x',
        'version' => '6.x-1.5',
        'project' => 'workflow',
        'datestamp' => '1288212335',
        'php' => '4.3.5',
      ),
      'project' => 'workflow',
      'version' => '6.x-1.5',
    ),
    'xmlsitemap_custom' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap_custom/xmlsitemap_custom.module',
      'name' => 'xmlsitemap_custom',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML sitemap custom',
        'description' => 'Adds user configurable links to the sitemap.',
        'package' => 'XML sitemap',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'xmlsitemap',
        ),
        'files' => 
        array (
          0 => 'xmlsitemap_custom.module',
          1 => 'xmlsitemap_custom.admin.inc',
          2 => 'xmlsitemap_custom.install',
          3 => 'xmlsitemap_custom.test',
        ),
        'version' => '6.x-2.0',
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'php' => '4.3.5',
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'xmlsitemap_engines_test' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap_engines/tests/xmlsitemap_engines_test.module',
      'name' => 'xmlsitemap_engines_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML sitemap engines test',
        'description' => 'Support module for XML sitemap engines testing.',
        'package' => 'Testing',
        'core' => '6.x',
        'files' => 
        array (
          0 => 'xmlsitemap_engines_test.module',
        ),
        'version' => '6.x-2.0',
        'hidden' => true,
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'xmlsitemap_engines' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap_engines/xmlsitemap_engines.module',
      'name' => 'xmlsitemap_engines',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML sitemap engines',
        'description' => 'Submit the sitemap to search engines.',
        'package' => 'XML sitemap',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'xmlsitemap',
        ),
        'files' => 
        array (
          0 => 'xmlsitemap_engines.module',
          1 => 'xmlsitemap_engines.admin.inc',
          2 => 'xmlsitemap_engines.install',
          3 => 'tests/xmlsitemap_engines.test',
        ),
        'recommends' => 
        array (
          0 => 'site_verify',
        ),
        'version' => '6.x-2.0',
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'php' => '4.3.5',
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'xmlsitemap_i18n' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap_i18n/xmlsitemap_i18n.module',
      'name' => 'xmlsitemap_i18n',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML sitemap internationalization',
        'description' => 'Enables multilingual XML sitemaps.',
        'package' => 'XML sitemap',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'xmlsitemap',
          1 => 'i18n',
        ),
        'files' => 
        array (
          0 => 'xmlsitemap_i18n.module',
          1 => 'xmlsitemap_i18n.test',
        ),
        'version' => '6.x-2.0',
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'php' => '4.3.5',
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'xmlsitemap_modal' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap_modal/xmlsitemap_modal.module',
      'name' => 'xmlsitemap_modal',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML sitemap modal UI',
        'description' => 'Provides an AJAX modal UI for common XML sitemap tasks.',
        'package' => 'XML sitemap',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'xmlsitemap',
          1 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'xmlsitemap_modal.module',
        ),
        'version' => '6.x-2.0',
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'php' => '4.3.5',
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'xmlsitemap_taxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap_taxonomy/xmlsitemap_taxonomy.module',
      'name' => 'xmlsitemap_taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML sitemap taxonomy',
        'description' => 'Add taxonomy term links to the sitemap.',
        'package' => 'XML sitemap',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'xmlsitemap',
          1 => 'taxonomy',
        ),
        'files' => 
        array (
          0 => 'xmlsitemap_taxonomy.module',
          1 => 'xmlsitemap_taxonomy.install',
          2 => 'xmlsitemap_taxonomy.test',
        ),
        'version' => '6.x-2.0',
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'php' => '4.3.5',
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'xmlsitemap_user' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap_user/xmlsitemap_user.module',
      'name' => 'xmlsitemap_user',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML sitemap user',
        'description' => 'Adds user profile links to the sitemap.',
        'package' => 'XML sitemap',
        'dependencies' => 
        array (
          0 => 'xmlsitemap',
        ),
        'core' => '6.x',
        'files' => 
        array (
          0 => 'xmlsitemap_user.module',
          1 => 'xmlsitemap_user.install',
          2 => 'xmlsitemap_user.test',
        ),
        'version' => '6.x-2.0',
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'php' => '4.3.5',
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'xml_parser' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xml_parser/xml_parser.module',
      'name' => 'xml_parser',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML Parser',
        'description' => 'Module containing a class for parsing XML Feeds.',
        'package' => 'CMS Professionals',
        'core' => '6.x',
        'version' => '6.x-1.0',
        'project' => 'xml_parser',
        'datestamp' => '1265911227',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'xml_parser',
      'version' => '6.x-1.0',
    ),
    'dc_storm' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/dc_storm/dc_storm.module',
      'name' => 'dc_storm',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'dc_storm_channels' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/dc_storm/modules/dc_storm_channels/dc_storm_channels.module',
      'name' => 'dc_storm_channels',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'fedict_search' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/fedictsearch/fedict_search.module',
      'name' => 'fedict_search',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fedict Search',
        'description' => 'Allow search in the Fedict Search',
        'dependencies' => 
        array (
          0 => 'search',
        ),
        'package' => 'Fedict Search',
        'core' => '6.x',
        'php' => '5.2',
        'version' => NULL,
      ),
      'project' => '',
      'version' => NULL,
    ),
    'mmw_addemar' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/mmw_addemar/mmw_addemar.module',
      'name' => 'mmw_addemar',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fast2Web Addemar',
        'description' => 'Addemar subscription form module',
        'core' => '6.x',
        'package' => 'Fast2Web',
        'version' => '6.x-0.3',
        'project' => 'mmw_addemar',
        'datestamp' => '1349695324',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'mmw_addemar',
      'version' => '6.x-0.3',
    ),
    'mmw_agenda' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/mmw_agenda/mmw_agenda.module',
      'name' => 'mmw_agenda',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fast2Web Simple Agenda',
        'description' => 'Fast2Web simple agenda module',
        'core' => '6.x',
        'package' => 'Fast2Web',
        'version' => '6.x-0.2',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'views',
          2 => 'ds',
          3 => 'date',
        ),
        'project' => 'mmw_agenda',
        'datestamp' => '1349730425',
        'php' => '4.3.5',
      ),
      'project' => 'mmw_agenda',
      'version' => '6.x-0.2',
    ),
    'mmw_icontheme' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/mmw_icontheme/mmw_icontheme.module',
      'name' => 'mmw_icontheme',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fast2Web icon theme',
        'description' => 'New icon theme for filefield fields',
        'version' => '6.x-0.1',
        'core' => '6.x',
        'package' => 'Fast2Web',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.x-0.1',
    ),
    'mmw_panels' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/mmw_panels/mmw_panels.module',
      'name' => 'mmw_panels',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fast2Web panels',
        'description' => 'Fast2Web panels extensions',
        'core' => '6.x',
        'package' => 'Fast2Web',
        'version' => '6.x-0.3',
        'project' => 'mmw_panels',
        'datestamp' => '1349786525',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'mmw_panels',
      'version' => '6.x-0.3',
    ),
    'mmw_totop' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/mmw_totop/mmw_totop.module',
      'name' => 'mmw_totop',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fast2Web Back to top link',
        'description' => 'Adds a link to top of page when view passes the first scroll',
        'core' => '6.x',
        'package' => 'Fast2Web',
        'version' => '6.x-0.2',
        'project' => 'mmw_totop',
        'datestamp' => '1349793120',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'mmw_totop',
      'version' => '6.x-0.2',
    ),
    'one_demo' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_demo/one_demo.module',
      'name' => 'one_demo',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'one_maxlength' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_maxlength/one_maxlength.module',
      'name' => 'one_maxlength',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'splash' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_splash/splash.module',
      'name' => 'splash',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Splash',
        'description' => 'A splash module with a normal name.',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'one_translations' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_translations/one_translations.module',
      'name' => 'one_translations',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'one_wiki' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_wiki/one_wiki.module',
      'name' => 'one_wiki',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'openeid_profile' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/openeid/modules/openeid_profile/openeid_profile.module',
      'name' => 'openeid_profile',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'openeid' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/openeid/openeid.module',
      'name' => 'openeid',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'poll_override' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/poll_override/poll_override.module',
      'name' => 'poll_override',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Poll Override',
        'description' => 'Override poll module to enable anonymous voting and aggregate poll translation results',
        'package' => 'Voting',
        'dependencies' => 
        array (
          0 => 'poll',
        ),
        'core' => '6.x',
        'version' => '6.x-0.x-dev',
        'project' => 'poll_override',
        'datestamp' => '1380623469',
        'php' => '4.3.5',
      ),
      'project' => 'poll_override',
      'version' => '6.x-0.x-dev',
    ),
    'coder' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/development/coder/coder.module',
      'name' => 'coder',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'devel' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/development/devel/devel.module',
      'name' => 'devel',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Devel',
        'description' => 'Various blocks, pages, and functions for developers.',
        'package' => 'Development',
        'dependencies' => 
        array (
          0 => 'menu',
        ),
        'core' => '6.x',
        'version' => '6.x-1.27',
        'project' => 'devel',
        'datestamp' => '1338940278',
        'php' => '4.3.5',
      ),
      'project' => 'devel',
      'version' => '6.x-1.27',
    ),
    'devel_generate' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/development/devel/devel_generate.module',
      'name' => 'devel_generate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Devel generate',
        'description' => 'Generate dummy users, nodes, and taxonomy terms.',
        'package' => 'Development',
        'core' => '6.x',
        'version' => '6.x-1.27',
        'project' => 'devel',
        'datestamp' => '1338940278',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'devel',
      'version' => '6.x-1.27',
    ),
    'devel_node_access' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/development/devel/devel_node_access.module',
      'name' => 'devel_node_access',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Devel node access',
        'description' => 'Developer block and page illustrating relevant node_access records.',
        'package' => 'Development',
        'core' => '6.x',
        'version' => '6.x-1.27',
        'project' => 'devel',
        'datestamp' => '1338940278',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'devel',
      'version' => '6.x-1.27',
    ),
    'performance' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/development/devel/performance/performance.module',
      'name' => 'performance',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'devel_themer' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/development/devel_themer/devel_themer.module',
      'name' => 'devel_themer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'taxonomy_export' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/development/taxonomy_export/taxonomy_export.module',
      'name' => 'taxonomy_export',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Taxonomy Export',
        'description' => 'Simple taxonomy export & import module.',
        'dependencies' => 
        array (
          0 => 'taxonomy',
        ),
        'core' => '6.x',
        'version' => '6.x-1.3',
        'project' => 'taxonomy_export',
        'datestamp' => '1291587073',
        'php' => '4.3.5',
      ),
      'project' => 'taxonomy_export',
      'version' => '6.x-1.3',
    ),
    'variable_export' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/development/variable_export/variable_export.module',
      'name' => 'variable_export',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'filemanager' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/filemanager/plugins/DrupalAuthenticator/filemanager.module',
      'name' => 'filemanager',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'MCFileManager',
        'description' => 'Enables the Tinymce MCFileManager as an editor plugin.',
        'package' => 'User interface',
        'core' => '6.x',
        'version' => '3.1.2.2',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '3.1.2.2',
    ),
    'imagemanager' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/imagemanager/plugins/DrupalAuthenticator/imagemanager.module',
      'name' => 'imagemanager',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'MCImageManager',
        'description' => 'Enables the Tinymce MCImageManager as an editor plugin.',
        'package' => 'User interface',
        'core' => '6.x',
        'version' => '3.1.2.2',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '3.1.2.2',
    ),
    'fast2web_shared' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/profiles/fast2web_shared/fast2web_shared.module',
      'name' => 'fast2web_shared',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fast2Web Shared',
        'description' => 'This module provides shared components between Fast2Web Profiles and modules.',
        'project' => 'fast2web_shared',
        'version' => '6.x-1.0',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'fast2web_shared',
      'version' => '6.x-1.0',
    ),
    'admin_language' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/admin_language/admin_language.module',
      'name' => 'admin_language',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6102',
      'weight' => '-1',
      'info' => 
      array (
        'name' => 'Administration language',
        'description' => 'Makes sure all administration pages are displayed in the preferred language of the administrator.',
        'core' => '6.x',
        'package' => 'Administration',
        'dependencies' => 
        array (
          0 => 'locale',
        ),
        'version' => '6.x-1.4+12-dev',
        'project' => 'admin_language',
        'datestamp' => '1402910511',
        'php' => '4.3.5',
      ),
      'project' => 'admin_language',
      'version' => '6.x-1.4+12-dev',
    ),
    'hs_taxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hierarchical_select/modules/hs_taxonomy.module',
      'name' => 'hs_taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6002',
      'weight' => '-1',
      'info' => 
      array (
        'name' => 'Hierarchical Select Taxonomy',
        'description' => 'Use Hierarchical Select for Taxonomy.',
        'dependencies' => 
        array (
          0 => 'hierarchical_select',
          1 => 'taxonomy',
        ),
        'package' => 'Form Elements',
        'core' => '6.x',
        'version' => '6.x-3.8',
        'project' => 'hierarchical_select',
        'datestamp' => '1330518354',
        'php' => '4.3.5',
      ),
      'project' => 'hierarchical_select',
      'version' => '6.x-3.8',
    ),
    'webform' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/webform/webform.module',
      'name' => 'webform',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6331',
      'weight' => '-1',
      'info' => 
      array (
        'name' => 'Webform',
        'description' => 'Enables the creation of forms and questionnaires.',
        'core' => '6.x',
        'package' => 'Webform',
        'php' => '5.1',
        'version' => '6.x-3.20',
        'project' => 'webform',
        'datestamp' => '1392182051',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'webform',
      'version' => '6.x-3.20',
    ),
    'block' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/block/block.module',
      'name' => 'block',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Block',
        'description' => 'Controls the boxes that are displayed around the main content.',
        'package' => 'Core - required',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'comment' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/comment/comment.module',
      'name' => 'comment',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6005',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Comment',
        'description' => 'Allows users to comment on and discuss published content.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'contact' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/contact/contact.module',
      'name' => 'contact',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Contact',
        'description' => 'Enables the use of both personal and site-wide contact forms.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'dblog' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/dblog/dblog.module',
      'name' => 'dblog',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Database logging',
        'description' => 'Logs and records system events to the database.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'filter' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/filter/filter.module',
      'name' => 'filter',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Filter',
        'description' => 'Handles the filtering of content in preparation for display.',
        'package' => 'Core - required',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'locale' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/locale/locale.module',
      'name' => 'locale',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6007',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Locale',
        'description' => 'Adds language handling functionality and enables the translation of the user interface to languages other than English.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'menu' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/menu/menu.module',
      'name' => 'menu',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu',
        'description' => 'Allows administrators to customize the site navigation menu.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'node' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/node/node.module',
      'name' => 'node',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node',
        'description' => 'Allows content to be submitted to the site and displayed on pages.',
        'package' => 'Core - required',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'path' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/path/path.module',
      'name' => 'path',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Path',
        'description' => 'Allows users to rename URLs.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'profile' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/profile/profile.module',
      'name' => 'profile',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Profile',
        'description' => 'Supports configurable user profiles.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'search' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/search/search.module',
      'name' => 'search',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Search',
        'description' => 'Enables site-wide keyword searching.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'system' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/system/system.module',
      'name' => 'system',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6055',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System',
        'description' => 'Handles general site configuration for administrators.',
        'package' => 'Core - required',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'taxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/taxonomy/taxonomy.module',
      'name' => 'taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Taxonomy',
        'description' => 'Enables the categorization of content.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'translation' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/translation/translation.module',
      'name' => 'translation',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content translation',
        'description' => 'Allows content to be translated into different languages.',
        'dependencies' => 
        array (
          0 => 'locale',
        ),
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'trigger' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/trigger/trigger.module',
      'name' => 'trigger',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Trigger',
        'description' => 'Enables actions to be fired on certain system events, such as when new content is created.',
        'package' => 'Core - optional',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'user' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/modules/user/user.module',
      'name' => 'user',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User',
        'description' => 'Manages the user registration and login system.',
        'package' => 'Core - required',
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'enterprise_premium' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/profiles/enterprise_premium/modules/enterprise_premium/enterprise_premium.module',
      'name' => 'enterprise_premium',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Enterprise Premium',
        'description' => 'The enterprise premium site',
        'version' => '0.1',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '0.1',
    ),
    'apachesolr' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/apachesolr/apachesolr.module',
      'name' => 'apachesolr',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6005',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Apache Solr framework',
        'description' => 'Framework for searching with Solr',
        'dependencies' => 
        array (
          0 => 'search',
        ),
        'package' => 'Apache Solr',
        'core' => '6.x',
        'php' => '5.1.4',
        'version' => '6.x-2.0-beta2',
        'project' => 'apachesolr',
        'datestamp' => '1270752624',
      ),
      'project' => 'apachesolr',
      'version' => '6.x-2.0-beta2',
    ),
    'apachesolr_search' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/apachesolr/apachesolr_search.module',
      'name' => 'apachesolr_search',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6003',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Apache Solr search',
        'description' => 'Search with Solr',
        'dependencies' => 
        array (
          0 => 'search',
          1 => 'apachesolr',
        ),
        'package' => 'Apache Solr',
        'core' => '6.x',
        'php' => '5.1.4',
        'version' => '6.x-2.0-beta2',
        'project' => 'apachesolr',
        'datestamp' => '1270752624',
      ),
      'project' => 'apachesolr',
      'version' => '6.x-2.0-beta2',
    ),
    'apachesolr_date' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/apachesolr/contrib/apachesolr_date/apachesolr_date.module',
      'name' => 'apachesolr_date',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Apache Solr Date',
        'description' => 'Index and facet on CCK date fields.',
        'dependencies' => 
        array (
          0 => 'apachesolr',
          1 => 'apachesolr_search',
          2 => 'date',
        ),
        'package' => 'Apache Solr',
        'core' => '6.x',
        'php' => '5.1.4',
        'version' => '6.x-2.0-beta2',
        'project' => 'apachesolr',
        'datestamp' => '1270752624',
      ),
      'project' => 'apachesolr',
      'version' => '6.x-2.0-beta2',
    ),
    'apachesolr_nodeaccess' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/apachesolr/contrib/apachesolr_nodeaccess/apachesolr_nodeaccess.module',
      'name' => 'apachesolr_nodeaccess',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Apache Solr node access',
        'description' => 'Integrates the node access system with Apache Solr search',
        'dependencies' => 
        array (
          0 => 'apachesolr',
        ),
        'package' => 'Apache Solr',
        'core' => '6.x',
        'version' => '6.x-2.0-beta2',
        'project' => 'apachesolr',
        'datestamp' => '1270752624',
        'php' => '4.3.5',
      ),
      'project' => 'apachesolr',
      'version' => '6.x-2.0-beta2',
    ),
    'apachesolr_attachments' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/apachesolr_attachments/apachesolr_attachments.module',
      'name' => 'apachesolr_attachments',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Apache Solr search attachments',
        'description' => 'Searching file attachments with Solr',
        'dependencies' => 
        array (
          0 => 'apachesolr',
          1 => 'apachesolr_search',
        ),
        'package' => 'Apache Solr',
        'core' => '6.x',
        'version' => '6.x-2.0-alpha3',
        'project' => 'apachesolr_attachments',
        'datestamp' => '1306388514',
        'php' => '4.3.5',
      ),
      'project' => 'apachesolr_attachments',
      'version' => '6.x-2.0-alpha3',
    ),
    'content' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cck/content.module',
      'name' => 'content',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6010',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content',
        'description' => 'Allows administrators to define new content types.',
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-2.9',
        'project' => 'cck',
        'datestamp' => '1294407979',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'cck',
      'version' => '6.x-2.9',
    ),
    'nodereference' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cck/modules/nodereference/nodereference.module',
      'name' => 'nodereference',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node Reference',
        'description' => 'Defines a field type for referencing one node from another.',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'text',
          2 => 'optionwidgets',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-2.9',
        'project' => 'cck',
        'datestamp' => '1294407979',
        'php' => '4.3.5',
      ),
      'project' => 'cck',
      'version' => '6.x-2.9',
    ),
    'optionwidgets' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cck/modules/optionwidgets/optionwidgets.module',
      'name' => 'optionwidgets',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Option Widgets',
        'description' => 'Defines selection, check box and radio button widgets for text and numeric fields.',
        'dependencies' => 
        array (
          0 => 'content',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-2.9',
        'project' => 'cck',
        'datestamp' => '1294407979',
        'php' => '4.3.5',
      ),
      'project' => 'cck',
      'version' => '6.x-2.9',
    ),
    'text' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cck/modules/text/text.module',
      'name' => 'text',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6003',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Text',
        'description' => 'Defines simple text field types.',
        'dependencies' => 
        array (
          0 => 'content',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'version' => '6.x-2.9',
        'project' => 'cck',
        'datestamp' => '1294407979',
        'php' => '4.3.5',
      ),
      'project' => 'cck',
      'version' => '6.x-2.9',
    ),
    'cd' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/cd/cd.module',
      'name' => 'cd',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Comment displays',
        'description' => 'Extended comment display options',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'ds',
          1 => 'comment',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.2',
        'project' => 'cd',
        'datestamp' => '1327298737',
        'php' => '4.3.5',
      ),
      'project' => 'cd',
      'version' => '6.x-2.2',
    ),
    'config_perms' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/config_perms/config_perms.module',
      'name' => 'config_perms',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Site Configuration Permissions',
        'description' => 'Adds more granular permissions for items under \'administer site configuration\'.',
        'package' => 'Other',
        'core' => '6.x',
        'version' => '6.x-1.0',
        'project' => 'config_perms',
        'datestamp' => '1236209110',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'config_perms',
      'version' => '6.x-1.0',
    ),
    'ctools' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ctools/ctools.module',
      'name' => 'ctools',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6007',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos tools',
        'description' => 'A library of helpful tools by Merlin of Chaos.',
        'core' => '6.x',
        'package' => 'Chaos tool suite',
        'version' => '6.x-1.11',
        'project' => 'ctools',
        'datestamp' => '1392220726',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'ctools',
      'version' => '6.x-1.11',
    ),
    'date' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/date/date/date.module',
      'name' => 'date',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6005',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date',
        'description' => 'Defines CCK date/time fields and widgets.',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'date_api',
          2 => 'date_timezone',
        ),
        'package' => 'Date/Time',
        'core' => '6.x',
        'version' => '6.x-2.10',
        'project' => 'date',
        'datestamp' => '1396284252',
        'php' => '4.3.5',
      ),
      'project' => 'date',
      'version' => '6.x-2.10',
    ),
    'date_api' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/date/date_api.module',
      'name' => 'date_api',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6006',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date API',
        'description' => 'A Date API that can be used by other modules.',
        'package' => 'Date/Time',
        'core' => '6.x',
        'version' => '6.x-2.10',
        'project' => 'date',
        'datestamp' => '1396284252',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'date',
      'version' => '6.x-2.10',
    ),
    'date_popup' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/date/date_popup/date_popup.module',
      'name' => 'date_popup',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Popup',
        'description' => 'Enables jquery popup calendars and time entry widgets for selecting dates and times.',
        'dependencies' => 
        array (
          0 => 'date_api',
          1 => 'date_timezone',
          2 => 'jquery_ui',
        ),
        'package' => 'Date/Time',
        'core' => '6.x',
        'version' => '6.x-2.10',
        'project' => 'date',
        'datestamp' => '1396284252',
        'php' => '4.3.5',
      ),
      'project' => 'date',
      'version' => '6.x-2.10',
    ),
    'date_timezone' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/date/date_timezone/date_timezone.module',
      'name' => 'date_timezone',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '5200',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Timezone',
        'description' => 'Needed when using Date API. Overrides site and user timezone handling to set timezone names instead of offsets.',
        'package' => 'Date/Time',
        'dependencies' => 
        array (
          0 => 'date_api',
        ),
        'core' => '6.x',
        'version' => '6.x-2.10',
        'project' => 'date',
        'datestamp' => '1396284252',
        'php' => '4.3.5',
      ),
      'project' => 'date',
      'version' => '6.x-2.10',
    ),
    'ds' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ds/ds.module',
      'name' => 'ds',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '3',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display suite',
        'description' => 'Manage the display of your data objects.',
        'core' => '6.x',
        'package' => 'Display suite',
        'version' => '6.x-1.6',
        'project' => 'ds',
        'datestamp' => '1322091348',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'ds',
      'version' => '6.x-1.6',
    ),
    'ds_ui' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ds/ds_ui.module',
      'name' => 'ds_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display suite UI',
        'description' => 'Administrative interface to display suite. Without this module, you cannot manage your display settings, fields etc.',
        'core' => '6.x',
        'package' => 'Display suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'version' => '6.x-1.6',
        'project' => 'ds',
        'datestamp' => '1322091348',
        'php' => '4.3.5',
      ),
      'project' => 'ds',
      'version' => '6.x-1.6',
    ),
    'faq' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/faq/faq.module',
      'name' => 'faq',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6003',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Frequently Asked Questions',
        'description' => 'Manages configuration of questions for a FAQ page.',
        'core' => '6.x',
        'version' => '6.x-1.13',
        'project' => 'faq',
        'datestamp' => '1329951344',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'faq',
      'version' => '6.x-1.13',
    ),
    'filefield' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/filefield/filefield.module',
      'name' => 'filefield',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6104',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'FileField',
        'description' => 'Defines a file field type.',
        'dependencies' => 
        array (
          0 => 'content',
        ),
        'package' => 'CCK',
        'core' => '6.x',
        'php' => '5.0',
        'version' => '6.x-3.12',
        'project' => 'filefield',
        'datestamp' => '1392169125',
      ),
      'project' => 'filefield',
      'version' => '6.x-3.12',
    ),
    'flag' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/flag/flag.module',
      'name' => 'flag',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6210',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Flag',
        'description' => 'Create customized flags that users can set on content.',
        'core' => '6.x',
        'package' => 'Flags',
        'php' => '5',
        'version' => '6.x-2.1',
        'project' => 'flag',
        'datestamp' => '1354216010',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'flag',
      'version' => '6.x-2.1',
    ),
    'fontyourface' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/fontyourface/fontyourface.module',
      'name' => 'fontyourface',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => '@font-your-face',
        'description' => 'Administrative interface for managing fonts.',
        'package' => '@font-your-face',
        'core' => '6.x',
        'version' => '6.x-1.4',
        'project' => 'fontyourface',
        'datestamp' => '1284262607',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'fontyourface',
      'version' => '6.x-1.4',
    ),
    'fontsquirrel' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/fontyourface/modules/fontsquirrel/fontsquirrel.module',
      'name' => 'fontsquirrel',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Font Squirrel API',
        'description' => '@font-your-face provider with Font Squirrel fonts.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '6.x',
        'version' => '6.x-1.4',
        'project' => 'fontyourface',
        'datestamp' => '1284262607',
        'php' => '4.3.5',
      ),
      'project' => 'fontyourface',
      'version' => '6.x-1.4',
    ),
    'globalredirect' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/globalredirect/globalredirect.module',
      'name' => 'globalredirect',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6101',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Global Redirect',
        'description' => 'Searches for an alias of the current URL and 301 redirects if found. Stops duplicate content arising when path module is enabled.',
        'core' => '6.x',
        'version' => '6.x-1.5',
        'project' => 'globalredirect',
        'datestamp' => '1339752680',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'globalredirect',
      'version' => '6.x-1.5',
    ),
    'gmap' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/gmap/gmap.module',
      'name' => 'gmap',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '5003',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'GMap',
        'description' => 'Filter to allow insertion of a google map into a node',
        'core' => '6.x',
        'package' => 'Location',
        'version' => '6.x-2.0-beta5',
        'project' => 'gmap',
        'datestamp' => '1378835214',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'gmap',
      'version' => '6.x-2.0-beta5',
    ),
    'googleanalytics' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/google_analytics/googleanalytics.module',
      'name' => 'googleanalytics',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6304',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Google Analytics',
        'description' => 'Adds Google Analytics javascript tracking code to all your site\'s pages.',
        'core' => '6.x',
        'package' => 'Statistics',
        'version' => '6.x-3.6',
        'project' => 'google_analytics',
        'datestamp' => '1382021061',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'google_analytics',
      'version' => '6.x-3.6',
    ),
    'hansel' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hansel/hansel.module',
      'name' => 'hansel',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hansel',
        'description' => 'Provides configurable breadcrumbs',
        'package' => 'Hansel',
        'core' => '6.x',
        'version' => '6.x-1.3',
        'project' => 'hansel',
        'datestamp' => '1323296441',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'hansel',
      'version' => '6.x-1.3',
    ),
    'hansel_taxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hansel/taxonomy/hansel_taxonomy.module',
      'name' => 'hansel_taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hansel Taxonomy',
        'description' => 'Taxonomy related switches and actions',
        'package' => 'Hansel',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'hansel',
          1 => 'taxonomy',
        ),
        'version' => '6.x-1.3',
        'project' => 'hansel',
        'datestamp' => '1323296441',
        'php' => '4.3.5',
      ),
      'project' => 'hansel',
      'version' => '6.x-1.3',
    ),
    'i18nblocks' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18nblocks/i18nblocks.module',
      'name' => 'i18nblocks',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Block translation',
        'description' => 'Enables multilingual blocks and block translation.',
        'dependencies' => 
        array (
          0 => 'i18n',
          1 => 'i18nstrings',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'imageapi' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/imageapi/imageapi.module',
      'name' => 'imageapi',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ImageAPI',
        'description' => 'ImageAPI supporting multiple toolkits.',
        'package' => 'ImageCache',
        'core' => '6.x',
        'php' => '5.1',
        'version' => '6.x-1.10',
        'project' => 'imageapi',
        'datestamp' => '1305563215',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'imageapi',
      'version' => '6.x-1.10',
    ),
    'imageapi_gd' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/imageapi/imageapi_gd.module',
      'name' => 'imageapi_gd',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ImageAPI GD2',
        'description' => 'Uses PHP\'s built-in GD2 image processing support.',
        'package' => 'ImageCache',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'imageapi',
        'datestamp' => '1305563215',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'imageapi',
      'version' => '6.x-1.10',
    ),
    'imagecache' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/imagecache/imagecache.module',
      'name' => 'imagecache',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ImageCache',
        'description' => 'Dynamic image manipulator and cache.',
        'package' => 'ImageCache',
        'dependencies' => 
        array (
          0 => 'imageapi',
        ),
        'core' => '6.x',
        'version' => '6.x-2.0-rc1',
        'project' => 'imagecache',
        'datestamp' => '1337742655',
        'php' => '4.3.5',
      ),
      'project' => 'imagecache',
      'version' => '6.x-2.0-rc1',
    ),
    'imagecache_ui' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/imagecache/imagecache_ui.module',
      'name' => 'imagecache_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ImageCache UI',
        'description' => 'ImageCache User Interface.',
        'dependencies' => 
        array (
          0 => 'imagecache',
          1 => 'imageapi',
        ),
        'package' => 'ImageCache',
        'core' => '6.x',
        'version' => '6.x-2.0-rc1',
        'project' => 'imagecache',
        'datestamp' => '1337742655',
        'php' => '4.3.5',
      ),
      'project' => 'imagecache',
      'version' => '6.x-2.0-rc1',
    ),
    'imagefield' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/imagefield/imagefield.module',
      'name' => 'imagefield',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6006',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ImageField',
        'description' => 'Defines an image field type.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'content',
          1 => 'filefield',
        ),
        'package' => 'CCK',
        'version' => '6.x-3.11',
        'project' => 'imagefield',
        'datestamp' => '1365969012',
        'php' => '4.3.5',
      ),
      'project' => 'imagefield',
      'version' => '6.x-3.11',
    ),
    'jqp' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/jqp/jqp.module',
      'name' => 'jqp',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'jQuery Plugin Handler',
        'description' => 'Provides both an API for modules, and an UI for administrators, to manage and include javascript libraries like jQuery plugins',
        'core' => '6.x',
        'version' => '6.x-2.5',
        'project' => 'jqp',
        'datestamp' => '1285670162',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'jqp',
      'version' => '6.x-2.5',
    ),
    'jquery_ui' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/jquery_ui/jquery_ui.module',
      'name' => 'jquery_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'jQuery UI',
        'description' => 'Provides the jQuery UI plug-in to other Drupal modules.',
        'package' => 'User interface',
        'core' => '6.x',
        'version' => '6.x-1.5',
        'project' => 'jquery_ui',
        'datestamp' => '1308323216',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'jquery_ui',
      'version' => '6.x-1.5',
    ),
    'l10n_client' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/l10n_client/l10n_client.module',
      'name' => 'l10n_client',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Localization client',
        'description' => 'Provides on-page localization',
        'dependencies' => 
        array (
          0 => 'locale',
        ),
        'core' => '6.x',
        'package' => 'Multilanguage',
        'version' => '6.x-1.8',
        'project' => 'l10n_client',
        'datestamp' => '1280784974',
        'php' => '4.3.5',
      ),
      'project' => 'l10n_client',
      'version' => '6.x-1.8',
    ),
    'lightbox2' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/lightbox2/lightbox2.module',
      'name' => 'lightbox2',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6003',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Lightbox2',
        'description' => 'Enables Lightbox2 for Drupal',
        'core' => '6.x',
        'version' => '6.x-1.11',
        'project' => 'lightbox2',
        'datestamp' => '1285342563',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'lightbox2',
      'version' => '6.x-1.11',
    ),
    'linkit' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/linkit/linkit.module',
      'name' => 'linkit',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6100',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Linkit',
        'description' => 'Linkit provides an easy way for users to link to internal content by search with an autocomplete field.',
        'core' => '6.x',
        'package' => 'Linkit',
        'version' => '6.x-1.12',
        'project' => 'linkit',
        'datestamp' => '1362876195',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'linkit',
      'version' => '6.x-1.12',
    ),
    'linkit_node' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/linkit/plugins/linkit_node/linkit_node.module',
      'name' => 'linkit_node',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Linkit Node',
        'description' => 'Extend Linkit with support for node links.',
        'core' => '6.x',
        'package' => 'Linkit',
        'dependencies' => 
        array (
          0 => 'linkit',
        ),
        'version' => '6.x-1.12',
        'project' => 'linkit',
        'datestamp' => '1362876195',
        'php' => '4.3.5',
      ),
      'project' => 'linkit',
      'version' => '6.x-1.12',
    ),
    'linkit_views' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/linkit/plugins/linkit_views/linkit_views.module',
      'name' => 'linkit_views',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Linkit Views',
        'description' => 'Extend Linkit with views links.',
        'core' => '6.x',
        'package' => 'Linkit',
        'dependencies' => 
        array (
          0 => 'linkit',
          1 => 'views',
        ),
        'version' => '6.x-1.12',
        'project' => 'linkit',
        'datestamp' => '1362876195',
        'php' => '4.3.5',
      ),
      'project' => 'linkit',
      'version' => '6.x-1.12',
    ),
    'linkit_picker' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/linkit_picker/linkit_picker.module',
      'name' => 'linkit_picker',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Linkit Picker',
        'description' => 'Linkit Picker adds the possibility to "browse" links like Node Picker had.',
        'core' => '6.x',
        'package' => 'Linkit',
        'dependencies' => 
        array (
          0 => 'linkit',
          1 => 'views',
        ),
        'version' => '6.x-1.x-dev',
        'project' => 'linkit_picker',
        'datestamp' => '1380585019',
        'php' => '4.3.5',
      ),
      'project' => 'linkit_picker',
      'version' => '6.x-1.x-dev',
    ),
    'location' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/location.module',
      'name' => 'location',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6310',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location',
        'package' => 'Location',
        'description' => 'The location module allows you to associate a geographic location with content and users. Users can do proximity searches by postal code.  This is useful for organizing communities that have a geographic presence.',
        'core' => '6.x',
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'location_node' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/location/location_node.module',
      'name' => 'location_node',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node Locations',
        'description' => 'Associate locations with nodes.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'package' => 'Location',
        'core' => '6.x',
        'version' => '6.x-3.3',
        'project' => 'location',
        'datestamp' => '1378891912',
        'php' => '4.3.5',
      ),
      'project' => 'location',
      'version' => '6.x-3.3',
    ),
    'logintoboggan' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/logintoboggan/logintoboggan.module',
      'name' => 'logintoboggan',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'LoginToboggan',
        'description' => 'Improves Drupal\'s login system.',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'logintoboggan',
        'datestamp' => '1320873035',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'logintoboggan',
      'version' => '6.x-1.10',
    ),
    'menu_block' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/menu_block/menu_block.module',
      'name' => 'menu_block',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6201',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu Block',
        'description' => 'Provides configurable blocks of menu items.',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'block',
          1 => 'menu',
        ),
        'version' => '6.x-2.4',
        'project' => 'menu_block',
        'datestamp' => '1297370223',
        'php' => '4.3.5',
      ),
      'project' => 'menu_block',
      'version' => '6.x-2.4',
    ),
    'menu_per_role' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/menu_per_role/menu_per_role.module',
      'name' => 'menu_per_role',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6004',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu Per Role',
        'description' => 'Allows restricting access to menu items per role.',
        'dependencies' => 
        array (
          0 => 'menu',
        ),
        'package' => 'Menu',
        'core' => '6.x',
        'version' => '6.x-1.11',
        'project' => 'menu_per_role',
        'datestamp' => '1321226140',
        'php' => '4.3.5',
      ),
      'project' => 'menu_per_role',
      'version' => '6.x-1.11',
    ),
    'nd' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd/nd.module',
      'name' => 'nd',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '2',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node displays',
        'description' => 'Extended node build and display modes',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.4',
        'project' => 'nd',
        'datestamp' => '1292540633',
        'php' => '4.3.5',
      ),
      'project' => 'nd',
      'version' => '6.x-2.4',
    ),
    'nd_cck' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd_contrib/nd_cck/nd_cck.module',
      'name' => 'nd_cck',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ND CCK',
        'description' => 'CCK support for node displays',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'nd',
          1 => 'content',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.4',
        'project' => 'nd_contrib',
        'datestamp' => '1292540634',
        'php' => '4.3.5',
      ),
      'project' => 'nd_contrib',
      'version' => '6.x-2.4',
    ),
    'nd_search' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd_contrib/nd_search/nd_search.module',
      'name' => 'nd_search',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'ND search',
        'description' => 'Search support for node displays',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'nd',
          1 => 'search',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.4',
        'project' => 'nd_contrib',
        'datestamp' => '1292540634',
        'php' => '4.3.5',
      ),
      'project' => 'nd_contrib',
      'version' => '6.x-2.4',
    ),
    'nice_dash' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nice_dash/nice_dash.module',
      'name' => 'nice_dash',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6201',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nice Dash',
        'description' => 'This modules enables webmasters to create a widget like dashboard',
        'package' => 'Nice dashboard',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'path',
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'nodeaccess' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodeaccess/nodeaccess.module',
      'name' => 'nodeaccess',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '3',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nodeaccess',
        'description' => 'Provides per node access control',
        'core' => '6.x',
        'version' => '6.x-1.4',
        'project' => 'nodeaccess',
        'datestamp' => '1389641305',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'nodeaccess',
      'version' => '6.x-1.4',
    ),
    'nodequeue' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodequeue/nodequeue.module',
      'name' => 'nodequeue',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6007',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nodequeue',
        'description' => 'Create queues which can contain nodes in arbitrary order',
        'package' => 'Nodequeue',
        'core' => '6.x',
        'version' => '6.x-2.11',
        'project' => 'nodequeue',
        'datestamp' => '1316558105',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'nodequeue',
      'version' => '6.x-2.11',
    ),
    'onepageprofile' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/onepageprofile/onepageprofile.module',
      'name' => 'onepageprofile',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'One page profile',
        'description' => 'A module for condensing user profiles into a single edit page',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'profile',
        ),
        'version' => '6.x-1.13',
        'project' => 'onepageprofile',
        'datestamp' => '1319627137',
        'php' => '4.3.5',
      ),
      'project' => 'onepageprofile',
      'version' => '6.x-1.13',
    ),
    'page_title' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/page_title/page_title.module',
      'name' => 'page_title',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6200',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Page Title',
        'description' => 'Enhanced control over the page title (in the &lt;head> tag).',
        'dependencies' => 
        array (
          0 => 'token',
        ),
        'core' => '6.x',
        'package' => 'SEO',
        'version' => '6.x-2.7',
        'project' => 'page_title',
        'datestamp' => '1336556783',
        'php' => '4.3.5',
      ),
      'project' => 'page_title',
      'version' => '6.x-2.7',
    ),
    'panels' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/panels/panels.module',
      'name' => 'panels',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6311',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Panels',
        'description' => 'Core Panels display functions; provides no external UI, at least one other Panels module should be enabled.',
        'core' => '6.x',
        'package' => 'Panels',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '6.x-3.10',
        'project' => 'panels',
        'datestamp' => '1326917148',
        'php' => '4.3.5',
      ),
      'project' => 'panels',
      'version' => '6.x-3.10',
    ),
    'panels_mini' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/panels/panels_mini/panels_mini.module',
      'name' => 'panels_mini',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6303',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Mini panels',
        'description' => 'Create mini panels that can be used as blocks by Drupal and panes by other panel modules.',
        'package' => 'Panels',
        'dependencies' => 
        array (
          0 => 'panels',
        ),
        'core' => '6.x',
        'version' => '6.x-3.10',
        'project' => 'panels',
        'datestamp' => '1326917148',
        'php' => '4.3.5',
      ),
      'project' => 'panels',
      'version' => '6.x-3.10',
    ),
    'pathfilter' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/pathfilter/pathfilter.module',
      'name' => 'pathfilter',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Path Filter',
        'description' => 'Input filter to convert internal paths, such as &quot;internal:node/99&quot;, to their corresponding absolute URL or relative path.',
        'core' => '6.x',
        'version' => '6.x-1.0',
        'project' => 'pathfilter',
        'datestamp' => '1203451809',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'pathfilter',
      'version' => '6.x-1.0',
    ),
    'protect_critical_users' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/protect_critical_users/protect_critical_users.module',
      'name' => 'protect_critical_users',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Protect Critical Users',
        'description' => 'Protects critical users from being deleted.',
        'package' => 'Administration',
        'core' => '6.x',
        'version' => '6.x-1.1',
        'project' => 'protect_critical_users',
        'datestamp' => '1227159919',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'protect_critical_users',
      'version' => '6.x-1.1',
    ),
    'robotstxt' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/robotstxt/robotstxt.module',
      'name' => 'robotstxt',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6100',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'robots.txt',
        'description' => 'Generates the robots.txt file dynamically and gives you the chance to edit it, on a per-site basis, from the web UI.',
        'core' => '6.x',
        'version' => '6.x-1.3',
        'project' => 'robotstxt',
        'datestamp' => '1361113561',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'robotstxt',
      'version' => '6.x-1.3',
    ),
    'scheduler' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/scheduler/scheduler.module',
      'name' => 'scheduler',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6101',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Scheduler',
        'description' => 'This module allows nodes to be published and unpublished on specified dates.',
        'core' => '6.x',
        'version' => '6.x-1.9',
        'project' => 'scheduler',
        'datestamp' => '1367088911',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'scheduler',
      'version' => '6.x-1.9',
    ),
    'securepages' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/securepages/securepages.module',
      'name' => 'securepages',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '1',
      'schema_version' => '1',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Secure Pages',
        'description' => 'Set which pages are always going to be used in secure mode (SSL) Warning: Do not enable this module without configuring your web server to handle SSL with this installation of Drupal',
        'core' => '6.x',
        'version' => '6.x-1.11',
        'project' => 'securepages',
        'datestamp' => '1384060105',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'securepages',
      'version' => '6.x-1.11',
    ),
    'securepages_prevent_hijack' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/securepages_prevent_hijack/securepages_prevent_hijack.module',
      'name' => 'securepages_prevent_hijack',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Secure Pages Hijack Prevention',
        'description' => 'Prevents hijacked session from accessing pages protected by the securepages module.',
        'dependencies' => 
        array (
          0 => 'securepages',
        ),
        'core' => '6.x',
        'version' => '6.x-1.6',
        'project' => 'securepages_prevent_hijack',
        'datestamp' => '1291317942',
        'php' => '4.3.5',
      ),
      'project' => 'securepages_prevent_hijack',
      'version' => '6.x-1.6',
    ),
    'site_map' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/site_map/site_map.module',
      'name' => 'site_map',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Site map',
        'description' => 'Display a site map.',
        'core' => '6.x',
        'version' => '6.x-1.2',
        'project' => 'site_map',
        'datestamp' => '1255552890',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'site_map',
      'version' => '6.x-1.2',
    ),
    'swfupload' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/swfupload/swfupload.module',
      'name' => 'swfupload',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'SWFupload Widget',
        'description' => 'A widget for CCK\'s Filefield which enables multiple file uploads using the SWFUpload library.',
        'package' => 'CCK',
        'version' => '6.x-2.0-beta8',
        'dependencies' => 
        array (
          0 => 'filefield',
          1 => 'jqp',
        ),
        'core' => '6.x',
        'project' => 'swfupload',
        'datestamp' => '1291307743',
        'php' => '4.3.5',
      ),
      'project' => 'swfupload',
      'version' => '6.x-2.0-beta8',
    ),
    'translation_overview' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/translation_overview/translation_overview.module',
      'name' => 'translation_overview',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Translation overview',
        'description' => 'Provides an overview of the translation status of the site\'s content.',
        'dependencies' => 
        array (
          0 => 'translation',
        ),
        'core' => '6.x',
        'php' => '5.1',
        'package' => 'Multilanguage',
        'version' => '6.x-2.4',
        'project' => 'translation_overview',
        'datestamp' => '1259943689',
      ),
      'project' => 'translation_overview',
      'version' => '6.x-2.4',
    ),
    'transliteration' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/transliteration/transliteration.module',
      'name' => 'transliteration',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Transliteration',
        'description' => 'Converts non-latin text to US-ASCII and sanitizes file names.',
        'core' => '6.x',
        'version' => '6.x-3.1',
        'project' => 'transliteration',
        'datestamp' => '1338540716',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'transliteration',
      'version' => '6.x-3.1',
    ),
    'ud' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ud/ud.module',
      'name' => 'ud',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User displays',
        'description' => 'Extended profile display options',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'package' => 'Display suite',
        'version' => '6.x-1.3',
        'project' => 'ud',
        'datestamp' => '1292540639',
        'php' => '4.3.5',
      ),
      'project' => 'ud',
      'version' => '6.x-1.3',
    ),
    'username_enumeration_prevention' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/username_enumeration_prevention/username_enumeration_prevention.module',
      'name' => 'username_enumeration_prevention',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Username Enumeration Prevention',
        'description' => 'Removes the error message produced, from the forgot password form, when an incorrect user has been supplied.',
        'core' => '6.x',
        'version' => '6.x-1.0',
        'project' => 'username_enumeration_prevention',
        'datestamp' => '1329612093',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'username_enumeration_prevention',
      'version' => '6.x-1.0',
    ),
    'views_ui' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views/views_ui.module',
      'name' => 'views_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views UI',
        'description' => 'Administrative interface to views. Without this module, you cannot create or edit your views.',
        'package' => 'Views',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'version' => '6.x-2.16',
        'project' => 'views',
        'datestamp' => '1321305946',
        'php' => '4.3.5',
      ),
      'project' => 'views',
      'version' => '6.x-2.16',
    ),
    'viewsdisplaytabs' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/viewsdisplaytabs/viewsdisplaytabs.module',
      'name' => 'viewsdisplaytabs',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Display Tabs',
        'description' => 'A module that exposes a view\'s displays as tabs.',
        'package' => 'Views',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'core' => '6.x',
        'version' => '6.x-1.0-beta6',
        'project' => 'viewsdisplaytabs',
        'datestamp' => '1267054214',
        'php' => '4.3.5',
      ),
      'project' => 'viewsdisplaytabs',
      'version' => '6.x-1.0-beta6',
    ),
    'views_bulk_operations' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views_bulk_operations/views_bulk_operations.module',
      'name' => 'views_bulk_operations',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Bulk Operations',
        'description' => 'Exposes new Views style \'Bulk Operations\' for selecting multiple nodes and applying operations on them.',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'package' => 'Views',
        'core' => '6.x',
        'php' => '5.0',
        'version' => '6.x-1.12',
        'project' => 'views_bulk_operations',
        'datestamp' => '1318585901',
      ),
      'project' => 'views_bulk_operations',
      'version' => '6.x-1.12',
    ),
    'views_slideshow_singleframe' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views_slideshow/contrib/views_slideshow_singleframe/views_slideshow_singleframe.module',
      'name' => 'views_slideshow_singleframe',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Slideshow: SingleFrame',
        'description' => 'Adds a Single Frame slideshow mode to Views Slideshows.',
        'dependencies' => 
        array (
          0 => 'views_slideshow',
        ),
        'package' => 'Views',
        'core' => '6.x',
        'version' => '6.x-2.4',
        'project' => 'views_slideshow',
        'datestamp' => '1332529262',
        'php' => '4.3.5',
      ),
      'project' => 'views_slideshow',
      'version' => '6.x-2.4',
    ),
    'views_slideshow_thumbnailhover' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views_slideshow/contrib/views_slideshow_thumbnailhover/views_slideshow_thumbnailhover.module',
      'name' => 'views_slideshow_thumbnailhover',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Slideshow: ThumbnailHover',
        'description' => 'Adds a Thumbnail Hover slideshow mode to Views Slideshows.',
        'dependencies' => 
        array (
          0 => 'views_slideshow',
        ),
        'package' => 'Views',
        'core' => '6.x',
        'version' => '6.x-2.4',
        'project' => 'views_slideshow',
        'datestamp' => '1332529262',
        'php' => '4.3.5',
      ),
      'project' => 'views_slideshow',
      'version' => '6.x-2.4',
    ),
    'views_slideshow' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views_slideshow/views_slideshow.module',
      'name' => 'views_slideshow',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Slideshow',
        'description' => 'Provides a View style that displays rows as a jQuery slideshow.',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'package' => 'Views',
        'core' => '6.x',
        'version' => '6.x-2.4',
        'project' => 'views_slideshow',
        'datestamp' => '1332529262',
        'php' => '4.3.5',
      ),
      'project' => 'views_slideshow',
      'version' => '6.x-2.4',
    ),
    'wysiwyg' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/wysiwyg/wysiwyg.module',
      'name' => 'wysiwyg',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6201',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Wysiwyg',
        'description' => 'Allows users to edit contents with client-side editors.',
        'package' => 'User interface',
        'core' => '6.x',
        'version' => '6.x-2.4',
        'project' => 'wysiwyg',
        'datestamp' => '1308450722',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'wysiwyg',
      'version' => '6.x-2.4',
    ),
    'xmlsitemap_menu' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap_menu/xmlsitemap_menu.module',
      'name' => 'xmlsitemap_menu',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6201',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML sitemap menu',
        'description' => 'Adds menu item links to the sitemap.',
        'package' => 'XML sitemap',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'xmlsitemap',
          1 => 'menu',
        ),
        'files' => 
        array (
          0 => 'xmlsitemap_menu.module',
          1 => 'xmlsitemap_menu.install',
          2 => 'xmlsitemap_menu.test',
        ),
        'version' => '6.x-2.0',
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'php' => '4.3.5',
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'xmlsitemap_node' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap_node/xmlsitemap_node.module',
      'name' => 'xmlsitemap_node',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6201',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML sitemap node',
        'description' => 'Adds content links to the sitemap.',
        'package' => 'XML sitemap',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'xmlsitemap',
        ),
        'files' => 
        array (
          0 => 'xmlsitemap_node.module',
          1 => 'xmlsitemap_node.install',
          2 => 'xmlsitemap_node.test',
        ),
        'version' => '6.x-2.0',
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'php' => '4.3.5',
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'one_actions' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_actions/one_actions.module',
      'name' => 'one_actions',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'One actions',
        'description' => 'One contextual actions',
        'core' => '6.x',
        'package' => 'One',
        'version' => '0.1',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '0.1',
    ),
    'admin_menu' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_admin/admin_menu.module',
      'name' => 'admin_menu',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'One administration menu',
        'description' => 'Provides a dropdown menu to most administrative tasks and other common destinations (to users with the proper permissions).',
        'package' => 'One',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'one_apachesolr' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_apachesolr/one_apachesolr.module',
      'name' => 'one_apachesolr',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'One ApacheSolr',
        'description' => 'One apachesolr extensions',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'nd_search',
          1 => 'apachesolr_search',
        ),
        'package' => 'One',
        'version' => '0.1',
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '0.1',
    ),
    'one_menu' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_menu/one_menu.module',
      'name' => 'one_menu',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'One menu',
        'description' => 'One menu extensions',
        'core' => '6.x',
        'package' => 'One',
        'version' => '0.1',
        'dependencies' => 
        array (
          0 => 'menu',
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '0.1',
    ),
    'one_panels' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_panels/one_panels.module',
      'name' => 'one_panels',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'One panel',
        'description' => 'One panels extensions',
        'core' => '6.x',
        'package' => 'One',
        'version' => '0.1',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '0.1',
    ),
    'nd_location' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nd_contrib/nd_location/nd_location.module',
      'name' => 'nd_location',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'ND location',
        'description' => 'Location support for node displays',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'nd',
          1 => 'location',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.4',
        'project' => 'nd_contrib',
        'datestamp' => '1292540634',
        'php' => '4.3.5',
      ),
      'project' => 'nd_contrib',
      'version' => '6.x-2.4',
    ),
    'nodeformcols' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodeformcols/nodeformcols.module',
      'name' => 'nodeformcols',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6100',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'Node form columns',
        'description' => 'Separates the node forms into two columns and a footer.',
        'core' => '6.x',
        'package' => 'Node form columns',
        'version' => '6.x-1.7',
        'project' => 'nodeformcols',
        'datestamp' => '1389798524',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'nodeformcols',
      'version' => '6.x-1.7',
    ),
    'pathauto' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/pathauto/pathauto.module',
      'name' => 'pathauto',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '7',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'Pathauto',
        'description' => 'Provides a mechanism for modules to automatically generate aliases for the content they manage.',
        'dependencies' => 
        array (
          0 => 'path',
          1 => 'token',
        ),
        'core' => '6.x',
        'recommends' => 
        array (
          0 => 'path_redirect',
        ),
        'version' => '6.x-1.6',
        'project' => 'pathauto',
        'datestamp' => '1320076535',
        'php' => '4.3.5',
      ),
      'project' => 'pathauto',
      'version' => '6.x-1.6',
    ),
    'xmlsitemap' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/xmlsitemap/xmlsitemap.module',
      'name' => 'xmlsitemap',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6204',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'XML sitemap',
        'description' => 'Creates an XML sitemap conforming to the <a href="http://sitemaps.org/">sitemaps.org protocol</a>.',
        'package' => 'XML sitemap',
        'core' => '6.x',
        'php' => '5.1',
        'files' => 
        array (
          0 => 'xmlsitemap.module',
          1 => 'xmlsitemap.inc',
          2 => 'xmlsitemap.admin.inc',
          3 => 'xmlsitemap.drush.inc',
          4 => 'xmlsitemap.generate.inc',
          5 => 'xmlsitemap.xmlsitemap.inc',
          6 => 'xmlsitemap.pages.inc',
          7 => 'xmlsitemap.install',
          8 => 'xmlsitemap.test',
        ),
        'recommends' => 
        array (
          0 => 'ctools',
          1 => 'elements',
          2 => 'robotstxt',
          3 => 'vertical_tabs',
        ),
        'configure' => 'admin/config/search/xmlsitemap',
        'version' => '6.x-2.0',
        'project' => 'xmlsitemap',
        'datestamp' => '1395017031',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'xmlsitemap',
      'version' => '6.x-2.0',
    ),
    'twitter' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/twitter/twitter.module',
      'name' => 'twitter',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6007',
      'weight' => '3',
      'info' => 
      array (
        'name' => 'Twitter',
        'description' => 'Adds integration with the Twitter microblogging service.',
        'php' => '5.1',
        'core' => '6.x',
        'version' => '6.x-2.6',
        'project' => 'twitter',
        'datestamp' => '1246548743',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'twitter',
      'version' => '6.x-2.6',
    ),
    'i18ntaxonomy' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18ntaxonomy/i18ntaxonomy.module',
      'name' => 'i18ntaxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6002',
      'weight' => '5',
      'info' => 
      array (
        'name' => 'Taxonomy translation',
        'description' => 'Enables multilingual taxonomy.',
        'dependencies' => 
        array (
          0 => 'i18n',
          1 => 'taxonomy',
          2 => 'i18nstrings',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'i18n' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18n.module',
      'name' => 'i18n',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '1',
      'schema_version' => '9',
      'weight' => '10',
      'info' => 
      array (
        'name' => 'Internationalization',
        'description' => 'Extends Drupal support for multilingual features.',
        'dependencies' => 
        array (
          0 => 'locale',
          1 => 'translation',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'i18nstrings' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/i18n/i18nstrings/i18nstrings.module',
      'name' => 'i18nstrings',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6006',
      'weight' => '10',
      'info' => 
      array (
        'name' => 'String translation',
        'description' => 'Provides support for translation of user defined strings.',
        'dependencies' => 
        array (
          0 => 'locale',
        ),
        'package' => 'Multilanguage',
        'core' => '6.x',
        'version' => '6.x-1.10',
        'project' => 'i18n',
        'datestamp' => '1318336004',
        'php' => '4.3.5',
      ),
      'project' => 'i18n',
      'version' => '6.x-1.10',
    ),
    'nodewords' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodewords/nodewords.module',
      'name' => 'nodewords',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6187',
      'weight' => '10',
      'info' => 
      array (
        'name' => 'Nodewords',
        'description' => 'Implement an API that other modules can use to implement meta tags.',
        'package' => 'Meta tags',
        'core' => '6.x',
        'recommends' => 
        array (
          0 => 'checkall',
          1 => 'vertical_tabs',
        ),
        'version' => '6.x-1.14',
        'project' => 'nodewords',
        'datestamp' => '1354723721',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'nodewords',
      'version' => '6.x-1.14',
    ),
    'token' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/token/token.module',
      'name' => 'token',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '1',
      'weight' => '10',
      'info' => 
      array (
        'name' => 'Token',
        'description' => 'Provides a shared API for replacement of textual placeholders with actual data.',
        'core' => '6.x',
        'version' => '6.x-1.19',
        'project' => 'token',
        'datestamp' => '1347470077',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'token',
      'version' => '6.x-1.19',
    ),
    'views' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/views/views.module',
      'name' => 'views',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6013',
      'weight' => '10',
      'info' => 
      array (
        'name' => 'Views',
        'description' => 'Create customized lists and queries from your database.',
        'package' => 'Views',
        'core' => '6.x',
        'version' => '6.x-2.16',
        'project' => 'views',
        'datestamp' => '1321305946',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'views',
      'version' => '6.x-2.16',
    ),
    'panels_node' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/panels/panels_node/panels_node.module',
      'name' => 'panels_node',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '11',
      'info' => 
      array (
        'name' => 'Panel nodes',
        'description' => 'Create nodes that are divided into areas with selectable content.',
        'package' => 'Panels',
        'dependencies' => 
        array (
          0 => 'panels',
        ),
        'core' => '6.x',
        'version' => '6.x-3.10',
        'project' => 'panels',
        'datestamp' => '1326917148',
        'php' => '4.3.5',
      ),
      'project' => 'panels',
      'version' => '6.x-3.10',
    ),
    'nodewords_basic' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/nodewords/nodewords_basic/nodewords_basic.module',
      'name' => 'nodewords_basic',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6114',
      'weight' => '12',
      'info' => 
      array (
        'name' => 'Nodewords basic meta tags',
        'description' => 'Add the \'abstract\', \'canonical\', \'copyright\', \'description\', \'keywords\', \'logo\', \'original-source\', \'revisit-after\', \'robots\', \'standout\' and \'syndication-source\' meta tags, and the \'title\' HTML tag.',
        'dependencies' => 
        array (
          0 => 'nodewords',
        ),
        'package' => 'Meta tags',
        'core' => '6.x',
        'version' => '6.x-1.14',
        'project' => 'nodewords',
        'datestamp' => '1354723721',
        'php' => '4.3.5',
      ),
      'project' => 'nodewords',
      'version' => '6.x-1.14',
    ),
    'one_seo' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_seo/one_seo.module',
      'name' => 'one_seo',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '15',
      'info' => 
      array (
        'name' => 'One seo',
        'description' => 'One SEO extensions',
        'core' => '6.x',
        'package' => 'One',
        'version' => '0.1',
        'dependencies' => 
        array (
          0 => 'page_title',
          1 => 'nodewords',
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '0.1',
    ),
    'hierarchical_select' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/hierarchical_select/hierarchical_select.module',
      'name' => 'hierarchical_select',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '9',
      'weight' => '16',
      'info' => 
      array (
        'name' => 'Hierarchical Select',
        'description' => 'Simplifies the selection of one or multiple items in a hierarchical tree.',
        'package' => 'Form Elements',
        'core' => '6.x',
        'version' => '6.x-3.8',
        'project' => 'hierarchical_select',
        'datestamp' => '1330518354',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'hierarchical_select',
      'version' => '6.x-3.8',
    ),
    'vd' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/vd/vd.module',
      'name' => 'vd',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '20',
      'info' => 
      array (
        'name' => 'Views displays',
        'description' => 'Extended views display modes',
        'core' => '6.x',
        'dependencies' => 
        array (
          0 => 'ds',
          1 => 'views',
        ),
        'package' => 'Display suite',
        'version' => '6.x-2.0',
        'project' => 'vd',
        'datestamp' => '1327273866',
        'php' => '4.3.5',
      ),
      'project' => 'vd',
      'version' => '6.x-2.0',
    ),
    'one_i18n' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/one_i18n/one_i18n.module',
      'name' => 'one_i18n',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '21',
      'info' => 
      array (
        'name' => 'One i18n',
        'description' => 'One i18n extensions',
        'core' => '6.x',
        'package' => 'One',
        'version' => '0.1',
        'dependencies' => 
        array (
          0 => 'views',
          1 => 'locale',
          2 => 'translation',
          3 => 'content',
          4 => 'text',
          5 => 'admin_language',
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '0.1',
    ),
    'sweaver' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/sweaver/sweaver.module',
      'name' => 'sweaver',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6001',
      'weight' => '99',
      'info' => 
      array (
        'name' => 'Sweaver',
        'description' => 'Visual interface for tweaking or building Drupal themes.',
        'core' => '6.x',
        'package' => 'User interface',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'jquery_ui',
        ),
        'version' => '6.x-1.2',
        'project' => 'sweaver',
        'datestamp' => '1337760994',
        'php' => '4.3.5',
      ),
      'project' => 'sweaver',
      'version' => '6.x-1.2',
    ),
    'better_formats' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/better_formats/better_formats.module',
      'name' => 'better_formats',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6110',
      'weight' => '100',
      'info' => 
      array (
        'name' => 'Better Formats',
        'description' => 'Enhances the core input format system by managing input format defaults and settings.',
        'core' => '6.x',
        'version' => '6.x-1.2',
        'project' => 'better_formats',
        'datestamp' => '1265402405',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'better_formats',
      'version' => '6.x-1.2',
    ),
    'conimbo' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrimbo/conimbo/conimbo.module',
      'name' => 'conimbo',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '6101',
      'weight' => '666',
      'info' => 
      array (
        'name' => 'Conimbo extensions',
        'description' => 'Provides several custom functionalities for Conimbo sites',
        'core' => '6.x',
        'package' => 'Conimbo',
        'version' => '0.1',
        'dependencies' => 
        array (
          0 => 'menu_block',
          1 => 'imagecache',
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '0.1',
    ),
    'ife' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/modules/contrib/ife/ife.module',
      'name' => 'ife',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '1000',
      'info' => 
      array (
        'name' => 'Inline Form Errors',
        'description' => 'Put form error messages inline with the form elements',
        'package' => 'User interface',
        'core' => '6.x',
        'version' => '6.x-1.3',
        'project' => 'ife',
        'datestamp' => '1351725415',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'ife',
      'version' => '6.x-1.3',
    ),
  ),
  'themes' => 
  array (
    'bluemarine' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/themes/bluemarine/bluemarine.info',
      'name' => 'bluemarine',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bluemarine',
        'description' => 'Table-based multi-column theme with a marine and ash color scheme.',
        'version' => '6.31',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'chameleon' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/themes/chameleon/chameleon.info',
      'name' => 'chameleon',
      'type' => 'theme',
      'owner' => 'themes/chameleon/chameleon.theme',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chameleon',
        'description' => 'Minimalist tabled theme with light colors.',
        'regions' => 
        array (
          'left' => 'Left sidebar',
          'right' => 'Right sidebar',
        ),
        'features' => 
        array (
          0 => 'logo',
          1 => 'favicon',
          2 => 'name',
          3 => 'slogan',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'style.css',
            1 => 'common.css',
          ),
        ),
        'version' => '6.31',
        'core' => '6.x',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'clean_theme' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/themes/clean_theme/clean_theme.info',
      'name' => 'clean_theme',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '1',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Clean',
        'description' => 'A clean theme with a lot of white and breathing space. Together with the typography this creates a professional look perfect for serious businesses.',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'base theme' => 'conimbo_base',
        'conimbo' => 
        array (
          'certified' => true,
        ),
        'regions' => 
        array (
          'toolbar' => 'Toolbar',
          'subheader' => 'Subheader',
          'left' => 'Sidebar A',
          'right' => 'Sidebar B',
          'content' => 'Content',
          'doormat' => 'Doormat',
          'footer' => 'Footer',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/style.css',
            1 => 'css/features.css',
            2 => 'css/menu.css',
          ),
        ),
        'columns' => 
        array (
          'container' => '12',
          'left' => '3',
          'right' => '3',
        ),
        'menu' => 
        array (
          'horizontal-dropdown' => 'horizontal with dropdown',
          'horizontal-horizontal' => 'horizontal and horizontal',
          'horizontal-vertical' => 'horizontal and vertical',
          'vertical' => 'only vertical',
        ),
        'placeholders' => 
        array (
          'logo' => 
          array (
            'title' => 'Logo',
            'description' => '',
            'default' => 'logo.png',
          ),
          'button' => 
          array (
            'title' => 'Button',
            'description' => 'The button arrow (14x14)',
            'default' => 'images/icons/button.png',
            'background' => true,
          ),
          'homepage' => 
          array (
            'title' => 'Homepage visual',
            'description' => 'The visual used only on the homepage. This can be a flash file or an image.',
            'default' => 'flash/header1_fv8.swf',
          ),
          'allpages' => 
          array (
            'title' => 'Visual on all other pages',
            'description' => 'The visual used on all other pages. This can be a flash file or an image.',
            'default' => 'flash/header2_fv8.swf',
          ),
          'doormat' => 
          array (
            'title' => 'Doormat background',
            'description' => 'Doormat background image.',
            'default' => 'images/doormat-bg.png',
            'background' => true,
          ),
        ),
        'block_styles' => 
        array (
          'highlight-1' => 
          array (
            'label' => 'Blue background',
            'class' => 'highlight-1',
          ),
          'highlight-2' => 
          array (
            'label' => 'Grey background',
            'class' => 'highlight-2',
          ),
        ),
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'cloud' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/themes/cloud_theme/cloud.info',
      'name' => 'cloud',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Cloud',
        'description' => 'A greyish theme with lots of round corners.',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'base theme' => 'conimbo_base',
        'conimbo' => 
        array (
          'certified' => true,
        ),
        'regions' => 
        array (
          'federalheader' => 'Federalheader',
          'toolbar' => 'Toolbar',
          'header' => 'Header',
          'subheader' => 'Subheader',
          'left' => 'Sidebar A',
          'right' => 'Sidebar B',
          'content' => 'Content',
          'doormat' => 'Doormat',
          'footer' => 'Footer',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/style.css',
            1 => 'css/menu.css',
            2 => 'css/features.css',
          ),
        ),
        'scripts' => 
        array (
          0 => 'js/roundcorners.js',
        ),
        'columns' => 
        array (
          'container' => '12',
          'left' => '2',
          'right' => '3',
        ),
        'menu' => 
        array (
          'horizontal-dropdown' => 'horizontal with dropdown',
          'horizontal-horizontal' => 'horizontal and horizontal',
          'horizontal-vertical' => 'horizontal and vertical',
          'vertical' => 'only vertical',
          'doormat' => 'doormat style',
        ),
        'placeholders' => 
        array (
          'logo' => 
          array (
            'title' => 'Logo',
            'description' => '',
            'default' => 'logo.png',
          ),
        ),
        'block_styles' => 
        array (
          'highlight-1' => 
          array (
            'label' => 'Blue background',
            'class' => 'highlight-1',
          ),
          'highlight-2' => 
          array (
            'label' => 'Grey background',
            'class' => 'highlight-2',
          ),
        ),
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'conimbo_base' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/themes/conimbo_base/conimbo_base.info',
      'name' => 'conimbo_base',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Conimbo base',
        'description' => 'The Conimbo Base theme is used for all conimbo themes. It contains things such as: a reset, 960 model, default images, rounded corners, IE adjustments,...',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/framework/960.css',
            1 => 'css/framework/reset.css',
            2 => 'css/base_style.css',
            3 => 'css/base_features.css',
          ),
          'print' => 
          array (
            0 => 'css/print.css',
          ),
        ),
        'scripts' => 
        array (
          0 => 'js/conimbo_base.js',
          1 => 'js/jquery.curvycorners.js',
        ),
        'media_player_skins' => 
        array (
          'simple_black' => 'Simple black',
        ),
        'features' => 
        array (
          0 => '',
        ),
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'conimbo_certified_starter_theme' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/themes/conimbo_certified_starter_theme/conimbo_certified_starter_theme.info',
      'name' => 'conimbo_certified_starter_theme',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Conimbo certified starter theme',
        'description' => 'This theme is used as a starting point to build new Conimbo certified themes.',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'base theme' => 'conimbo_base',
        'conimbo' => 
        array (
          'certified' => true,
        ),
        'regions' => 
        array (
          'toolbar' => 'Toolbar',
          'header' => 'Header',
          'subheader' => 'Subheader',
          'left' => 'Sidebar A',
          'right' => 'Sidebar B',
          'content' => 'Content',
          'doormat' => 'Doormat',
          'footer' => 'Footer',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/style.css',
            1 => 'css/menu.css',
            2 => 'css/features.css',
          ),
        ),
        'scripts' => 
        array (
          0 => '; --------------------------------------------------------------------------',
        ),
        'columns' => 
        array (
          'container' => '12',
          'left' => '3',
          'right' => '3',
        ),
        'menu' => 
        array (
          'horizontal-dropdown' => 'horizontal with dropdown',
          'horizontal-horizontal' => 'horizontal and horizontal',
          'horizontal-vertical' => 'horizontal and vertical',
          'vertical' => 'only vertical',
        ),
        'placeholders' => 
        array (
          'logo' => 
          array (
            'title' => 'Logo',
            'description' => '',
            'default' => 'logo.png',
          ),
        ),
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'fast2web' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/themes/fast2web/fast2web.info',
      'name' => 'fast2web',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fast2Web',
        'description' => 'This theme is used as a starting point to build new Conimbo certified themes.',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'base theme' => 'conimbo_base',
        'conimbo' => 
        array (
          'certified' => true,
        ),
        'regions' => 
        array (
          'federalheader' => 'Federal header',
          'toolbar' => 'Toolbar',
          'header' => 'Header',
          'subheader' => 'Subheader',
          'left' => 'Sidebar A',
          'right' => 'Sidebar B',
          'content' => 'Content',
          'doormat' => 'Doormat',
          'footer' => 'Footer',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/styles.css',
            1 => 'css/framework/960.css',
            2 => 'css/rte.css',
          ),
        ),
        'scripts' => 
        array (
          0 => '; --------------------------------------------------------------------------',
        ),
        'columns' => 
        array (
          'container' => '12',
          'left' => '3',
          'right' => '3',
        ),
        'menu' => 
        array (
          'horizontal-dropdown' => 'horizontal with dropdown',
          'horizontal-vertical' => 'horizontal and vertical',
        ),
        'placeholders' => 
        array (
          'logo' => 
          array (
            'title' => 'Logo',
            'description' => '',
            'default' => 'logo.png',
          ),
        ),
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'garland' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/themes/garland/garland.info',
      'name' => 'garland',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Garland',
        'description' => 'Tableless, recolorable, multi-column, fluid width theme (default).',
        'version' => '6.31',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'style.css',
          ),
          'print' => 
          array (
            0 => 'print.css',
          ),
        ),
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'marvin' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/themes/chameleon/marvin/marvin.info',
      'name' => 'marvin',
      'type' => 'theme',
      'owner' => '',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Marvin',
        'description' => 'Boxy tabled theme in all grays.',
        'regions' => 
        array (
          'left' => 'Left sidebar',
          'right' => 'Right sidebar',
        ),
        'version' => '6.31',
        'core' => '6.x',
        'base theme' => 'chameleon',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'minnelli' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/themes/garland/minnelli/minnelli.info',
      'name' => 'minnelli',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Minnelli',
        'description' => 'Tableless, recolorable, multi-column, fixed width theme.',
        'version' => '6.31',
        'core' => '6.x',
        'base theme' => 'garland',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'minnelli.css',
          ),
        ),
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'myblog_theme' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/themes/myblog_theme/myblog_theme.info',
      'name' => 'myblog_theme',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'MyBlog theme',
        'description' => 'A brown theme with blue and white typography for a soft warm contrast, ideal for a blog.',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'base theme' => 'conimbo_base',
        'conimbo' => 
        array (
          'certified' => true,
        ),
        'regions' => 
        array (
          'toolbar' => 'Toolbar',
          'header' => 'Header',
          'subheader' => 'Subheader',
          'left' => 'Sidebar A',
          'right' => 'Sidebar B',
          'content' => 'Content',
          'doormat' => 'Doormat',
          'footer' => 'Footer',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/style.css',
            1 => 'css/menu.css',
            2 => 'css/features.css',
          ),
        ),
        'scripts' => 
        array (
          0 => '; --------------------------------------------------------------------------',
        ),
        'columns' => 
        array (
          'container' => '12',
          'left' => '2',
          'right' => '3',
        ),
        'menu' => 
        array (
          'horizontal-dropdown' => 'horizontal with dropdown',
          'horizontal-vertical' => 'horizontal and vertical',
          'vertical' => 'only vertical',
        ),
        'placeholders' => 
        array (
          'logo' => 
          array (
            'title' => 'Logo',
            'description' => '',
            'default' => 'logo.png',
          ),
          'subheader' => 
          array (
            'title' => 'Subheader banner',
            'description' => 'The visual used at the top.',
            'default' => 'images/subheader.png',
          ),
        ),
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'pushbutton' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/themes/pushbutton/pushbutton.info',
      'name' => 'pushbutton',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Pushbutton',
        'description' => 'Tabled, multi-column theme in blue and orange tones.',
        'version' => '6.31',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => '6.31',
    ),
    'rubik' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/themes/rubik/rubik.info',
      'name' => 'rubik',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Rubik',
        'description' => 'Clean admin theme.',
        'base theme' => 'tao',
        'core' => '6.x',
        'engine' => 'phptemplate',
        'scripts' => 
        array (
          0 => 'js/rubik.js',
          1 => 'js/rubik_conimbo.js',
        ),
        'stylesheets' => 
        array (
          'screen' => 
          array (
            0 => 'core.css',
            1 => 'icons.css',
            2 => 'style.css',
            3 => 'conimbo/conimbo.css',
            4 => 'conimbo/icons.css',
          ),
        ),
        'regions' => 
        array (
          'content' => 'Content',
        ),
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '4.3.5',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'tao' => 
    array (
      'filename' => '/home/conimbo/www/conimbo-1.40-6.31/sites/all/themes/tao/tao.info',
      'name' => 'tao',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'throttle' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '6.x',
        'description' => 'Tao is a base theme that is all about going with the flow. It takes care of key reset and utility tasks so that sub-themes can get on with their job.',
        'engine' => 'phptemplate',
        'name' => 'Tao',
        'regions' => 
        array (
          'left' => 'Left sidebar',
          'right' => 'Right sidebar',
          'content' => 'Content',
          'header' => 'Header',
          'footer' => 'Footer',
        ),
        'scripts' => 
        array (
          0 => 'js/tao.js',
        ),
        'stylesheets' => 
        array (
          'screen' => 
          array (
            0 => 'reset.css',
            1 => 'drupal.css',
            2 => 'base.css',
          ),
          'print' => 
          array (
            0 => 'reset.css',
            1 => 'base.css',
            2 => 'print.css',
          ),
          'all' => 
          array (
            0 => 'admin.css',
            1 => 'block.css',
            2 => 'book.css',
            3 => 'comment.css',
            4 => 'dblog.css',
            5 => 'defaults.css',
            6 => 'forum.css',
            7 => 'help.css',
            8 => 'maintenance.css',
            9 => 'node.css',
            10 => 'openid.css',
            11 => 'poll.css',
            12 => 'profile.css',
            13 => 'search.css',
            14 => 'system.css',
            15 => 'system-menus.css',
            16 => 'taxonomy.css',
            17 => 'tracker.css',
            18 => 'update.css',
            19 => 'user.css',
          ),
        ),
        'datestamp' => '1281425129',
        'project' => 'tao',
        'project status url' => 'http://code.developmentseed.org/fserver',
        'version' => '6.x-2.2',
        'dependencies' => 
        array (
        ),
        'php' => '4.3.5',
      ),
      'project' => 'tao',
      'version' => '6.x-2.2',
    ),
  ),
);
$options['site_ip_addresses'] = array (
  '@server_master' => '153.89.236.4',
);
$options['installed'] = true;
# Aegir additions
$_SERVER['db_type'] = $options['db_type'];
$_SERVER['db_port'] = $options['db_port'];
$_SERVER['db_host'] = $options['db_host'];
$_SERVER['db_user'] = $options['db_user'];
$_SERVER['db_passwd'] = $options['db_passwd'];
$_SERVER['db_name'] = $options['db_name'];
