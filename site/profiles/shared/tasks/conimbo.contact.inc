<?php
// $client_email is set (and passed) by Aegir.
global $client_email;

// Create a default category for the Drupal contact form.
$contact = array(
  'cid' => NULL,
  'category' => 'general',
  'recipients' => ($client_email ? $client_email : 'webmaster@localhost'),
  'reply' => '',
  'weight' => '0',
  'selected' => '0',

);
drupal_write_record('contact', $contact);