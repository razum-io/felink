<?php

// System.
$system = array(
  'filename' => 'swfupload-module-jqp',
  'name' => 'swfupload',
  'type' => 'javascript library',
  'owner' => '',
  'status' => 1,
  'throttle' => 0,
  'bootstrap' => 0,
  'schema_version' => -1,
  'weight' => 0,
  'info' => 'a:11:{s:4:"name";s:9:"SWFUpload";s:11:"description";s:97:"This library allows you to upload multiple files at once by ctrl/shift-selecting in dialog boxed.";s:11:"project_url";s:35:"http://code.google.com/p/swfupload/";s:7:"scripts";a:1:{s:7:"2.2.0.1";a:1:{s:42:"sites/all/libraries/swfupload/swfupload.js";s:42:"sites/all/libraries/swfupload/swfupload.js";}}s:8:"filename";s:20:"swfupload-module-jqp";s:6:"status";s:1:"1";s:8:"throttle";s:1:"0";s:14:"schema_version";s:2:"-1";s:12:"old_filename";s:20:"swfupload-module-jqp";s:7:"version";N;s:4:"base";N;}',
);
drupal_write_record('system', $system);

// Refresh Javascript library list.
module_load_include('inc', 'jqp', 'jqp.admin');
$libraries = jqp_list(TRUE);
