<?php

/**
 * Helper to implementation of hook_content_default_fields().
 */
function _enterprise_basic_content_default_fields() {
  $fields = array();

  // Exported field: field_news_image
  $fields[] = array(
    'field_name' => 'field_news_image',
    'type_name' => 'news',
    'display_settings' => array(
      'label' => array(
        'format' => 'hidden',
      ),
      'format' => 'default',
      'region' => 'disabled',
      'ds_weight' => '-20',
      'full' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'large_default',
        'css-class' => 'float-left',
        'region' => 'middle',
        'exclude' => 0,
        'weight' => '-20',
      ),
      'teaser' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'medium_linked',
        'css-class' => 'image_highlight',
        'region' => 'left',
        'exclude' => 0,
        'weight' => '-20',
      ),
      '3' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'medium_linked',
        'css-class' => 'image_highlight',
        'region' => 'left',
        'exclude' => 0,
        'weight' => '-20',
      ),
      'text_list' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'disabled',
        'exclude' => 1,
        'weight' => '-12',
      ),
      'block' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'small_linked',
        'css-class' => 'image_highlight',
        'region' => 'left',
        'exclude' => 0,
        'weight' => '-20',
      ),
      '4' => array(
        'format' => 'small_default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '0',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'widget' => array(
      'file_extensions' => 'jpg gif png jpeg',
      'file_path' => '',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'label' => 'Image',
      'weight' => 0,
      'description' => '',
      'type' => 'imagefield_widget',
      'module' => 'imagefield',
    ),
  );

  // Exported field: field_news_teaser
  $fields[] = array(
    'field_name' => 'field_news_teaser',
    'type_name' => 'news',
    'display_settings' => array(
      'label' => array(
        'format' => 'hidden',
      ),
      'format' => 'default',
      'region' => 'disabled',
      'ds_weight' => '-19',
      'full' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'middle',
        'exclude' => 0,
        'weight' => '-19',
      ),
      'teaser' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'middle',
        'exclude' => 0,
        'weight' => '-17',
      ),
      '3' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'middle',
        'exclude' => 0,
        'weight' => '-20',
      ),
      'text_list' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'disabled',
        'exclude' => 1,
        'weight' => '-14',
      ),
      'block' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'disabled',
        'exclude' => 1,
        'weight' => '-15',
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '2',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_news_teaser][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Teaser',
      'weight' => 0,
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_news_source
  $fields[] = array(
    'field_name' => 'field_news_source',
    'type_name' => 'news',
    'display_settings' => array(
      'label' => array(
        'format' => 'hidden',
      ),
      'format' => 'default',
      'region' => 'disabled',
      'ds_weight' => '-20',
      'full' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'filter_url',
        'region' => 'footer',
        'exclude' => 0,
        'weight' => '-20',
      ),
      'teaser' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'disabled',
        'exclude' => 1,
        'weight' => '-12',
      ),
      '3' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'disabled',
        'exclude' => 1,
        'weight' => '-13',
      ),
      'text_list' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'disabled',
        'exclude' => 1,
        'weight' => '-13',
      ),
      'block' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'disabled',
        'exclude' => 1,
        'weight' => '-14',
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_news_source][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Source',
      'weight' => 0,
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_page_attachments
  $fields[] = array(
    'field_name' => 'field_page_attachments',
    'type_name' => 'page',
    'display_settings' => array(
      'label' => array(
        'format' => 'hidden',
      ),
      'format' => 'default',
      'region' => 'disabled',
      'ds_weight' => '-13',
      'teaser' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'disabled',
        'exclude' => 1,
        'weight' => '-13',
      ),
      'full' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'footer',
        'exclude' => 0,
        'weight' => '-20',
      ),
      '3' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'disabled',
        'exclude' => 1,
        'weight' => '-14',
      ),
      '4' => array(
        'label' => array(
          'format' => 'hidden',
        ),
        'format' => 'default',
        'region' => 'middle',
        'exclude' => 1,
        'weight' => '-18',
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '1',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '1',
    'widget' => array(
      'file_extensions' => 'txt xlsx docx ppt pptx pdf pages jpg gif png xml',
      'file_path' => '',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'label' => 'Attachments',
      'weight' => 0,
      'description' => '',
      'type' => 'filefield_widget',
      'module' => 'filefield',
    ),
  );

  // Exported field: field_views_text_empty
  $fields[] = array(
    'field_name' => 'field_views_text_empty',
    'type_name' => 'views_text',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_views_text_empty][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Views empty',
      'weight' => 0,
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_views_text_footer
  $fields[] = array(
    'field_name' => 'field_views_text_footer',
    'type_name' => 'views_text',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_views_text_footer][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Views footer',
      'weight' => 0,
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_views_text_header
  $fields[] = array(
    'field_name' => 'field_views_text_header',
    'type_name' => 'views_text',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_views_text_header][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Views header',
      'weight' => 0,
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_views_text_title
  $fields[] = array(
    'field_name' => 'field_views_text_title',
    'type_name' => 'views_text',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_views_text_title][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Views title',
      'weight' => 0,
      'description' => 'You can use tokens in this field to have some more control about the title. Tokens available are:
<ul>
<li>[conimbo-single-term] : display a single term from the global node context.</li>
<li>[conimbo-single-term-strtolower] : display a single term from the global node context in lowercase.</li>
</ul>',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );
  $fields[] = array(
    'field_name' => 'field_page_teaser',
    'type_name' => 'page',
    'display_settings' =>
    array (
      'label' =>
      array (
        'format' => 'hidden',
      ),
      'format' => 'default',
      'css-class' => '',
      'region' => 'disabled',
      'ds_weight' => '-34',
      'text_list' =>
      array (
        'label' =>
        array (
          'format' => 'hidden',
        ),
        'format' => 'default',
        'css-class' => '',
        'region' => 'disabled',
        'type' => 'text',
        'parent_id' => '',
        'field_id' => 'field_page_teaser',
        'label_value' => '',
        'exclude' => 1,
        'weight' => '-34',
      ),
      'block' =>
      array (
        'label' =>
        array (
          'format' => 'hidden',
        ),
        'format' => 'default',
        'css-class' => '',
        'region' => 'disabled',
        'type' => 'text',
        'parent_id' => '',
        'field_id' => 'field_page_teaser',
        'label_value' => '',
        'exclude' => 1,
        'weight' => '-34',
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '2',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_page_teaser][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Teaser',
      'weight' => 0,
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );
  // Gallery
  $fields[] = array (
    'label' => 'Gallery image',
    'field_name' => 'field_gallery_image',
    'type_name' => 'gallery',
    'type' => 'filefield',
    'widget_type' => 'swfupload_widget',
    'change' => 'Change basic information',
    'weight' => '-4',
    'file_extensions' => 'png gif jpg jpeg',
    'progress_indicator' => 'bar',
    'file_path' => '',
    'max_filesize_per_file' => '',
    'max_filesize_per_node' => '',
    'max_resolution' => 0,
    'min_resolution' => 0,
    'custom_alt' => 1,
    'alt' => '',
    'custom_title' => 0,
    'title_type' => 'textfield',
    'title' => '',
    'use_default_image' => 0,
    'default_image' => NULL,
    'description' => '',
    'required' => 0,
    'multiple' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '1',
    'module' => 'filefield',
    'widget_module' => 'swfupload',
    'columns' =>
    array (
      'fid' =>
      array (
        'type' => 'int',
        'not null' => false,
        'views' => true,
      ),
      'list' =>
      array (
        'type' => 'int',
        'size' => 'tiny',
        'not null' => false,
        'views' => true,
      ),
      'data' =>
      array (
        'type' => 'text',
        'serialize' => true,
        'views' => true,
      ),
    ),
    'display_settings' =>
    array (
      'label' =>
      array (
        'format' => 'hidden',
      ),
      'format' => 'default',
      'css-class' => '',
      'region' => 'disabled',
      'ds_weight' => '-39',
      'full' =>
      array (
        'label' =>
        array (
          'format' => 'hidden',
        ),
        'format' => 'default',
        'css-class' => '',
        'region' => 'disabled',
        'type' => 'filefield',
        'parent_id' => '',
        'field_id' => 'field_gallery_image',
        'label_value' => '',
        'exclude' => 1,
        'weight' => '-39',
      ),
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'sticky' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  );

  // Translatables
  array(
    t('Attachments'),
    t('Image'),
    t('Introduction'),
    t('Source'),
    t('Views empty'),
    t('Views footer'),
    t('Views header'),
    t('Views title'),
  );

  return $fields;
}
