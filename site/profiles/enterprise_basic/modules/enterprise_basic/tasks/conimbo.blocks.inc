<?php

  // The themes key might be expanded later on with more settings per theme.
  $key = 0;
  $blocks = array(
    $key++ => array(
      'module' => 'one_i18n',
      'delta' => 0,
      'region' => 'toolbar',
      'visibility' => '',
      'title' => '<none>',
      'themes' => array(
        'clean_theme' => 1,
      ),
    ),
    $key++ => array(
      'module' => 'views',
      'delta' => 'News-block_2',
      'region' => 'content',
      'visibility' => '',
      'title' => '',
      'themes' => array(
        'clean_theme' => 1,
      ),
    ),
    $key++ => array(
      'module' => 'views',
      'delta' => 'News-block_1',
      'region' => 'left',
      'visibility' => '',
      'title' => '',
      'themes' => array(
        'clean_theme' => 1,
      ),
      'pages' => "news\nnieuws",
    ),
    $key++  => array(
      'module' => 'views',
      'delta' => 'twitter-block2',
      'region' => 'left',
      'visibility' => '0',
      'pages' => '<front>',
      'title' => 'Tweets',
      'themes' => array(
        'clean_theme' => 1,
      )
    ),
    $key++  => array(
      'module' => 'conimbo',
      'delta' => 'conimbo_feeds',
      'region' => 'toolbar',
      'visibility' => '0',
      'pages' => '',
      'weight' => 0,
      'title' => '<none>',
      'themes' => array(
        'clean_theme' => 1,
      ),
    ),
    $key++  => array(
      'module' => 'menu',
      'delta' => 'menu-footer-en-us',
      'region' => 'footer',
      'visibility' => '',
      'title' => '<none>',
      'themes' => array(
        'clean_theme' => 1,
      )
    ),
    $key++  => array(
      'module' => 'menu',
      'delta' => 'menu-footer-nl',
      'region' => 'footer',
      'visibility' => '',
      'title' => '<none>',
      'themes' => array(
        'clean_theme' => 1,
      )
    ),
    //Mini panels blocks
    $key++  => array(
      'module' => 'panels_mini',
      'delta' => 'doormat_en_us',
      'region' => 'doormat',
      'visibility' => '0',
      'pages' => '',
      'weight' => 0,
      'title' => '<none>',
      'themes' => array(
        'clean_theme' => 1,
      ),
    ),
    $key++  => array(
      'module' => 'panels_mini',
      'delta' => 'doormat_nl',
      'region' => 'doormat',
      'visibility' => '0',
      'pages' => '',
      'weight' => 0,
      'title' => '<none>',
      'themes' => array(
        'clean_theme' => 1,
      ),
    ),
    $key++  => array(
      'module' => 'views',
      'delta' => 'Gallery-block_1',
      'region' => '',
      'visibility' => '0',
      'pages' => '',
      'weight' => 0,
      'title' => '<none>',
      'themes' => array(
        'clean_theme' => 1,
      ),
    ),
    $key++  => array(
      'module' => 'views',
      'delta' => 'Gallery-block_2',
      'region' => '',
      'visibility' => '0',
      'pages' => '',
      'weight' => 0,
      'title' => '<none>',
      'themes' => array(
        'clean_theme' => 1,
      ),
    ),
  );

  foreach ($blocks as $key => $block) {
    foreach ($block['themes'] as $block_theme => $block_theme_status) {
      db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, region, visibility, title, pages)
        VALUES('%s', '%s', '%s', %d, %d, '%s', '%s', '%s', '%s')",
        $block['module'], $block['delta'], $block_theme, $block_theme_status, isset($block['weight']) ? $block['weight'] : 0,
        $block['region'], $block['visibility'], $block['title'], isset($block['pages']) ? $block['pages'] : ''
      );
    }
  }


  $i18n_blocks = array(
    1 => array(
      'module' => 'menu',
      'delta' => 'menu-footer-en-us',
      'type' => '0',
      'language' => 'en-us',
    ),
    2 => array(
      'module' => 'menu',
      'delta' => 'menu-footer-nl',
      'type' => '0',
      'language' => 'nl',
    ),
    3 => array(
      'module' => 'panels_mini',
      'delta' => 'doormat_en_us',
      'type' => '0',
      'language' => 'en-us',
    ),
    4 => array(
      'module' => 'panels_mini',
      'delta' => 'doormat_nl',
      'type' => '0',
      'language' => 'nl',
    ),
  );

  foreach ($i18n_blocks as $key => $block) {
    db_query("INSERT INTO {i18n_blocks} (module, delta, type, language) VALUES ('%s', '%s', %d, '%s')",
      $block['module'], $block['delta'], $block['type'], $block['language']
    );
  }