<?php

/**
 * Rebuild translation overview priority table
 */

$ret = array();
db_drop_table($ret, 'translation_overview_priority');
module_load_include('install', 'translation_overview');
drupal_install_schema('translation_overview');