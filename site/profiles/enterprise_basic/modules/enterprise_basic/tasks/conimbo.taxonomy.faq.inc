<?php

function taxonomy_get_faq() {
  $taxonomy_export = array (
    'vocabulary' => (array(
    'name' => 'FAQ category',
    'description' => 'This vocabulary allows you to order your faq items by subject.',
    'help' => '',
    'relations' => '1',
    'hierarchy' => '0',
    'multiple' => '0',
    'required' => '1',
    'tags' => '0',
    'module' => 'taxonomy',
    'weight' => '0',
    'language' => '',
    'nodes' =>
      array (
        'faq' => 'faq',
      ),
    'i18nmode' => '3',
    )),
  );

  return $taxonomy_export;
}