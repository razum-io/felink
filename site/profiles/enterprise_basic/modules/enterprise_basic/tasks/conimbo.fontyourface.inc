<?php

/**
 * @font-your-face intallation.
 */

function addFontToDatabase($provider, $group_name, $name = NULL) {
	$new_name = is_null($name) ? $group_name : $name;
	db_query("INSERT INTO {fontyourface} (provider, group_name, name) VALUES ('%s', '%s', '%s')", $provider, $group_name, $new_name);
}

$folder = file_directory_path() . '/fontyourface/';
$font_file = drupal_get_path('module', 'conimbo') .'/data/fontsquirrel.zip';

if (file_check_directory($folder, FILE_CREATE_DIRECTORY)) {
	$zip = new ZipArchive;
	if ($zip->open($font_file) === TRUE) {
		$zip->extractTo($folder);
		$zip->close();
 		drush_log("Unzip of $font_file succesfull", 'message');

		addFontToDatabase('fontsquirrel', 'Deutsch Gothic');
		addFontToDatabase('fontsquirrel', 'JungleFever');
		addFontToDatabase('fontsquirrel', 'Caviar Dreams');
		addFontToDatabase('fontsquirrel', 'Impact Label');
		addFontToDatabase('fontsquirrel', 'League Gothic');
		addFontToDatabase('fontsquirrel', 'Riesling');
		addFontToDatabase('fontsquirrel', 'DejaVu Sans');
		addFontToDatabase('fontsquirrel', 'Negotiate');
		addFontToDatabase('fontsquirrel', 'Pusab');
		addFontToDatabase('fontsquirrel', 'TeX Gyre Adventor');

	} else {
		drush_log("Unzip of $font_file failed", 'warning');
	}
} else {
 	drush_log("Unzip of $font_file failed, folder error ($folder)", 'warning');
}
