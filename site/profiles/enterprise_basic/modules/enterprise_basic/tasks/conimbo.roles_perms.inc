<?php
  $incremental_perms =  'access comments, ' .
                        'post comments, ' .
                        'post comments without approval, ' .
                        'access site-wide contact form, ' .
                        'view faq page, ' .
                        'access content, ' .
                        'search content, ' .
                        'access site map, ';
  // Standard roles.
  $standard_perms = array(
    1 => $incremental_perms,
    2 => $incremental_perms .= 'restrictive menu permissions, ',
  );

  foreach ($standard_perms as $rid => $perm) {
    db_query("DELETE FROM {permission} WHERE rid = %d", $rid);
    db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, '%s')", $rid, $perm);
  }

  // Extra roles
  $extra_roles = array(
    3 => 'editor',
    4 => 'builder',
    5 => 'configurator',
    6 => 'editor in chief',
  );

  foreach ($extra_roles as $rid => $role) {
    db_query("INSERT INTO {role} (rid, name) VALUES (%d, '%s')", $rid, $role);
  }

  $extra_perms = array(
    3 => $incremental_perms .= 'administer comments, ' .
                               'access admin menu, ' .
                               'display admin pages in another language, ' .
                               'administer faq order, ' .
                               'create faq, ' .
                               'edit faq, ' .
                               'edit own faq, ' .
                               'access tinymce filemanager, ' .
                               'access tinymce imagemanager, ' .
                               'administer all languages, ' .
                               'administer translations, ' .
                               'administer nodes, ' .
                               'revert revisions, ' .
                               'view revisions, ' .
                               'access actions, ' .
                               'one admin menu, ' .
                               'use panels caching features, ' .
                               'use panels dashboard, ' .
                               'view all panes, ' .
                               'access administration pages, ' .
                               'translate content, ' .
                               'manage nl translation overview priorities, ' .
                               'view translation overview assigments, ' .
                               // SEO
                               'set page title, ' .
                               'create url aliases, ' .
                               'edit meta tag DESCRIPTION, ' .
                               'edit meta tag KEYWORDS, ' .
                               // Swfupload
                               'upload files with swfupload, ' .
                               // Workflow
                               'access workflow summary views, ' ,
    4 => $incremental_perms .= 'administer menu, ' .
                               'administer site information, ' .
                               'administer themes, ' .
                               'display site building menu, ' .
                               'display site configuration menu, ' .
                               'administer site-wide contact form, ' .
                               'access display suite, ' .
                               'administer nd, ' .
                               'configure layout for nd, ' .
                               'export and import settings, ' .
                               'revert overridden settings, ' .
                               'administer faq, ' .
                               'administer google analytics, ' .
                               'submit translations to localization server, ' .
                               'use on-page translation, ' .
                               'administer languages, ' .
                               'administer content types, ' .
                               'administer taxonomy, ' .
                               'administer permissions, ' .
                               'administer users, ' .
                               'configure cron, ' .
                               'administer @font-your-face, ' ,
    5 => $incremental_perms .= 'administer blocks, ' .
                               // Following only relevant if permission 'administer site configuration' is not granted
                               'administer clean-urls, ' .
                               'administer content node settings, ' .
                               'administer date-time, ' .
                               'administer error reporting, ' .
                               'administer file system, ' .
                               'access administration menu, ' .
                               // DS
                               'administer styles, ' .
                               'administer ud, ' .
                               'administer vd, ' .
                               'configure layout for ud, ' .
                               'configure layout for vd, ' .
                               'administer inline form errors, ' .
                               // Panels
                               'administer advanced pane settings, ' .
                               'administer pane access, ' .
                               'administer pane visibility, ' .
                               // Pathauto
                               'administer pathauto, ' .
                               'notify of path changes, ' .
                               // Search
                               'use advanced search, ' .
                               // System
                               'access site reports, ' .
                               // Views
                               'administer views, ' .
                               // jQuery plugin handler
                               'administer javascript libraries, ' .
                               'view all modules',
    6 => 'display admin pages in another language,
          access comments,
          administer comments,
          post comments,
          post comments without approval,
          access tinymce filemanager,
          access tinymce imagemanager,
          access site-wide contact form,
          configure layout for nd,
          configure layout for vd,
          administer faq,
          administer faq order,
          create faq,
          delete faq content,
          delete own faq content,
          edit faq,
          edit own faq,
          view faq page,
          create blog content,
          create files content,
          create gallery content,
          create news content,
          create page content,
          delete any blog content,
          delete any files content,
          delete any gallery content,
          delete any news content,
          delete any page content,
          delete own blog content,
          delete own files content,
          delete own gallery content,
          delete own news content,
          delete own page content,
          edit any blog content,
          edit any error content,
          edit any files content,
          edit any gallery content,
          edit any news content,
          edit any page content,
          edit own blog content,
          edit own files content,
          edit own gallery content,
          edit own news content,
          edit own page content,
          administer feeds,
          clear feed feeds,
          clear meteo_be_rss feeds,
          import feed feeds,
          import meteo_be_rss feeds,
          administer all languages,
          administer translations,
          access content,
          administer content types,
          revert revisions,
          view revisions,
          administer meta tags,
          edit meta tag DESCRIPTION,
          edit meta tag KEYWORDS,
          access actions,
          restrictive menu permissions,
          set page title,
          use panels caching features,
          use panels dashboard,
          view all panes,
          create url aliases,
          cancel own vote,
          create poll content,
          delete any poll content,
          delete own poll content,
          edit any poll content,
          edit own poll content,
          vote on polls,
          schedule (un)publishing of nodes,
          search content,
          access site map,
          upload files with swfupload,
          access administration pages,
          translate content,
          manage fr translation overview priorities,
          manage nl translation overview priorities,
          view translation overview assigments,
          add twitter accounts,
          use global twitter account',
  );

  foreach ($extra_perms as $rid => $perm) {
    db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, '%s')", $rid, $perm);
  }

  // Filters & Better formats.

  db_query("TRUNCATE {filters}");
  db_query("TRUNCATE {filter_formats}");
  db_query("TRUNCATE {better_formats_defaults}");

  $filter_formats = array(
    0 => array(
      'format' => '1',
      'name' => 'Filtered HTML',
      'roles' => ',1,2,',
      'cache' => 1,
    ),
    1 => array(
      'format' => '2',
      'name' => 'Full HTML',
      'roles' => ',1,2,3,4,5',
      'cache' => 1,
    ),
  );

  foreach ($filter_formats as $key => $format) {
    db_query("INSERT INTO {filter_formats} (format, name, roles, cache) VALUES (%d, '%s', '%s', %d)", $format['format'], $format['name'], $format['roles'], $format['cache']);
  }

  $filters = array(
    0 => array('format' => 1, 'module' => 'filter', 'delta' => 2, 'weight' => 0),
    1 => array('format' => 1, 'module' => 'filter', 'delta' => 0, 'weight' => 1),
    2 => array('format' => 1, 'module' => 'filter', 'delta' => 1, 'weight' => 2),
    3 => array('format' => 1, 'module' => 'filter', 'delta' => 3, 'weight' => 10),
    4 => array('format' => 2, 'module' => 'filter', 'delta' => 2, 'weight' => 0),
    5 => array('format' => 2, 'module' => 'filter', 'delta' => 3, 'weight' => 10),
    6 => array('format' => 2, 'module' => 'nodepicker', 'delta' => 0, 'weight' => 10),
  );

  foreach ($filters as $key => $filter) {
    db_query("INSERT INTO {filters} (format, module, delta, weight) VALUES (%d, '%s', %d, %d)", $filter['format'], $filter['module'], $filter['delta'], $filter['weight']);
  }

  $better_formats = array(
    0 => array('rid' => 1, 'type' => 'node', 'format' => 1, 'type_weight' => 1, 'weight' => -22),
    1 => array('rid' => 2, 'type' => 'node', 'format' => 1, 'type_weight' => 1, 'weight' => -23),
    2 => array('rid' => 3, 'type' => 'node', 'format' => 2, 'type_weight' => 1, 'weight' => -24),
    3 => array('rid' => 4, 'type' => 'node', 'format' => 2, 'type_weight' => 1, 'weight' => -25),
    4 => array('rid' => 5, 'type' => 'node', 'format' => 2, 'type_weight' => 1, 'weight' => -26),
    5 => array('rid' => 1, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -22),
    6 => array('rid' => 2, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -23),
    7 => array('rid' => 3, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -24),
    8 => array('rid' => 4, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -25),
    9 => array('rid' => 5, 'type' => 'comment', 'format' => 1, 'type_weight' => 1, 'weight' => -26),
    10 => array('rid' => 1, 'type' => 'block', 'format' => 1, 'type_weight' => 1, 'weight' => -22),
    11 => array('rid' => 2, 'type' => 'block', 'format' => 1, 'type_weight' => 1, 'weight' => -23),
    12 => array('rid' => 3, 'type' => 'block', 'format' => 2, 'type_weight' => 1, 'weight' => -24),
    13 => array('rid' => 4, 'type' => 'block', 'format' => 2, 'type_weight' => 1, 'weight' => -25),
    14 => array('rid' => 5, 'type' => 'block', 'format' => 2, 'type_weight' => 1, 'weight' => -26),
  );

  foreach ($better_formats as $key => $better_format) {
    db_query("INSERT INTO {better_formats_defaults} (rid, type ,format, type_weight, weight) VALUES (%d, '%s', %d, %d, %d)",
      $better_format['rid'], $better_format['type'], $better_format['format'], $better_format['type_weight'], $better_format['weight']
    );
  }

  // Add user 1 with configurator role.
  $user_roles = array(
    1 => 5
  );

  foreach ($user_roles as $uid => $rid) {
    db_query("INSERT INTO {users_roles} (uid, rid) VALUES (%d, %d)", $uid, $rid);
  }
