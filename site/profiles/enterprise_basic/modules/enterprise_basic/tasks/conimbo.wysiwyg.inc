<?php

  $settings = unserialize('a:20:{s:7:"default";i:1;s:11:"user_choose";i:0;s:11:"show_toggle";i:1;s:5:"theme";s:8:"advanced";s:8:"language";s:2:"en";s:7:"buttons";a:17:{s:7:"default";a:24:{s:4:"bold";i:1;s:6:"italic";i:1;s:9:"underline";i:1;s:13:"strikethrough";i:1;s:11:"justifyleft";i:1;s:13:"justifycenter";i:1;s:12:"justifyright";i:1;s:11:"justifyfull";i:1;s:7:"bullist";i:1;s:7:"numlist";i:1;s:7:"outdent";i:1;s:6:"indent";i:1;s:4:"link";i:1;s:6:"anchor";i:1;s:7:"cleanup";i:1;s:3:"sup";i:1;s:3:"sub";i:1;s:10:"blockquote";i:1;s:4:"code";i:1;s:3:"cut";i:1;s:4:"copy";i:1;s:5:"paste";i:1;s:9:"visualaid";i:1;s:12:"removeformat";i:1;}s:11:"contextmenu";a:1:{s:11:"contextmenu";i:1;}s:4:"font";a:1:{s:12:"formatselect";i:1;}s:10:"fullscreen";a:1:{s:10:"fullscreen";i:1;}s:12:"inlinepopups";a:1:{s:12:"inlinepopups";i:1;}s:5:"paste";a:2:{s:9:"pastetext";i:1;s:9:"pasteword";i:1;}s:7:"preview";a:1:{s:7:"preview";i:1;}s:13:"searchreplace";a:2:{s:6:"search";i:1;s:7:"replace";i:1;}s:5:"style";a:1:{s:10:"styleprops";i:1;}s:5:"table";a:1:{s:13:"tablecontrols";i:1;}s:5:"media";a:1:{s:5:"media";i:1;}s:6:"linkit";a:1:{s:6:"linkit";i:1;}s:12:"imagemanager";a:1:{s:11:"insertimage";i:1;}s:11:"filemanager";a:1:{s:10:"insertfile";i:1;}s:9:"cssextras";a:1:{s:24:"bramus_cssextras_classes";i:1;}s:10:"paste_html";a:1:{s:10:"paste_html";i:1;}s:6:"drupal";a:1:{s:5:"break";i:1;}}s:11:"toolbar_loc";s:3:"top";s:13:"toolbar_align";s:4:"left";s:8:"path_loc";s:6:"bottom";s:8:"resizing";i:1;s:11:"verify_html";i:0;s:12:"preformatted";i:0;s:22:"convert_fonts_to_spans";i:0;s:17:"remove_linebreaks";i:0;s:23:"apply_source_formatting";i:0;s:27:"paste_auto_cleanup_on_paste";i:0;s:13:"block_formats";s:11:"p,h2,h3,div";s:11:"css_setting";s:4:"self";s:8:"css_path";s:0:"";s:11:"css_classes";s:0:"";}');

  $settings['css_path'] = '%b'. drupal_get_path('theme', 'clean_theme') .'/css/style.css';

  // Save wysiwyg settings.
  $wysiwyg = array(
    '1' => array(
      'editor' => '',
      'settings' => '',
    ),
    '2' => array(
      'editor' => 'tinymce',
      'settings' => serialize($settings),
    ),
  );
  foreach ($wysiwyg as $format => $values) {
    db_query("INSERT INTO {wysiwyg} (format, editor, settings) VALUES (%d, '%s', '%s')", $format, $values['editor'], $values['settings']);
  }

  // Add pathfilter to text input formats.
  $formats = filter_formats();
  foreach ($formats as $format) {
    // Run query to insert data.
    db_query("INSERT INTO {filters} (format, module, delta, weight)
      VALUES (%d, '%s', %d, %d)", (int)$format->format, 'pathfilter', 0, 10);
  }
