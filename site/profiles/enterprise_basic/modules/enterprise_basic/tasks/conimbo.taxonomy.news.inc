<?php

function taxonomy_get_news() {
  $taxonomy_export = array (
    'vocabulary' => (array(
       'name' => 'News',
       'description' => '',
       'help' => '',
       'relations' => '1',
       'hierarchy' => '1',
       'multiple' => '0',
       'required' => '0',
       'tags' => '0',
       'module' => 'taxonomy',
       'weight' => '0',
       'language' => '',
       'nodes' =>
      array (
        'news' => 'news',
      ),
       'i18nmode' => '3',
    )),
  );

  return $taxonomy_export;
}