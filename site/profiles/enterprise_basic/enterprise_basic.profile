<?php

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *   An array of modules to enable.
 */
function enterprise_basic_profile_modules() {
  return array(
    // Core.
    'menu',
    'locale',
    'dblog',
    'path',
    'search',
    'taxonomy',
    'trigger',

    // Content layouts.
    'content',
    'text',
    'imagefield',
    'filefield',
    'views',
    'ctools',
    'ds',
    'ds_ui',
    'nd',
    'nd_cck',
    'nd_search',
    'vd',
    'nodeformcols',
    'ife',
    'lightbox2',
    'date_api',

    // Media.
    'imagecache',
    'imageapi',
    'imageapi_gd',
    'jqp',
    'swfupload',

    // Various contrib.
    'admin_language',
    'better_formats',
    'config_perms',
    'contact',
    'globalredirect',
    'menu_block',
    'token',
    'pathauto',
    'jquery_ui',
    'wysiwyg',
    'transliteration',
    'views_bulk_operations',
    'twitter',
    'twitter_actions',
    'tweetmeme',
    'site_map',
    'faq',
    'hansel',
    'hansel_taxonomy',
    'nice_dash',

    // Linkit.
    'pathfilter',
    'linkit',
    'linkit_node',
    'linkit_views',
    'linkit_picker',

    // i18n
    'translation',
    'i18n',
    'i18nstrings',
    'one_i18n',
    //'i18nmenu',
    'i18ntaxonomy',
    'i18nblocks',
    'l10n_client',
    'translation_overview',

    // Panels
    'panels',
    'panels_node',
    'panels_mini',

    // Conimbo.
    'conimbo',
    'one_actions',
    'one_menu',
    'one_panels',
    'enterprise_basic',
    'protect_critical_users',
    'admin_menu',

    // Ui which can be disabled.
    'views_ui',
    'imagecache_ui',

    // SEO modules
    'nodewords',
    'nodewords_basic',
    'page_title',
    'one_seo',
    'xmlsitemap',
    'xmlsitemap_menu',
    'xmlsitemap_node',
    'robotstxt',

    // Slide Show
    'views_slideshow',
    'views_slideshow_thumbnailhover',
    'views_slideshow_singleframe',

    //FedICT
    'fedict_search',
  );
}

/**
 * List of optional modules.
 */
function enterprise_basic_profile_optional_modules() {
  return array(
    'views_ui',
    'imagecache_ui',
    'splash',
    'nd_search',
    'vd',
    'cd',
    'mollom',
    'tagadelic',
    'comment',
    'fivestar',
    'votingapi',
    'nd_fivestar',
    'aggregator',
    'nice_dash',
    'hansel',
    'hansel_taxonomy',
    'hansel_export',
    'feeds',
    'feeds_ui',
    'feeds_xpathparser',
    'xml_parser',
    'googleanalytics',
    'backup_migrate',
    'apachesolr',
    'apachesolr_search',
    'linkchecker',
    'sweaver',
    // Xmlsitemap
    'xmlsitemap',
    'xmlsitemap_custom',
    'xmlsitemap_engines',
    'xmlsitemap_i18n',
    'xmlsitemap_menu',
    'xmlsitemap_modal',
    'xmlsitemap_node',
    'xmlsitemap_taxonomy',
    'xmlsitemap_user',

    // i18n
    'i18ncck',
    'i18ncontent',
    'i18nmenu',
    'i18nstrings',
    'i18nsync',
    'i18nviews',
  );
}

/**
 * List of required modules.
 */
function enterprise_basic_profile_required_modules() {
  return array(
    'conimbo',
    'content',
    'ds',
    'nd',
    'nd_cck',
    'config_perms',
    'one_menu',
    'better_formats',
    'dblog',
    'enterprise_basic',
    'logintoboggan',
    'menu_block',
    'path',
    'pathauto',
    'robotstxt',
    'token',
    'transliteration',
    'wysiwyg',
    'protect_critical_users',
    'nodeformcols',
    'admin_language',
    'securepages',
    'securepages_prevent_hijack',
  );
}

function enterprise_basic_profile_details() {
  return array(
    'name' => 'Enterprise Basic',
    'description' => 'Select this profile to enable the enterprise basic site.'
  );
}

function enterprise_basic_profile_tasks(&$task, $url) {
  if ($task == 'profile') {
    // CCK Fields.
    enterprise_basic_build_content_fields();
    // Default content.
    require_once('modules/enterprise_basic/tasks/conimbo.nodes.inc');
    // Panels (has to be executed after nodes.inc)
    require_once('modules/enterprise_basic/tasks/conimbo.panels.inc');
    // Rebuild translation priority.
    require_once('modules/enterprise_basic/tasks/conimbo.tp.inc');
    // Breadcrumbs.
    require_once('modules/enterprise_basic/tasks/conimbo.hansel.inc');
    // Nice dash.
    require_once('modules/enterprise_basic/tasks/conimbo.nicedash.inc');
    // Contact form.
    require_once(dirname(__FILE__) . '/../shared/tasks/conimbo.contact.inc');
    // SWF Upload library.
    require_once(dirname(__FILE__) . '/../shared/tasks/conimbo.swfupload.inc');
  }
}

function enterprise_basic_profile_task_list() {
}
