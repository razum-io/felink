<?php

function taxonomy_get_file_type() {
  $taxonomy_export = array (
    'vocabulary' => (array(
      'name' => 'File type',
      'description' => '',
      'help' => 'Choose a file type',
      'relations' => '1',
      'hierarchy' => '1',
      'multiple' => '0',
      'required' => '0',
      'tags' => '0',
      'module' => 'taxonomy',
      'weight' => '2',
      'language' => '',
      'nodes' =>
        array (
          'document' => 'document',
        ),
      'i18nmode' => '1',
    )),
  );

  return $taxonomy_export;
}

function taxonomy_get_file_type_terms($vid) {
  return array(
    'vid' => $vid,
    'terms' => array('ZIP', 'PDF'),
  );
}