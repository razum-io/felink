<?php

/**
 * Date formats.
 *
 */

  $tables = array(
    'date_formats' => array(
    ),
    'date_format_types' => array(
      0 => array(
        'type' => 'long',
        'title' => 'Long',
        'locked' => 1,
      ),
      1 => array(
        'type' => 'medium',
        'title' => 'Medium',
        'locked' => 1,
      ),
      2 => array(
        'type' => 'short',
        'title' => 'Short',
        'locked' => 1,
      ),
    ),
  );

  foreach ($tables as $table => $rows) {
    db_query("TRUNCATE {$table}");

    foreach ($rows as $row => $fields) {
      $field_names = implode(',', array_keys($fields));
      $field_types_array = array();
      foreach ($fields as $field_name => $field_value) {
        if (is_numeric($field_value)) {
          $field_types_array[] = '%d';
        } else {
          $field_types_array[] = '\'%s\'';
        }
      }
      $field_values = array_values($fields);
      $field_types = implode(',', array_values($field_types_array));
      db_query("INSERT INTO {$table} ($field_names) VALUES ($field_types)", $field_values);
    }
  }