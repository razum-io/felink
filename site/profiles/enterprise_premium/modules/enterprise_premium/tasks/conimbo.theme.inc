<?php

  /**
   * Maintence theme settings.
   */
  require_once('sites/all/modules/contrimbo/conimbo/includes/conimbo.theme.inc');
  $themes = system_theme_data();

  db_query("UPDATE {system} set status = 1 WHERE type ='theme' AND name = 'clean_theme'");
  db_query("UPDATE {system} set status = 1 WHERE type ='theme' AND name = 'cloud_theme'");

  // Configure maintenance theme
  if (function_exists('drush_get_option')) {
    $form_state = array();
    $form_state['values']['theme_default'] = 'clean_theme';
    $form_state['group'] = drush_get_option('web_group', 'www-data');

    conimbo_save_maintenance_theme(array(), $form_state);
  }