<?php
  $key = 0;
  $aliases = array(
    $key++ => array(
      'src' => 'news',
      'dest' => 'nieuws',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'news/rss.xml',
      'dest' => 'nieuws/rss.xml',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'node/7', // homepage english
      'dest' => 'section/homepage',
      'language' => 'en-us',

    ),
    $key++ => array(
      'src' => 'node/8', // homepage nederlands
      'dest' => 'sectie/homepage',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'locations',
      'dest' => 'locaties',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'products',
      'dest' => 'producten',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'memory-list',
      'dest' => 'geheugenlijst',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'products/rss.xml',
      'dest' => 'producten/rss.xml',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'locations/rss.xml',
      'dest' => 'locaties/rss.xml',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'events/rss.xml',
      'dest' => 'evenementen/rss.xml',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'documents/rss.xml',
      'dest' => 'documenten/rss.xml',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'galleries',
      'dest' => 'galerijen',
      'language' => 'nl',
    ),
    $key++ => array(
      'src' => 'blogs/archive',
      'dest' => 'blogs/archief',
      'language' => 'nl',
    ),
  );

  foreach ($aliases as $key => $alias) {
    path_set_alias($alias['src'], $alias['dest'], NULL, $alias['language']);
  }
