<?php

function taxonomy_get_document() {
  $taxonomy_export = array (
    'vocabulary' => (array(
      'name' => 'Document category',
      'description' => '',
      'help' => 'Choose a document category.',
      'relations' => '1',
      'hierarchy' => '1',
      'multiple' => '0',
      'required' => '0',
      'tags' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => '',
      'nodes' =>
     		array (
        	'document' => 'document',
      	),
      'i18nmode' => '3',
    )),
  );

  return $taxonomy_export;
}