<?php

/**
 * Install profile fields.
 */

$profile_fields = array(
  1 => array(
    'title' => 'First name',
    'name' => 'profile_firstname',
    'category' => 'Personal information',
    'type' => 'textfield',
    'required' => 1,
    'register' => 1,
    'visibility' => 2,
    'autocomplete' => 0,
  ),
  2 => array(
    'title' => 'Last name',
    'name' => 'profile_lastname',
    'category' => 'Personal information',
    'type' => 'textfield',
    'required' => 1,
    'register' => 1,
    'visibility' => 2,
    'autocomplete' => 0,
  ),
);

foreach ($profile_fields as $key => $field) {
  db_query("INSERT INTO {profile_fields} (title, name, category, type, required, register, visibility, autocomplete) VALUES
    ('%s', '%s', '%s', '%s', %d, %d, %d, %d)",
  $field['title'], $field['name'], $field['category'], $field['type'], $field['required'], $field['register'], $field['visibility'], $field['autocomplete']);
}