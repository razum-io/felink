<?php

require_once 'includes/locale.inc';

variable_set('language_negotiation','2');

// Rename original english
db_query("UPDATE {languages} SET name = '%s', native = '%s', domain = '%s', prefix = '%s', direction = %d , enabled = %d WHERE language = '%s'", 'Drupal English', 'Drupal English', '', '', LANGUAGE_LTR, 1, 'en');

// Add English, set as active and default language.
locale_add_language('en-us', 'English', 'English', LANGUAGE_LTR, '', 'en', TRUE, TRUE);

// Adding dutch and set as active.
locale_add_language('nl', 'Dutch', 'Nederlands', LANGUAGE_LTR, '', 'nl', TRUE, FALSE);

// Adding french and set as active
//locale_add_language('fr', 'French', 'Français', LANGUAGE_LTR, '', 'fr', TRUE, FALSE);

// Set Admin language
variable_set('admin_language_default', 'en');

// Import English translations (for interface texts we would like somewhat customized.)
$batch = locale_batch_by_language('en-us');
_conimbo_locale_batch_import($batch['operations']);

// Import NL translations
$batch = locale_batch_by_language('nl');
_conimbo_locale_batch_import($batch['operations']);

// Import FR translations
/*$batch = locale_batch_by_language('fr');
_conimbo_locale_batch_import($batch['operations']);*/

function _conimbo_locale_batch_import($files) {
  foreach ($files as $key => $fileset) {
    $files = $fileset[1];
    foreach ($files as $file => $filepath) {
      if (preg_match('!(/|\.)([^\./]+)\.po$!', $filepath, $langcode)) {
        $file = (object) array('filename' => basename($filepath), 'filepath' => $filepath);
        _locale_import_read_po('db-store', $file, LOCALE_IMPORT_KEEP, $langcode[2]);
      }
    }
  }
}