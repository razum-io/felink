<?php

function taxonomy_get_location() {
  $taxonomy_export = array (
    'vocabulary' => (array(
      'name' => 'Location category',
      'description' => '',
      'help' => 'Choose a location category.',
      'relations' => '1',
      'hierarchy' => '1',
      'multiple' => '0',
      'required' => '0',
      'tags' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => '',
      'nodes' =>
        array (
          'location' => 'location',
        ),
      'i18nmode' => '3',
    )),
  );

  return $taxonomy_export;
}