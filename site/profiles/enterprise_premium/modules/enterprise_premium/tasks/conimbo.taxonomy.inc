<?php

  // If you add new taxonomies, check the 'one_menu_taxonomy_menus'
  // variable and taxonomy_hierarchical_select_x in conimbo.variables.inc
  // to see if it needs an update.
  // Blocks also might need updates too.

  $taxonomies = array(
    'news', // 1
    'job', // 2
    'document', // 3
    'document_tags', // 4
    'file_type', // 5
    'location', // 6
    'event', // 7
    'faq', // 8
    'blog', // 9
    'blog_tags', // 10
  );

  foreach ($taxonomies as $taxonomy) {
    _taxonomy_create($taxonomy);
  }

  cache_clear_all('*', 'cache');

function _taxonomy_create($type) {

  if (file_exists(dirname(__FILE__) .'/conimbo.taxonomy.'. $type .'.inc')) {
    require_once('conimbo.taxonomy.'. $type .'.inc');

    $function = 'taxonomy_get_'. $type;
    if (function_exists($function)) {
      $type_array = $function();
      module_invoke('taxonomy', 'save_vocabulary', $type_array['vocabulary']);

      // $vid = db_last_insert_id('vocabulary', 'vid'); /* Doesn't work with given table, deprecated in D7' */
      $vid = db_result(db_query("SELECT MAX(vid) FROM {vocabulary}"));
      // Terms.
      $terms_function = 'taxonomy_get_'. $type .'_terms';
      if (function_exists($terms_function)) {
        $terms = $terms_function($vid);
        foreach ($terms['terms'] as $term) {
          $term_to_save = array(
            'vid' => $terms['vid'],
            'name' => $term,
          );
          taxonomy_save_term($term_to_save);
        }
      }
    }
  }
}