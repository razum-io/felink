<?php

/**
 * Flag.
 *
 */

  $tables = array(
    'flags' => array(
      0 => array(
        'fid' => 1,
        'content_type' => 'node',
        'name' => 'memory_list',
        'title' => 'Memory list',
        'roles' => 2,
        'global' => 0,
        'options' => 'a:13:{s:10:"flag_short";s:18:"Remember this item";s:9:"flag_long";s:33:"Add this item to your memory list";s:12:"flag_message";s:45:"This item has been added to your memory list.";s:17:"flag_confirmation";s:54:"Are you sure you want to add this to your memory list?";s:12:"unflag_short";s:16:"Forget this item";s:11:"unflag_long";s:38:"Remove this item from your memory list";s:14:"unflag_message";s:49:"This item has been removed from your memory list.";s:19:"unflag_confirmation";s:64:"Are you sure you want to remove this item from your memory list?";s:9:"link_type";s:6:"toggle";s:12:"show_on_page";i:1;s:14:"show_on_teaser";i:1;s:12:"show_on_form";i:0;s:4:"i18n";i:0;}',
      ),
    ),
    'flag_content' => array(
    ),
    'flag_counts' => array(
    ),
    'flag_types' => array(
      0 => array(
        'fid' => 1,
        'type' => 'page',
      ),
      1 => array(
        'fid' => 1,
        'type' => 'news',
      ),
      2 => array(
        'fid' => 1,
        'type' => 'location',
      ),
      3 => array(
        'fid' => 1,
        'type' => 'job',
      ),
      4 => array(
        'fid' => 1,
        'type' => 'document',
      ),
      5 => array(
        'fid' => 1,
        'type' => 'faq',
      ),
      6 => array(
        'fid' => 1,
        'type' => 'product',
      ),
      7 => array(
        'fid' => 1,
        'type' => 'event',
      ),
      8 => array(
        'fid' => 1,
        'type' => 'product_category',
      ),
      9 => array(
        'fid' => 1,
        'type' => 'blog',
      ),
    ),
  );

  foreach ($tables as $table => $rows) {
    db_query("TRUNCATE {$table}");

    foreach ($rows as $row => $fields) {
      $field_names = implode(',', array_keys($fields));
      $field_types_array = array();
      foreach ($fields as $field_name => $field_value) {
        if (is_numeric($field_value)) {
          $field_types_array[] = '%d';
        } else {
          $field_types_array[] = '\'%s\'';
        }
      }
      $field_values = array_values($fields);
      $field_types = implode(',', array_values($field_types_array));
      db_query("INSERT INTO {$table} ($field_names) VALUES ($field_types)", $field_values);
    }
  }