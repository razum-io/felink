<?php

/**
 * Nodequeue.
 *
 */

  $tables = array(
    'nodequeue_nodes' => array(
      0 => array(
        'qid' => 1,
        'sqid' => 1,
        'nid' => $nids_by_type['call_to_action']['en-us']['global'],
        'position' => 1,
        'timestamp' => time(),
      ),
      1 => array(
        'qid' => 1,
        'sqid' => 1,
        'nid' => $nids_by_type['call_to_action']['nl']['global'],
        'position' => 2,
        'timestamp' => time(),
      ),
    ),
    'nodequeue_queue' => array(
      0 => array(
        'qid' => 1,
        'title' => 'Call to action',
        'subqueue_title' => '',
        'size' => 0,
        'link' => 'Add to global call to action',
        'link_remove' => 'Remove from global call to action',
        'owner' => 'nodequeue',
        'show_in_ui' => 1,
        'show_in_tab' => 1,
        'show_in_links' => 1,
        'reference' => 0,
        'reverse' => 0,
        'i18n' => 0,
      ),
    ),
    'nodequeue_roles' => array(
      0 => array(
        'qid' => 1,
        'rid' => 3,
      ),
      1 => array(
        'qid' => 1,
        'rid' => 4,
      ),
    ),
    'nodequeue_subqueue' => array(
      0 => array(
        'sqid' => 1,
        'qid' => 1,
        'reference' => 1,
        'title' => 'Call to action',
      ),
    ),
    'nodequeue_types' => array(
      0 => array(
        'qid' => 1,
        'type' => 'call_to_action',
      ),
    ),
  );

  foreach ($tables as $table => $rows) {
    db_query("TRUNCATE {$table}");

    foreach ($rows as $row => $fields) {
      $field_names = implode(',', array_keys($fields));
      $field_types_array = array();
      foreach ($fields as $field_name => $field_value) {
        if (is_numeric($field_value)) {
          $field_types_array[] = '%d';
        } else {
          $field_types_array[] = '\'%s\'';
        }
      }
      $field_values = array_values($fields);
      $field_types = implode(',', array_values($field_types_array));
      db_query("INSERT INTO {$table} ($field_names) VALUES ($field_types)", $field_values);
    }
  }