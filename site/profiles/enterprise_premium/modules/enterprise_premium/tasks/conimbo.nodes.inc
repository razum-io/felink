<?php
  //Beware for tnid's of translated nodes!
  //Beware of nid values of nodereference fields!

  $nodes = array();
  //We need to save nids for some content types for further use. F.i. referencing in panels and nodequeues.
  $nids_by_type = array();
  $key = 0;
  $nodes[++$key] = array(
    'type' => 'error',
    'title' => 'Page not found',
    'body' => 'The page you requested was not found.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nodes[++$key] = array(
    'type' => 'error',
    'title' => 'Pagina niet gevonden',
    'body' => 'De pagina die u opzocht werd niet gevonden.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Jobs in this category',
    'body' => '',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
    'one_views_text' => array('Jobs|block_1' => 'Jobs|block_1'),
    'field_views_text_title' => array(
      0 => array('value' => 'Jobs in [conimbo-single-term]')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Jobs in deze categorie',
    'body' => '',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
    'one_views_text' => array('Jobs|block_1' => 'Jobs|block_1'),

    'field_views_text_title' => array(
      0 => array('value' => 'Jobs in [conimbo-single-term]')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'News in this category',
    'body' => '',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
    'one_views_text' => array('News|block_2' => 'News|block_2'),
    'field_views_text_title' => array(
      0 => array('value' => 'News in [conimbo-single-term]')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Nieuws in deze categorie',
    'body' => '',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
    'one_views_text' => array('News|block_2' => 'News|block_2'),
    'field_views_text_title' => array(
      0 => array('value' => 'Nieuws in [conimbo-single-term]')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'panel',
    'title' => 'Homepage',
    'body' => 'Homepage english',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']][0] = $key;
  $nodes[++$key] = array(
    'type' => 'panel',
    'title' => 'homepage',
    'body' => 'Homepage nederlands',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']][0] = $key;
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'News',
    'body' => '',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
    'one_views_text' => array('News|block_1' => 'News|block_1'),
    'field_views_text_title' => array(
      0 => array('value' => 'News')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Nieuws',
    'body' => '',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
    'one_views_text' => array('News|block_1' => 'News|block_1'),
    'field_views_text_title' => array(
      0 => array('value' => 'Nieuws')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Documents',
    'body' => '',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
    'one_views_text' => array('Documents|block_1' => 'Documents|block_1'),
    'field_views_text_title' => array(
      0 => array('value' => 'Documents')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Documenten',
    'body' => '',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
    'one_views_text' => array('Documents|block_1' => 'Documents|block_1'),
    'field_views_text_title' => array(
      0 => array('value' => 'Documenten')
    ),
   );
   $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Our job openings',
    'body' => '',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
    'one_views_text' => array('Jobs|block_2' => 'Jobs|block_2'),
    'field_views_text_title' => array(
      0 => array('value' => 'Our job openings')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Onze vacatures',
    'body' => '',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
    'one_views_text' => array('Jobs|block_2' => 'Jobs|block_2'),
    'field_views_text_title' => array(
      0 => array('value' => 'Onze vacatures')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'News',
    'body' => '',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
    'one_views_text' => array('News|page_1' => 'News|page_1'),
    'field_views_text_title' => array(
      0 => array('value' => 'News')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Nieuws',
    'body' => '',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
    'one_views_text' => array('News|page_1' => 'News|page_1'),
    'field_views_text_title' => array(
      0 => array('value' => 'Nieuws')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Jobs',
    'body' => '',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
    'one_views_text' => array('Jobs|page_1' => 'Jobs|page_1'),
    'field_views_text_title' => array(
      0 => array('value' => 'Jobs')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Jobs',
    'body' => '',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => 17,
    'one_views_text' => array('Jobs|page_1' => 'Jobs|page_1'),
    'field_views_text_title' => array(
      0 => array('value' => 'Jobs')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'This is your first global call to action',
    'body' => 'Edit your first global call to action here.',
    'teaser' => 'Edit your first global call to action here.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['global'] = $key;
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'Dit is uw eerste algemene oproep tot actie',
    'body' => 'Wijzig uw eerste algemene oproep tot actie hier.',
    'teaser' => 'Wijzig uw eerste algemene oproep tot actie hier.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['global'] = $key;
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'This is your first call to action for pages',
    'body' => 'Edit your first call to action for pages here.',
    'teaser' => 'Edit your first call to action for pages here.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['page'] = $key;
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'Dit is uw eerste oproep tot actie voor pagina\'s',
    'body' => 'Wijzig uw eerste oproep tot actie voor pagina\'s hier.',
    'teaser' => 'Wijzig uw eerste oproep tot actie voor pagina\'s hier.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['page'] = $key;
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'This is your first call to action for news items',
    'body' => 'Edit your first call to action for news items here.',
    'teaser' => 'Edit your first call to action for news items here.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['news'] = $key;
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'Dit is uw eerste oproep tot actie voor nieuws items',
    'body' => 'Wijzig uw eerste oproep tot actie voor nieuws items hier.',
    'teaser' => 'Wijzig uw eerste oproep tot actie voor nieuws items hier.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['news'] = $key;

  $nodes[++$key] = array(
    'type' => 'news',
    'title' => 'This is the title of your first news item',
    'field_news_teaser' => array(
      0 => array('value' => 'This is the teaser content for this news item in overviews.')
    ),
    'field_news_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['en-us']['news'])
    ),
    'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This
style will used throughout the whole website as a default, non formatted style
for textual content.</p>',
    'field_news_source' => array(
      0 => array('value' => 'This is the source of the news item')
    ),
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nodes[++$key] = array(
    'type' => 'news',
    'title' => 'Dit is de titel van uw eerste nieuwsbericht',
    'field_news_teaser' => array(
      0 => array('value' => 'Dit is de intro van dit nieuwsbericht in overzichten.')
    ),
    'field_news_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['nl']['news'])
    ),
    'body' => 'Dit is uw eerste nieuwsbericht.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'This is your first call to action for documents',
    'body' => 'Edit your first call to action for documents here.',
    'teaser' => 'Edit your first call to action for documents here.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['document'] = $key;
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'Dit is uw eerste oproep tot actie voor documenten',
    'body' => 'Wijzig uw eerste oproep tot actie voor documenten hier.',
    'teaser' => 'Wijzig uw eerste oproep tot actie voor documenten hier.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['documents'] = $key;
  $nodes[++$key] = array(
    'type' => 'document',
    'title' => 'This is your first document',
    'field_document_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['en-us']['document'])
    ),
    'body' => 'This is your first document.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nodes[++$key] = array(
    'type' => 'document',
    'title' => 'Dit is uw eerste document',
    'field_document_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['nl']['document'])
    ),
    'body' => 'Dit is uw eerste document.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'This is your first call to action for jobs',
    'body' => 'Edit your first call to action for jobs here.',
    'teaser' => 'Edit your first call to action for jobs here.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['job'] = $key;
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'Dit is uw eerste oproep tot actie voor vacatures',
    'body' => 'Wijzig uw eerste oproep tot actie voor vacatures hier.',
    'teaser' => 'Wijzig uw eerste oproep tot actie voor vacatures hier.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['job'] = $key;
  $nodes[++$key] = array(
    'type' => 'job',
    'title' => 'This is your first job posting',
    'field_job_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['en-us']['job'])
    ),
    'body' => 'This is your first job posting.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nodes[++$key] = array(
    'type' => 'job',
    'title' => 'Dit is uw eerste vacature',
    'field_job_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['nl']['job'])
    ),
    'body' => 'Dit is uw eerste vacature.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nodes[++$key] = array(
    'type' => 'product_category',
    'title' => 'This is your first product category',
    'field_product_category_intro' => array(
      0 => array('value' => 'Edit this category or delete it to start building your products section.')
    ),
    'body' => 'This is your first product category.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nodes[++$key] = array(
    'type' => 'product_category',
    'title' => 'Dit is uw eerste product categorie',
    'field_product_category_intro' => array(
      0 => array('value' => 'Pas deze product categorie aan of verwijder ze om uw product catalogus samen te stellen.')
    ),
    'body' => 'Dit is uw eerste product categorie.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'This is your first call to action for products',
    'body' => 'Edit your first call to action for products here.',
    'teaser' => 'Edit your first call to action for products here.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['product'] = $key;
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'Dit is uw eerste oproep tot actie voor producten',
    'body' => 'Wijzig uw eerste oproep tot actie voor producten hier.',
    'teaser' => 'Wijzig uw eerste oproep tot actie voor producten hier.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['product'] = $key;
  $nodes[++$key] = array(
    'type' => 'product',
    'title' => 'This is your first product',
    'field_product_intro' => array(
      0 => array('value' => 'Edit this product or delete it to start building your products section.')
    ),
    'field_product_category_reference' => array(
      0 => array('nid' => $key-2)
    ),
    'field_product_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['en-us']['product'])
    ),
    'body' => 'This is your first product.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nodes[++$key] = array(
    'type' => 'product',
    'title' => 'Dit is uw eerste product',
    'field_product_intro' => array(
      0 => array('value' => 'Pas dit product aan of verwijder ze om uw product catalogus samen te stellen.')
    ),
    'field_product_category_reference' => array(
      0 => array('nid' => $key-2)
    ),
    'field_product_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['nl']['product'])
    ),
    'body' => 'Dit is uw eerste product.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'This is your first call to action for events',
    'body' => 'Edit your first call to action for events here.',
    'teaser' => 'Edit your first call to action for events here.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['event'] = $key;
  $nodes[++$key] = array(
    'type' => 'call_to_action',
    'title' => 'Dit is uw eerste oproep tot actie voor evenementen',
    'body' => 'Wijzig uw eerste oproep tot actie voor evenementen hier.',
    'teaser' => 'Wijzig uw eerste oproep tot actie voor evenementen hier.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nids_by_type[$nodes[$key]['type']][$nodes[$key]['language']]['event'] = $key;
  $nodes[++$key] = array(
    'type' => 'event',
    'title' => 'This is your first event',
    'field_event_intro' => array(
      0 => array('value' => 'Modify this event, or delete it and create new ones.')
    ),
    'field_event_date' => array(
      0 => array(
        'value' => time(),
        'value2' => mktime(0,0,0,date("m")+1,date("d"),date("Y")),
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datestamp',
      ),
    ),
    'field_event_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['en-us']['event'])
    ),
    'body' => 'This is your first event.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nodes[++$key] = array(
    'type' => 'event',
    'title' => 'Dit is uw eerste evenement',
    'field_event_intro' => array(
      0 => array('value' => 'Pas dit evenement aan of verwijder het en maak nieuwe aan.')
    ),
    'field_event_date' => array(
      0 => array(
        'value' => time(),
        'value2' => mktime(0,0,0,date("m")+1,date("d"),date("Y")),
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datestamp',
      ),
    ),
    'field_event_cta' => array(
      0 => array('nid' => $nids_by_type['call_to_action']['nl']['event'])
    ),
    'body' => 'Dit is uw eerste evenement.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Events',
    'body' => '',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => 31,
    'one_views_text' => array('Events|block_3' => 'Events|block_3'),
    'field_views_text_title' => array(
      0 => array('value' => 'Events')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Evenementen',
    'body' => '',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => 31,
    'one_views_text' => array('Events|block_3' => 'Events|block_3'),
    'field_views_text_title' => array(
      0 => array('value' => 'Evenementen')
    ),
   );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Call to action',
    'body' => '',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
    'one_views_text' => array('nodequeue_1|block' => 'nodequeue_1|block'),
    'field_views_text_title' => array(
      0 => array('value' => 'Call to action')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'views_text',
    'title' => 'Oproep tot actie',
    'body' => '',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
    'one_views_text' => array('nodequeue_1|block' => 'nodequeue_1|block'),
    'field_views_text_title' => array(
      0 => array('value' => 'Oproep tot actie')
    ),
  );
  $nodes[++$key] = array(
    'type' => 'page',
    'title' => 'Disclaimer',
    'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more
prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h3>This is a level 3 subtitle.</h3>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h4>This is a level 4 subtitle.</h4>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h5>This is a level 5 subtitle.</h5>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h6>This is a level 6 subtitle.</h6>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<ul>
  <li>Unordered list first level.</li>
  <li>Unordered list first level.
    <ul>
      <li>Unordered list second level.</li>
      <li>Unordered list second level.</li>
    </ul>
  </li>
  <li>Unordered list first level.</li>
</ul>
<ol>
  <li>Ordered list first level.</li>
  <li>Ordered list first level.
    <ol>
      <li>Ordered list second level.</li>
      <li>Ordered list second level.</li>
    </ol>
  </li>
  <li>Ordered list first level.</li>
</ol>
<p class="highlight1">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<p class="highlight2">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<div class="messages warning">
This is a warning text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages status">
This is a status text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages error">
This is an error text. This paragraph style is used to give feedback to the user.
</div>
<blockquote>
This is a quote. This paragraph style is commonly used to point out some quoted
text lines.
</blockquote>
<p>This is a <a href="">link tag</a> example</a><br />
This is an <i>italic tag</i> example<br />
This is a <strike>strike tag</strike> example<br />
This is a <strong>strong</strong> tag example<br />
This is <sub>a sub tag</sub> example<br />
This is a <sup>sup tag</sup> example</p>.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nodes[++$key] = array(
    'type' => 'page',
    'title' => 'Disclaimer',
    'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more
prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h3>This is a level 3 subtitle.</h3>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h4>This is a level 4 subtitle.</h4>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h5>This is a level 5 subtitle.</h5>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h6>This is a level 6 subtitle.</h6>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<ul>
  <li>Unordered list first level.</li>
  <li>Unordered list first level.
    <ul>
      <li>Unordered list second level.</li>
      <li>Unordered list second level.</li>
    </ul>
  </li>
  <li>Unordered list first level.</li>
</ul>
<ol>
  <li>Ordered list first level.</li>
  <li>Ordered list first level.
    <ol>
      <li>Ordered list second level.</li>
      <li>Ordered list second level.</li>
    </ol>
  </li>
  <li>Ordered list first level.</li>
</ol>
<p class="highlight1">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<p class="highlight2">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<div class="messages warning">
This is a warning text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages status">
This is a status text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages error">
This is an error text. This paragraph style is used to give feedback to the user.
</div>
<blockquote>
This is a quote. This paragraph style is commonly used to point out some quoted
text lines.
</blockquote>
<p>This is a <a href="">link tag</a> example</a><br />
This is an <i>italic tag</i> example<br />
This is a <strike>strike tag</strike> example<br />
This is a <strong>strong</strong> tag example<br />
This is <sub>a sub tag</sub> example<br />
This is a <sup>sup tag</sup> example</p>.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );
  $nodes[++$key] = array(
    'type' => 'page',
    'title' => 'Content page',
    'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more
prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h3>This is a level 3 subtitle.</h3>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h4>This is a level 4 subtitle.</h4>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h5>This is a level 5 subtitle.</h5>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h6>This is a level 6 subtitle.</h6>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<ul>
  <li>Unordered list first level.</li>
  <li>Unordered list first level.
    <ul>
      <li>Unordered list second level.</li>
      <li>Unordered list second level.</li>
    </ul>
  </li>
  <li>Unordered list first level.</li>
</ul>
<ol>
  <li>Ordered list first level.</li>
  <li>Ordered list first level.
    <ol>
      <li>Ordered list second level.</li>
      <li>Ordered list second level.</li>
    </ol>
  </li>
  <li>Ordered list first level.</li>
</ol>
<p class="highlight1">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<p class="highlight2">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<div class="messages warning">
This is a warning text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages status">
This is a status text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages error">
This is an error text. This paragraph style is used to give feedback to the user.
</div>
<blockquote>
This is a quote. This paragraph style is commonly used to point out some quoted
text lines.
</blockquote>
<p>This is a <a href="">link tag</a> example</a><br />
This is an <i>italic tag</i> example<br />
This is a <strike>strike tag</strike> example<br />
This is a <strong>strong</strong> tag example<br />
This is <sub>a sub tag</sub> example<br />
This is a <sup>sup tag</sup> example</p>.',
    'language' => 'en-us',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key,
  );
  $nodes[++$key] = array(
    'type' => 'page',
    'title' => 'Pagina',
    'body' => '<p class="intro">This is an introduction paragraph. This paragraph style is usually placed on top
of a text. The layout of this paragraph style, stands out stronger and more
prominent than regular text.</p>
<h2>This is a level 2 subtitle.</h2>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h3>This is a level 3 subtitle.</h3>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h4>This is a level 4 subtitle.</h4>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h5>This is a level 5 subtitle.</h5>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<h6>This is a level 6 subtitle.</h6>
<p>This is a basic paragraph, containing text with a regular text styling. This style will used throughout the whole website as a default, non formatted style
for textual content.</p>
<ul>
  <li>Unordered list first level.</li>
  <li>Unordered list first level.
    <ul>
      <li>Unordered list second level.</li>
      <li>Unordered list second level.</li>
    </ul>
  </li>
  <li>Unordered list first level.</li>
</ul>
<ol>
  <li>Ordered list first level.</li>
  <li>Ordered list first level.
    <ol>
      <li>Ordered list second level.</li>
      <li>Ordered list second level.</li>
    </ol>
  </li>
  <li>Ordered list first level.</li>
</ol>
<p class="highlight1">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<p class="highlight2">
This is some highlighted text. This paragraph style is used to emphasize the
importance of the content of this paragraph.
</p>
<div class="messages warning">
This is a warning text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages status">
This is a status text. This paragraph style is used to give feedback to the user.
</div>
<div class="messages error">
This is an error text. This paragraph style is used to give feedback to the user.
</div>
<blockquote>
This is a quote. This paragraph style is commonly used to point out some quoted
text lines.
</blockquote>
<p>This is a <a href="">link tag</a> example</a><br />
This is an <i>italic tag</i> example<br />
This is a <strike>strike tag</strike> example<br />
This is a <strong>strong</strong> tag example<br />
This is <sub>a sub tag</sub> example<br />
This is a <sup>sup tag</sup> example</p>.',
    'language' => 'nl',
    'uid' => 1,
    'status' => 1,
    'tnid' => $key-1,
  );

  $nids = array();
  foreach ($nodes as $key => $data) {
    $node = (object) $data;
    $node->format = 2;
    node_save($node);
  }
