<?php
function taxonomy_get_document_tags() {
  $taxonomy_export = array (
    'vocabulary' => (array(
      'name' => 'Document free tagging',
      'description' => '',
      'help' => 'Add tags to your document.',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '0',
      'required' => '0',
      'tags' => '1',
      'module' => 'taxonomy',
      'weight' => '2',
      'language' => '',
      'nodes' =>
     		array (
        	'document' => 'document',
      	),
      'i18nmode' => '3',
    )),
  );

  return $taxonomy_export;
}