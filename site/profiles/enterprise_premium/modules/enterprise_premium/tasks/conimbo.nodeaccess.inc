<?php

/**
 * Nodeaccess.
 *
 */

  $nodeaccess_tables = array(
    'nodeaccess' => array(
    ),
    'nodeaccess_role_alias' => array(
      0 => array(
        'rid' => 1,
        'name' => 'anonymous user',
        'weight' => 0,
      ),
      1 => array(
        'rid' => 2,
        'name' => 'authenticated user',
        'weight' => 0,
      ),
      2 => array(
        'rid' => 3,
        'name' => 'editor',
        'weight' => 0,
      ),
      3 => array(
        'rid' => 4,
        'name' => 'webmaster',
        'weight' => 0,
      ),
    ),
  );

  foreach ($nodeaccess_tables as $table => $rows) {
    db_query("TRUNCATE {$table}");

    foreach ($rows as $row => $fields) {
      $field_names = implode(',', array_keys($fields));
      $field_types_array = array();
      foreach ($fields as $field_name => $field_value) {
        if (is_numeric($field_value)) {
          $field_types_array[] = '%d';
        } else {
          $field_types_array[] = '\'%s\'';
        }
      }
      $field_values = array_values($fields);
      $field_types = implode(',', array_values($field_types_array));
      db_query("INSERT INTO {$table} ($field_names) VALUES ($field_types)", $field_values);
    }
  }