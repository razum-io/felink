<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _enterprise_premium_node_info() {
  $items = array(
    'error' => array(
      'name' => t('Error pages'),
      'module' => 'enterprise_premium',
      'description' => t('An error page'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
      'language_content_type' => '2',
    ),
    'news' => array(
      'name' => t('News'),
      'module' => 'enterprise_premium',
      'description' => t('A news item is a simple method for creating and displaying news information. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page'),
      'module' => 'enterprise_premium',
      'description' => t('A <em>page</em> is a simple method for creating and displaying information that rarely changes, such as an "About us" section of a website. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'views_text' => array(
      'name' => t('Views text'),
      'module' => 'enterprise_premium',
      'description' => t('Views text content which makes it possible to configure the views title, header, footer and empty text per views, per language.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
    'job' => array(
      'name' => t('Job'),
      'module' => 'enterprise_premium',
      'description' => t('A job item is a simple method for creating and displaying job information. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
      'language_content_type' => '2',
    ),
    'document' => array(
      'name' => t('Document'),
      'module' => 'enterprise_premium',
      'description' => t('A document item allows uploading, previewing and downloading of a document.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'location' => array(
      'name' => t('Location'),
      'module' => 'enterprise_premium',
      'description' => t('A location item allows marking location on Gmap and adding address.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'product' => array(
      'name' => t('Product'),
      'module' => 'enterprise_premium',
      'description' => t('A product is a simple method for creating and displaying product information.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'product_category' => array(
      'name' => t('Product category'),
      'module' => 'enterprise_premium',
      'description' => t('A product category is a simple method for creating and displaying product categories.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'event' => array(
      'name' => t('Event'),
      'module' => 'enterprise_premium',
      'description' => t('An event item is a simple method for creating and displaying event information. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'call_to_action' => array(
      'name' => t('Call to action'),
      'module' => 'enterprise_premium',
      'description' => t('A call to action draws the attention of the end user. It invites the user to take action, converting them from visitors into clients.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'gallery' => array(
      'name' => t('Gallery'),
      'module' => 'enterprise_premium',
      'description' => t('A gallery allows uploading and displaying images in an overview or slideshow.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'blog' => array(
      'name' => t('Blog entry'),
      'module' => 'enterprise_premium',
      'description' => t('A blog is an online diary.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );

  $conimbo_ctypes_disabled = variable_get('conimbo_ctypes_disabled', array());

  foreach ($items as $key => $item) {
    if (isset($conimbo_ctypes_disabled[$key])) {
      unset($items[$key]);
    }
  }

  return $items;
}
