<?php

/**
 * Helper to implementation of hook_imagecache_default_presets().
 */
function _enterprise_premium_imagecache_default_presets() {
  $function = enterprise_premium_default_settings_function('_enterprise_premium_imagecache');
  return $function();
}

/**
 * Default imagecache settings.
 */
function _enterprise_premium_imagecache_default() {
  $items = array(
    'small' => array(
      'presetname' => 'small',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_deprecated_scale',
          'data' => array(
            'fit' => 'outside',
            'width' => '50',
            'height' => '50',
          ),
        ),
        '1' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_crop',
          'data' => array(
            'width' => '50',
            'height' => '50',
            'xoffset' => 'center',
            'yoffset' => 'center',
          ),
        ),
      ),
    ),
    'large' => array(
      'presetname' => 'large',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '310',
            'height' => '100%',
            'upscale' => 0,
          ),
        ),
      ),
    ),
    'medium' => array(
      'presetname' => 'medium',
      'actions' => array(
        '0' => array(
          'weight' => '-10',
          'module' => 'imagecache',
          'action' => 'imagecache_deprecated_scale',
          'data' => array(
            'fit' => 'outside',
            'width' => '100',
            'height' => '100',
          ),
        ),
        '1' => array(
          'weight' => '-9',
          'module' => 'imagecache',
          'action' => 'imagecache_crop',
          'data' => array(
            'width' => '100',
            'height' => '100',
            'xoffset' => 'center',
            'yoffset' => 'center',
          ),
        ),
      ),
    ),
    'slide_show' => array(
      'presetname' => 'slide_show',
      'actions' => array (
        '0' => array (
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_deprecated_scale',
          'data' => array (
            'fit' => 'outside',
            'width' => '460',
            'height' => '460',
          ),
        ),
        '1' => array (
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array (
            'width' => '460',
            'height' => '300',
          ),
        ),
      ),
    ),
    'gallery_thumbnail' => array(
      'presetname' => 'gallery_thumbnail',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_deprecated_scale',
          'data' => array(
            'fit' => 'outside',
            'width' => '95',
            'height' => '95',
          ),
        ),
        '1' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_crop',
          'data' => array(
            'width' => '95',
            'height' => '95',
            'xoffset' => 'center',
            'yoffset' => 'center',
          ),
        ),
      ),
    ),
  );
  return $items;
}
