<?php

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *   An array of modules to enable.
 */
function enterprise_premium_profile_modules() {
  return array(
    // Core.
    'menu',
    'locale',
    'dblog',
    'path',
    'search',
    'taxonomy',
    'trigger',
    'comment',

    // Content & layouts.
    'content',
    'text',
    'optionwidgets',
    'nodereference',
    'imagefield',
    'filefield',
    'views',
    'viewsdisplaytabs',
    'ctools',
    'ds',
    'ds_ui',
    'nd',
    'nd_cck',
    'ud',
    'nodeformcols',
    'vd',
    'cd',
    'ife',
    'lightbox2',
    'date_api',
    'date_timezone',
    'date',
    'date_popup',

    // Location.
    'gmap',
    'location',
    'location_node',
    'nd_location',

    // Media.
    'imagecache',
    'imageapi',
    'imageapi_gd',
    'jqp',
    'swfupload',

    // Various contrib.
    'admin_language',
    'better_formats',
    'config_perms',
    'contact',
    'globalredirect',
    'menu_block',
    'token',
    'pathauto',
    'jquery_ui',
    'wysiwyg',
    'twitter',
    'twitter_actions',
    'tweetmeme',
    'site_map',
    'faq',
    'nodequeue',
    'transliteration',
    'nodeaccess',
    'menu_per_role',
    'flag',
    'views_bulk_operations',
    'hansel',
    'hansel_taxonomy',
    'scheduler',
    'nice_dash',

    // Linkit.
    'pathfilter',
    'linkit',
    'linkit_node',
    'linkit_views',
    'linkit_picker',

    // i18n
    'translation',
    'i18n',
    'i18nstrings',
    'one_i18n',
    //'i18nmenu',
    'i18ntaxonomy',
    'i18nblocks',
    'l10n_client',
    'translation_overview',
    'logintoboggan',
    'profile',
    'onepageprofile',

    // Panels
    'panels',
    'panels_node',
    'panels_mini',

    // Conimbo.
    'conimbo',
    'one_actions',
    'one_apachesolr',
    'one_menu',
    'one_panels',
    'enterprise_premium',
    'one_seo',
    'protect_critical_users',
    'admin_menu',

    // Ui which can be disabled.
    'views_ui',
    'imagecache_ui',

    // SEO modules
    'nodewords',
    'nodewords_basic',
    'page_title',
    'one_seo',
    'xmlsitemap',
    'xmlsitemap_menu',
    'xmlsitemap_node',
    'robotstxt',

    // Slide Show
    'views_slideshow',
    'views_slideshow_thumbnailhover',
    'views_slideshow_singleframe',

    //FedICT
    'fedict_search',
  );
}

/**
 * List of optional modules.
 */
function enterprise_premium_profile_optional_modules() {
  return array(
    'poll',
    'views_ui',
    'imagecache_ui',
    'ds_ui',
    'splash',
    'nd_search',
    'vd',
    'ud',
    'cd',
    'mollom',
    'tagadelic',
    'comment',
    'fivestar',
    'votingapi',
    'nd_fivestar',
    'advpoll',
    'feeds',
    'feeds_ui',
    'feeds_xpathparser',
    'xml_parser',
    'aggregator',
    'hansel',
    'hansel_taxonomy',
    'hansel_export',
    'webform',
    'emf',
    'emf_campaign_monitor',
    'googleanalytics',
    'backup_migrate',
    'linkchecker',
    'sweaver',

    // Apachesolr
    'apachesolr',
    'apachesolr_search',
    'apachesolr_date',
    'apachesolr_attachments',
    'apachesolr_nodeaccess',

    // Workflow
    'workflow',
    'workflow_access',

    // Video
    'emfield',
    'emvideo',
    'media_youtube',
    'media_vimeo',

    'securepages',
    'securepages_prevent_hijack',

    // Xmlsitemap
    'xmlsitemap',
    'xmlsitemap_custom',
    'xmlsitemap_engines',
    'xmlsitemap_i18n',
    'xmlsitemap_menu',
    'xmlsitemap_modal',
    'xmlsitemap_node',
    'xmlsitemap_taxonomy',
    'xmlsitemap_user',

    // i18n
    'i18ncck',
    'i18ncontent',
    'i18nmenu',
    //'i18npoll',
    'i18nstrings',
    'i18nsync',
    'i18nviews',

    // Makemeweb
    'poll_override',
  );
}

/**
 * List of required modules.
 */
function enterprise_premium_profile_required_modules() {
  return array(
    'conimbo',
    'content',
    'ds',
    'nd',
    'nd_cck',
    'config_perms',
    'one_menu',
    'better_formats',
    'dblog',
    'enterprise_premium',
    'logintoboggan',
    'menu_block',
    'path',
    'pathauto',
    'robotstxt',
    'token',
    'transliteration',
    'wysiwyg',
    'protect_critical_users',
    'nodeformcols',
    'admin_language',
  );
}

function enterprise_premium_profile_details() {
  return array(
    'name' => 'Enterprise Premium',
    'description' => 'Select this profile to enable the enterprise premium site.'
  );
}

function enterprise_premium_profile_tasks(&$task, $url) {
  if ($task == 'profile') {
    // Date and date formats
    require_once('modules/enterprise_premium/tasks/conimbo.date.inc');
    // CCK Fields.
    enterprise_premium_build_content_fields();
    // Default content.
    require_once('modules/enterprise_premium/tasks/conimbo.nodes.inc');
    // Panels (has to be executed after nodes.inc)
    require_once('modules/enterprise_premium/tasks/conimbo.panels.inc');
    // Translation priority.
    require_once('modules/enterprise_premium/tasks/conimbo.tp.inc');
    // Nodequeue (has to be executed after nodes.inc and taxonomy.faq.inc)
    require_once('modules/enterprise_premium/tasks/conimbo.nodequeue.inc');
    // Breadcrumbs.
    require_once('modules/enterprise_premium/tasks/conimbo.hansel.inc');
    // Nice dash.
    require_once('modules/enterprise_premium/tasks/conimbo.nicedash.inc');
    // Contact form.
    require_once(dirname(__FILE__) . '/../shared/tasks/conimbo.contact.inc');
    // SWF Upload library.
    require_once(dirname(__FILE__) . '/../shared/tasks/conimbo.swfupload.inc');
    // Rebuild permissions. We need to call variable_init here, so nodeaccess module
    // has access to the variables we have set in conimbo.variables.inc.
    global $conf;
    $conf = variable_init();
    node_access_rebuild();
  }
}

function enterprise_premium_profile_task_list() {
}
