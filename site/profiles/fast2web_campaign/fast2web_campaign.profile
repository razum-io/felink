<?php

// This profile depends on the starter profile.
require_once(dirname(__FILE__) . '/../fast2web_starter/fast2web_starter.profile');

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *   An array of modules to enable.
 */
function fast2web_campaign_profile_modules() {
  $starter_modules = fast2web_starter_profile_modules();

  // Specific modules for this profile.
  $campaign_modules = array(
    'emfield',
    'emvideo',
    'media_youtube',
    'fast2web_campaign'
  );

  return array_merge($starter_modules, $campaign_modules);
}

/**
 * List of optional modules.
 */
function fast2web_campaign_profile_optional_modules() {
  $starter_modules = fast2web_starter_profile_optional_modules();

  $campaign_modules = array(
    'mmw_agenda',
  );

  return array_merge($starter_modules, $campaign_modules);
}

/**
 * List of required modules.
 */
function fast2web_campaign_profile_required_modules() {
  $starter_modules = fast2web_starter_profile_required_modules();

  $campaign_modules = array();

  return array_merge($starter_modules, $campaign_modules);
}

function fast2web_campaign_profile_details() {
  return array(
    'name' => 'Fast2Web Campaign',
    'description' => 'Select this profile to enable Fast2Web Campaign site.'
  );
}

function fast2web_campaign_profile_tasks(&$task, $url) {
  fast2web_starter_profile_tasks(&$task, $url);
}

function fast2web_campaign_profile_task_list() {
}
