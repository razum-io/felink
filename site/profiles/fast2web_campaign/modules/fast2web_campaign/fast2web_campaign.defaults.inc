<?php

/**
 * Helper to implementation of hook_content_default_fields().
 */
function _fast2web_campaign_content_default_fields() {
  $fields = array();
  // Exported field_video on content type blog.
  $fields[]  = array (
    'label' => 'Video',
    'field_name' => 'field_video',
    'type' => 'emvideo',
    'type_name' => 'blog',
    'widget_type' => 'emvideo_textfields',
    'change' => 'Change basic information',
    'weight' => '32',
    'providers' =>
    array (
      'youtube' => true,
      0 => 1,
      'conimbo' => false,
    ),
    'video_width' => '425',
    'video_height' => '350',
    'video_autoplay' => '',
    'preview_width' => '425',
    'preview_height' => '350',
    'preview_autoplay' => '',
    'thumbnail_width' => '120',
    'thumbnail_height' => '90',
    'thumbnail_default_path' => '',
    'thumbnail_link_title' => 'See video',
    'meta_fields' =>
    array (
      0 => 1,
      'title' => false,
      'description' => false,
    ),
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'embed' => '',
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'required' => 0,
    'multiple' => '0',
    'op' => 'Save field settings',
    'module' => 'emvideo',
    'widget_module' => 'emvideo',
    'columns' =>
    array (
      'embed' =>
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
      ),
      'value' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'provider' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'data' =>
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => false,
      ),
      'status' =>
      array (
        'description' => 'The availability status of this media.',
        'type' => 'int',
        'unsigned' => 'TRUE',
        'not null' => true,
        'default' => 1,
      ),
      'version' =>
      array (
        'description' => 'The version of the provider\'s data.',
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
      ),
      'title' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'description' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'duration' =>
      array (
        'description' => 'Store the duration of a video in seconds.',
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
      ),
    ),
    'display_settings' =>
    array (
      'label' =>
      array (
        'format' => 'hidden',
      ),
      'format' => 'default',
      'css-class' =>
      array (
      ),
      'region' => 'disabled',
      'ds_weight' => '-97',
      'full' =>
      array (
        'label' =>
        array (
          'format' => 'hidden',
        ),
        'format' => 'default',
        'css-class' => '',
        'region' => 'middle',
        'type' => 'emvideo',
        'parent_id' => '',
        'field_id' => 'field_video',
        'label_value' => '',
        'exclude' => 0,
        'weight' => '-97',
      ),
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'sticky' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'block' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'text_list' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'blog' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  );

  // Exported field_video on content type page.
  $fields[]  = array (
    'label' => 'Video',
    'field_name' => 'field_video',
    'type' => 'emvideo',
    'type_name' => 'page',
    'widget_type' => 'emvideo_textfields',
    'change' => 'Change basic information',
    'weight' => '31',
    'providers' =>
    array (
      1 => 1,
      0 => 1,
      'conimbo' => false,
      'youtube' => false,
    ),
    'video_width' => '425',
    'video_height' => '350',
    'video_autoplay' => '',
    'preview_width' => '425',
    'preview_height' => '350',
    'preview_autoplay' => '',
    'thumbnail_width' => '120',
    'thumbnail_height' => '90',
    'thumbnail_default_path' => '',
    'thumbnail_link_title' => 'See video',
    'meta_fields' =>
    array (
      1 => 1,
      0 => 1,
      'title' => false,
      'description' => false,
    ),
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'embed' => '',
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'required' => 0,
    'multiple' => '0',
    'op' => 'Save field settings',
    'module' => 'emvideo',
    'widget_module' => 'emvideo',
    'columns' =>
    array (
      'embed' =>
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
      ),
      'value' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'provider' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'data' =>
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => false,
      ),
      'status' =>
      array (
        'description' => 'The availability status of this media.',
        'type' => 'int',
        'unsigned' => 'TRUE',
        'not null' => true,
        'default' => 1,
      ),
      'version' =>
      array (
        'description' => 'The version of the provider\'s data.',
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
      ),
      'title' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'description' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'duration' =>
      array (
        'description' => 'Store the duration of a video in seconds.',
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
      ),
    ),
    'display_settings' =>
    array (
      'label' =>
      array (
        'format' => 'hidden',
      ),
      'format' => 'default',
      'css-class' =>
      array (
      ),
      'region' => 'disabled',
      'ds_weight' => '-97',
      'full' =>
      array (
        'label' =>
        array (
          'format' => 'hidden',
        ),
        'format' => 'default',
        'css-class' => '',
        'region' => 'middle',
        'type' => 'emvideo',
        'parent_id' => '',
        'field_id' => 'field_video',
        'label_value' => '',
        'exclude' => 0,
        'weight' => '-97',
      ),
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'sticky' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'block' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'text_list' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'blog' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  );


  // Exported field_video on content type news.
  $fields[]  = array (
    'label' => 'Video',
    'field_name' => 'field_video',
    'type' => 'emvideo',
    'type_name' => 'news',
    'widget_type' => 'emvideo_textfields',
    'change' => 'Change basic information',
    'weight' => '32',
    'providers' =>
    array (
      'youtube' => true,
      0 => 1,
      'conimbo' => false,
    ),
    'video_width' => '425',
    'video_height' => '350',
    'video_autoplay' => '',
    'preview_width' => '425',
    'preview_height' => '350',
    'preview_autoplay' => '',
    'thumbnail_width' => '120',
    'thumbnail_height' => '90',
    'thumbnail_default_path' => '',
    'thumbnail_link_title' => 'See video',
    'meta_fields' =>
    array (
      0 => 1,
      'title' => false,
      'description' => false,
    ),
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'embed' => '',
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'required' => 0,
    'multiple' => '0',
    'op' => 'Save field settings',
    'module' => 'emvideo',
    'widget_module' => 'emvideo',
    'columns' =>
    array (
      'embed' =>
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
      ),
      'value' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'provider' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'data' =>
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => false,
      ),
      'status' =>
      array (
        'description' => 'The availability status of this media.',
        'type' => 'int',
        'unsigned' => 'TRUE',
        'not null' => true,
        'default' => 1,
      ),
      'version' =>
      array (
        'description' => 'The version of the provider\'s data.',
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
      ),
      'title' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'description' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'duration' =>
      array (
        'description' => 'Store the duration of a video in seconds.',
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
      ),
    ),
    'display_settings' =>
    array (
      'label' =>
      array (
        'format' => 'hidden',
      ),
      'format' => 'default',
      'css-class' =>
      array (
      ),
      'region' => 'disabled',
      'ds_weight' => '-97',
      'full' =>
      array (
        'label' =>
        array (
          'format' => 'hidden',
        ),
        'format' => 'default',
        'css-class' => '',
        'region' => 'middle',
        'type' => 'emvideo',
        'parent_id' => '',
        'field_id' => 'field_video',
        'label_value' => '',
        'exclude' => 0,
        'weight' => '-97',
      ),
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'sticky' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'block' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'text_list' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'blog' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  );

  return $fields;
}