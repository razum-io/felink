<?php

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *   An array of modules to enable.
 */
function fast2web_starter_profile_modules() {
  return array(
    // Core.
    'menu',
    'locale',
    'dblog',
    'path',
    'search',
    'taxonomy',
    'trigger',
    'color',
    'comment',

    // Content layouts.
    'content',
    'text',
    'imagefield',
    'filefield',
    'views',
    'ctools',
    'ds',
    'ds_ui',
    'nd',
    'nd_cck',
    'nd_search',
    'vd',
    'nodeformcols',
    'ife',
    'lightbox2',
    'date_api',
    'date_timezone',
    'date',
    'date_popup',

    // Media.
    'imagecache',
    'imageapi',
    'imageapi_gd',
    'jqp',
    'swfupload',

    // Various contrib.
    'admin_language',
    'better_formats',
    'config_perms',
    'contact',
    'content_taxonomy',
    'content_taxonomy_autocomplete',
    'content_taxonomy_options',
    'globalredirect',
    'menu_block',
    'token',
    'pathauto',
    'jquery_ui',
    'wysiwyg',
    'transliteration',
    'views_bulk_operations',
    'site_map',
    'faq',
    'hansel',
    'hansel_taxonomy',
    'nice_dash',
    'securepages',
    'securepages_prevent_hijack',
    'seckit',

    // Linkit.
    'pathfilter',
    'linkit',
    'linkit_node',
    'linkit_views',
    'linkit_picker',

    // i18n
    'translation',
    'i18n',
    'i18nstrings',
    'i18nsync',
    'one_i18n',
    //'i18nmenu',
    'i18ntaxonomy',
    'i18nblocks',
    'l10n_client',
    'translation_overview',

    // Panels
    'panels',
    'panels_node',
    'panels_mini',

    // Conimbo.
    'conimbo',
    'one_actions',
    'one_menu',
    'one_panels',
    'fast2web_shared',
    'protect_critical_users',
    'admin_menu',

    // Ui which can be disabled.
    'views_ui',
    'imagecache_ui',

    // SEO modules
    'nodewords',
    'nodewords_basic',
    'page_title',
    'one_seo',
    'xmlsitemap',
    'xmlsitemap_menu',
    'xmlsitemap_node',
    'robotstxt',

    // Slide Show
    'views_slideshow',
    'views_slideshow_thumbnailhover',
    'views_slideshow_singleframe',

    // Social
    'social_share',

    // Fast2Web modules (created by MakeMeWeb).
    'mmw_totop',
    'mmw_panels',
    'mmw_icontheme',

    //FedICT
    'fedict_search',
  );
}

/**
 * List of optional modules.
 */
function fast2web_starter_profile_optional_modules() {
  return array(
    'poll',
    'advpoll',
    'views_ui',
    'imagecache_ui',
    'splash',
    'nd_search',
    'vd',
    'cd',
    'mollom',
    'linkchecker',
    'tagadelic',
    'comment',
    'fivestar',
    'votingapi',
    'nd_fivestar',
    'aggregator',
    'nice_dash',
    'hansel',
    'hansel_taxonomy',
    'hansel_export',
    'feeds',
    'feeds_ui',
    'feeds_xpathparser',
    'xml_parser',
    'googleanalytics',
    'backup_migrate',
    'apachesolr',
    'apachesolr_search',
    'twitter',
    'twitter_actions',
    'optionwidgets',
    'nodereference',

    // Xmlsitemap
    'xmlsitemap',
    'xmlsitemap_custom',
    'xmlsitemap_engines',
    'xmlsitemap_i18n',
    'xmlsitemap_menu',
    'xmlsitemap_modal',
    'xmlsitemap_node',
    'xmlsitemap_taxonomy',
    'xmlsitemap_user',

    // i18n
    'i18ncck',
    'i18ncontent',
    'i18nmenu',
    //'i18npoll',
    'i18nstrings',
    'i18nsync',
    'i18nviews',

    // Makemeweb
    'mmw_addemar',
    'poll_override',
  );
}

/**
 * List of required modules.
 */
function fast2web_starter_profile_required_modules() {
  return array(
    'conimbo',
    'content',
    'ds',
    'nd',
    'nd_cck',
    'config_perms',
    'one_menu',
    'better_formats',
    'dblog',
    'fast2web_starter',
    'logintoboggan',
    'menu_block',
    'path',
    'pathauto',
    'robotstxt',
    'token',
    'transliteration',
    'wysiwyg',
    'protect_critical_users',
    'nodeformcols',
    'admin_language',
    'securepages',
  );
}

function fast2web_starter_profile_details() {
  return array(
    'name' => 'Fast2Web Starter',
    'description' => 'Select this profile to enable Fast2Web Starter site.'
  );
}

function fast2web_starter_profile_tasks(&$task, $url) {
  if ($task == 'profile') {
    // CCK Fields.
    fast2web_shared_build_content_fields();
    // Default content.
    require_once(drupal_get_path('module', 'fast2web_shared') . '/tasks/conimbo.nodes.inc');
    // Panels (has to be executed after nodes.inc)
    require_once(drupal_get_path('module', 'fast2web_shared') . '/tasks/conimbo.panels.inc');
    // Rebuild translation priority.
    require_once(drupal_get_path('module', 'fast2web_shared') . '/tasks/conimbo.tp.inc');
    // Breadcrumbs.
    require_once(drupal_get_path('module', 'fast2web_shared') . '/tasks/conimbo.hansel.inc');
    // Nice dash.
    require_once(drupal_get_path('module', 'fast2web_shared') . '/tasks/conimbo.nicedash.inc');
    // Contact form.
    require_once(dirname(__FILE__) . '/../shared/tasks/conimbo.contact.inc');
    // SWF Upload library.
    require_once(dirname(__FILE__) . '/../shared/tasks/conimbo.swfupload.inc');
  }
}

function fast2web_starter_profile_task_list() {
}
