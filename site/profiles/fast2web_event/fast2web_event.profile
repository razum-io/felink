<?php

// This profile depends on the starter profile.
require_once(dirname(__FILE__) . '/../fast2web_starter/fast2web_starter.profile');

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *   An array of modules to enable.
 */
function fast2web_event_profile_modules() {
  $starter_modules = fast2web_starter_profile_modules();

  // Specific modules for this profile.
  $event_modules = array(
    'webform',
    'eventreg',
      // Location.
    'gmap',
    'gmap_location',
    'location',
    'location_node',
    'nd_location',
    // Others.
    'nodereference',
  );

  return array_merge($starter_modules, $event_modules);
}

/**
 * List of optional modules.
 */
function fast2web_event_profile_optional_modules() {
  $starter_modules = fast2web_starter_profile_optional_modules();

  $event_modules = array(
    'gmap_macro_builder',
    'gmap_taxonomy'
  );

  return array_merge($starter_modules, $event_modules);
}

/**
 * List of required modules.
 */
function fast2web_event_profile_required_modules() {
  $starter_modules = fast2web_starter_profile_required_modules();

  $event_modules = array();

  return array_merge($starter_modules, $event_modules);
}

function fast2web_event_profile_details() {
  return array(
    'name' => 'Fast2Web Event',
    'description' => 'Select this profile to enable Fast2Web Event site.'
  );
}

function fast2web_event_profile_tasks(&$task, $url) {
  fast2web_starter_profile_tasks(&$task, $url);
}

function fast2web_event_profile_task_list() {
}
